#Include "PROTHEUS.CH"
#include "rwmake.ch"   
#INCLUDE 'TOPCONN.CH'
//--------------------------------------------------------------
/*/{Protheus.doc} MyFunction
Description                                                     
                                                                
@param xParam Parameter Description                             
@return xRet Return Description                                 
@author  -                                               
@since 20/05/2021                                                   
/*/                                                             
//--------------------------------------------------------------
User Function comissao123()                        
Local oButton1
Local oButton2
Local oButton3
Local oGroup1
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Comissao" FROM 000, 000  TO 500, 500 COLORS 0, 16777215 PIXEL

    @ 000, 001 GROUP oGroup1 TO 250, 251 PROMPT "Manutencao de Comissao " OF oDlg COLOR 0, 16777215 PIXEL
    @ 045, 045 BUTTON oButton1 PROMPT "Inclui Vendedor"  SIZE 156, 020 OF oDlg ACTION comiss1()  PIXEL
    @ 070, 045 BUTTON oButton2 PROMPT "Altera Comissao " SIZE 156, 020 OF oDlg ACTION comiss3()  PIXEL
    @ 095, 045 BUTTON oButton3 PROMPT "Altera Vendedor " SIZE 156, 020 OF oDlg ACTION comiss2()  PIXEL
    @ 120, 045 BUTTON oButton4 PROMPT "Comissao por Pedido" SIZE 156, 020 OF oDlg ACTION u_comiss4()  PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return   

static Function comiss1()      

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � MANNF1   � Autor � JOSE AMADEU R R       � Data � 12.10.20 ���
��a����������������������������������������������������������������������Ĵ��
���Descricao � Altera (%) comissao nas notas iscais                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

cPerg := PADR("MANNF1", 10)

DbSelectArea("SX1")
DbSetOrder(1)

IF !DbSeek(cPerg)

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Nota Fiscal:"
   SX1->X1_ORDEM   := "01"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 9
   SX1->X1_VARIAVL := "mv_ch1"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR01"
   SX1->X1_F3      := "SF2"
   MsUnlock()

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Novo Vendedor:"
   SX1->X1_ORDEM   := "02"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 6
   SX1->X1_VARIAVL := "mv_ch2"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR02"
   SX1->X1_F3      := "SA3"
   MsUnlock()

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Nova Comissao:"
   SX1->X1_ORDEM   := "03"
   SX1->X1_TIPO    := "N"
   SX1->X1_TAMANHO := 5
   SX1->X1_DECIMAL := 2
   SX1->X1_VARIAVL := "mv_ch3"
   SX1->X1_VAR01   := "MV_PAR03"
   SX1->X1_GSC     := "G"
   MsUnlock()

ENDIF

WHILE .T.
        IF !PERGUNTE(cPerg,.T.)
                EXIT
        ENDIF
        PROCESSA({|| RUNPROC1(MV_par01,mv_par02,mv_par03)},"Manutencao (%) Comissao da Nota Fiscal","Aguarde...")
ENDDO

RETURN

*****************************************************************************
Static FUNCTION RUNPROC1(para1,para2,para3)
*****************************************************************************



Local cNumVend :=  ' '
Local nAux := 1
Local cVend:=' '


IF EMPTY(para1)
   MSGBOX("Obrigatorio informr a Nota  !","Atencao","STOP")
   RETURN
ENDIF
IF EMPTY(para2)
   MSGBOX("Obrigatorio informar o VENDEDOR !","Atencao","STOP")
   RETURN
ENDIF
IF para3 == 0
   MSGBOX("Obrigatorio informar o (%) COMISSAO!","Atencao","STOP")
   RETURN
ENDIF
DBSELECTAREA("SF2")
DBSETORDER(1)
IF !DBSEEK( xFILIAL("SF2") + para1, .F. )
   MSGBOX("Nota Fiscal nao cadastrada!","Atencao","STOP")
   RETURN
ENDIF
IF EMPTY(SF2->F2_DUPL)
   MSGBOX("Nota Fiscal nao gerou financeiro, verifique!","Atencao","STOP")
   RETURN
ENDIF

For nAux := 1 to 5 
     cVend := 'SF2->F2_VEND'+ALLTRIM(STR(nAux))
    If empty (&cVend) .or.  (&cVend) == ' '
        cNumVend := alltrim(str(nAux))
        exit
    elseif nAux==5 
         MSGBOX("Todos os campos reservados  para vendedor estao preenchidos , verifique!","Atencao","STOP")
         return
    endif 

next nAux 
    SF2->(RECLOCK('SF2',.F.))
        &cVend:=para2
    SF2->(MsUnlock())



	DBSELECTAREA("SD2")
	DBSETORDER(3)
	PROCREGUA(LASTREC())
	DBSEEK( SF2->F2_FILIAL + SF2->F2_DOC + SF2->F2_SERIE, .F. )
	While !EOF() .AND. SD2->D2_FILIAL = SF2->F2_FILIAL .AND. SD2->D2_DOC     = SF2->F2_DOC     .AND.;
	                   SD2->D2_SERIE  = SF2->F2_SERIE  .AND. SD2->D2_CLIENTE = SF2->F2_CLIENTE .AND. SD2->D2_LOJA = SF2->F2_LOJA
	        SD2->(RECLOCK("SD2",.F.))
                &('SD2->D2_COMIS'+cNumVend) := para3
	        SD2->(MSUNLOCK())

	        SD2->( DBSKIP())
	ENDDO
	
	DBSELECTAREA("SE1")
	DBSETORDER(1)
	PROCREGUA(LASTREC())
	DBSEEK( SF2->F2_FILIAL + SF2->F2_SERIE + SF2->F2_DOC, .F. )
	While !EOF() .AND. SE1->E1_FILIAL = SF2->F2_FILIAL .AND. SE1->E1_NUM     = SF2->F2_DOC     .AND.;
	                   SE1->E1_PREFIXO= SF2->F2_SERIE  .AND. SE1->E1_CLIENTE = SF2->F2_CLIENTE .AND. SE1->E1_LOJA = SF2->F2_LOJA
	        
            
                RECLOCK("SE1",.F.)
                    &('SE1->E1_COMIS'+cNumVend) := para3
                    &('SE1->E1_VEND'+cNumVend) := para2

                MSUNLOCK()
                IF !EMPTY(SE1->E1_BAIXA)
                    u_acertaSE3(SE1->E1_BAIXA,para2)
                ENDIF
                DBSELECTAREA("SE1")
            
                DBSKIP()
             
	ENDDO


RETURN


static Function comiss2()      
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � MANNF1   � Autor � JOSE AMADEU R R       � Data � 12.10.20 ���
��a����������������������������������������������������������������������Ĵ��
���Descricao � Altera (%) comissao nas notas iscais                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
cPerg := PADR("MANNF2", 10)
DbSelectArea("SX1")
DbSetOrder(1)

IF !DbSeek(cPerg)

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Nota Fiscal:"
   SX1->X1_ORDEM   := "01"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 9
   SX1->X1_VARIAVL := "mv_ch1"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR01"
   SX1->X1_F3      := "SF2"
   MsUnlock()

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Do Vendedor:"
   SX1->X1_ORDEM   := "02"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 6
   SX1->X1_VARIAVL := "mv_ch2"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR02"
   SX1->X1_F3      := "SA3"
   MsUnlock()

 RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Para Vendedor:"
   SX1->X1_ORDEM   := "02"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 6
   SX1->X1_VARIAVL := "mv_ch3"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR03"
   SX1->X1_F3      := "SA3"
   MsUnlock()

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Nova Comissao:"
   SX1->X1_ORDEM   := "04"
   SX1->X1_TIPO    := "N"
   SX1->X1_TAMANHO := 5
   SX1->X1_DECIMAL := 2
   SX1->X1_VARIAVL := "mv_ch4"
   SX1->X1_VAR01   := "MV_PAR04"
   SX1->X1_GSC     := "G"
   MsUnlock()


ENDIF

WHILE .T.
        IF !PERGUNTE(cPerg,.T.)
                EXIT
        ENDIF
        PROCESSA({|| RUNPROC2(MV_PAR01 ,MV_PAR02,MV_PAR03,MV_PAR04)},"Manutencao (%) Comissao da Nota Fiscal","Aguarde...")
ENDDO

RETURN

*****************************************************************************
Static FUNCTION RUNPROC2(para1, para2 , para3 , para4)
*****************************************************************************
Local cCampoVend :=' '



IF EMPTY(para1)
   MSGBOX("Obrigatorio informar a Nota  !","Atencao","STOP")
   RETURN
ENDIF
IF EMPTY(para2)
   MSGBOX("Obrigatorio informar o Do VENDEDOR !","Atencao","STOP")
   RETURN
ENDIF
IF empty(para3)
   MSGBOX("Obrigatorio informar o Para Vendedor","Atencao","STOP")
   RETURN

ENDIF   

if para4== 0
    msgalert('Atencao nao foi informada o novo percentual , o antigo ser� mantido')
endif 


DBSELECTAREA("SF2")
DBSETORDER(1)
IF !DBSEEK( xFILIAL("SF2") + para1, .F. )
   MSGBOX("Nota Fiscal nao cadastrada!","Atencao","STOP")
   RETURN
ENDIF
IF EMPTY(SF2->F2_DUPL)
   MSGBOX("Nota Fiscal nao gerou financeiro, verifique!","Atencao","STOP")
   RETURN
ENDIF


IF (!Empty(SF2->F2_VEND1) .AND. SF2->F2_VEND1 == para2) 
    cCampoVend:= '1'
  

ElseIF (!Empty(SF2->F2_VEND2) .AND. SF2->F2_VEND2 == para2)
  cCampoVend:= '2'

elseIF (!Empty(SF2->F2_VEND3) .AND. SF2->F2_VEND3 == para2)
  cCampoVend:= '3'

elseIF (!Empty(SF2->F2_VEND4) .AND. SF2->F2_VEND4 == para2)
  cCampoVend:= '4'

elseIF (!Empty(SF2->F2_VEND5) .AND. SF2->F2_VEND5 == para2)
  cCampoVend:= '5'
ENDIF

 if empty (cCampoVend )
    msgalert('Atencao ! nao foi poss�vel encontrar este vendedor para esta nota ')
    return
 endif 


	DBSELECTAREA("SD2")
	DBSETORDER(3)
	PROCREGUA(LASTREC())
	DBSEEK( SF2->F2_FILIAL + SF2->F2_DOC + SF2->F2_SERIE, .F. )
	While !EOF() .AND. SD2->D2_FILIAL = SF2->F2_FILIAL .AND. SD2->D2_DOC     = SF2->F2_DOC     .AND.;
	                   SD2->D2_SERIE  = SF2->F2_SERIE  .AND. SD2->D2_CLIENTE = SF2->F2_CLIENTE .AND. SD2->D2_LOJA = SF2->F2_LOJA
	        
            if para4 <> 0
                SD2->(RECLOCK("SD2",.F.))
	       
	        	
                        &('SD2->D2_COMIS'+cCampoVend) := para4

                
	        	
	            SD2->(MSUNLOCK())
            endif
	        SD2->(DBSKIP())
	ENDDO
    
    SF2->(RECLOCK("SF2",.F.))
	       
	
    &('SF2->F2_VEND'+cCampoVend) := para3

   
	        	
	SF2->(MSUNLOCK())


	DBSELECTAREA("SE1")
	DBSETORDER(1)
	PROCREGUA(LASTREC())
	DBSEEK( SF2->F2_FILIAL + SF2->F2_SERIE + SF2->F2_DOC, .F. )
	While !EOF() .AND. SE1->E1_FILIAL = SF2->F2_FILIAL .AND. SE1->E1_NUM     = SF2->F2_DOC     .AND.;
	                   SE1->E1_PREFIXO= SF2->F2_SERIE  .AND. SE1->E1_CLIENTE = SF2->F2_CLIENTE .AND. SE1->E1_LOJA = SF2->F2_LOJA
	        
                SE1->(RECLOCK("SE1",.F.))
                    IF   para4 <> 0
                        &("SE1->E1_COMIS"+cCampoVend) := para4
                        &("SE1->E1_VEND"+cCampoVend) := para3
                    ENDIF
            SE1->(MSUNLOCK())

            IF !EMPTY(SE1->E1_BAIXA)
                    u_acertaSE3(SE1->E1_BAIXA,para3)
            ENDIF
            SE1->(DBSKIP())
	ENDDO
	MSGBOX("Alteracao do (%) comissao dos TITULOS, referente a nota fiscal, realizada com sucesso!","Mensagem do Sistema","STOP")

RETURN



static Function comiss3()      

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � MANNF1   � Autor � JOSE AMADEU R R       � Data � 12.10.20 ���
��a����������������������������������������������������������������������Ĵ��
���Descricao � Altera (%) comissao nas notas iscais                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

cPerg := PADR("MANNF3", 10)

DbSelectArea("SX1")
DbSetOrder(1)

IF !DbSeek(cPerg)

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Nota Fiscal:"
   SX1->X1_ORDEM   := "01"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 9
   SX1->X1_VARIAVL := "mv_ch1"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR01"
   SX1->X1_F3      := "SF2"
   MsUnlock()

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Do Vendedor:"
   SX1->X1_ORDEM   := "02"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 6
   SX1->X1_VARIAVL := "mv_ch2"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR02"
   SX1->X1_F3      := "SA3"
   MsUnlock()

   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Novo % Comissao?"
   SX1->X1_ORDEM   := "03"
   SX1->X1_TIPO    := "N"
   SX1->X1_TAMANHO := 5
   SX1->X1_DECIMAL := 2
   SX1->X1_VARIAVL := "mv_ch3"
   SX1->X1_VAR01   := "MV_PAR03"
   SX1->X1_GSC     := "G"
   MsUnlock()

ENDIF

WHILE .T.
        IF !PERGUNTE(cPerg,.T.)
                EXIT
        ENDIF
        PROCESSA({|| RUNPROC3(mv_par01,mv_par02,mv_par03)},"Manutencao (%) Comissao da Nota Fiscal","Aguarde...")
ENDDO

RETURN

*****************************************************************************
Static FUNCTION RUNPROC3(para1,para2,para3)
*****************************************************************************
Local nAux

IF EMPTY(para2)
   MSGBOX("Obrigatorio informar o VENDEDOR !","Atencao","STOP")
   RETURN
ENDIF
IF para3 == 0
   MSGBOX("Obrigatorio informar o (%) COMISSAO!","Atencao","STOP")
   RETURN
ENDIF
DBSELECTAREA("SF2")
DBSETORDER(1)
IF !DBSEEK( xFILIAL("SF2") + para1, .F. )
   MSGBOX("Nota Fiscal nao cadastrada!","Atencao","STOP")
   RETURN
ENDIF
IF EMPTY(SF2->F2_DUPL)
   MSGBOX("Nota Fiscal nao gerou financeiro, verifique!","Atencao","STOP")
   RETURN
ENDIF



For nAux := 1 to 5 
     cVend := 'SF2->F2_VEND'+ALLTRIM(STR(nAux))
    If !empty (&cVend) .and. &cVend == para2
        cNumVend := alltrim(str(nAux))
        exit
    elseif nAux==5 
         MSGBOX("Vendedor nao encontrado ! , verifique!","Atencao","STOP")
    endif 

next nAux 

IF MSGNOYES("Deseja realmente alterar o (%) de COMISSAO da nota fiscal/vendedor?","Pergunta do sistema!")
	DBSELECTAREA("SD2")
	DBSETORDER(3)
	PROCREGUA(LASTREC())
	DBSEEK( SF2->F2_FILIAL + SF2->F2_DOC + SF2->F2_SERIE, .F. )
	While !EOF() .AND. SD2->D2_FILIAL = SF2->F2_FILIAL .AND. SD2->D2_DOC     = SF2->F2_DOC     .AND.;
	                   SD2->D2_SERIE  = SF2->F2_SERIE  .AND. SD2->D2_CLIENTE = SF2->F2_CLIENTE .AND. SD2->D2_LOJA = SF2->F2_LOJA
	        RECLOCK("SD2",.F.)
	        
	        		&('SD2->D2_COMIS'+cNumVend) := para3
	        	
	        
	        MSUNLOCK()
	        DBSELECTAREA("SD2")
	        DBSKIP()
	ENDDO

	DBSELECTAREA("SE1")
	DBSETORDER(1)
	PROCREGUA(LASTREC())
	DBSEEK( SF2->F2_FILIAL + SF2->F2_SERIE + SF2->F2_DOC, .F. )
	While !EOF() .AND. SE1->E1_FILIAL = SF2->F2_FILIAL .AND. SE1->E1_NUM     = SF2->F2_DOC     .AND.;
	                   SE1->E1_PREFIXO= SF2->F2_SERIE  .AND. SE1->E1_CLIENTE = SF2->F2_CLIENTE .AND. SE1->E1_LOJA = SF2->F2_LOJA
	        
           
                RECLOCK("SE1",.F.)

                     &('SE1->E1_COMIS'+cNumVend) := para3
	    
	            MSUNLOCK()
                IF !EMPTY(SE1->E1_BAIXA)
                    u_acertaSE3(SE1->E1_BAIXA,para2)
                ENDIF
	            DBSELECTAREA("SE1")
            

	        DBSKIP()
	ENDDO
	MSGBOX("Alteracao do (%) comissao dos TITULOS, referente a nota fiscal, realizada com sucesso!","Mensagem do Sistema","STOP")
ELSE
	MSGBOX("Alteracao do (%) Comissao CANCELADA!","Mensagem do Sistema","STOP")
ENDIF

RETURN


