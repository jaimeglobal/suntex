#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'AP5MAIL.CH'
#INCLUDE 'TBICONN.CH'
#DEFINE  ENTER CHR(13)+CHR(10)     
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  |          �Autor  |Fabiano Pereira     � Data �  02/06/2010 ���
�������������������������������������������������������������������������͹��
��� Programa para verificar se e-mail eh valido 					      ���
��� caso e-mail nao seja valido, programa envia email 1 a 1.              ���  
���                                                                       ���
�������������������������������������������������������������������������͹��
���Uso       � AP10 - Novus Automation                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

//	INFORMAR SINTAXE DA FUNCAO
//	E-MAIL DA NOVUS NAO PRECISA COLOCAR @novus...

****************************************************************************************         
User Function ENVIA_EMAIL(_cRotina, _cAssunto, _cPara, _cCopia, _cCopOcult, _cEmail, _cAtachado, _cRemetente, lApagaArq)
****************************************************************************************
Private _lEnviou	:=	.F.

Private cNaoRecebeu	:=	''
Private cError    	:= 	''
Private cTRecebe	:=	''
Private cTCopia		:=	''
Private cTCopOculta	:=	''

Private	aRecebe		:=	{}
Private	aCopia		:=	{}
Private	aCopOculta	:=	{}
Private	aTodosEmail	:=	{}

Private lFErase		:=	IIF(ValType(lApagaArq)!='L',.F., lApagaArq )

/*
Private cServer   	:= AllTrim(GetMv("MV_WFSMTP")	) // 	MV_RELSERV
Private cAccount  	:= AllTrim(GetMV("MV_WFACC")	) //	MV_RELACNT
Private cPassword 	:= AllTrim(GetMv("MV_WFPASSW")	) //	MV_RELPS00W 
// Info 	- Ok
// Protheus - No
*/

Private cServer   	:= AllTrim(GetMv("MV_RELSERV")	) // 	MV_RELSERV
Private cAccount  	:= AllTrim(GetMV("MV_RELACNT")	) //	MV_RELACNT
Private cPassword 	:= AllTrim(GetMv("MV_RELPSW")	) //	MV_RELPSW 

Private cNomeRotina	:=	_cRotina                                                                         

_cRemetente := IIF( ValType('_cRemetente')=='U' .Or. Empty(_cRemetente),'',_cRemetente)
Private cRemetente	:= 	IIF(!Empty(_cRemetente), _cRemetente,  'thermomax@thermomax.com.br' ) 

Private cAssunto	:=	AllTrim(_cAssunto)
Private cRecebe		:=	AllTrim(_cPara)
Private cCopia		:=	AllTrim(_cCopia)
Private cCopOcult	:=	AllTrim(_cCopOcult)   
Private cCorpoEmail	:=	_cEmail   
Private cAnexo		:=	_cAtachado
                                                                              
IIF( !ExisteSX6('GL_ADMIN_E'), CriarSX6('GL_ADMIN_E','C','Usu�rio recebem quando e-mail nao vai...' , 'jose.amadeu@sadglobal.com.br ' ) ,  )

Conout( ENTER+'INICIO DA ROTINA: ' + cNomeRotina +' -  DATA: '+DtoC(Date())+" - Hora: "+Time() )

//������������������������������
//�		RECEBE EMAIL           �
//������������������������������ 
cRecebe	+=	IIF(Len(cRecebe)>1, IIF(Right(AllTrim(cRecebe),1)!=';',';',''), '')
Do While !Empty(cRecebe)   
	nPontoVirg 	:= At(";", cRecebe )-1
		
	If 	nPontoVirg <= 0
		Exit
	Endif

	Aadd(aTodosEmail, 	Left(cRecebe, nPontoVirg)+ 	IIF( '@' $ Left(cRecebe, nPontoVirg),'','@thermomax.com.br;')  )
	cTRecebe	+=	AllTrim(Left(cRecebe, nPontoVirg)+ 	IIF( '@' $ Left(cRecebe, nPontoVirg),';','@thermomax.com.br;') )
	cRecebe		:=	AllTrim(Right(cRecebe, ( Len(cRecebe)-nPontoVirg-1) )  )
EndDo
cTRecebe := IIF(Right(cTRecebe,1)==';',SubStr(cTRecebe,1,Len(cTRecebe)-1),cTRecebe)
                          

//������������������������������
//�		COPIA  EMAIL           �
//������������������������������ 
cCopia	+=	IIF(Len(cCopia)>1, IIF(Right(AllTrim(cCopia),1)!=';',';',''), '')
Do While !Empty(cCopia)   
	nPontoVirg 	:= At(";", cCopia )-1
	If nPontoVirg <= 0
		Exit
	Endif
	
	Aadd(aTodosEmail, 	Left(cCopia, nPontoVirg)+ IIF(  '@' $ Left(cCopia, nPontoVirg),'','@thermomax.com.br;')  )
	cTCopia		+=	AllTrim(Left(cCopia, nPontoVirg)+ IIF(  '@' $ Left(cCopia, nPontoVirg),';','@thermomax.com.br;'))
	cCopia		:=	AllTrim(Right(cCopia, ( Len(cCopia)-nPontoVirg-1) ))
EndDo
cTCopia := IIF(Right(cTCopia,1)==';',SubStr(cTCopia,1,Len(cTCopia)-1),cTCopia)

              
//������������������������������
//�		COPIA OCULTA           �
//������������������������������                                  
cCopOcult	+=	IIF(Len(cCopOcult)>1, IIF(Right(AllTrim(cCopOcult),1)!=';',';',''), '')
Do While !Empty(cCopOcult)   
	nPontoVirg 	:= At(";", cCopOcult )-1
	If nPontoVirg <= 0
		Exit
	Endif
	
	Aadd(aTodosEmail, 	Left(cCopOcult, nPontoVirg)+ IIF(  '@' $ Left(cCopOcult, nPontoVirg),'','@thermomax.com.br;')  )
	cTCopOculta		+=	AllTrim(Left(cCopOcult, nPontoVirg)+ IIF(  '@' $ Left(cCopOcult, nPontoVirg),';','@thermomax.com.br;'))
	cCopOcult		:=	AllTrim(Right(cCopOcult, ( Len(cCopOcult)-nPontoVirg-1) ) )
EndDo
cTCopOculta := IIF(Right(cTCopOculta,1)==';',SubStr(cTCopOculta,1,Len(cTCopOculta)-1),cTCopOculta)

lRet := ENVIA_TODOS()

If !lRet
	lRet := ENVIA_1_A_1()
EndIf            

//���������������������������������������Ŀ
//� 		Apaga arquivo JPG             �
//�����������������������������������������
If lFErase .And. lRet
	Conout( 'Apagou arquivo '+ _cAtachado ) 
	FErase(_cAtachado)
EndIf	
	
Conout( 'FIM DA ROTINA: ' + cNomeRotina +' -  DATA: '+DtoC(Date())+" - Hora: "+Time() +ENTER)

Return(lRet)
*********************************************************************************
Static Function ENVIA_TODOS()
*********************************************************************************
Local lRetorno	:=	.F.

Conout('ENVIANDO PARA TODOS...') 	
		
	CONNECT SMTP SERVER cServer ACCOUNT cAccount PASSWORD cPassword Result lConectou 
	MailAuth(cAccount,cPassword)
	If lConectou
		SEND MAIL FROM cRemetente TO cTRecebe  CC cTCopia  BCC cTCopOculta SUBJECT cAssunto BODY cCorpoEmail ATTACHMENT cAnexo	RESULT lEnviado               
			
		If !lEnviado
			  GET MAIL ERROR cSmtpError                                   
			  //MsgSTop( "Erro de envio para todos os e-mail: " + cSmtpError)
		      Conout( "Erro de envio para todos os e-mail: " + cSmtpError)
		Else
			DISCONNECT SMTP SERVER
		Endif

	Else
		Conout('NAO CONECTOU NO SERVIDOR DE E-MAIL    -  ENVIA_TODOS') 	
	Endif


	If lConectou .And. lEnviado
		Conout('ENVIADO COM SUCESSO PARA: ' + cTRecebe )
		IIF(Len(aCopia)>0, Conout('COPIA PARA: '+ cTCopia ), )  
		IIF(Len(aCopOculta)>0 , Conout('COPIA OCULTA PARA: '+ cTCopOculta ), )  
		lRetorno	:=	.T.
	Endif

Return(lRetorno)
*********************************************************************************
Static Function ENVIA_1_A_1()
*********************************************************************************
Local lRetorno		:=	.F.
Local cNaoRecebeu	:=	''
Local nCount		:=	0
Local Y := 1

Conout('ENVIANDO 1 - 1') 

For Y:=1 To Len(aTodosEmail)
 
	CONNECT SMTP SERVER cServer ACCOUNT cAccount PASSWORD cPassword Result lConectou 
	MailAuth(cAccount,cPassword)
	
	If lConectou                                    
		SEND MAIL FROM cRemetente TO aTodosEmail[Y]  SUBJECT cAssunto BODY cCorpoEmail ATTACHMENT cAnexo RESULT lEnviado		 
				
		If !lEnviado
			  GET MAIL ERROR cSmtpError                                       
  			  //MsgSTop( "Erro de envio e-mail: " + cSmtpError+ ' - ' +aTodosEmail[Y])
		      Conout( "Erro de envio : " + cSmtpError + ' - ' +aTodosEmail[Y])
		Else
			DISCONNECT SMTP SERVER
		Endif

	Else
		Conout('NAO CONECTOU NO SERVIDOR DE E-MAIL    -  1-1')  	
	Endif
	
	
	If !lConectou .Or. !lEnviado
		cNaoRecebeu	+= IIF(!lConectou,'NAO CONECTOU NO SERVIDOR DE E-MAIL   ', 'PROBLEMA NO E-MAIL   ' ) + aTodosEmail[Y]+ENTER
		nCount++
	Endif

Next

		
If !Empty(cNaoRecebeu)                                  

	cDataHora		:= AllTrim(Str(Year(dDataBase))) 	+'_'+ Time()

	cEmailAdm	:=	AllTrim(GetMv('GL_ADMIN_E'))
	cTexto		:=	'ERRO NO ENVIO DE EMAIL'+ENTER+'PROGRAMA: '+cNomeRotina +ENTER+ENTER+'PROBLEMA NOS SEGUINTES E-MAIL:'+ENTER+ cNaoRecebeu                                                                       
	
	CONNECT SMTP SERVER cServer ACCOUNT cAccount PASSWORD cPassword Result lConectou 
	MailAuth(cAccount,cPassword)
		
	If lConectou                                    
		SEND MAIL FROM cRemetente TO cEmailAdm SUBJECT cAssunto BODY cTexto ATTACHMENT cAnexo RESULT lEnviado		
		If !lEnviado
			GET MAIL ERROR cSmtpError                                                     
			Conout( "Erro de envio para o administrador: " + cSmtpError )  
			//MsgSTop( "Erro de envio e-mail: " + cSmtpError)
		Else
			DISCONNECT SMTP SERVER
		Endif
	Else
		Conout('NAO CONECTOU NO SERVIDOR DE E-MAIL - ADMINISTRADOR')
	Endif

Endif
		
Return(IIF(nCount==Len(aTodosEmail), .F., .T.) )
