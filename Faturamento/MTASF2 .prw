#Include "PROTHEUS.CH"  

user function MTASF2 ()
Local cNome := space(100)

if SF2->F2_ESPECIE <>'SPED'

    cNome := FwInputBox("Mensagem para nota", cNome)
    if !empty(cNome)
       SF2->F2_MENNOTA  := ALLTRIM(SF2->F2_MENNOTA)+'  '+ALLTRIM(cNome)
    endif
endif 
Return


User function FISTRFNFE

    aAdd(aRotina,{'Vizualiza mensagem da nota',"u_mennota()",0,8,0,.F.}) //"Complementos"

return


user function mennota()

cTeste := SF2->F2_MENNOTA

msgalert(cTeste)

return 
