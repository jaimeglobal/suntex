//Bibliotecas
#Include "Protheus.ch"
#Include "TBIConn.ch" 
#Include "Colors.ch"
#Include "RPTDef.ch"
#Include "FWPrintSetup.ch"

/*
Vers�es:	Microsiga Protheus 12 ou superior VERSAO MVC
Idiomas:	Espanhol , Ingl�s
Descri��o: Utilizado na Rotina MVC da Liquida��o substituindo pontos entradas anteriores
Programa Fonte: FINA460.PRW
Sintaxe: FINA460A
Retorno: .T. ou .F. - Retorno verdadeiro ou falso
*/

User Function FINA460A()

Local aParam := PARAMIXB
Local xRet := .T.
Local oObj := ''
Local cIdPonto := ''
Local cIdModel := ''
Local nLinha := 0
Local nQtdLinhas:= 0
Local cMsg := ''
Local cClasse := ""
Private nxJuros := 0

If aParam <> NIL

    oObj := aParam[1]
    cIdPonto := aParam[2]
    cIdModel := aParam[3]


    If cIdPonto == 'MODELPOS' // Bloco substitui o ponto de entrada F460TOK e FA460CON
        
        cMsg := 'Chamada na valida��o total do formul�rio (MODELPOS).' + CRLF
        cMsg += 'ID ' + cIdModel + CRLF
        If cClasse == 'FWFORMGRID' // Bloco substitui o ponto de entrada FA460LOK, valida��o do Grid, utilizar o ID de Model 'TITGERFO2'
            nQtdLinhas := oObj:Length()
             nLinha := oObj:GetLine()

            cMsg += '� um FORMGRID com ' + Alltrim( Str( nQtdLinhas ) ) + ' linha(s).' + CRLF
            cMsg += 'Posicionado na linha ' + Alltrim( Str( nLinha ) ) + CRLF

        EndIf

        //If !( xRet := ApMsgYesNo( cMsg + 'Continua ?' ) )
        //    Help( ,, 'Help',, 'O MODELPOS retornou .F.', 1, 0 )
        //EndIf
    ElseIf cIdPonto == 'MODELCANCEL' // Bloco substitui os pontos de entrada F460CAN, F460CON e F460SAID no cancelamento da tela de gera��o de liquida��o.
        cMsg := 'Chamada no Bot�o Cancelar (MODELCANCEL).' + CRLF + 'Deseja Realmente Sair ?'
        //If !( xRet := ApMsgYesNo( cMsg ) )
        //    Help( ,, 'Help',, 'O MODELCANCEL retornou .F.', 1, 0 )
        //EndIf
    ElseIf cIdPonto == 'FORMLINEPRE' // Bloco substitui o ponto de entrada A460VALLIN.
        
        If AllTrim(cIdModel) == 'TITSELFO1'
            // Chamada Fun��o - SAD - Recalcula parccelas com JUROS
            //recall()
            //
        EndIf
        If cIdModel == 'TITGERFO2'
            nQtdLinhas := oObj:Length()
            nLinha := oObj:GetLine()
            If aParam[5] == 'DELETE' // Dele��o de Linha do Grid
                cMsg := 'Chamada na pre valida��o da linha do formul�rio (FORMLINEPRE).' + CRLF
                cMsg += 'Onde esta se tentando deletar uma linha' + CRLF
                cMsg += '� um FORMGRID com ' + Alltrim( Str( nQtdLinhas ) ) + ' linha(s).' + CRLF
                cMsg += 'Posicionado na linha ' + Alltrim( Str( nLinha ) ) + CRLF
                cMsg += 'ID ' + cIdModel + CRLF

                //If !( xRet := ApMsgYesNo( cMsg + 'Continua ?' ) )
                //    Help( ,, 'Help',, 'O FORMLINEPRE retornou .F.', 1, 0 )
                //EndIf
            EndIf
        EndIf
    ElseIf cIdPonto == 'FORMLINEPOS' // Substitui o ponto de entrada FA460LOK

        If AllTrim(cIdModel) == 'TITSELFO1'
            // Chamada Fun��o - SAD - Recalcula parccelas com JUROS
            //recall()
            //
        EndIf
        If cIdModel == 'TITGERFO2'
            nQtdLinhas := oObj:Length()
            nLinha     := oObj:GetLine()
            nx         := 0
            cMsg := 'Chamada na valida��o da linha do formul�rio (FORMLINEPOS).' + CRLF
            cMsg += 'ID ' + cIdModel + CRLF
            cMsg += '� um FORMGRID com ' + Alltrim( Str( nQtdLinhas ) ) + ' linha(s).' + CRLF
            cMsg += 'Posicionado na linha ' + Alltrim( Str( nLinha ) ) + CRLF
            //If !( xRet := ApMsgYesNo( cMsg + 'Continua ?' ) )
            //    Help( ,, 'Help',, 'O FORMLINEPOS retornou .F.', 1, 0 )
            //EndIf
        EndIf

    ElseIf cIdPonto == 'MODELCOMMITNTTS' // Bloco substitui o ponto de entrada F460GRV.

        //ApMsgInfo('Chamada apos a grava��o total do modelo e fora da transa��o (MODELCOMMITNTTS).' + CRLF + 'ID ' + cIdModel)
        // Chamada Fun��o - SAD - Recalcula parccelas com JUROS

        //oModel 	  := FWModelActive()
        //oModelFO0 := oModel:GetModel("MASTERFO0")
        //oModelFO2 := oModel:GetModel("TITGERFO2")
        //If oModelFO0:GetValue("FO0_VLRJUR") > 0 .and. oModelFO2:GetValue("FO2_VALOR", 1) == 0
        //    xRet := ApMsgNoYes( 'H� Juros que foram calculados nas parcelas, N�O ESQUECER DE CLICAR NO BOT�O Outras A��es < RECALCULAR >' + 'Continua ?' ) 
        //EndIf
        // 

    ElseIf cIdPonto == 'BUTTONBAR' // Bloco substitui o ponto de entrada F460BOT.

        //ApMsgInfo('Adicionando Botao na Barra de Botoes (BUTTONBAR).' + CRLF + 'ID ' + cIdModel )
        xRet := { {'Recalcular', 'Recalcular', { || Recall() }, 'Este botao Recalcula' } }

    EndIf
    // Chamada Fun��o - SAD - Recalcula parccelas com JUROS
    If AllTrim(cIdModel) == 'TITSELFO1' 
        oModel := FWModelActive()
        If oModel:isActive()
            recall()
        Endif        
    EndIf
    //
    
EndIf

Return xRet

Static Function Recall()

Local oModel 	:= FWModelActive()
Local oView		:= FWViewActive()
Local oModelFO2 := oModel:GetModel("TITGERFO2")
//Local oModelFO1 := oModel:GetModel('TITSELFO1')
Local oModelFO0 := oModel:GetModel("MASTERFO0")
Local nLinhas	:= oModelFO2:Length()
Local nxJuros   := oModelFO0:GetValue("FO0_VLRJUR")	/ nLinhas //"Valor Total Juros" / Numero Parcelas
Local FO0VLRLIQ := oModelFO0:GetValue("FO0_VLRLIQ") //"Valor Total a Liquidar"
Local FO0VLRNEG := oModelFO0:GetValue("FO0_VLRNEG") //"Valor Total Negociado"
//Local FO0TTLTIT := oModelFO0:GetValue("FO0_TTLTIT") //"Qtde Titulos"
Local nx        := 0	
Local nTotLiq   := 0
Local nVlrPar   := 0

If oModelFO2:Length() > 0 .and. nxJuros > 0
    //Alert(FO0VLRLIQ)
    //Alert(FO0VLRNEG)
    nVlrPar := ( Round(FO0VLRLIQ / nLinhas, 2) )
	nTotLiq := Round(FO0VLRNEG / nLinhas, 2)
    For nx := 1 to nLinhas
        oModelFO2:GoLine(nX)
		If !oModelFO2:IsDeleted()
            oModelFO2:LoadValue("FO2_VALOR",  nVlrPar ) // posi��es do aCols e o nome do campo no modelo
            oModelFO2:LoadValue("FO2_VLPARC", nVlrPar ) // posi��es do aCols e o nome do campo no modelo
            oModelFO2:LoadValue("FO2_ACRESC", nxJuros ) // posi��es do aCols e o nome do campo no modelo
            oModelFO2:LoadValue("FO2_TOTAL",  nTotLiq ) // posi��es do aCols e o nome do campo no modelo
        EndIf
    Next

EndIf

Return .t.
