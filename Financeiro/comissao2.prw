#Include "PROTHEUS.CH"
#include "rwmake.ch"   
#INCLUDE 'TOPCONN.CH'

user function   comiss4()

//-- -- Variaveis
Local     oDlg
Local     oGrp
Local     oGridAtd
Local     aH          := {}
Local     aC          := {}
Local     aCmpAlt     := {"C6_COMISS3","C6_COMISS2","C6_COMISS1"}
cPerg := PADR("MANNF4", 10)

DbSelectArea("SX1")
DbSetOrder(1)

IF !DbSeek(cPerg)
   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Pedido :"
   SX1->X1_ORDEM   := "01"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 9
   SX1->X1_VARIAVL := "mv_ch1"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR01"
   SX1->X1_F3      := "SC5"
   MsUnlock()
   
   RecLock("SX1",.T.)
   SX1->X1_GRUPO   := cPerg
   SX1->X1_PERGUNT := "Nota :"
   SX1->X1_ORDEM   := "02"
   SX1->X1_TIPO    := "C"
   SX1->X1_TAMANHO := 9
   SX1->X1_VARIAVL := "mv_ch2"
   SX1->X1_GSC     := "G"
   SX1->X1_VAR01   := "MV_PAR02"
   SX1->X1_F3      := "SF2"
   MsUnlock()
endif 
IF EMPTY(MV_PAR01) .OR. EMPTY(MV_PAR02)
    msgalert('Aten��o , os par�metros de pedido e nota s�o obrigat�rios')
ENDIF 

IF !PERGUNTE(cPerg,.T.)
        msgalert('rotina cancelada !')
        Return
ENDIF

//-- -- Popula aHeader.
AADD(aH, {"Item"      , "C6_ITEM" , "@!" , 02 , 00, , , "C", "", , , })
AADD(aH, {"Produto", "C6_PRODUTO", "@!", 15 , 00, , , "C", "", , , })
AADD(aH, {"Comissao1" , "C6_COMISS1", "@E 999.99", 6, 2, , , "N", "", , , })
AADD(aH, {"Comissao2" , "C6_COMISS2", "@E 999.99", 6, 2, , , "N", "", , , })
AADD(aH, {"Comissao3" , "C6_COMISS3", "@E 999.99", 6, 2, , , "N", "", , , })
AADD(aH, {"Valor " , "C6_VALOR", "@E 999,999.99", 10, 2, , , "N", "", , , })

cQuery := " SELECT * FROM "+ retsqlname('SC6')+" WHERE "+;
          " C6_NUM ='"+MV_PAR01+"' AND C6_FILIAL ='"+xFilial('SC6')+"' and C6_NOTA ='"+MV_PAR02+"' "

if Select("tmpp") > 0
    tmpp->(DbCloseArea())
endif

TcQuery cQuery New Alias "tmpp"

while ! tmpp->(eof())
    
    aadd(aC,{tmpp->C6_ITEM,tmpp->C6_PRODUTO,tmpp->C6_COMIS1,tmpp->C6_COMIS2,tmpp->C6_COMIS3,tmpp->C6_VALOR,.F.})
    tmpp->(dbSkip())          
EndDo

//-- -- Cria Janela Dialog
oDlg := TDialog():New( 000, 000, 500, 500, "Comiss�o por Pedido ", , , , , , , , , .T., , , , , , .F.)
     
//-- -- Cria objeto MsNewGetDados
//-- -- Documenta��: http://tdn.totvs.com/display/public/mp/MsNewGetDados
oGridAtd:= MsNewGetDados():New( 001, 001, 150, 255,GD_INSERT + GD_UPDATE + GD_DELETE , , , , aCmpAlt, , 10, , , , oDlg, aH, aC, , )

    @ 200, 080 BUTTON oButton1 PROMPT "Confirma" SIZE 037, 012 OF oDlg PIXEL ACTION (Confirma(@oGridAtd:aCols),oDlg:end())
    @ 200, 125 BUTTON oButton2 PROMPT "Cancela" SIZE 037, 012 OF oDlg ACTION ( oDlg:End()) PIXEL    
//-- -- Ativa Janela DIalog
oDlg:Activate( , , , .T., {||}, , {||}, , )
     
Return

static function Confirma(aTeste)
Local nAux
Local aComiss:={}
Local nTotal := 0
Local lAtualizapedido := .F.
Local lNota:=.F.
Local cChaveSE1 :=' '


nTotal := gettotal(@aTeste)
lAtualizapedido:= AtuaizaPedido(@aTeste,@lNota,@cChaveSE1)

if ! lAtualizapedido
    msgalert('Aten��o , n�o foi encontrada diferen�a entre os valores digitados e os do pedido , programa encerrado ! ')
    return
endif
// desenvolvimento de l�gica para aplicar comiss�o nas tabelas 

if lNota 
    AtualizaNota(@aTeste,nTotal,cChaveSE1 )
endif 

for nAux := 1 to len(aTeste)

NEXT
msgalert('Altera��o conclu�da com sucesso !')
return



//Fun��o para alimentar o total do pedido 
static function gettotal(aTeste)
Local nRet:= 0
Local nAux 

for nAux :=1 to len(aTeste)
    nRet+= aTeste[nAux][6]
Next


return nRet


static function AtuaizaPedido(aTeste,lNota , cChaveSE1)
Local lRet :=.F.
Local nAux

for nAux :=1 to len(aTeste)
    if SC6->(DbSeek(xFilial('SC6')+alltrim(MV_PAR01)+aTeste[nAux][1]+aTeste[nAux][2])) .and. ;
    (aTeste[nAux][3] <> SC6->C6_COMIS1 .OR. aTeste[nAux][4] <> SC6->C6_COMIS2 .OR. aTeste[nAux][5] <> SC6->C6_COMIS3 )
        lRet:=.T.
        SC6->(Reclock('SC6',.f.))
        SC6->C6_COMIS1 := aTeste[nAux][3]
        SC6->C6_COMIS2 := aTeste[nAux][4]
        SC6->C6_COMIS3 := aTeste[nAux][5]
        SC6->(MsUnlock())
        IF !Empty(SC6->C6_NOTA)
        SD2->(DbSetOrder(8))
            if SD2->(dBseek(xFilial('SD2')+SC6->C6_NUM+SC6->C6_ITEM))
                lNota:=.t.
                cChaveSE1:= xFilial('SE1')+SC6->C6_SERIE+SC6->C6_NOTA
                SD2->(RecLock('SD2'),.F.)
                SD2->D2_COMIS1 :=aTeste[nAux][3]
                SD2->D2_COMIS2 :=aTeste[nAux][4]
                SD2->D2_COMIS3 :=aTeste[nAux][5]
                SD2->(MsUnlock())
            endif 
        endif
    endif
next 


return lRet

static function AtualizaNota(aTeste, nTotal ,cChaveSE1)
Local nPercente := 0
Local nAux 
Local nAux2
// aqui vou calcular o percentual em cada um e atualizar 
for nAux := 1 to 3
    nPercente:= 0
    for nAux2 := 1 to len(aTeste)
 
        nPercente +=  (aTeste[nAux2][nAux+2] * aTeste[nAux2][6]) / nTotal 
       

    next 
    nPercente := round(nPercente ,2)
     AjustaNota(cChaveSE1 ,nAux,nPercente)

next 


return


static function AjustaNota(cChaveSE1,nVend,nPercente)
Local cVend := alltrim(str(nVend))

Local cCampoE1 := 'E1_COMIS'+cVend
Local cVendedor := 'SE1->E1_VEND'+cVend

//msgalert('Vou ver se a chave a seguir encontrou algo:'+cChaveSE1)
    if SE1->(DbSeek(cChaveSE1))
       // msgalert('achou a e1')
        while (!SE1->(eof())) .and. SE1->(E1_FILIAL+E1_PREFIXO+E1_NUM) == cChaveSE1 
        if SE1->E1_TIPO=='NF '    
                SE1->(RECLOCK("SE1",.F.))
                &("SE1->"+cCampoE1) := nPercente
	             SE1->(MSUNLOCK())
	           // msgalert('� nf ! ')
        ENDIF
                IF !EMPTY(SE1->E1_BAIXA)
                    u_acertaSE3(SE1->E1_BAIXA, &cVendedor)
                ENDIF
                SE1->(DbSkip())
        enddo
    else 
        //msgalert('n�o achou e1')
    ENDIF


return


IIF(SD2->D2_TES=='599' .AND. SF2->F2_TIPO='D',((SD2->D2_TOTAL+SD2->D2_VALFRE)*7.60)/100,0)
