#INCLUDE 'RWMAKE.CH'
#DEFINE  ENTER CHR(13)+CHR(10)

User Function Mta410()

Local aAreaAtu 		:= GetArea()
Local aAreaSC5 		:= SC5->(GetArea())
Local aAreaSC6 		:= SC6->(GetArea())
Local aAreaSF4 		:= SF4->(GetArea())
Local aAreaSA1     := SA1->(GetArea())
Local oBloqueio    
Local nPTES 		:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_TES"  	 })
Local nPoc 			:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_PEDCLI"  	 })
Local nPosPrd 		:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_PRODUTO"  	 })
Local nPosLib 	    :=aScan(aHeader,{|x| AllTrim(x[2])=="C6_QTDLIB"  	 })
Local nPosNota  	:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_NOTA"  	 })
Local nPosPrecven	:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_PRCVEN"  	 })
Local nPosLote 		:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_LOTECTL"  	 })
Local nPosLoc		:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_LOCAL"  	 })
Local nPositem		:=aScan(aHeader,{|x| AllTrim(x[2])=="C6_ITEM"  	 })
Local nLc           := 1 
Local LOCvazio			:=	.F.
Local Ok    		:= .T.
Local nX
Local cTeste:= ' '
Local aArea :=  GetArea() 
Private lInverte := .F.
Private cMark   := GetMark()   
Private oMark 

If Inclui .or. Altera
	DbSelectArea("SC5");DbSetOrder(1)
	DbSelectArea("SC6");DbSetOrder(1)

		
		
	    if alltrim(M->C5_YSEMOC)  =="2" .and. LOCvazio .and. M->C5_CLIENTE ='000264'

			msgalert('Aten��o , pedido com oc , mas a mesma n�o est� preenchida nos itens ')
			M->C5_YNUMOC  := '5'
			M->C5_BLQ    :='1' 
			//Aqui eu gero os bloqueios 
			for nLc := 1 To len(aHeader)
					cTeste += aHeader[nLc][2]
			next
				MemoWrit('C:\TEMP\SG_VALMOD.TXT', cTeste )
			For nX := 1 To Len( aCols )	
				
			
				if alltrim(aCols[nX][nPoc])==''
					oBloqueio := Bloqueio_pedido():new(M->C5_NUM,aCols[nX][nPosItem])
        			oBloqueio:Filial     :=xFilial('SC9')	
        			oBloqueio:Pedido     :=M->C5_NUM
        			oBloqueio:Item       :=aCols[nX][nPosItem]	
        			oBloqueio:Cliente    :=M->C5_CLIENTE
        			oBloqueio:Loja       :=M->C5_LOJACLI
					oBloqueio:Produto    :=aCols[nX][nPosPrd]	
					oBloqueio:QtdLib     :=aCols[nX][nPosLib]	
					oBloqueio:NF         :=aCols[nX][nPosNota]	
					oBloqueio:Serie      :=' '
					oBloqueio:Grupo      :=posicione('SB1',1,xFilial('SB1')+aCols[nX][nPosPrd]	,'B1_GRUPO')
					oBloqueio:PrecoVenda :=0
					oBloqueio:BloqEst    :='09'
					oBloqueio:BloqCred   :='01'	
					oBloqueio:Lote       :=aCols[nX][nPosLote]	
					oBloqueio:Armazem    :=aCols[nX][nPosLoc]	
					oBloqueio:DataEnt    :=date()
					oBloqueio:grava()
					freeobj(oBloqueio)

					
				endif 
				

			Next


		endif 
		if empty(M->C5_YSEMOC) .and. M->C5_CLIENTE ='000264'
			msgalert('Aten��o para o cliente beira rio deve ser preenchido o campo SEM OC')
			Ok :=.F.
		endif 
		if M->C5_CLIENTE ='000264' .and. !empty(M->C5_YNUMOC) .and. !LOCvazio
			M->C5_YNUMOC:=' '
		endif 
		if  M->C5_CLIENTE ='000264'
			For nX := 1 To Len( aCols )
				if  alltrim(aCols[nX][nPoc])<>'' .or.  M->C5_YSEMOC =='1'
					if SC9->(DbSeek(xFilial('SC9')+M->C5_NUM+aCols[nX][nPosItem]))
						IF SC9->C9_BLINF == "BLOQUEIO API"
							oLibera := 	 Bloqueio_pedido():libera(M->C5_NUM,aCols[nX][nPosItem])
							M->C5_BLQ    :='0' 
						ENDIF 
					endif 
				endif 
			next 
			if M->C5_YNUMOC  == '5' .and. !LOCvazio
				M->C5_YNUMOC :=' ' 
			endif 
		endif 

endif       
	
Restarea(aAreaSF4)
Restarea(aAreaSC6)
Restarea(aAreaSC5)
Restarea(aAreaAtu)
Restarea(aAreaSA1)

Return Ok



//interface para posi��o do campo
static function campo (array ,cCampo)
Local nRet := 0 

	nRet :=aScan(array,{|x| AllTrim(x[2])==alltrim(cCampo)  	 })
  
return nRet
