#Include 'Protheus.ch'

user function F811MAILTO()

local cEmail := AllTrim(SA1->A1_EMAILBL)
local cRet := ''

if !empty(cEmail)
      cRet := cEmail
else
      cRet := SA1->A1_EMAIL
endIf

return cRet
