#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'XMLXFUN.CH'
#INCLUDE 'AP5MAIL.CH'
#INCLUDE 'SHELL.CH'
#INCLUDE 'XMLXFUN.CH'
#INCLUDE 'TBICONN.CH'
#DEFINE  ENTER CHR(13)+CHR(10)


User Function MyQuery( cQuery , cursor )

//cQuery := ChangeQuery(cQuery)
Local bError		:= { |e| oError := e , BREAK(e) }
Local bErrorBlock	:= ErrorBlock( bError )
Local cTCSqlError	:= ""
Local nRet 		:= 0

IF SELECT( cursor ) <> 0
	dbSelectArea(cursor)
	DbCloseArea(cursor)
	//	Use
Endif

BEGIN SEQUENCE

TCQUERY cQuery NEW ALIAS (cursor)

RECOVER

cError      := oError:Description
cTCSqlError := TCSqlError()

END SEQUENCE

ErrorBlock( bErrorBlock )

if !empty(cTCSqlError)
	MsgAlert(cTCSqlError,"Erro na Query")
	nRet := -1
else
	nRet := 1
	
endif

Return nRet
