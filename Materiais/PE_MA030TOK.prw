#include 'protheus.ch'
#include 'parmtype.ch'

user function MA030TOK()
lOCAL aAreaAnt := GetArea()
	
	CTD->(DbSetOrder(1))
	If ! CTD->(DbSeek(xFilial('CTD') +alltrim(M->A1_COD) ))		
		CTD->(RecLock("CTD",.T.)) //Inclui
	   	CTD->CTD_FILIAL := XFILIAL('CTD')
		CTD->CTD_ITEM   := alltrim(M->A1_COD)
		CTD->CTD_CLASSE :='2'
		CTD->CTD_NORMAL :='2'
		CTD->CTD_DESC01 := M->A1_NOME
		CTD->CTD_BLOQ   :='2'
		CTD->CTD_ITLP   :=alltrim(M->A1_COD)
		CTD->CTD_ITSUP	:='1'
		CTD->CTD_CLOBRG :='2'
		CTD->CTD_ACCLVL :='1'
		CTD->CTD_DTEXIS :=STOD('19800101')
		CTD->(MsUnlock())
	endif
	M->A1_ITEMCTA :=alltrim(M->A1_COD)	
	
	RESTAREA(aAreaAnt) 
return .T.
