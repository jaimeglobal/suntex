#include 'totvs.ch'   
#INCLUDE 'TOPCONN.CH'

#Define STR_PULA    Chr(13)+Chr(10)

user function pingado()
Local nAux :=0
Local cRet :=' '

for nAux := 1 to 50
    cRet += 'milissegundos:'+strzero(Ping(10),3)+STR_PULA
next 

msgalert(cRet)

return
