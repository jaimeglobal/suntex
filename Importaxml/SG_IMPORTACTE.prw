#INCLUDE 'Protheus.ch'  
#INCLUDE 'MATA116I.ch'  
User Function GRIDCTE()                  
   Private cCadastro := "Importador C T E  [ Compras ]  [ Vendas ] "
   Private aRotina := { {"Importar"   ,"u_CTEA250",0,3} }
                     //   {"Excluir"   ,"AxDeleta",0,5} }
   Private cDelFunc := ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
   Private cString := "SDS"
   dbSelectArea("SDS")
   dbSetOrder(1)
   mBrowse( 6,1,22,75,cString,,,,,,,,,,,,,,,)
Return


user Function CTEA250()    //    [4.1]
************************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Seleciona XML PASTA             ³
//³ Chamada -> Carrega_XML()        ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
LOcal cDiretorioXML	:=	Space(200)
Private cMarca        	:=	GetMark()
Private nQtdArqXML  	:= 	0
Private aStatus			:= 	{}

SetPrvt('oDlg1','oGrp','oSay1','oGet1','oBrw1','oBtn1','oBtn2','oBtn3')

//	COM "\" NO FINAL NAO ENCONTRA O DIRETORIO
cPathXML :=' '
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³    TELA CARREGA ARQUIVO XML     ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
oDlg1      := MSDialog():New( D(095),D(232),D(427),D(740),"Carregar arquivo XML - IMPORTAR",,,.F.,,,,,,.T.,,,.T. )
oGrp       := TGroup():New( D(004),D(004),D(146),D(250),"",oDlg1,CLR_BLACK,CLR_WHITE,.T.,.F. )
oSay1      := TSay():New( D(012),D(008),{||"Caminho do(s) Arquivo(s)"},oGrp,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(064),D(008) )
oGet1      := TGet():New( D(020),D(008),{|u| If(PCount()>0,cDiretorioXML :=u,cDiretorioXML)},oGrp,D(230),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,{|| IIF(!Empty(cDiretorioXML),(AdicionaAnexo(), oBrw1:oBrowse:Refresh()),) },.F.,.F.,"","",,)
oBtn1      := TButton():New( D(018),D(238),"...",oGrp,{|| cDiretorioXML  := 	cGetFile("Anexos (*xml)|*xml|","Arquivos (*xml)",0,cPathXML,.T.,GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_MULTISELECT+GETF_RETDIRECTORY),; 
																		IIF(!Empty(cDiretorioXML), Processa ({||AdicionaAnexo(cDiretorioXML),'Processando....','Carregando Arquivos',.F.}),), IIF(nQtdArqXML==0, oSay2:Hide(), oSay2:Show()),oBrw1:oBrowse:Refresh() },D(011),D(011),,,,.T.,,"",,,,.F. )

oSay2      := TSay():New( D(034),D(008),{|| AllTrim(Str(nQtdArqXML))+" arquivo(s) selecionado(s)"},oGrp,,,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(008) )
IIF(nQtdArqXML==0, oSay2:Hide(), oSay2:Show())
oSay2:Refresh()
****************************
   oTbl()    // CRIAR ARQ. TEMP
****************************
DbSelectArea("TMP")
oBrw1      := MsSelect():New( "TMP","OK","",{{"OK","","",""},{"ARQUIVO","","Arquivo",""}},.F.,cMarca,{D(040),D(008),D(140),D(245) },,, oGrp )
oBtn2      := TButton():New( D(150),D(086),"Ok"  ,oDlg1,{|| iif(nQtdArqXML >0 ,Processa ({|| Open_Xml('XML_TMP','','', cDiretorioXML,0,0)},'Verificando Arquivos XML','Processando...', .F.),msgalert('Nenhum XML selecionado ')) ,oDlg1:End() },D(037),D(012),,,,.T.,,"",,,,.F. )
oBtn3      := TButton():New( D(150),D(146),"Sair",oDlg1,{|| oDlg1:End(), lClose:=.T.},D(037),D(012),,,,.T.,,"",,,,.F. )
oBrw1:oBrowse:bAllMark      :=    {|| MarcaTMP(),         oSay2:Refresh(), oBrw1:oBrowse:Refresh() }
oBrw1:oBrowse:bLDblClick    :=    {|| Des_MarcaTMP(),     oSay2:Refresh(), oSay2:Refresh()}
oDlg1:Activate(,,,.T.,{||.T.})
IIF(Select("TMP")!= 0, TMP->(DbCLoseArea()), )
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ MOSTRA TELA COM STATUS DA IMPORTACAO DO XML  ³
//|	XML IMPORTADO VIA PASTA						 |
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
If Len(aStatus) > 0
   ******************************
       MensStatus()
   ******************************
EndIf
Return()

Static Function D(nTam)	//	QDO TELA Eh DESENVOLVIDA COM RESOLUCAO 1080
***********************************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Ajusta Tamanho dos Objetos            ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
Local nHRes    :=    oMainWnd:nClientWidth		// Resolucao horizontal do monitor
   If nHRes == 640                            	// Resolucao 640x480 (soh o Ocean e o Classic aceitam 640)
       nTam /= 1.28
   ElseIf (nHRes == 798).Or.(nHRes == 800)    	// Resolucao 800x600
       nTam /= 1.28
   Else                                        	// Resolucao 1024x768 e acima
      nTam *= 1.28
   EndIf
   //ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
   //³Tratamento para tema "Flat"³
   //ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
   If "MP8" $ oApp:cVersion .Or. '10' $ cVersao 
       If (Alltrim(GetTheme()) == "FLAT") .Or. SetMdiChild()
           nTam /= 1.04
       EndIf
   EndIf
Return Int(nTam)            

Static Function oTbl()    //    [4.1.1]
************************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Cria\Limpa Arq. Temp            			³
//³ Chamada -> Carrega_XML()->LoadFiles()       ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
Local aFds := {}
Local cTmp
aArqTemp := {}
If Select("TMP") == 0
   Aadd( aFds , {"OK"      ,"C",002,000} )
   Aadd( aFds , {"ARQUIVO" ,"C",200,000} )

   cTmp := CriaTrab( aFds, .T. )
   Use (cTmp) Alias TMP New Exclusive
   Aadd(aArqTemp, cTmp)
Else
   LimpaTMP()
EndIf

Return()

Static Function LimpaTMP()    //    [4.1.5]
***********************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Limpa Arq.Temp                                 |
//³ Chamada -> Carrega_XML()->LoadFiles()          ³
//³         -> oTbl(), AdicionaAnexo()             ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
If Select('TMP') > 0
   DbSelectArea("TMP")
   RecLock('TMP', .F.)
       Zap
   MsUnLock()
Endif
TMP->(DbGoTop())
Return()

Static Function MarcaTMP(cPar)    //    [4.1.3]
***********************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Desmarca TODOS os itens do Browse              |
//³ Chamada -> Carrega_XML()->LoadFiles()          ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
nQtdArqXML := 0
DbSelectArea('TMP');DbGoTop()
_cFlag    :=    IIF(!Empty(TMP->OK), cMarca,'')
Do While TMP->(!Eof())
   If !Empty(_cFlag)
       nQtdArqXML :=	0
	Else
		nQtdArqXML++	
   EndIf
   RecLock("TMP",.F.)
       TMP->OK  := IIF(!Empty(_cFlag),'', cMarca)
   MsUnLock()
   DbSkip()
EndDo
TMP->(DbGoTop())
Return()
***********************************************************
Static Function Des_MarcaTMP()    //    [4.1.4]
***********************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Marca\Desmarca itens do Browse                 |
//³ Chamada -> Carrega_XML()->LoadFiles()          ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
_cMarca    :=    IIF(Empty(TMP->OK), cMarca,'')
If !Empty(_cMarca)
   nQtdArqXML++
Else
   nQtdArqXML--
EndI
RecLock("TMP",.F.)
   TMP->OK  := _cMarca
MsUnLock()
Return()



Static Function AdicionaAnexo(cDiretorioXML)		//	[4.1.2]
***********************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Adiciona no Browser os itens XML de uma pasta  |
//³ Chamada -> Carrega_XML()->LoadFiles()          ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
Local    aArqXml     :=    {}
Local _X := 0
If !Empty(cDiretorioXML)
	nQtdArqXML    := 0
	cDiretorioXML := cDiretorioXML+IIF(Right(AllTrim(cDiretorioXML),1)=='\','','\')
	aDir(AllTrim(cDiretorioXML)+'*.xml',aArqXml)
Else
	cDiretorioXML := Space(200)
	Return()
EndIf                
   LimpaTMP1()
For _X:=1 To Len(aArqXml)
   nQtdArqXML++
   RecLock("TMP",.T.)
       TMP->OK 	     := cMarca
       TMP->ARQUIVO  := aArqXml[_X]
   MsUnLock()
Next
TMP->(DbGoTop())
Return()

Static Function LimpaTMP1()    //    [4.1.5]
***********************************************************
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Limpa Arq.Temp                                 |
//³ Chamada -> Carrega_XML()->LoadFiles()          ³
//³         -> oTbl(), AdicionaAnexo()             ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ

If Select('TMP') > 0
   DbSelectArea("TMP")
   RecLock('TMP', .F.)
       Zap
   MsUnLock()
Endif

TMP->(DbGoTop())

Return()



static function Open_Xml(cTpOpen, cAnexo, cNomeArq, cDiretorioXML, nImportados, nMarcados)

Local  cArqImp    := ' '
Local  aProc	:= {}
Local  aErros:= {}
Local  aErroErp:= {}
Local cXml :=''
Local oXml
Local cError := ' '
Local  cWarning :=' '
Local  lSF1    := .F.
Local aRet := {}
Local nx 
Private	cTpCarga :=	''
Private oLista                    //Declarando o objeto do browser
Private aCabecalho  := {}         //Variavel que montará o aHeader do grid
  Private aColsEx 	:= {}         //Variável que receberá os dados
 

    
    aTescond := tescond()
    cTes :=aTescond[1]
    cCond := aTescond[2]
	nMarcados	:=	0
	nImportados	:=	0	
	For nX:=1 To nQtdArqXML
		If !Empty(cMarca)
			nMarcados++
		EndIf
	Next
	ProcRegua(nMarcados)
	DbSelectArea('TMP');DbGoTop()
	Do While !Eof()
		If IsMark('OK', cMarca )
			nImportados++
			IncProc('Importando arquivos XML... '+ AllTrim(Str(nImportados))+' de '+ AllTrim(Str(nMarcados))  )
			cArqXML	:=	AllTrim(TMP->ARQUIVO)
			cAnexo	:=	''
           	ProcRegua(Len(cAnexo))
			cTeste := ''
			//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
			//³REALIZA A LEITURA DO ARQUIVO XML ³
			//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
			If File(AllTrim(cDiretorioXML)+cArqXML)
				FT_FUSE(AllTrim(cDiretorioXML)+cArqXML)
				FT_FGOTOP()
				Do While !FT_FEOF()
					cLinha := FT_FREADLN()
					cLinha := StrTran(cLinha,'</','')
					cLinha := StrTran(cLinha,'<','')
					cLinha := StrTran(cLinha,'>','')					
					cTeste += cLinha
					cAnexo += FT_FREADLN()
					FT_FSKIP()
               	EndDo
               	FT_FUSE()
                    oXml := 	XmlParser(cAnexo,"_",@cError, @cWarning )
                    if 'evCancCTe' $ cAnexo 
                         aadd(aRet,{.F.,cArqXML, 'CTE CANCELADO'}) 
                    elseif !('cteProc'  $ cAnexo) 
						 aadd(aRet,{.F.,cArqXML, 'XML SEM ESTRUTURA DE CTE'}) 
					else 
                        if ImpXML_CTe(cArqXML,.F.,@aProc,@aErros,oXML:_CTeProc:_Cte,@aErroErp,oXML:_CTeProc:_ProtCte,@lSF1)
                            aadd(aRet,{.T.,cArqXML, 'Arquivo importado com sucesso !'}) 
                        else 
                            aadd(aRet,{.f.,aErros[len(aErros)][1],aErros[len(aErros)][2]}) 
                        endif
                    endif
                    conout('cTeste')
			    EndIf
		    EndIf
		IncProc()
		DbSelectArea('TMP')
		DbSkip()

		
		//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
		//³ JA IMPORTOU TODOS OS ARQUIVOS MARCADOS...        ³
		//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
		If nImportados == nMarcados
			Exit
		EndIf

	EndDo
    Mostralog(aRet)
	TMP->(DbGoTop())
return


static function tescond() 
Local aRet := {}
Local cGet1 := space(3) 
Local cGet2 := space(3)
  DEFINE MSDIALOG oDlg TITLE "Informe a tes e a condição de pagamento " FROM 000, 000  TO 350, 350 COLORS 0, 16777215 PIXEL
    @ 125, 080 BUTTON oButton1 PROMPT "Confirma" SIZE 037, 012 OF oDlg ACTION (oDlg:end(),nOpc:=1) PIXEL
    @ 045, 055 MSGET oGet1 VAR cGet1 SIZE 030, 010 OF oDlg COLORS 0, 16777215 F3 "SF4" PIXEL
    @ 060, 055 MSGET oGet2 VAR cGet2 SIZE 030, 010 OF oDlg COLORS 0, 16777215 F3 "SE4"PIXEL
    @ 045, 015 SAY oSay1 PROMPT "Tes" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 060, 015 SAY oSay2 PROMPT "Condicao de pagamento " SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
  ACTIVATE MSDIALOG oDlg CENTERED

	aadd(aRet,cGet1)
	aadd(aRet,cGet2)
  return aRet



static Function ImpXML_Cte(cFile,lJob,aProc,aErros,oXml,aErroErp,oXMLCanc , lSF1)
Local cPrdFrete  := ""
Local lRet       := .T.
Local lRemet 	 := .F.
Local lToma3Dest := .F.
Local lCliente	 := .F.
Local lDevSemSF1 := .F.
Local lToma4	 := .F.
Local nX		 := 0
Local nPesoBruto := 0
Local nPesoLiqui := 0    
Local nY		 := 0
Local nCount	 := 0
Local nPos			:= 0
Local cError     := ""
Local cCNPJ_CT	 := ""
Local cCNPJ_CF	 := ""
Local cFornCTe   := ""
Local cLojaCTe   := ""
Local cNomeCTe   := ""
Local cCodiRem   := ""
Local cLojaRem   := ""
Local cChaveNF   := ""
Local cTES_CT 	 := ""
Local cCPag_CT 	 := ""
Local cTipoFrete := ""
Local cTagDest	 := ""
local cTagRem	 := ""
Local cTagExped	 := ""
Local cIEExped	 := ""
Local cQuery	 := ""
Local cTabRem	 := ""
Local cRetPrd	 := ""
Local cMotivo		:= ""
Local aDadosAux  := {}
Local aDadosFor  := Array(2) //-- 1- Codigo; 2-Loja
Local aDadosCli  := Array(2) //-- 1- Codigo; 2-Loja
Local aCabec116	 := {}
Local aItens116	 := {}
Local aAux		 := {}
Local aEspecVol  := {}
Local aAux1		 := {}
Local aAuxCliFor	 := {}
Local aParamPE	 := {}
Local aChave	:= {}
Local lMt116XmlCt:= ExistBlock("MT116XMLCT")
Local lA116IFor	 := ExistBlock("A116IFOR")
Local lA116ITPCT := ExistBlock("A116ITPCT")
Local lA116ICHV	:= ExistBlock("A116ICHV")
Local lA116iComp	:= ExistBlock("A116ICOMP")
Local cVersao	:= oXML:_InfCte:_Versao:Text
Local nTamFil	:= 0
Local lComp	:= .F. // Indica se CTe é do tipo complementar
Local cTpCte := ""
Local lFornExt	:= .F.
Local lDevBen	:= .F. // Devolução ou Beneficiamento
Local cDoc		:= ""
Local cSerie	:= ""
Local aA116IDOC	:= {}
Local lCliForDup	:= .F.
Local _errinho :=' '
Local cChvNFOr	:= ""
Local nDevOri	:= 0
Local nNorOri	:= 0
Local lFornOri := .T.
Local aFornChv	:= {}
Local nPFornChv	:= 0
Local aExcStat 	:= {"100","102","103","104","105","106","107","108","109","110","111","112","113","114","134","135","136","301"}
Local cHrEmis		:= ""
Local lVlrFrtZero	:= .F.
Local nVlrFrt		:= 0
Local cCFBN		:= SuperGetMv("MV_XMLCFBN",.F.,"")
Local lRateiaDev	:= SuperGetMv("MV_XMLRATD",.F.,.T.)
Local cUFOrigem		:= ""
Local cMunOrigem	:= ""
Local cUFDestino	:= ""
Local cMunDestin	:= ""

//lPreNota = .T. (Não Classificado), lPreNota = .F. (Classificado)
//MV_CTECLAS = .F. (Não Classificado), MV_CTECLAS = .T. (Classificado)
Local lPreNota	:= .F.

// Variaveis para apuracao do ICMS
Local oICMS
Local oICMSTipo
Local oICMSNode
Local nZ		:= 0
Local nBaseICMS := 0
Local nValICMS	:= 0
Local nAliqICMS	:= 0
Local lBaseICMS := .F.
Local lValICMS	:= .F.
Local lAliqICMS	:= .F.
Local lT4DifDest:= .F.
Local lT3Exped	:= .F.
Local lT3Remet	:= .F.
Local lDSNotF1	:= .F.
Local lUnidMed	:= SDT->(ColumnPos("DT_UM")) > 0 .And. SDT->(ColumnPos("DT_SEGUM")) > 0 .And. SDT->(ColumnPos("DT_QTSEGUM")) > 0

Private lImpXML	  := SuperGetMv("MV_IMPXML",.F.,.F.) .And. CKO->(FieldPos("CKO_ARQXML")) > 0 .And. !Empty(CKO->(IndexKey(5)))

Default lJob    := .T.
Default aProc   := {}
Default aErros  := {} 
Default aErroERP := {} 
lSF1 :=  .F.

//-- Verifica se o arquivo pertence a filial corrente
lRet := CTe_VldEmp(oXML,SM0->M0_CGC,@lToma3Dest,@lToma4,lJob,cFile,,,@lT4DifDest,@lT3Exped,@lT3Remet,@aErros,@aErroERP)

//Verifica se XML é Cancelado ou Rejeitado.
If lRet .And. ValType(oXMLCanc) <> "U"
	If Valtype(XmlChildEx(oXMLCanc:_infProt,"_CSTAT")) <> "U"
		//Chave CT-e
		cChaveCte	:= Right(AllTrim(oXML:_InfCte:_Id:Text),44)
		
		//Motivo rejeição
		If Valtype(XmlChildEx(oXMLCanc:_infProt,"_XMOTIVO")) <> "U"
			cMotivo := ConvASC(oXMLCanc:_infProt:_xMotivo:Text)  
		Endif
		
		//Busca status
		nPos := aScan(aExcStat,AllTrim(oXMLCanc:_infProt:_cStat:Text))
		
		//Status de cancelado ou rejeitado
		If nPos == 0
			If AllTrim(oXMLCanc:_infProt:_cStat:Text) == "101" //Cancelado
				
				
				aAdd(aErros,{cFile,"COM036 - CT-e cancelado: " + cChaveCte,""})
				aAdd(aErroErp,{cFile,"COM036"})
				
				lRet := .F.
			Else //Rejeitado
				
				
				aAdd(aErros,{cFile,"COM037 - CT-e rejeitado: " + cChaveCte + " - Motivo: " + cMotivo,""})
				aAdd(aErroErp,{cFile,"COM037"})
				lRet := .F.
			Endif	
		Endif
	Endif
	
	If Valtype(XmlChildEx(oXMLCanc:_infProt,"_DHRECBTO")) <> "U"
		cHrEmis := Substr(oXMLCanc:_infProt:_DhRecbto:Text,12)
	Endif
Endif
                                      
//-- Verifica se o ID ja foi processado
If lRet
	SDS->(dbSetOrder(2))
	If lRet .And. SDS->(dbSeek(xFilial("SDS")+Right(AllTrim(oXML:_InfCte:_Id:Text),44)))
	
			aAdd(aErros,{cFile,"COM019 - " + STR0003 +SDS->(DS_DOC+"/"+SerieNfId('SDS',2,'DS_SERIE')); //"ID de CT-e já registrado na NF "
							 + STR0004 + " (" +SDS->(DS_FORNEC +"/" +DS_LOJA)+ ").",STR0005}) //"do Fornecedor/Cliente "#"Exclua o documento registrado na ocorrência."
		
		
		aAdd(aErroErp,{cFile,"COM019"})
		lRet := .F.
	EndIf
EndIf

//-- Verifica se CTe é do tipo complementar
If lRet .And. Valtype(XmlChildEx(oXml:_InfCte,"_INFCTECOMP")) != "U"
	lComp := .T.
EndIf

//-- Verifica se o fornecedor do conhecimento esta cadastrado no sistema.
If lRet
	If ValType(XmlChildEx(oXML:_InfCte:_Emit,"_CNPJ")) <> "U"
		cCNPJ_CT := AllTrim(oXML:_InfCte:_Emit:_CNPJ:Text)
	Else
		cCNPJ_CT := AllTrim(oXML:_InfCte:_Emit:_CPF:Text)
	EndIf
	
	// Envio das letras E, D ou R pela funcao A116ICLIFOR. E = Emitente, D = Destinatario e R = Remetente.
	// Essa informacao sera utilizada no P.E. A116IFOR.
	aDadosAux := A116ICLIFOR(cCNPJ_CT,AllTrim(oXML:_InfCte:_Emit:_IE:Text),"SA2",,oXML,'E')
	
	If Len(aDadosAux) == 0
		
			aAdd(aErros,{cFile,"COM007 - " + STR0006 + oXML:_InfCte:_Emit:_Xnome:Text +" [" + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") +"] "+ STR0007,STR0008}) //"Fornecedor"#"inexistente na base."#"Gere cadastro para este fornecedor."
				
		aAdd(aErroErp,{cFile,"COM007"})			
		lRet := .F.
	ElseIf !aDadosAux[1,1] .And. aDadosAux[1,5] // Se encontrou o cadastro e o mesmo esta bloqueado.
		
			aAdd(aErros,{cFile,"COM030 - " + STR0006 + oXML:_InfCte:_Emit:_Xnome:Text +" [" + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") +"] "+ STR0037,STR0038}) //"Fornecedor"#"bloqueado."#"Realize o desbloqueio"
			
		aAdd(aErroErp,{cFile,"COM030"})			
		lRet := .F.
	Elseif !aDadosAux[1,1]
		
			aAdd(aErros,{cFile,"COM028 - " + STR0032 + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") + STR0033, STR0034})
		
		aAdd(aErroErp,{cFile,"COM028"})
		lRet := .F.
	Else
		cFornCTe := aDadosAux[1,2]
		cLojaCTe := aDadosAux[1,3]
		cNomeCTe := Posicione("SA2",1,xFilial("SA2") + cFornCTe + cLojaCTe,"A2_NOME")
	Endif
EndIf

//Identifica se a empresa foi remetente das notas fiscais contidas no conhecimento:

//Se sim, significa que as notas contidas no conhecimento sao notas de saida, podendo ser
//notas de venda, devolucao de compras ou devolucao de remessa para beneficiamento.

//Se nao, significa que as notas contidas no conhecimento sao notas de entrada, podendo ser
//notas de compra, devolucao de vendas ou remessa para beneficiamento.
If lRet
	aDadosAux := {}
	cTagRem := If(ValType(XmlChildEx(oXML:_InfCte,"_REM")) == "O",If(ValType(XmlChildEx(oXML:_InfCte:_Rem,"_CNPJ")) == "O","_CNPJ","_CPF"),"")
	If ( lRemet := (SM0->M0_CGC == If(!Empty(cTagRem),(AllTrim(XmlChildEx(oXML:_InfCte:_Rem,cTagRem):Text)),"")) .And. !lToma3Dest .And. lT3Remet )
		cTagDest := If(ValType(XmlChildEx(oXML:_InfCte:_Dest,"_CNPJ")) == "O","_CNPJ","_CPF")
		cIEDestT4:= If(ValType(XmlChildEx(oXML:_InfCte:_Dest,"_IE")) == "O",AllTrim(oXML:_InfCte:_Dest:_IE:Text),"")
		cCNPJ_CF := AllTrim(XmlChildEx(oXML:_InfCte:_Dest,cTagDest):Text) //-- Armazena o CNPJ do destinatario das notas contidas no conhecimento
		cTipoFrete := "F"
		
		If !Empty(cCNPJ_CF) 
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA2",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA1",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		EndIf
		
		// Verifica a TAG <TpCte> para analisar se a nota eh complementar.
		cTpCte := If(ValType(XmlChildEx(oXML:_InfCte,"_IDE")) == "O",AllTrim(oXML:_InfCte:_Ide:_tpCTe:Text),"") //-- Armazena o tipo do CT-e.
		
		// Opcoes para <TpCte>:
		// 0 - CT-e Normal;
		// 1 - CT-e de Complemento de Valores;
		// 2 - CT-e de Anulação de Valores;
		// 3 - CT-e Substituto.
		
		If cTpCte == '1' // CT-e de Complemento de Valores
			lRemet := .F.
		EndIf
		
		cTagMsg := '_Dest'
	ElseIf lToma4
		If ValType(XmlChildEx(oXML:_InfCte,"_REM")) == "O"
			cCNPJ_CF	:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,cTagRem):Text)
			
			If ValType(XmlChildEx(oXML:_InfCte:_Rem,"_IE")) == "O"
				cIEDestT4		:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,"_IE"):Text)
			Else
				cIEDestT4		:= ""
			Endif
		Else
			cCNPJ_CF	:= ""
			cIEDestT4	:= ""
		Endif
		
		cTipoFrete := "F"
		
		If !Empty(cCNPJ_CF)
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA2",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA1",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		EndIf
		
		cTagMsg := '_Rem'
		
	Elseif lT3Exped
		If ValType(XmlChildEx(oXML:_InfCte,"_EXPED")) == "O"
			cTagExped := If(ValType(XmlChildEx(oXML:_InfCte:_Exped,"_CNPJ")) == "O","_CNPJ","_CPF")
			cCNPJ_CF	:= AllTrim(XmlChildEx(oXML:_InfCte:_Exped,cTagExped):Text)
			
			If ValType(XmlChildEx(oXML:_InfCte:_Exped,"_IE")) == "O"
				cIEExped		:= AllTrim(XmlChildEx(oXML:_InfCte:_Exped,"_IE"):Text)
			Else
				cIEExped		:= ""
			Endif
		Else
			cCNPJ_CF	:= ""
			cIEExped	:= ""
		Endif
		
		cTipoFrete := "C"
		
		If !Empty(cCNPJ_CF)
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEExped,"SA2",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEExped,"SA1",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		Endif
		
		cTagMsg := '_Exped'
	Else
		If ValType(XmlChildEx(oXML:_InfCte,"_REM")) == "O"
			cCNPJ_CF	:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,cTagRem):Text)
			
			If ValType(XmlChildEx(oXML:_InfCte:_Rem,"_IE")) == "O"
				cIERem		:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,"_IE"):Text)
			Else
				cIERem		:= ""
			Endif
		Else
			cCNPJ_CF	:= ""
			cIERem		:= ""
		Endif
		
		If lToma3Dest
			cTipoFrete := "F"
		Else
			cTipoFrete := "C"
		EndIf
		
		If !Empty(cCNPJ_CF)
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIERem,"SA2",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIERem,"SA1",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		Endif
		
		cTagMsg := '_Rem'
	Endif
	
	// Verifica a TAG <TpCte> para analisar se a nota eh de Anulacao de Valores.
	If cTpCte <> '1'
		cTpCte := If(ValType(XmlChildEx(oXML:_InfCte,"_IDE")) == "O",AllTrim(oXML:_InfCte:_Ide:_tpCTe:Text),"") //-- Armazena o tipo do CT-e.
	EndIf
		
	If lComp
		aAux1 := If(ValType(oXML:_InfCte:_InfCTeComp) == "O",{oXML:_InfCte:_InfCTeComp},oXML:_InfCte:_InfCTeComp)
	ElseIf cTpCte == '2'
		If ValType(XmlChildEx(oXML:_InfCte:_infCteAnu,"_INFDOC")) != "U" .And. ValType(XmlChildEx(oXML:_InfCte:_infCteAnu:_InfDoc,"_INFNF")) != "U"
			aAux := If(ValType(oXML:_InfCte:_infCteAnu:_InfDoc:_INFNF) == "O",{oXML:_InfCte:_infCteAnu:_InfDoc:_INFNF},oXML:_InfCte:_infCteAnu:_InfDoc:_INFNF)
		EndIf
	ElseIf cVersao >= "2.00"
		If Valtype(XmlChildEx(oXml:_InfCte,"_INFCTENORM")) != "U"
			If ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm,"_INFDOC")) != "U" .And. ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm:_InfDoc,"_INFNF")) != "U"
				aAux := If(ValType(oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNF) == "O",{oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNF},oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNF)
			ElseIf ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm,"_INFDOC")) != "U" .And. ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm:_InfDoc,"_INFNFE")) != "U"
				aAux1 := If(ValType(oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNFE) == "O",{oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNFE},oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNFE)
			EndIf
		EndIf
	Else
		If ValType(XmlChildEx(oXML:_InfCte:_Rem,"_INFNF")) != "U"
			aAux := If(ValType(oXML:_InfCte:_Rem:_INFNF) == "O",{oXML:_InfCte:_Rem:_INFNF},oXML:_InfCte:_Rem:_INFNF)
		ElseIf ValType(XmlChildEx(oXML:_InfCte:_Rem,"_INFNFE")) != "U"
			aAux1 := If(ValType(oXML:_InfCte:_Rem:_INFNFE) == "O",{oXML:_InfCte:_Rem:_INFNFE},oXML:_InfCte:_Rem:_INFNFE)
		EndIf
	EndIf
	
	If Len(aAux) > 0 .Or. (Len(aAux) == 0 .And. Len(aAux1) == 0)
		If Len(aAux) > 0
			cCodiRem 		:= CriaVar("A2_COD",.F.)
			cLojaRem 		:= CriaVar("A2_LOJA",.F.)
			aDadosFor[1]	:= CriaVar("A2_COD",.F.)
			aDadosFor[2]	:= CriaVar("A2_LOJA",.F.)
			aDadosCli[1]	:= CriaVar("A1_COD",.F.)
			aDadosCli[2]	:= CriaVar("A1_LOJA",.F.)
		Endif
		
		If lRet //Validou fornecedor ou cliente
			
			SF2->(dbClearFilter())
			SF2->(dbSetOrder(1))
			
			SF1->(dbClearFilter())
			SF1->(dbSetOrder(1))
			
			SDS->(dbClearFilter())
			SDS->(dbSetOrder(1))
			
			For nX := 1 To Len(aAux)
				lDSNotF1 := .F.
				//Verifica CFOP para saber se documento origem é Beneficamento
				lNFOriBen	:= aAux[nX]:_nCFOP:Text $ cCFBN
				
				cChaveNF :=	PadL(AllTrim(aAux[nX]:_nDoc:Text),TamSX3("F1_DOC")[1], "0") +;
								PadR(AllTrim(aAux[nX]:_Serie:Text),TamSX3("F1_SERIE")[1])
					
				//Ponto de entrada para manipulaça da chave(nº do doc + serie) do doc de origem
				If lA116ICHV
					aChave := ExecBlock("A116ICHV",.F.,.F.,{AllTrim(aAux[nX]:_nDoc:Text),AllTrim(aAux[nX]:_Serie:Text)})
					If ValType(aChave) == "A" .And. Len(aChave) >= 2
						cChaveNF :=	Padr(AllTrim(aChave[1]),TamSX3("F1_DOC")[1],) +;
										Padr(AllTrim(aChave[2]),TamSX3("F1_SERIE")[1])   
					EndIf
				EndIf
					
				If lNFOriBen
					//Busca documento de origem (beneficiamento)
					aDadosCli := A116IFOCL(aDadosAux,"C")
					
					If aDadosCli[1] .And. !Empty(aDadosCli[2])
						If SF1->(dbSeek(xFilial("SF1")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
							cCodiRem := aDadosCli[2]
							cLojaRem := aDadosCli[3]
						Else
							If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
								lDSNotF1 := .T.
							Endif
							lRet := .F.
						EndIf
					Else
						lRet := .F.
					Endif
				Else
					//Não sabe o tipo do documento, busca pelo fornecedor e/ou cliente
					aDadosFor := A116IFOCL(aDadosAux,"F")
					
					If aDadosFor[1] .And. !Empty(aDadosFor[2])
						If SF1->(dbSeek(xFilial("SF1")+cChaveNF+aDadosFor[2]+aDadosFor[3]))
							cCodiRem := aDadosFor[2]
							cLojaRem := aDadosFor[3]
						Else
							If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosFor[2]+aDadosFor[3]))
								lDSNotF1 := .T.
							Endif
							lRet := .F.
						EndIf
					Else
						lRet := .F.
					Endif
					
					If !lRet
						aDadosCli := A116IFOCL(aDadosAux,"C")
					
						If aDadosCli[1] .And. !Empty(aDadosCli[2])
							If SF1->(dbSeek(xFilial("SF1")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
								cCodiRem := aDadosCli[2]
								cLojaRem := aDadosCli[3]
								lRet := .T.
							Else
								If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
									lDSNotF1 := .T.
								Endif
								lRet := .F.
							EndIf
						Else
							lRet := .F.
						Endif
					Endif
				Endif
				
				If !lRet
					// Tratamento para CTe de devolucao quando o cliente nao aceitou receber a mercadoria
					// Neste caso a transportadora emite um novo CTe referenciando as notas de venda, as notas estarao em SF2 e nao SF1
					cChaveNF :=	Padr(AllTrim(aAux[nX]:_nDoc:Text),TamSX3("F2_DOC")[1]) +;
									Padr(AllTrim(aAux[nX]:_Serie:Text),TamSX3("F2_SERIE")[1])
									
					aDadosCli := A116IFOCL(aDadosAux,"C") 
					
					If aDadosCli[1] .And. !Empty(aDadosCli[2])
						If SF2->(dbSeek(xFilial("SF2")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
							cCodiRem := aDadosCli[2]
							cLojaRem := aDadosCli[3]
						
							aErros	:= {}
							cTipoFrete := "F"
							lDevSemSF1 := .T.
						Else
							If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
								lDSNotF1 := .T.
							Endif
						EndIf
					Else
						lRet := .F.
					Endif
				Endif
				
				//-- Registra notas que farao parte do conhecimento
				If lRet
					nTamFil := Len(xFilial("SF1"))
					cChvNFOr := SF1->F1_CHVNFE
					aAdd(aItens116,{{"PRIMARYKEY",SubStr(SF1->&(IndexKey()),nTamFil+1), cCodiRem, cLojaRem}})
					
					lDevBen	:= SF1->F1_TIPO $ "D*B"
					
					If lDevBen
						nDevOri++
					Else
						nNorOri++
					Endif
					
					If nDevOri > 0 .And. nNorOri > 0
						lFornOri := .F.
					Endif
				Else
					If lT3Exped .Or. lT4DifDest .Or. lT3Remet .Or. lDevSemSF1
						lRet := .T.
					Else
						If lDSNotF1
							
								aAdd(aErros,{cFile,"COM044 - " + STR0011 + cChaveNF + 'STR0049','STR0050'}) //"Documento de entrada existente no monitor. Processe o recebimento deste documento de entrada para importar o CTE corretamente"
							
							aAdd(aErroErp,{cFile,"COM044"})							
						Else
							
								aAdd(aErros,{cFile,"COM020 - " + STR0011 + cChaveNF + STR0007,STR0012}) //"Documento de entrada inexistente na base. Processe o recebimento deste documento de entrada."
							
							aAdd(aErroErp,{cFile,"COM020"})
						Endif
						Exit
					Endif
				EndIf
			Next nX
		Endif
	Endif
	
	If lRet .And. Len(aAux1) > 0
		For nX := 1 To Len(aAux1)
			lDSNotF1 := .F.
			SDS->(dbSetOrder(2))
			
			SF1->(dbSetOrder(8))
			
			If ValType(XmlChildEx(aAux1[nX],"_CHAVE")) == "O"
				cChaveNF := Padr(AllTrim(aAux1[nX]:_chave:Text),TamSX3("F1_CHVNFE")[1])
			ElseIf ValType(XmlChildEx(aAux1[nX],"_CHCTE")) == "O"
				cChaveNF := Padr(AllTrim(aAux1[nX]:_chCTE:Text),TamSX3("F1_CHVNFE")[1])
               
			EndIf
            if ValType(XmlChildEx(aAux1[nX],"_CHCTE")) == "O"
                cchavecte:=Padr(AllTrim(aAux1[nX]:_chCTE:Text),TamSX3("F1_CHVNFE")[1])
            endif
				cChaveXML := cChaveNF
			//Verifica existência da nota
			If SF1->(dbSeek(xFilial("SF1")+cChaveNF)) .And. !Empty(cChaveNF) //Se nota existir, preenche informações do Remetente com dados da nota
				cCodiRem 	:= SF1->F1_FORNECE
				cLojaRem 	:= SF1->F1_LOJA
				lDevBen	:= SF1->F1_TIPO $ "D*B"
				
				If lDevBen
					nDevOri++
				Else
					nNorOri++
				Endif
				
				//Se as chaves nfe pertencem a mais de 1 fornecedor, ao incluir o documento de frente
				//não deve ser filtrado pelo fornecedor.
				nPFornChv := aScan(aFornChv,{|x| x == cCodiRem+cLojaRem})
				If nPFornChv == 0
					aAdd(aFornChv,cCodiRem+cLojaRem)
				Endif
				
				If (nDevOri > 0 .And. nNorOri > 0) .Or. Len(aFornChv) > 1
					lFornOri := .F.
				Endif
				
				//-- Registra notas que farao parte do conhecimento
				SF1->(dbSetOrder(1))
				nTamFil := Len(xFilial("SF1"))
				cChvNFOr := SF1->F1_CHVNFE
				aAdd(aItens116,{{"PRIMARYKEY",SubStr(SF1->&(IndexKey()),nTamFil+1), cCodiRem, cLojaRem}})
			Else
				If SDS->(dbSeek(xFilial("SDS")+cChaveNF))
					lDSNotF1 := .T.
					lRet	 := .F.
					
						aAdd(aErros,{cFile,"COM044 - " + STR0011 + cChaveNF + " existente no monitor.","Processe o recebimento deste documento de entrada para importar o CTE corretamente"}) //"Documento de entrada existente no monitor. Processe o recebimento deste documento de entrada para importar o CTE corretamente"
					
					aAdd(aErroErp,{cFile,"COM044"})
					Exit
				Endif
				
				SF2->(dbClearFilter())
				SF2->(dbSetOrder(1))
				
				cQuery := " SELECT R_E_C_N_O_ AS RECNO "
				cQuery += " FROM " + RetSqlName("SF2") + " SF2 "
				cQuery += " WHERE D_E_L_E_T_ = ' ' AND"
				cQuery += " F2_CHVNFE = '" + cChaveNF + "' AND"
				cQuery += " F2_FILIAL = '" + xFilial("SF2") + "'" 	
				cQuery := ChangeQuery(cQuery)
				
				if select('TMP59') > 0 
					TMP59->(DbCloseArea())
				endif 

				dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),"TMP59", .T., .T.)

				TMP59->(dbGoTop())
				If TMP59->(!Eof())
					aErros	:= {}
					cTipoFrete := "F"
					lDevSemSF1 := .T.
				else
                    aAdd(aErros,{cFile,"COM099 - " + 'Não foi encontrado nota de entrada nem de saida par vincular','Não foi encontrado nota de entrada nem de saida par vincular'})
                    return .F.
                endif
				TMP59->(dbCloseArea())
			EndIf			
		Next nX
	Endif

	If lDevBen .And. !lRateiaDev
		aItens116 := {}
	EndIf

	If lToma4 .And. Len(aItens116) == 0 
		lToma4NFOri := .F.
	Endif

	If lRet .And. (Len(aAux1) == 0 .And. Len(aAux) == 0) .Or. lDevSemSF1 .Or. Len(aItens116) == 0
		//-- Ponto de entrada para mudar o produto frete
 		If ExistBlock("A116PRDF")
			cPrdFrete := ExecBlock("A116PRDF",.F.,.F.,{oXML})
			If ValType(cPrdFrete) <> "C" .Or. !SB1->(dbSeek(xFilial("SB1")+cPrdFrete))
				cPrdFrete := SuperGetMV("MV_XMLPFCT",.F.,"")
				If At(";",cPrdFrete) > 0
					cPrdFrete := SubStr(cPrdFrete,1,(At(";",cPrdFrete)-1))
				EndIf
				cPrdFrete := PadR(cPrdFrete,TamSX3("B1_COD")[1])
			EndIf
		Elseif lA116ITPCT
			aParamPE := If(Len(aAux) > 0,aAux,aAux1)
			cRetPrd := ExecBlock("A116ITPCT",.F.,.F.,{aParamPE, oXML})
			If ValType(cRetPrd) == "C" .And. SB1->(dbSeek(xFilial("SB1")+cRetPrd))
				cPrdFrete := PadR(cRetPrd,TamSX3("B1_COD")[1])
				cTipoFrete := "F"
				lRemet := .T.
			Else
				cPrdFrete := SuperGetMV("MV_XMLPFCT",.F.,"")
				If At(";",cPrdFrete) > 0
					cPrdFrete := SubStr(cPrdFrete,1,(At(";",cPrdFrete)-1))
				EndIf
				cPrdFrete := PadR(cPrdFrete,TamSX3("B1_COD")[1])	
			EndIf
		Else
			cPrdFrete := SuperGetMV("MV_XMLPFCT",.F.,"")
			If At(";",cPrdFrete) > 0
				cPrdFrete := SubStr(cPrdFrete,1,(At(";",cPrdFrete)-1))
			EndIf
			cPrdFrete := PadR(cPrdFrete,TamSX3("B1_COD")[1])
		EndIf
	
		If Empty(cPrdFrete) .Or. !SB1->(dbSeek(xFilial("SB1")+cPrdFrete))
			
				aAdd(aErros,{cFile,"COM023 - " + STR0017,STR0018}) //"Produto frete não informado no parâmetro MV_XMLPFCT ou inexistente no cadastro correspondente."#"Verifique a configuração do parâmetro."
			
			aAdd(aErroErp,{cFile,"COM023"})
			lRet := .F.
		Else
			cTipoFrete := "F"
			lRemet := .T.
		EndIf
	Endif
	
	//-- Obtem TES e cond. pagto para utilizacao no CT-e

	SF4->(DbSetOrder(1))
    SF4->(DbSeek(xFilial('SF4')+cTes))
	//-- Valida se o TES esta desbloqueado.
	If lRet
		If SF4->F4_MSBLQL == '1' .And. !lPreNota // TES bloqueado.
	 		
				aAdd(aErros,{cFile,"COM029 - " + STR0035 + SF4->F4_CODIGO,STR0036}) //"TES bloqueado. Codigo: "#"Verifique a configuração do cadastro."
		
			aAdd(aErroErp,{cFile,"COM029"})
			lRet := .F.
		EndIf
	EndIf
	
	//-- Se TES gera dup., valida existencia da cond. pgto a utilizar no CTe
	If lRet .And. SF4->F4_DUPLIC == "S" .And. (Empty(cCond) .Or. !SE4->(dbSeek(xFilial("SE4")+cCond)))
 		
			aAdd(aErros,{cFile,"COM022 - " + STR0015,STR0016}) //"Condição de pagamento não informada no parâmetro MV_XMLCPCT ou inexistente no cadastro correspondente.Verifique a configuração do parâmetro.
		
		lRet := .F.
		aAdd(aErroErp,{cFile,"COM022"})
	EndIf
Endif

// Verifica existência de documento com mesma numeração.
If lRet	
	cDoc	:= PadL(oXML:_InfCte:_Ide:_nCt:Text,TamSx3("F1_DOC")[1],"0")
	cSerie	:= PadR(oXML:_InfCte:_Ide:_Serie:Text,SerieNfId("SF1",6,"F1_SERIE")) 
	
	If ExistBlock("A116IDOC") //Manipula numero e serie do documento
		aA116IDOC := ExecBlock("A116IDOC",.F.,.F.,{cDoc, cSerie, cFornCTe, cLojaCTe})
		If ValType(aA116IDOC) == 'A' .And. Len(aA116IDOC) >= 2
			cDoc	:= aA116IDOC[1]
			cSerie := aA116IDOC[2]
		EndIf
	EndIf
	
	dbSelectArea("SDS")
	dbSetorder(1)
	If msSeek(xFilial("SDS")+cDoc+cSerie+cFornCTe+cLojaCTe)
		
			aAdd(aErros,{cFile,"COM025 - " + STR0026+" "+cDoc+", "+STR0027+" "+cSerie+" , "+STR0028,'STR002'}) //Documento cdoc, serie cSerie, já cadastrado. verifique se o documento já foi importado para o ERP. 
		
		aAdd(aErroErp,{cFile,"COM025"})
		lRet := .F.
	EndIf
EndIf

If lRet
	//-- Separa secao que contem as notas do conhecimento para laco
	If ValType(XmlChildEx(oXML:_InfCte,"_INFCTENORM")) <> "U"
	 	aAux := If(ValType(oXML:_InfCte:_InfCteNorm:_InfCarga:_InfQ) == "O",{oXML:_InfCte:_InfCteNorm:_InfCarga:_InfQ},oXML:_InfCte:_InfCteNorm:_InfCarga:_InfQ)
	EndIf
 	For nX := 1 To Len(aAux)
		If Upper(AllTrim(aAux[nX]:_TPMED:Text)) == "PESO BRUTO"
			nPesoBruto := Val(aAux[nX]:_QCARGA:Text)
		EndIf
		If Upper(AllTrim(aAux[nX]:_TPMED:Text)) == "PESO LíQUIDO"
			nPesoLiqui := Val(aAux[nX]:_QCARGA:Text)
		EndIf
		If !("PESO" $ Upper(aAux[nX]:_TPMED:Text)) .And. Len(aEspecVol) < 5
			aAdd(aEspecVol,{AllTrim(aAux[nX]:_TPMED:Text),Val(aAux[nX]:_QCARGA:Text)})
		EndIf
	Next nX
	
	// Apuracao do ICMS para as diversas situacoes tributarias
	If ValType(XmlChildEx(oXML:_InfCte:_imp,"_ICMS")) <> "U"
		If ( oICMS := oXML:_INFCTE:_IMP:_ICMS ) != Nil
			If ( oICMSTipo := XmlGetChild( oICMS, 1 )) != Nil
				For nZ := 1 To 5	// O nivel maximo para descer dentro da tag que define o tipo do ICMS para obter tanto base quanto valor é 5, conforme manual de orientacao do CTe
					If ( oICMSNode := XmlGetChild( oICMSTipo, nZ )) != Nil
						If "vBC" $ oICMSNode:REALNAME
							nBaseICMS := Val(oICMSNode:TEXT)
							lBaseICMS := .T.
						ElseIf "vICMS" $ oICMSNode:REALNAME
							nValICMS := Val(oICMSNode:TEXT)
							lValICMS := .T.
						ElseIf "pICMS" $ oICMSNode:REALNAME
							nAliqICMS := Val(oICMSNode:TEXT)
							lAliqICMS := .T.
						EndIf
						If lBaseICMS .And. lValICMS .And. lAliqICMS
							Exit
						EndIf
					EndIf
				Next nZ
			EndIf
		EndIf
		
		lRet := !COMXIMPVLD({{nAliqICMS,">=100"}})
		If !lRet
		
				aAdd(aErros,{cFile,"COM043 - " + 'STR0046','STR0047'}) 
		
			aAdd(aErroErp,{cFile,"COM043"})
		Endif
	EndIf
Endif

// Ponto de entrada utilizado para manipular a gravacao dos itens da nota de frete
// Exemplo: Desmembrar o valor do frete entre seus componentes (tags <vPrest><Comp>)
If lRet .And. lA116iComp
	If (lRemet .And. !lToma4) .Or. lDevSemSF1 .Or. (lToma4 .And. !lToma4NFOri)
		aItensComp := ExecBlock("A116ICOMP",.F.,.F.,{oXml})
		If ValType(aItensComp) == "A" .And. Len(aItensComp) > 0
			For nX := 1 To Len(aItensComp)
				If	aScan(aItensComp[nX],{|x| x[1] == "DT_ITEM" })	> 0 .And.;
					aScan(aItensComp[nX],{|x| x[1] == "DT_COD" })	> 0 .And.;
					aScan(aItensComp[nX],{|x| x[1] == "DT_VUNIT" })	> 0 .And.;
					aScan(aItensComp[nX],{|x| x[1] == "DT_TOTAL" })	> 0
					lRet := .T.
				Else
					lRet := .F.
					Exit
				EndIf
			Next nX
		Else
			lRet := .F.
		EndIf
		If !lRet
			aAdd(aErros,{cFile,"COM032 - A116ICOMP - " + STR0039, STR0040}) //"Retorno do ponto de entrada A116ICOMP inconsistente." - "Verifique a documentacao do mesmo no portal TDN."
			aAdd(aErroErp,{cFile,"COM032"})
		EndIf
	EndIf
EndIf

//Valor do frete
If lRet .And. ValType(XmlChildEx(oXML:_InfCte,"_VPREST")) <> "U"
	If ValType(XmlChildEx(oXML:_InfCte:_vPrest,"_VREC")) <> "U" .And. ValType(XmlChildEx(oXML:_InfCte:_vPrest,"_VTPREST")) <> "U"
		If Val(oXML:_InfCte:_VPrest:_VRec:Text) == 0 .And. Val(oXML:_InfCte:_VPrest:_vTPrest:Text) == 0
			
			If cTpCte == "1" //Complemento de imposto, pois não tem valor de prestação e valor recolhido.
				lRet := .F.
				aAdd(aErros,{cFile,"COM047 - Complemento de imposto não é tratado pelo Totvs Colaboração/Importador. Gere o documento manualmente.", STR0040})
				aAdd(aErroErp,{cFile,"COM047"})
			Endif
			
			lVlrFrtZero := .T.
		Elseif Val(oXML:_InfCte:_VPrest:_VRec:Text) == 0 .And. Val(oXML:_InfCte:_VPrest:_vTPrest:Text) > 0
			nVlrFrt := Val(oXML:_InfCte:_VPrest:_vTPrest:Text)
			lVlrFrtZero := .T.
		Else
			nVlrFrt := Val(oXML:_InfCte:_VPrest:_VRec:Text)
		Endif
	Endif
Endif

If lRet
	//Estado de origem do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_UFINI")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_UFIni:Text)
		cUFOrigem := oXML:_InfCte:_Ide:_UFIni:Text		
	Else
		cUFOrigem := Posicione("SA2",1,xFilial("SA2")+cFornCTe+cLojaCTe,"A2_EST")
	Endif
	
	//Codigo Municipio de origem do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_CMUNINI")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_cMunIni:Text)
		cMunOrigem := SubStr(oXML:_InfCte:_Ide:_cMunIni:Text,3,5)
	Endif
	
	//Estado de destino do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_UFFIM")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_UFFim:Text)
		cUFDestino := oXML:_InfCte:_Ide:_UFFim:Text
	Endif
	
	//Codigo Municipio de destino do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_CMUNFIM")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_cMunFim:Text)
		cMunDestin := SubStr(oXML:_InfCte:_Ide:_cMunFim:Text,3,5)
	Endif
Endif

If lRet
	Begin Transaction

	//-- Grava cabeca do conhecimento de transporte
	RecLock("SDS",.T.)
	SDS->DS_FILIAL	:= xFilial("SDS")																					// Filial			
    SDS->DS_CNPJ		:= cCNPJ_CT																						// CGC
    SDS->DS_DOC		:= cDoc																							// Numero do Documento
    SDS->DS_FORNEC	:= cFornCTe																						// Fornecedor do Conhecimento de transporte
    SDS->DS_LOJA		:= cLojaCTe																						// Loja do Fornecedor do Conhecimento de transporte
    SDS->DS_EMISSA	:= StoD(StrTran(AllTrim(oXML:_InfCte:_Ide:_Dhemi:Text),"-",""))							// Data de Emissão
    SDS->DS_EST		:= cUFOrigem																			// Estado de emissao da NF
    SDS->DS_TIPO		:= "T"													 											// Tipo da Nota
    SDS->DS_FORMUL	:= "N" 																							// Formulario proprio
    SDS->DS_ESPECI	:= "CTE"															  								// Especie
    SDS->DS_ARQUIVO	:= AllTrim(cFile)																					// Arquivo importado
    SDS->DS_CHAVENF	:= Right(AllTrim(oXML:_InfCte:_Id:Text),44)													// Chave de Acesso da NF
    SDS->DS_VERSAO	:= AllTrim(oXML:_InfCte:_Versao:Text) 															// Versão
    SDS->DS_USERIMP	:= If(!lJob,cUserName,"TOTVS Colaboração")													// Usuario na importacao
    SDS->DS_DATAIMP	:= dDataBase																						// Data importacao do XML
    SDS->DS_HORAIMP	:= SubStr(Time(),1,5)																			// Hora importacao XML
    SDS->DS_VALMERC	:= nVlrFrt																	  						// Valor Mercadoria
    SDS->DS_TPFRETE	:= cTipoFrete																						// Tipo de Frete
    SDS->DS_PBRUTO	:= nPesoBruto																						// Peso Bruto
    SDS->DS_PLIQUI	:= nPesoLiqui																						// Peso Liquido
    
    For nX := 1 To Len(aEspecVol)
    	If SDS->(ColumnPos("DS_ESPECI" +Str(nX,1))) > 0
		    SDS->&("DS_ESPECI" +Str(nX,1)) := aEspecVol[nX,1]							 							// Especie
			SDS->&("DS_VOLUME" +Str(nX,1)) := aEspecVol[nX,2]							 							// Volume
		EndIf
	Next nX
	
	If SDS->(ColumnPos("DS_BASEICM")) > 0 .And. lBaseICMS
		SDS->DS_BASEICM := nBaseICMS
	EndIf
	
	If SDS->(ColumnPos("DS_VALICM")) > 0 .And. lValICMS
		SDS->DS_VALICM := nValICMS
	EndIf
	
	If SDS->(ColumnPos('DS_TPCTE')) > 0
		SDS->DS_TPCTE := TIPOCTE116(oXML:_InfCte:_Ide:_tpCTe:Text)
	EndIf
	
	//Chave da Nota de Origem
    If SDS->(ColumnPos("DS_CHVNFOR")) > 0
    	SDS->DS_CHVNFOR		:= cChvNFOr
    Endif
    
   	If !Empty(cHrEmis)
		SDS->DS_HORNFE := cHrEmis
	Endif
	
	//Valor a pagar igual a 0
	If lVlrFrtZero
		SDS->DS_FRETE := Iif(nVlrFrt==0,1,nVlrFrt)
	Endif
	
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_MODAL")) <> "U" .And. SDS->(ColumnPos("DS_MODAL")) > 0
		SDS->DS_MODAL := AllTrim(oXML:_InfCte:_Ide:_modal:Text)
	Endif
	
	//Estado/Municipio de Origem e Destino do CTE
	If SDS->(ColumnPos("DS_UFDESTR")) > 0 .And. SDS->(ColumnPos("DS_MUDESTR")) > 0 .And. SDS->(ColumnPos("DS_UFORITR")) > 0 .And. SDS->(ColumnPos("DS_MUORITR")) > 0
		SDS->DS_UFDESTR := cUFDestino
		SDS->DS_MUDESTR := cMunDestin
		SDS->DS_UFORITR := cUFOrigem
		SDS->DS_MUORITR := cMunOrigem
	Endif
	
	SerieNfId("SDS",1,"DS_SERIE",SDS->DS_EMISSA,SDS->DS_ESPECI,cSerie) 
	
	SDS->(MsUnlock())
	
	//Se for remetente mas a notas de origem existem no sistema
	If (lRemet .And. !lToma4) .And. Len(aItens116) > 0
		lRemet := .F.
	Endif
	
	//-- Grava itens do conhecimento de transporte
	If (lRemet .And. !lToma4) .Or. lDevSemSF1 .Or. (lToma4 .And. !lToma4NFOri)
       
		//static Function EXEC103(cNum, dEmissao, cFornece , cLoja, cCond, nValor , cTes , cchavecte)
		if ! EXEC103(cDoc, cSerie,StoD(StrTran(AllTrim(oXML:_InfCte:_Ide:_Dhemi:Text),"-","")), cFornCTe , cLojaCTe, cCond, nVlrFrt, cTes , Right(AllTrim(oXML:_InfCte:_Id:Text),44)	,AllTrim(cFile),@_errinho)
			aAdd(aErros,{cFile,"COM024 - " + _errinho,STR0019})
			return .F.
		endif
		If lA116iComp
			For nX := 1 To Len(aItensComp)
				RecLock("SDT",.T.)
				SDT->DT_FILIAL:= xFilial("SDT")
				SDT->DT_DOC	:= cDoc
				SDT->DT_SERIE	:= cSerie
				SDT->DT_FORNEC:= cFornCTe
				SDT->DT_LOJA	:= cLojaCTe
				SDT->DT_CNPJ	:= SDS->DS_CNPJ
				SDT->DT_QUANT	:= 1
				For nY := 1 To Len(aItensComp[nX])
					&("SDT->"+AllTrim(aItensComp[nX][nY][1])) := aItensComp[nX][nY][2]
				Next nY
				SDT->(MsUnlock())
			Next nX
		Else
			RecLock("SDT",.T.)
			SDT->DT_FILIAL		:= xFilial("SDT")													// Filial
			SDT->DT_ITEM			:= StrZero(1,TamSX3("DT_ITEM")[1])						   		// Item
			SDT->DT_COD			:= cPrdFrete														// Codigo do produto
			SDT->DT_FORNEC		:= cFornCTe														// Forncedor
			SDT->DT_LOJA			:= cLojaCTe														// Loja
			SDT->DT_DOC			:= cDoc															// Docto
			SDT->DT_SERIE			:= cSerie 					   										// Serie
			SDT->DT_CNPJ			:= SDS->DS_CNPJ													// Cnpj do Fornecedor
			SDT->DT_QUANT			:= 1																// Quantidade
			SDT->DT_VUNIT			:= Val(oXML:_InfCte:_VPrest:_VRec:Text)						// Valor Unitário 			
			SDT->DT_TOTAL			:= Val(oXML:_InfCte:_VPrest:_VRec:Text)				   		// Vlor Total 			 	

			If SDT->(FieldPos("DT_PICM")) > 0 .And. lAliqICMS
				SDT->DT_PICM := nAliqICMS
			EndIf
			
			If lUnidMed
				SDT->DT_UM		:= GetAdvFVal("SB1","B1_UM",xFilial("SB1")+cPrdFrete,1)
				SDT->DT_SEGUM	:= GetAdvFVal("SB1","B1_SEGUM",xFilial("SB1")+cPrdFrete,1)
				
				If !Empty(SDT->DT_SEGUM)
					SDT->DT_QTSEGUM := ConvUM(cPrdFrete,1,1,2)
				Endif
			Endif
			SDT->(MsUnlock())
		EndIf
		If lMt116XmlCt
			ExecBlock("Mt116XmlCt",.F.,.F.,{oXML,cDoc,cSerie,cFornCTe,cLojaCTe,"T","PF"})
		EndIf
	
	Else

	
	
		aAdd(aCabec116,{"",dDataBase-90})       												// Data inicial para filtro das notas
		aAdd(aCabec116,{"",dDataBase})          												// Data final para filtro das notas
		aAdd(aCabec116,{"",2})                  												// 2-Inclusao ; 1=Exclusao
		aAdd(aCabec116,{"",Space(TamSx3("F1_FORNECE")[1])}) 									// Rementente das notas contidas no conhecimento
		aAdd(aCabec116,{"",Space(TamSx3("F1_LOJA")[1])})
		aAdd(aCabec116,{"",IIf(lDevBen,2,1)})                  												// Tipo das notas contidas no conhecimento: 1=Normal ; 2=Devol/Benef
		aAdd(aCabec116,{"",2})                  												// 1=Aglutina itens ; 2=Nao aglutina itens
		aAdd(aCabec116,{"F1_EST",""})  		  													// UF das notas contidas no conhecimento
		aAdd(aCabec116,{"",nVlrFrt}) 															// Valor do conhecimento
		aAdd(aCabec116,{"F1_FORMUL",1})															// Formulario proprio: 1=Nao ; 2=Sim
		aAdd(aCabec116,{"F1_DOC",strzero(val(oXML:_InfCte:_Ide:_nCt:Text),TamSx3("F1_DOC")[1])})		// Numero da nota de conhecimento
		aAdd(aCabec116,{"F1_SERIE",PadR(oXML:_InfCte:_Ide:_Serie:Text,SerieNfId("SF1",6,"F1_SERIE"))})	// Serie da nota de conhecimento
		aAdd(aCabec116,{"F1_FORNECE",cFornCTe}) 												// Fornecedor da nota de conhecimento
		aAdd(aCabec116,{"F1_LOJA",cLojaCTe})													// Loja do fornecedor da nota de conhecimento
		aAdd(aCabec116,{"",cTes})															// TES a ser utilizada nos itens do conhecimento
		aAdd(aCabec116,{"F1_BASERET",nBaseICMS})												// Valor da base de calculo do ICMS retido
		aAdd(aCabec116,{"F1_ICMRET",nValICMS})													// Valor do ICMS retido
		aAdd(aCabec116,{"F1_COND",cCond})											   		// Condicao de pagamento
		aAdd(aCabec116,{"F1_EMISSAO",SToD(Substr(StrTran(oXML:_InfCte:_Ide:_dhEmi:Text,"-",""),1,8))}) // Data de emissao do conhecimento
		aAdd(aCabec116,{"F1_ESPECIE","CTE"})															 // Especie do documento
        aAdd(aCabec116,{"F1_CHVNFE",cchavecte})
		aadd(aCabec116,{"E2_NATUREZ",""})  	
		
		If SDT->(FieldPos("DT_PICM")) > 0 .And. lAliqICMS
			aAdd(aCabec116,{"DT_PICM",nAliqICMS})
		EndIf												   				 // Chave para tratamentos especificos
		
		If SDS->(ColumnPos('DS_TPCTE')) > 0
			aAdd(aCabec116,{"F1_TPCTE",TIPOCTE116(oXML:_InfCte:_Ide:_tpCTe:Text)})				// Tipo do CT-e.
		EndIf
		
		//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
		//³ Executa a ExecAuto do MATA116 para gravar os itens com o valor de frete rateado ³
		//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
		lMsErroAuto    := .F.
		lAutoErrNoFile := .T.
		MsExecAuto({|x,y,z| MATA116(x,y,,z)},aCabec116,aItens116,lPreNota)
		
		If lMsErroAuto
			//-- Desfaz transacao
			lSF1 := .T.
			DisarmTran()
	 			aAux := GetAutoGRLog()
				 
				 
				if len(aAux) == 1
					cError := aAux[1]
				else
					For nX := 1 To Len(aAux)
						cError += iif('Invalid' $ aAux[nX],aAux[nX],'')
					Next nX
				endif
		 		aAdd(aErros,{cFile,"COM024 - " + cError,STR0019}) //"Corrija a inconsistência apontada no log."
			aAdd(aErroErp,{cFile,"COM024"})
			lRet := .F.
		Else
			If lMt116XmlCt
				ExecBlock("Mt116XmlCt",.F.,.F.,{oXML,cDoc,cSerie,cFornCTe,cLojaCTe,"T","PN"})
			EndIf
		EndIf
	EndIf
	
	End Transaction
EndIf

If lRet
	aAdd(aProc,{	PadL(oXML:_InfCte:_Ide:_nCt:Text,TamSx3("F1_DOC")[1],"0"),;
					PadR(oXML:_InfCte:_Ide:_Serie:Text,SerieNfId("SF1",6,"F1_SERIE")),;
					cNomeCTe})
Endif

oXML := Nil
DelClassIntf()

DisarmTran()
Return lRet

static Function Mostralog(aArray)

Local oOK := LoadBitmap(GetResources(),'br_verde')
Local oNO := LoadBitmap(GetResources(),'br_vermelho')  
DEFINE DIALOG oDlg TITLE "Exemplo TWBrowse" FROM 180,180 TO 550,1200 PIXEL	    

oBrowse := TWBrowse():New( 01 , 01, 500,184,,{'','Arquivo ','Descrição'},{20,30,80},;                              
oDlg,,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )    
aBrowse   := aArray
oBrowse:SetArray(aBrowse)    
oBrowse:bLine := {||{If(aBrowse[oBrowse:nAt,01],oOK,oNO),aBrowse[oBrowse:nAt,02],;                      
aBrowse[oBrowse:nAt,03] } }    

ACTIVATE DIALOG oDlg CENTERED 
Return


Static Function A116ICLIFOR(c116CNPJ,c116INSC,c116Alias,aFilDest,oXML,cBusca116)

Local aRet			:= {}
Local aAuxFor		:= {}
Local aArea			:= {}
Local c116Fil		:= xFilial(c116Alias)
Local cCodigo		:= ""
Local cLoja			:= ""
Local cNomeFor		:= ""
Local nQCNPJ		:= 0
Local nQINSC		:= 0
Local lAchoCliFor	:= .F.
Local lCliente		:= .F.
Local nBloq			:= 0
Local lBloq      	:= .F.
Local cCodBlq		:= ""
Local cLojBlq		:= ""
Local cNomFBlq		:= ""
Local cCliFor		:= ""

Default cBusca116 := ''

aArea := GetArea()

If c116Alias == "SA2"
	cCliFor := "F"
Elseif c116Alias == "SA1"
	cCliFor := "C"
Endif

DbSelectArea(c116Alias)
(c116Alias)->(DbSetOrder(3))

If (c116Alias)->(DbSeek(c116Fil+c116CNPJ))
	While !(c116Alias)->(EOF()) .And. c116CNPJ $ A140ICAES((c116Alias)->&(Substr(c116Alias,2,2)+"_CGC"))
		// Validacao para cliente ou fornecedor bloqueado.
		If (c116Alias)->&(Substr(c116Alias,2,2)+"_MSBLQL") == "1" .And. (A140INSC(c116INSC,(c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")) .Or. Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))))
			cCodBlq		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
			cLojBlq		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
			cNomFBlq	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
			nBloq++
		ElseIf (c116Alias)->&(Substr(c116Alias,2,2)+"_MSBLQL") <> "1" .And. (A140INSC(c116INSC,(c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")) .Or. Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))))
			If A140INSC(c116INSC,(c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR"))  
				cCodigo 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
				cLoja   	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
				cNomeFor 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
				nQCNPJ++
				nQINSC++
				lAchoCliFor := .T.
			ElseIf Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))) .And. nQINSC = 0
				cCodigo	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
				cLoja		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
				cNomeFor 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
				nQCNPJ++
				lAchoCliFor := .T.
			ElseIf Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))) .And. nQINSC <> 0 
				nQCNPJ++
				lAchoCliFor := .T.
			ElseIf Empty(c116INSC) // Caso nao tenha a TAG <IE> no XML ou a mesma venha vazia.
				cCodigo		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
				cLoja		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
				cNomeFor 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
				nQCNPJ++
				lAchoCliFor := .T.
			EndIf
		EndIf
		(c116Alias)->(DbSkip())
	EndDo
	
	If nQCNPJ > 1 .And. nQINSC <> 1 .And. ExistBlock("A116IFor")
		aAuxFor := ExecBlock("A116IFor",.F.,.F.,{oXML, c116Alias, cBusca116})
		If	Len(aAuxFor) == 3 .And.;
			ValType(aAuxFor[1]) == "C" .And. !Empty(aAuxFor[1]) .And. (aAuxFor[1] $ "SA1#SA2") .And.;
			ValType(aAuxFor[2]) == "C" .And. !Empty(aAuxFor[2]) .And.;
			ValType(aAuxFor[3]) == "C" .And. !Empty(aAuxFor[3])
			
			c116Alias	:= aAuxFor[1]
			cCodigo		:= Padr(aAuxFor[2],TamSX3("A2_COD")[1])
			cLoja		:= Padr(aAuxFor[3],TamSX3("A2_LOJA")[1])
			
			If c116Alias == "SA1" .Or. c116Alias == "SA2"
				(c116Alias)->(dbSetOrder(1))
				If (c116Alias)->(dbSeek(xFilial(c116Alias)+cCodigo+cLoja))
					If c116Alias $ "SA1"
						cCodigo		:= SA1->A1_COD
						cLoja		:= SA1->A1_LOJA
						lCliente	:= .T.
						nQCNPJ 		:= 1
						nQINSC 		:= 1
						lAchoCliFor	:= .T.
					Else
						cCodigo		:= SA2->A2_COD
						cLoja		:= SA2->A2_LOJA
						nQCNPJ		:= 1
						nQINSC		:= 1
						lAchoCliFor	:= .T.
					EndIf
				EndIf
			EndIf
		EndIf
	Endif
Endif

If !lAchoCliFor .And. nBloq > 0
	lBloq		:= .T.
	cCodigo	:= cCodBlq
	cLoja		:= cLojBlq
Endif

If (nQCNPJ > 1 .And. nQINSC <> 1) .Or. lBloq
	aAdd(aRet,{.F.,cCodigo,cLoja,lCliente,lBloq,cCliFor})
ElseIf lAchoCliFor
	aAdd(aRet,{.T.,cCodigo,cLoja,lCliente,lBloq,cCliFor})
EndIf

RestArea(aArea)

Return aRet


static Function EXEC103(cNum, cSerie,dEmissao, cFornece , cLoja, cCond, nValor , cTes , cchavecte, cFile ,cError)

Local aCab := {}
Local aItem := {}
Local aItens := {}
Local aAutoImp := {}
Local aItensRat := {}
Local aCodRet := {}
Local aParamAux := {}
Local nOpc := 3
Local nI := 0
Local nX := 0
Local nReg := 1
Private lMsErroAuto := .F.
Private lMsHelpAuto := .T.


//Cabeçalho
aadd(aCab,{"F1_TIPO" ,"N" ,NIL})
aadd(aCab,{"F1_FORMUL" ,"N" ,NIL})
aadd(aCab,{"F1_DOC" ,cNum ,NIL})
aadd(aCab,{"F1_SERIE" ,cSerie,NIL})
aadd(aCab,{"F1_EMISSAO" ,dEmissao ,NIL})
aadd(aCab,{"F1_DTDIGIT" ,DDATABASE ,NIL})
aadd(aCab,{"F1_FORNECE" ,cFornece ,NIL})
aadd(aCab,{"F1_LOJA" ,cLoja ,NIL})
aadd(aCab,{"F1_ESPECIE" ,"CTE" ,NIL})
aadd(aCab,{"F1_COND" ,cCond ,NIL})
aadd(aCab,{"F1_DESPESA" ,0 ,NIL})
aadd(aCab,{"F1_DESCONT" , 0 ,Nil})
aadd(aCab,{"F1_SEGURO" , 0 ,Nil})
aadd(aCab,{"F1_FRETE" , 0 ,Nil})
aadd(aCab,{"F1_MOEDA" , 1 ,Nil})
aadd(aCab,{"F1_TXMOEDA" , 1 ,Nil})
aadd(aCab,{"F1_STATUS" , "A" ,Nil})
aAdd(aCab,{"F1_CHVNFE",cchavecte})
//Itens
For nX := 1 To 1
          aItem := {}
          aadd(aItem,{"D1_ITEM" ,StrZero(nX,4) ,NIL})
         
          IF !(SF4->F4_CF $ "5911/6911")
         	 aadd(aItem,{"D1_COD" ,PadR("504000002",TamSx3("D1_COD")[1]) ,NIL})
		  ELSE
		     aadd(aItem,{"D1_COD" ,PadR("504000067",TamSx3("D1_COD")[1]) ,NIL})
		  ENDIF	  
          aadd(aItem,{"D1_UM" ,"MT" ,NIL})
          aadd(aItem,{"D1_LOCAL" ,"DIV" ,NIL})
          aadd(aItem,{"D1_QUANT" ,1 ,NIL})
          aadd(aItem,{"D1_VUNIT" ,nValor ,NIL})
          aadd(aItem,{"D1_TOTAL" ,nValor ,NIL})
          aadd(aItem,{"D1_TES" ,cTes ,NIL})
          aadd(aItem,{"D1_RATEIO" ,"2" ,NIL})
         if(nOpc == 4)//Se for classificação deve informar a variável LINPOS
              aAdd(aItem, {"LINPOS" , "D1_ITEM",  StrZero(nX,4)}) //ou SD1->D1_ITEM  se estiver posicionado.
         endIf
       aAdd(aItens,aItem)
Next nX
//Rateio de Centro de Custo
//3-Inclusão / 4-Classificação / 5-Exclusão
MSExecAuto({|x,y,z,k,a,b| MATA103(x,y,z,,,,k,a,,,b)},aCab,aItens,nOpc,aParamAux,aItensRat,aCodRet)

If !lMsErroAuto
    
Else
	cError := MostraErro("/dirdoc", "error.log")
	if 'EXISTNF' $ cError
		cError:='Ja existe um CTE com o este numero\fornecedor'
	endif
    ConOut("Erro na inclusao!")
EndIf
Return !lMsErroAuto

