#INCLUDE 'INKEY.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'XMLXFUN.CH'
#INCLUDE 'AP5MAIL.CH'
#INCLUDE 'SHELL.CH'
#INCLUDE 'XMLXFUN.CH'
#INCLUDE 'TBICONN.CH'
#DEFINE  ENTER CHR(13)+CHR(10)

//Fun��o main da factori
User Function IMPXML3()    //    [4.0]
Local	aAlias4  := GetArea()
Local  cArqImp    := ' '
Local cXml :=''
Local oXml
Local cError := ' '
Local  cWarning :=' '
Local cTipo :=''
Local lSucesso :=.F.
Local cCnpjFilial :=FWArrFilAtu(cEmpant,cNumemp)[18]
Private	cTpCarga :=	''

cArqImp := cGetFile("Arquivo .XML |*.XML","Selecione o Arquivo XML",0,"",.T.,GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE)
cXml := LerDeArquivo(cArqImp)
oXml  := 	XmlParser(cXml,"_",@cError, @cWarning )
if '#01' $ cXml
	msgalert('Erro ao ler aquivo !')
	return .F.
endif 

GetVAlue(@cTipo, 'C','oXml:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT',oXml)
if empty(cTipo)
	importacte(cXml,@oXml,cArqImp)
	freeobj(oXml)
elseif cTipo == cCnpjFilial
	importaimp(cXml,@oXml,cArqImp)
	freeobj(oXml)
else
	ImporNorm(cXml,@oXml,cArqImp)
	freeobj(oXml)
endif
RestArea(aAlias4)

Return .T.

// Fun��o que pega o arquivo do xml e transforma em uma String 
static function LerDeArquivo(cArquivo )
Local cRet:=''
If File(cArquivo)
	FT_FUSE(cArquivo)
	FT_FGOTOP()
	Do While !FT_FEOF()
		cRet += FT_FREADLN()
		FT_FSKIP()
    EndDo
    FT_FUSE()
else
	cRet := '#01 arquivo nao encontrado'
endif 
return cRet

/*Fun��o usada para pegar valor do XML 
user function GetVAlue(xVar,cTipo,cString,oXml)
if type(cString) =='U'
	if cTipo == 'C'	
		xVar:= ' '
	elseif cTipo=='D'
		xVar := ctod('  /  /  ')
	elseif cTipo =='N'
		xVar:= 0
	endif
elseif type(cString) <> cTipo
	msgalert('Uso Errado da fun��o Get Value no campo '+cString)

else
	if cTipo =='C' 
		xVar := &cString
	elseif cTipo == 'D'
		xVar := StoD(StrTran(&cString, '-','',10))
	elseif cTipo =='N'
		xVar := val(&cString)
	endif	
endif
return */
// Interface da fun��o de CTE
static function importacte(cXml,oXml,cArqImp)
Local  aProc	:= {}
Local  aErros:= {}
Local  aErroErp:= {}
lRet := u_ImpXML_CTe(cArqImp,.F.,@aProc,@aErros,oXML:_CTeProc:_Cte,@aErroErp,oXML:_CTeProc:_ProtCte)
if lRet 
	msgalert('CTE Importada com sucesso !')
else 
	msgalert('Erro ao importar CT-E')
endif 
return

// Fun��o que verifica e retorna se encontrar produto para aquele c�digo vs Fornecedor 
user function ProdFor(cProduto,cForn,cLoja)
SA5->(DbSetOrder(14))
If SA5->(DbSeek(XFilial('SA5')+cForn+cLoja+cProd))
	return SA5->A5_PRODUTO
endif
return ' '

//INterface para a chamada de NFE de importa��o 
Static function importaimp (cXml,oXml,cArqImp)
RETURN u_importaimp(cXml,oXml,cArqImp)
//Interface para a chamada da fun��o de import��o de notas 
Static Function Importanota(cXml, oXml,cArqimp)
return u_importanota(cXml,oXml,cArqimp)
static function Getvalue(xVar,cTipo,cString,oXml)
return u_Getvalue(xVar,cTipo,cString,oXml)

user function GetVAlue(xVar,cTipo,cString,oXml)
Local bError := ErrorBlock( { |oError| MyError( @xVar,cTipo ) } )
BEGIN SEQUENCE
xVar := &cString 
Return
RECOVER
END SEQUENCE
ErrorBlock( bError )
Return( NIL )
Static Function MyError( xVar ,ctipo )
if ctipo == 'C'
	xVar:=' '
elseif ctipo == 'N'
	xVar := 0
elseif ctipo == 'D'
	xVar:= Dtoc(' / / ')
elseif ctipo == 'A'
	xVar := {}
endif

BREAK
Return( NIL )
