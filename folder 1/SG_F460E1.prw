//Bibliotecas
#Include "Protheus.ch"
#Include "TBIConn.ch" 
#Include "Colors.ch"
#Include "RPTDef.ch"
#Include "FWPrintSetup.ch"

/*
/*
Vers�es:	Microsiga Protheus 12 ou superior VERSAO MVC
Idiomas:	Espanhol , Ingl�s
Descri��o: Utilizado na Rotina MVC da Liquida��o substituindo pontos entradas anteriores
Programa Fonte: FINA460.PRW
F460SE1 - Informa dados complementares do t�tulo
Ir para o final dos metadados
Criado por Silvia Regina Nazareno Morato, �ltima altera��o por Mauricio Pequim Junior em 11 fev, 2016
Tempo aproximado para leitura: 1 minuto
Ir para o in�cio dos metadados
Ponto-de-Entrada: F460SE1 - Informa dados complementares do t�tulo
Idiomas:	Espanhol , Ingl�s
Descri��o:
O ponto de entrada F460SE1 sera utilizado para informar dados necessarios � gravacao complementar, dos titulos gerados apos a liquidacao.

Programa Fonte
FINA460.PRW
Sintaxe
F460SE1 - Informa dados complementares do t�tulo ( < aComplem> ) --> aComplem

Par�metros:
Nome			Tipo			Descri��o			Default			Obrigat�rio			Refer�ncia	
aComplem			Vetor			Complemento						X				
Retorno
aComplem(qualquer)
Neste ponto de entrada dever  se retornar um array com os dados de campo e conte�do com dados dos titulos geradores a serem gravados de forma complementar nos titulos gerados ap�s a liquidacao.
*/


User Function F460SE1()
//Local nxRecno  := SE1->(RECNO()) 
//Local cxChvSE5 := SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_CLIENTE+SE1->E1_LOJA
//Local cxChvFK1 := ''
Local nxValLiq := 0
Local aComplem := {}
//ALERT(SE1->E1_NUM)
//ALERT(SE1->E1_PARCELA)
//ALERT(SE1->E1_VALOR)
//ALERT(SE1->E1_CLIENTE)
//ALERT(SE1->E1_NUMLIQ)
//Alert(cxChvSE5)
//ALERT(SE1->E1_SALDO)
If SE1->E1_SALDO > 0 .and. SE1->E1_TIPOLIQ='LIQ'
//    nxRecno  := SE1->(RECNO())
    nxValLiq := SE1->E1_VALLIQ + SE1->E1_SALDO
    Reclock('SE1',.F.)
        SE1->E1_VALLIQ := nxVALLIQ
        SE1->E1_SALDO  := 0
    MsUnlock()
//    DBSELECTAREA('SE1')
//    DbGoto(nxRecno)
EndIf

Return aComplem
