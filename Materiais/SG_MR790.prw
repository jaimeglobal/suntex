#include 'protheus.ch'
#include 'parmtype.ch'
#include "tbiconn.ch"
#INCLUDE 'topconn.ch'


USER FUNCTION SG_MR790()
    Local oFWMsExcel
    Local oExcel
    Local cPerg      := 'MTR700'
    Local aArea     // :=  GetArea()
    Local cQuery     := ""
    Local cArquivo  // := GetTempPath()+'sgmr790.xml'
    Local cTempAlias  //   := GetNextAlias()
    Local nAproduzir := 0
    Local nVendido := 0
    Local nEntregue := 0
//	PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES "SM0" USER 'admin' PASSWORD 'globalad'//PREPARE ENVIRONMENT EMPRESA aParam[2] FILIAL aParam[1] TABLES "SM0"
//	INITPUBLIC()
    aArea      := GetArea()
    cQuery     := ""
    cArquivo   := GetTempPath()+'sgmr790.xml'
    cTempAlias     := GetNextAlias()

    
    Pergunte(cPerg, .T.)

    cQuery := "SELECT C6_FILIAL, C6_NUM ,C5_NATUREZ  "
    cQuery += "	  ,A1_NOME   "
    cQuery += "	  ,C5_EMISSAO   "
    cQuery += "	  ,B1_COD   "
    cQuery += "	  ,B1_DESC   "
    cQuery += "	  , 0 AS DISP   "
    cQuery += "	  ,C6_QTDVEN   "
    cQuery += "	  ,C6_QTDENT   "
    cQuery += "	  ,ROUND(C6_QTDVEN - C6_QTDENT,4) AS SALDOPV   "
    cQuery += "	  ,C6_PRCVEN   "
    cQuery += "	  ,C6_VALOR   "
    cQuery += "	  ,C6_ENTREG   "
    cQuery += "	  ,ROUND(SUM(ISNULL(C2_QUANT - C2_QUJE - C2_PERDA,0)),4) SALDOOP   "
    cQuery += "	  ,0 SALDOPRV   "
    cQuery += "	    "
    cQuery += "	  FROM "+RetSqlName("SC6")+"  SC6   "
    cQuery += "	INNER JOIN "+RetSqlName("SC5")+" SC5    "
    cQuery += "			ON C5_NUM = C6_NUM   "
    cQuery += "			AND SC5.D_E_L_E_T_<>'*'   AND SC5.C5_FILIAL='"+XFILIAL("SC5")+"' "
    cQuery += "	LEFT JOIN "+RetSqlName("SED")+" SED    "
    cQuery += "			ON C5_NATUREZ = ED_CODIGO   "
    cQuery += "			AND SED.D_E_L_E_T_<>'*'   AND SED.ED_FILIAL='"+XFILIAL("SED")+"' "
    cQuery += "	INNER JOIN "+RetSqlName("SB1")+" SB1    "
    cQuery += "			ON  SB1.B1_COD=SC6.C6_PRODUTO   "
    cQuery += "			AND SB1.D_E_L_E_T_ = ' '  AND SB1.B1_FILIAL='"+XFILIAL("SB1")+"' "
    cQuery += "	LEFT JOIN "+RetSqlName("SA1")+" SA1    "
    cQuery += "			ON A1_COD = C6_CLI   "
    cQuery += "			AND A1_LOJA = C6_LOJA   "
    cQuery += "			AND SA1.D_E_L_E_T_ = ' '  AND SA1.A1_FILIAL='"+XFILIAL("SA1")+"' "
    cQuery += "	LEFT JOIN "+RetSqlName("SC2")+" SC2   "
    cQuery += "			ON SC2.C2_PRODUTO = C6_PRODUTO   "
    cQuery += "			AND SC2.D_E_L_E_T_<>'*'    "
    cQuery += "			AND C2_QUANT - C2_QUJE > 0  AND SC2.C2_FILIAL='"+XFILIAL("SC2")+"' "
    cQuery += "	WHERE C6_QTDVEN - C6_QTDENT > 0       "
    cQuery += "	AND C2_QUANT - C2_QUJE > 0   "
    cQuery += "	AND C2_DATRF = ''   "
    cQuery += "	AND C5_TIPO = 'N'   AND SC6.D_E_L_E_T_=' '"
    cQuery += " AND C5_NUM BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"'"
    cQuery += " AND B1_COD BETWEEN '"+MV_PAR03+"' AND '"+MV_PAR04+"'"
    cQuery += " AND C6_ENTREG BETWEEN '"+DTOS(MV_PAR10)+"' AND '"+DTOS(MV_PAR11)+"'"
    cQuery += " AND C5_EMISSAO BETWEEN '"+DTOS(MV_PAR15)+"' AND '"+DTOS(MV_PAR16)+"'"
    cQuery += "		GROUP BY    "
    cQuery += " C6_FILIAL,	C6_NUM,A1_NOME,C5_EMISSAO,C5_NATUREZ,ED_DESCRIC,B1_COD,B1_DESC,C6_QTDVEN,C6_QTDENT,C6_PRCVEN,C6_VALOR,C6_ENTREG   "
    cQuery += "  ORDER BY B1_COD,C6_ENTREG
    TCQuery cQuery New Alias (cTempAlias)


    oFWMsExcel := FWMSExcel():New()
    oFWMsExcel:AddworkSheet("Dados") //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
    //Criando a Tabela
    oFWMsExcel:AddTable("Dados","Detalhe")
   
 //   oFWMsExcel:AddColumn("Dados","Detalhe","Filial",1)
   
    oFWMsExcel:AddColumn("Dados","Detalhe","Pedido",1,1)
    oFWMsExcel:AddColumn("Dados","Detalhe","Natureza",1,1)
    oFWMsExcel:AddColumn("Dados","Detalhe","Cliente",1,1)
    oFWMsExcel:AddColumn("Dados","Detalhe","Emissao",1,4)
    oFWMsExcel:AddColumn("Dados","Detalhe","Produto",1,1)
    oFWMsExcel:AddColumn("Dados","Detalhe","Descricao",1,1)
    // oFWMsExcel:AddColumn("Dados","Detalhe","Disponivel",1,2)
    oFWMsExcel:AddColumn("Dados","Detalhe","Qt. Ven.",1,2)
    oFWMsExcel:AddColumn("Dados","Detalhe","Qt. Ent.",1,2)
    oFWMsExcel:AddColumn("Dados","Detalhe","A Programar",1,2)
    oFWMsExcel:AddColumn("Dados","Detalhe","Saldo Est.",1,2)
    //  oFWMsExcel:AddColumn("Dados","Detalhe","Prc. Ven.",1,3)
    // oFWMsExcel:AddColumn("Dados","Detalhe","Total",1,3)
    oFWMsExcel:AddColumn("Dados","Detalhe","Entrega",1,4)
    oFWMsExcel:AddColumn("Dados","Detalhe","Saldo OP",1,2)
    oFWMsExcel:AddColumn("Dados","Detalhe","Saldo Final",1,2)
    cProdAtu := ""
    cProdAnt := ""
    cQantAtu := 0
    cQantAnt := 0
    While !((cTempAlias)->(EoF()))
        cProdAtu :=  (cTempAlias)->B1_COD
        nSaldo := MostrarSaldo((cTempAlias)->B1_COD,MV_PAR17,MV_PAR18,.F.,.F.)
        nSaldoTot := MostrarSaldo((cTempAlias)->B1_COD,MV_PAR17,MV_PAR18,.T.,.F.)
        IF cProdAnt <> cProdAtu
            cQantAtu :=  (nSaldo - (cTempAlias)->SALDOPV) + (cTempAlias)->SALDOOP
            
            if cProdAnt <> ""
                
              //oFWMsExcel:AddRow("Dados","Detalhe",{"totais",(cTempAlias)->C5_NATUREZ,(cTempAlias)->A1_NOME,STOD((cTempAlias)->C5_EMISSAO),(cTempAlias)->B1_COD,(cTempAlias)->B1_DESC,(cTempAlias)->C6_QTDVEN,(cTempAlias)->C6_QTDENT,(cTempAlias)->SALDOPV,nSaldoTot,STOD((cTempAlias)->C6_ENTREG),(cTempAlias)->SALDOOP,cQantAtu})
               // oFWMsExcel:AddRow("Dados","Detalhe",{, , , , , , , , , , , , })
                oFWMsExcel:AddRow("Dados","Detalhe",{"TOTAIS", , , , , , nVendido,nEntregue ,nAproduzir , , , , })
                oFWMsExcel:AddRow("Dados","Detalhe",{, , , , , , , , , , , , })
               

            endif
            nAproduzir := (cTempAlias)->SALDOPV
            nVendido   := (cTempAlias)->C6_QTDVEN
            nEntregue   := (cTempAlias)->C6_QTDENT

        else
            cQantAtu := cQantAnt - (cTempAlias)->SALDOPV
            nAproduzir := nAproduzir + (cTempAlias)->SALDOPV
            nVendido   :=nVendido+ (cTempAlias)->C6_QTDVEN
            nEntregue   := nEntregue+(cTempAlias)->C6_QTDENT
        endif



        //    cQantAtu :=  (nSaldo - (cTempAlias)->SALDOPV) + (cTempAlias)->SALDOOP


        oFWMsExcel:AddRow("Dados","Detalhe",{;
            (cTempAlias)->C6_NUM,;
            (cTempAlias)->C5_NATUREZ,;
            (cTempAlias)->A1_NOME,;
            STOD((cTempAlias)->C5_EMISSAO),;
            (cTempAlias)->B1_COD,;
            (cTempAlias)->B1_DESC,;
        (cTempAlias)->C6_QTDVEN,;
            (cTempAlias)->C6_QTDENT,;
            (cTempAlias)->SALDOPV,;
            nSaldoTot,;
        STOD((cTempAlias)->C6_ENTREG),;
            (cTempAlias)->SALDOOP,;
            cQantAtu})
        //	(nSaldo - (cTempAlias)->SALDOPV) + (cTempAlias)->SALDOOP})

        cProdAnt := (cTempAlias)->B1_COD
        cQantAnt := cQantAtu
        (cTempAlias)->(DbSkip())
    EndDo





    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)

    //Abrindo o excel e abrindo o arquivo xml
    oExcel := MsExcel():New()             //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)                 //Visualiza a planilha
    oExcel:Destroy()


    (cTempAlias)->(DbCloseArea())
    RestArea(aArea)

RETURN


Static Function MostrarSaldo(cProduto,cArmDe,cArmAte,lTot, lAltera)
    Local nSaldo := 0 //Saldo em Estoque
    Local aArea := GetArea() //Area
    Local aAreaSB2 := SB2->(GetArea())
    Local nSaldoAtu := 0
    Default lAltera := .f.
     Default cArmDe = "   " 
    Default cArmAte = "ZZZ"

    Default lTot := .f.
    SB2->( dbSetOrder(1) )
        SB2->( dbSeek(xFilial("SB2") + cProduto) )
    While !SB2->( Eof() ) .And. SB2->B2_FILIAL + SB2->B2_COD == xFilial("SB2") + cProduto 
      
        IF SB2->B2_LOCAL < cArmDe .or. SB2->B2_LOCAL =='REJ'
            SB2->( dbSkip() )
        ENDIF
        if lTot
            nSaldoAtu := SB2->B2_QATU
            nSaldo += nSaldoAtu
        else
            nSaldoAtu := SaldoSB2()
            nSaldo += nSaldoAtu
            If nSaldoAtu <> SB2->B2_ECSALDO
                lAltera := .T.
                If SB2->( SoftLock("SB2") ) //Se nao conseguir travar ira provavelmente ser enviado novamente no proximo processamento.
                    SB2->B2_ECSALDO := nSaldoAtu //Grava com o mesmo calculo usado na Query da MF1.
                    SB2->( MsUnLock() )
                EndIf
            EndIf
        endif
        IF SB2->B2_LOCAL >= cArmAte
            EXIT
        ENDIF
        SB2->( dbSkip() )
    EndDO
    IF nSaldo < 0
        nSaldo := 0
    EndIf
    RestArea(aAreaSB2)
    RestArea(aArea)
Return nSaldo


