#INCLUDE 'INKEY.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'XMLXFUN.CH'
#INCLUDE 'AP5MAIL.CH'
#INCLUDE 'SHELL.CH'
#INCLUDE 'XMLXFUN.CH'
#INCLUDE 'TBICONN.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#DEFINE  ENTER CHR(13)+CHR(10)

/*                                              
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ImpXMLNFe      � Autor�Fabiano Pereira� Data � 23/02/2011  ���
�������������������������������������������������������������������������͹��
���Desc.     �Importa Arquivo Xml Nota Eletronica                         ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ĺ��
���Observacao� Padrao                                                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Protheus 10   - IMELTRON                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
**********************************************************************
User Function ImpXMLNFe()
**********************************************************************
Local lTableField	:=	.T.
Local aRetIni 		:=	{} 
Local lRetTela 		:=	.F.

Private	cArqConfig	:=	''
Private	cIndConfig	:=	''
Private	aAlias		:=	GetArea()
Private	cCadastro	:=	'Status XML'
Private	cOldArq_Cfg	:=	''
Private	cArqErro	:=	'ZXM_ERROS_NFE.TXT'
Private	aRotina		:=	{}
Private	aCores		:=	{}
Private	aLegenda	:=	{}
Private	aArqTemp	:=	{}        

Private _dDtDesemb  :=	{}            /* Colocado por Diego Cerioli em 01/04/13 para ajustar erros de variavel que n�o */
Private _dDtDI		:=	{}
Private _cNumDI		:=	{}
Private _cUFDesemb	:=	{}
Private _cLocDesemb	:=	{}

Static 	nHRes    	:=	IIF(Type('oMainWnd')!='U', oMainWnd:nClientWidth, )       // Resolucao horizontal do monitor

Private bSavDblClick:=	Nil
Static _StartPath
Static _RootPath
Static _RpoDb
Static _cPathTable
Static _cArqIni
Static _cMail
Static _aEmpFil
Static SA2Recno

//SetKey( VK_F12, {|| Aviso('Chave NF-e', ZXM->ZXM_CHAVE ,{'Ok'},3) })


//���������������������������������Ŀ
//� VERIFICA SE PATH ESTA APLICADO	|
//�����������������������������������
//aRpo := GetSrcArray("CHECKLS.PRW")
//If Ascan( aRpo, "CHECKLS.PRW" ) == 0
//	Alert(	'ATEN��O !!!'+ENTER+ENTER+'PATH COM FUN��ES PARA UTILIZAR ESSA ROTINA'+ENTER+'N�O ESTA APLICADO.'+ENTER+ENTER+;
//			'Entre em contato com o Administrador.') 
//	Return()	
//EndIf

//������������������������������������������������������������Ŀ
//� INICIALIZACAO VARIAVEIS \ aEmpFil\PARAMETROS \ ARQ.TEMP    |
//��������������������������������������������������������������
//MsgRun('Verificando arquivo de configura��o...', 'Aguarde... ',{|| aRetIni := ExecBlock("IniConfig",.F.,.F.,) }) 		//    [1.0] 
AADD(aRetIni,.F.)
AADD(aRetIni,"\system\")
AADD(aRetIni,"D:\TOTVS12\Microsiga\protheus_data")
AADD(aRetIni,"top")              
AADD(aRetIni,"\SYSTEM\")                   
AADD(aRetIni," ")
AADD(aRetIni," ")              
aadd(aRetIni , {{"01", "0101" , "26683626000133", "Suntex", "MATRIZ"}})
//aadd(aRetIni , /*{{"01", "01" , "88176995000197", "NOVUS", "MATRIZ - RS"}, {"01", "02" , "88176995000278", "NOVUS", "NOVUS - SP"}, {"01", "03" , "88176995000359", "NOVUS", "NOVUS - PR"}}*/"")
aRetIni[01] 	:= .T.    
aEMP            := {}
//AADD(,"88176995000197")
//AADD(aEMP,"88176995000278")
//AADD(aEMP,"88176995000359")
aadd( aEMP , {"94985603000133"} )  //, "13315214000107" , "13503335000174"} )

//aRetIni[08]     := aEMP

If !aRetIni[01]
	Return()
Else
	nX:=2      
	_StartPath	:=	aRetIni[nX]; nX++
	_RootPath	:=	aRetIni[nX]; nX++
	_RpoDb		:=	aRetIni[nX]; nX++
	_cPathTable	:=	aRetIni[nX]; nX++
	_cArqIni	:=	aRetIni[nX]; nX++
	_cMail		:=	aRetIni[nX]; nX++
	_aEmpFil	:=	aRetIni[nX][01][02]
EndIf

//���������������������������������������������Ŀ
//� INICIALIZA\ATUALIZA ARQUIVO ZXM_CFG.DBF     |
//�����������������������������������������������          
MsgRun('Verificando arquivo config...', 'Aguarde... ', {|| lRetTela := ExecBlock("TelaConfig",.F.,.F., {.F.}) })        //    [2.0] 
aRetIni[01] 	:= .T. 
If !aRetIni[01]
	Return()
Else
	nX:=2      
	_StartPath	:=	aRetIni[nX]; nX++
	_RootPath	:=	aRetIni[nX]; nX++
	_RpoDb		:=	aRetIni[nX]; nX++
	_cPathTable	:=	aRetIni[nX]; nX++
	_cArqIni	:=	aRetIni[nX]; nX++
	_cMail		:=	aRetIni[nX]; nX++
	_aEmpFil	:=	aRetIni[nX]
EndIf

//���������������������������������������������Ŀ
//� INICIALIZA\ATUALIZA ARQUIVO ZXM_CFG.DBF     |
//�����������������������������������������������          
MsgRun('Verificando arquivo config...', 'Aguarde... ', {|| lRetTela := ExecBlock("TelaConfig",.F.,.F., {.F.}) })        //    [2.0]
lRetTela:= .T.
If !lRetTela
	Return()
EndIf
//���������������������������������Ŀ
//� ABRE ARQUIVO DE CONFIGURACAO    |
//�����������������������������������
If Select('TMPCFG') == 0
   DbUseArea(.T.,'CTREECDX',_StartPath+cArqConfig, 'TMPCFG',.T.,.F.)
   DbSetIndex(_StartPath+cIndConfig)
EndIf

//������������������������������������������������������Ŀ
//� VERIFICA SE ESTRUTURA DA TABELA ZXM                  |
//��������������������������������������������������������
DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
If DbSeek(cEmpAnt+cFilAnt, .F.)     
   If !TMPCFG->ESTRUT_ZXM

       If !ExecBlock("CreateTable",.F.,.F.,{cEmpAnt} )
           MsgAlert('Estrutura da Tabela ZXM \ ZXN n�o esta completa.')
           Return()
       EndIf

   EndIf
EndIf
          

DbSelectArea('ZXM');DbSetorder(1)
DbSelectArea('ZXN');DbSetorder(1)

aCores    := {	{ 'ZXM->ZXM_SITUAC=="C" '	, 'BR_VERMELHO'	},;
                { 'ZXM->ZXM_STATUS=="*" '   , 'BR_LARANJA'	},;
                { 'ZXM->ZXM_STATUS=="I" '   , 'BR_AZUL'		},;
                { 'ZXM->ZXM_STATUS=="R" '   , 'BR_CINZA'	},;
                { 'ZXM->ZXM_STATUS=="X" '   , 'BR_CANCEL'	},;
                { 'ZXM->ZXM_STATUS=="G" '   , 'BR_VERDE'	}}


aLegenda:= {	{ 'BR_AZUL'		,'XML Importado'			},;
                { 'BR_LARANJA'	,'XML Arquivado'			},;
                { 'BR_CINZA'	,'XML Recusado'				},;
                { 'BR_CANCEL'	,'XML Cancelado pelo Usuario'},;
                { 'BR_VERDE'	,'XML Gravado'				},;
                { 'BR_VERMELHO'	,'XML Cancelado na Sefaz'	}}




Aadd( aRotina, { "Pesquisar"		, 'AxPesqui'                            ,	0, 1, 0, .F.})
Aadd( aRotina, { "Visualizar"		, 'ExecBlock("Visual_XML",.F.,.F.,{""})' ,	0, 4 })        		//    [3.0]	-	BROWSER COM CABEC, ITEM DO XML, GERA DOC. ETC...
Aadd( aRotina, { "Carregar XML"		, 'ExecBlock("Carrega_XML",.F.,.F.,{""})',	0, 3 })        		//    [4.0]	-	CARREGA XML
Aadd( aRotina, { "Importa CT-e"		, 'u_IMPXML2',	0, 3 })        		//    [4.0]	-	CARREGA XML

_aRot:={}
Aadd( _aRot, { "Status XML"			, 'ExecBlock("ConsNFeSefaz",.F.,.F.,{""})',	0, 6 })  	     	//    [5.0]	-	VERIFICA STATUS DO XML NA SEFAZ
Aadd( _aRot, { "Status Sefaz"		, 'ExecBlock("SefazStatus",.F.,.F.,{""})',	0, 6 })     	   	//    [5.1]	-	VERIFICA STATUS DA SEFAZ
Aadd( _aRot, { "Site Sefaz"       	, 'ExecBlock("SiteSefaz",.F.,.F.,{""})',	0, 6 })        		//    [6.0]	-	ABRE SITE SEFAZ, COM CHAVE DE ACESSO
Aadd( _aRot, { "Msg. Erro"			, 'ExecBlock("ErroNFe",.F.,.F.,{""})',		0, 6 })        	   	//    [7.0]	-	VERIFICA COD.RETORNO SEFAZ

Aadd( aRotina,    { 'Consulta Sefaz',    _aRot, 0 , 4} )


Aadd( aRotina, { "Exportar"        	, 'ExecBlock("Exporta_XML",.F.,.F.,{""})',	0, 4 })        		//    [8.0]	-	EXPORTA XML PARA ARQUIVO
Aadd( aRotina, { "Recusar XML"     	, 'ExecBlock("Recusar_XML",.F.,.F.,{""})',	0, 4 })        		//    [9.0]	-	RECUSA XML
Aadd( aRotina, { "Cancelar"        	, 'ExecBlock("Cancelar_XML",.F.,.F.,{""})',	0, 4 })        		//    [10.0]-	CANCELA XML - ALTERA STATUS
Aadd( aRotina, { "Excluir"        	, 'ExecBlock("Excluir_XML",.F.,.F.,{""})',	0, 4 })        		//    [10.0]-	CANCELA XML - ALTERA STATUS

#IFNDEF TOP
   Aadd( aRotina, { "Filtro"		, 'ExecBlock("Filtro_XML",.F.,.F.,{""})',	0, 4 })            //    [11.0]-	OPCAO DE FIILTRO PRA VERSAO DBF
#ENDIF
Aadd( aRotina, { "Configura��es"	, 'ExecBlock("TelaConfig",.F.,.F., {.T.} )',	0, 3 })
// Aadd( aRotina, { "Gerencia XML"	, 'ExecBlock("GerenciaXML",.F.,.F., {.T.} )',	0, 3 })   
Aadd( aRotina, { "Legenda"			, 'BrwLegenda("Legenda Status XML","Status XML",aLegenda)',    0, 2 })


//��������������������������������������������������������������Ŀ
//� P.E. Utilizado para adicionar botoes ao Menu Principal       �
//����������������������������������������������������������������
IF ExistBlock("ROTIMPXMLNFE")
	aRotNew := ExecBlock("ROTIMPXMLNFE",.F.,.F.,aRotina)
	For nX := 1 To Len(aRotNew)
		aAdd(aRotina,aRotNew[nX])
	Next
Endif


MBrowse( 06, 01,22,75,"ZXM",,,,,,aCores)


//    APAGA ARQUIVOS TEMPORARIOS
If Len(aArqTemp) > 0
   For nX := 1 To Len(aArqTemp)
       FErase( aArqTemp[nX] )
   Next
EndIf

IIF(Select("ZXM")     != 0, ZXM->(DbCLoseArea()), )
IIF(Select("TMP")     != 0, TMP->(DbCLoseArea()), )
IIF(Select('TMPCFG')  != 0, TMPCFG->(DbCLoseArea()), )

RestArea(aAlias)
Return()
************************************************************************
User Function TelaConfig()    //    [2.0]
************************************************************************
//���������������������������������������������Ŀ
//� INICIALIZA\ATUALIZA ARQUIVO ZXM_CFG.DBF     |
//� Chamada -> Inicio Programa e                |
//|            Rotina - Configuracao            �
//�����������������������������������������������
Local	aFds            :=	{}
Local	aNewField		:=	{}
Local	aReturn			:=	{}

Static 	cCfgVersao 	:= '1.0'
Static 	nHRes    	:=	IIF(Type('oMainWnd')!='U', oMainWnd:nClientWidth, )       // Resolucao horizontal do monitor

//Private	cArqConfig	:=	'ZXM_CFG.DBF'
Private	cArqConfig	:=	'ZXM_CFG.DTC'  // AJUSTE P12
Private	cIndConfig	:=	'TMPCFG.CDX'
Private	cOldArq_Cfg	:=	Left(cArqConfig, (Len(cArqConfig)-4))+'_OLD'+Right(AllTrim(cArqConfig), 4)

Private	lMostraTela	:=	ParamIxb[01]


Private	cTpMail        	:=	_cMail
Private	cPathTables    	:=	_cPathTable+Space(15)
Private	cEmail        	:=	Space(100)
Private	cPassword    	:=	Space(100)
Private	cPOrigem 		:= 	'Caixa de Entrada'+Space(095)
Private	cPDestino    	:= 	Space(100)
Private	cServerEnv		:= 	Alltrim(GetMv('MV_RELSERV'))+    Space(100)
Private	cServerRec		:= 	Alltrim(GetMv('MV_RELSERV'))+    Space(100)
Private	cNFe9Dig		:= 	'SIM'
Private	cSer3Dig		:= 	'SIM'
Private	cSerTransf    	:= 	Space(TamSX3('F1_SERIE')[1])
Private	cTagProt		:=	'N�O'
Private	cDescFor		:=	'SIM'
Private	cDescEmp		:=	'N�O'
Private	cPreDoc        	:= 	'PRE'
Private	cPortRec    	:= 	IIF(Left(_cMail,3)=='POP','110',IIF(_cMail=='IMAP','143','##'))
Private	cPortEnv    	:= 	'25 '
Private	cConect        	:= 	''
Private	cPath_XML      	:= 	Space(100)
Private	cProbEmail		:=	Space(100)
Private	cXmlCancFor		:=	Space(200)
Private	cTamDFor		:=	'120'	//	TAMANHO PADRAO SEFAZ
Private	cTamDEmp		:=	AllTrim(Str(TamSX3('B1_DESC')[1]))
Private	lAutent			:= 	.F.
Private	lSegura			:=	.F.
Private	lSSL		 	:= 	.F.
Private	lTLS		 	:= 	.F.
Private	lEstrut_Xml 	:= 	.F.
Private	cSem_XML		:= 	'N�O'
Private	cGrv_Dif_A		:= 	'N�O'

Private	lTesteR			:=	.F.
Private	lTesteE			:=	.T.
            
Private nHoraCanc   	:=	GetMv('MV_SPEDEXC')
Private	cProbEmail   	:= 	Space(200)
Private	cDominio		:=	Space(100)
Private	cVerifEsp       :=	Space(100)

Private	aMyHeader   	:=	{}
Private	aMyCols     	:=	{}
Private	aConfig     	:=	{}

Private	lNewVersao		:=	.F.

SetPrvt('oDlgSM0'	,'oGrp1' ,'oGrp2' ,'oGrp3' ,'oGrp4'	,'oGrp5'	,'oFolder')
SetPrvt('oSay1'		,'oSay2' ,'oSay3' ,'oSay4' ,'oSay5'	,'oSay6' ,'oSay7' ,'oSay8' ,'oSay9' ,'oSay10')
SetPrvt('oSay11'	,'oSay12','oSay13','oSay14','oSay15','oSay16','oSay17','oSay18','oSay19','oSay20','oSay21')
SetPrvt('oGet1' 	,'oGet2' ,'oGet3' ,'oGet4' ,'oGet5'	,'oGet6' ,'oGet7' ,'oGet8' ,'oGet9' ,'oGet10')
SetPrvt('oGetr11'	,'oGet12','oGet13','oGet14','oGet15','oGet16','oGet17','oGet18','oGet19','oGet20','oGet21')
SetPrvt('oCBox4'	,'oCBox5','oCBox6','oCBox9','oCBox10','oCBox11','oCBox14','oCBox12b','oCBox12c')
SetPrvt('oBtnTest'	,'oOkConect','oNoConect')
SetPrvt('oSay1NomeFil','oSay2NomeFil','oSay3NomeFil')




//�����������������������������������������������Ŀ
//�VERIFICA SE NUMERO DO HARDLOCK ESTA RELACIONADO�
//�PARA UTILIZAR A ROTINA                         �
//|												  |	
//|VALIDA TEMPO DE LICENCA FREE					  |
//�������������������������������������������������
*****************************************************
//If !ExecBlock('CheckLS',.F.,.F.,{.F.})
//	Return(.F.)
//EndIf
*****************************************************


//�������������������������Ŀ
//� CAMPOS DO MSNEWGETDADOS	|
//���������������������������
Aadd( aMyHeader , {'Empresa'			,'M0_CODIGO'   	,'@!',002,000,'','','C','',''} )
Aadd( aMyHeader , {'Nome Empresa'		,'M0_NOME'    	,'@!',015,000,'','','C','',''} )
Aadd( aMyHeader , {'Filial'				,'M0_CODFIL'  	,'@!',002,000,'','','C','',''} )
Aadd( aMyHeader , {'Nome Filial'		,'M0_FILIAL'  	,'@!',015,000,'','','C','',''} )
Aadd( aMyHeader , {'CNPJ'				,'M0_CGC'     	,'@R 99.999.999/9999-99',014,000,'','','C','',''} )
Aadd( aMyHeader , {'Inscr Estadual'		,'M0_INSC'     	,'@!',014,000,'','','C','',''} )
Aadd( aMyHeader , {'Endere�o Entrega'	,'M0_ENDENT'    ,'@!',060,000,'','','C','',''} )
Aadd( aMyHeader , {'Compl End Entrega'	,'M0_COMPENT'   ,'@!',012,000,'','','C','',''} )
Aadd( aMyHeader , {'Bairro Entrega'		,'M0_BAIRENT'   ,'@!',020,000,'','','C','',''} )
Aadd( aMyHeader , {'Cidade Entrega'		,'M0_CIDENT'    ,'@!',020,000,'','','C','',''} )
Aadd( aMyHeader , {'Est Ent'			,'M0_ESTENT'    ,'@!',002,000,'','','C','',''} )
Aadd( aMyHeader , {'CEP Ent'			,'M0_CEPENT'    ,'@R 99999-999',008,000,'','','C','',''} )
Aadd( aMyHeader , {'Fone Ent'			,'M0_TEL'       ,'@!',014,000,'','','C','',''} )
Aadd( aMyHeader , {'FAX Ent'			,'M0_FAX'       ,'@!',014,000,'','','C','',''} )
Aadd( aMyHeader , {'Endere�o Cobran�a'	,'M0_ENDCOB'   	,'@!',060,000,'','','C','',''} )
Aadd( aMyHeader , {'Bairro Cobran�a'    ,'M0_BAIRCOB'	,'@!',020,000,'','','C','',''} )
Aadd( aMyHeader , {'Cidade Cobran�a'    ,'M0_CIDCOB'	,'@!',020,000,'','','C','',''} )
Aadd( aMyHeader , {'Est Cob'			,'M0_ESTCOB'    ,'@!',002,000,'','','C','',''} )
Aadd( aMyHeader , {'CEP Cob'			,'M0_CEPCOB'    ,'@R 99999-999',008,000,'','','C','',''} )
Aadd( aMyHeader , {'CNAE'				,'M0_CNAE'		,'@!',007,000,'','','C','',''} )
Aadd( aMyHeader , {'C�digo Municipio'	,'M0_CODMUN'    ,'@!',007,000,'','','C','',''} )
                             

//�������������������������������������Ŀ
//�  CAMPOS DO ARQUIVO ZXM_CFG.DBF 		�
//���������������������������������������
Aadd( aFds, {"EMPRESA"		,"C",    002,000} )
Aadd( aFds, {"NOME_EMP"   	,"C",    040,000} )
Aadd( aFds, {"FILIAL"      	,"C",    002,000} )
Aadd( aFds, {"NOME_FIL"   	,"C",    040,000} )
Aadd( aFds, {"CNPJ"       	,"C",    014,000} )
Aadd( aFds, {"PATCHTABLE"	,"C",    100,000} )
Aadd( aFds, {"EMAIL"       	,"C",    150,000} )
Aadd( aFds, {"SENHA"       	,"C",    100,000} )
Aadd( aFds, {"PASTA_ORIG" 	,"C",    100,000} )
Aadd( aFds, {"PASTA_DEST" 	,"C",    100,000} )
Aadd( aFds, {"TIPOEMAIL"   	,"C",    004,000} )    //    POP,IMAP,MAPI,SMTP
Aadd( aFds, {"SERVERENV"   	,"C",    100,000} )
Aadd( aFds, {"PORTA_ENV"   	,"C",    003,000} )
Aadd( aFds, {"SERVERREC"   	,"C",    100,000} )
Aadd( aFds, {"PORTA_REC"   	,"C",    003,000} )
Aadd( aFds, {"NFE_9DIG"   	,"C",    003,000} )
Aadd( aFds, {"SER_3DIG"   	,"C",    003,000} )
Aadd( aFds, {"SER_TRANSF" 	,"C",    003,000} )
Aadd( aFds, {"PRE_DOC"  	,"C",    003,000} )
Aadd( aFds, {"ESTRUT_ZXM" 	,"L",    001,000} )
Aadd( aFds, {"AUTENTIFIC" 	,"L",    001,000} )
Aadd( aFds, {"SEGURA" 	  	,"L",    001,000} )   	
Aadd( aFds, {"SSL"		   	,"L",    001,000} )
Aadd( aFds, {"TLS"		  	,"L",    001,000} )   	
Aadd( aFds, {"PATH_XML"   	,"C",    200,000} )
Aadd( aFds, {"PROB_EMAIL" 	,"C",    200,000} )   
Aadd( aFds, {"FOR_CANC"   	,"C",    200,000} )   
Aadd( aFds, {"DOMINIO" 	 	,"C",    200,000} )   
Aadd( aFds, {"SEM_XML"     	,"C",    003,000} )   
Aadd( aFds, {"GRV_DIF_A"   	,"C",    003,000} )
Aadd( aFds, {"VERSAO"		,"C",    003,000} )
Aadd( aFds, {"ESPECIE" 	 	,"C",    100,000} )	
Aadd( aFds, {"TAGPROT" 	 	,"C",    003,000} )	
Aadd( aFds, {"DESCFOR" 	 	,"C",    003,000} )		//	DESCRICAO PRODUTO FORNECEDOR
Aadd( aFds, {"DESCEMP" 	 	,"C",    003,000} )		//	DESCRICAO PRODUTO EMPRESA
Aadd( aFds, {"TAMDFOR" 	 	,"C",    003,000} )	
Aadd( aFds, {"TAMDEMP" 	 	,"C",    003,000} )	

		
//������������������������������������������������������������������������������������������Ŀ
//|																							 |
//|				QDO ADICIONAR CAMPO ALTERAR VARIAVEL cCfgVersao                              |
//|																							 |
//��������������������������������������������������������������������������������������������



//���������������������������������������������Ŀ
//�  VERFICA VERSAO DO ARQUIVO ZXM_CFG.DBF 		�
//�����������������������������������������������
If File(_StartPath+cArqConfig)

	If Select('TMPCFG') == 0
		DbUseArea(.T.,'CTREECDX',_StartPath+cArqConfig, 'TMPCFG',.T.,.F.)
	  	If !File(_StartPath+cIndConfig)						// TMPCFG.CDX
		  	DBCreateIndex('TMPCFG','EMPRESA+FILIAL', {|| 'EMPRESA+FILIAL' } )
		EndIf
	  	DbSetIndex(_StartPath+cIndConfig)
	EndIf
             

	If Len(aFds) > TMPCFG->(FCount())
		lNewVersao	:= .T.		
	EndIf


	DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
	                                                                
	
	//��������������������������������������������������������������Ŀ
	//� VERIFICA SE INDICE EXISTE, CASO NEGATIVO CRIA NOVO INDICE	 |
	//����������������������������������������������������������������	
	If !File(_StartPath+cIndConfig)						// TMPCFG.CDX
	  	DBCreateIndex('TMPCFG','EMPRESA+FILIAL', {|| 'EMPRESA+FILIAL' } )
	EndIf


	
	If lNewVersao
                
		//������������������������������������������Ŀ
		//� COPIA ZXM_CFG.DBF PARA ZXM_CFG_OLD.DBF	 |
		//��������������������������������������������
		_CopyFile(_StartPath+cArqConfig, _StartPath+cOldArq_Cfg)
		

		//�������������������������������Ŀ
		//� INCREMENTA CONTADOR DE VERSAO �
		//���������������������������������
	    c1 := Left(cCfgVersao,1)
	    c2 := Right(cCfgVersao,1)
		 If c2 == '9'    
			c1 := Val(c1); c1++
		 	cCfgVersao :=  Alltrim(Str(c1)) + '.0'
		 Else
			c2 := Val(c2); c2++
		 	cCfgVersao :=  c1+'.'+Alltrim(Str(c2))
		 EndIf

		MsgInfo('Foi criado novo(s) par�metro(s). Verifique arquivo de configura��o.')

   	ElseIf !lMostraTela
		Return(.T.)
	EndIf

EndIf


//���������������������������Ŀ
//�   ALIMENTA ZXM_CFG.DBF	  |
//�����������������������������
IF !File(_StartPath+cArqConfig) .Or. lNewVersao      //    \SYSTEM\XML_CFG.DBF
	
	IIF(Select('TMPCFG') != 0, TMPCFG->(DbCLoseArea()), )

	If lNewVersao                       
	
		//�������������������������������������������������������������������������Ŀ
		//�VERIFICA SE TEM ALGUEM UTILIZANDO O ARQUIVO - TENTA ABRIR EXCLUSIVO 		|
		//���������������������������������������������������������������������������
	   	DbUseArea(.T.,,_StartPath+cArqConfig, 'TMPCFG',.F.,.F.)		//	DBUSEAREA (<Lnome �rea>,<nome driver>, <arquivo> <apelido>,<Lconpartilhado>,<Lapenas leitura> ).
		If Select('TMPCFG') == 0
			Alert('O arquivo ZXM_CFG.DTC esta sendo utilizado.'+ENTER+'Feche o arquivo para poder continuar.')
			Return(.F.)
		Else	   
	
			IIF(Select('TMPCFG') != 0, TMPCFG->(DbCLoseArea()), )
			FErase(_StartPath+cArqConfig)	//	APAGA ZXM_CFG.DBF
			DbUseArea(.T.,,_StartPath+cOldArq_Cfg, 'TMPCFG_OLD',.T.,.F.)		//	 ABRE ZXM_CFG_OLD.DBF
		   	DBCreateIndex('TMPCFG_OLD','EMPRESA+FILIAL', {|| 'EMPRESA+FILIAL' })
			Aadd(aArqTemp, _StartPath+'TMPCFG_OLD.CDX')   

		EndIf
		
	EndIf
		
	DbCreate(_StartPath+cArqConfig, aFds )	// CRIA NOVO ZXM_CFG.DBF 
	DbUseArea(.T.,,_StartPath+cArqConfig, 'TMPCFG',.T.,.F.)
  	DBCreateIndex('TMPCFG','EMPRESA+FILIAL', {|| 'EMPRESA+FILIAL' } )
	

	DbSelectArea('SM0');DbGoTop()
	Do While !Eof()   

		If lNewVersao  // __cRdd
			DbSelectArea('TMPCFG_OLD');DbSetOrder(1);DbGoTop()
			DbSeek(SM0->M0_CODIGO + SM0->M0_CODFIL, .F.)
		EndIf

			***************************
				GET_MV_ESPECIE()	//	BUSCA SERIE == SPED
			***************************

		//�������������������������������������������������������������������������Ŀ
		//�   						ALIMENTA ZXM_CFG.DBF	  						|
		//| SE ZXM_CFG FOR NOVA VERSAO JOGA O CONTEUDO OLD PARA O NOVO ZXM_CFG		|
		//| CASO CONTRARIO JOGA AS VARIAVEIS										|
		//���������������������������������������������������������������������������
		DbSelectArea('TMPCFG') 
       	RecLock('TMPCFG',.T.)
           	TMPCFG->VERSAO			:=	AllTrim(cCfgVersao)
           	TMPCFG->EMPRESA			:=	AllTrim(SM0->M0_CODIGO)
           	TMPCFG->NOME_EMP		:=	AllTrim(SM0->M0_NOME)
           	TMPCFG->FILIAL			:=	AllTrim(SM0->M0_CODFIL)
           	TMPCFG->NOME_FIL		:=	AllTrim(SM0->M0_FILIAL)
           	TMPCFG->CNPJ			:=	AllTrim(SM0->M0_CGC)
           	
           	TMPCFG->PATCHTABLE		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PATCHTABLE"))>0,	TMPCFG_OLD->PATCHTABLE,		AllTrim(cPathTables))
           	TMPCFG->EMAIL			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("EMAIL"))>0		, 	TMPCFG_OLD->EMAIL,			AllTrim(cEmail))
           	TMPCFG->SENHA			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SENHA"))>0		, 	TMPCFG_OLD->SENHA,			AllTrim(cPassword))
           	TMPCFG->PASTA_ORIG		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PASTA_ORIG"))>0, 	TMPCFG_OLD->PASTA_ORIG,		AllTrim(cPOrigem)	)
           	TMPCFG->PASTA_DEST		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PASTA_DEST"))>0, 	TMPCFG_OLD->PASTA_DEST,		AllTrim(cPDestino))
           	TMPCFG->TIPOEMAIL		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("TIPOEMAIL"))>0	, 	TMPCFG_OLD->TIPOEMAIL,		AllTrim(cTpMail))
           	TMPCFG->SERVERENV		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SERVERENV"))>0	, 	TMPCFG_OLD->SERVERENV,		AllTrim(cServerEnv))
           	TMPCFG->SERVERREC		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SERVERREC"))>0	, 	TMPCFG_OLD->SERVERREC,		AllTrim(cServerRec))           	
           	TMPCFG->NFE_9DIG		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("NFE_9DIG"))>0	, 	TMPCFG_OLD->NFE_9DIG,		'SIM'	)
           	TMPCFG->SER_3DIG		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SER_3DIG"))>0	, 	TMPCFG_OLD->SER_3DIG,		'SIM'	)
           	TMPCFG->SER_TRANSF		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SER_TRANSF"))>0, 	TMPCFG_OLD->SER_TRANSF,		AllTrim(cSerTransf))
           	TMPCFG->PRE_DOC			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PRE_DOC"))>0	,	TMPCFG_OLD->PRE_DOC,		AllTrim(cPreDoc))
           	TMPCFG->PORTA_REC		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PORTA_REC"))>0	, 	TMPCFG_OLD->PORTA_REC,		AllTrim(cPortRec))
           	TMPCFG->PORTA_ENV		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PORTA_ENV"))>0	, 	TMPCFG_OLD->PORTA_ENV,		AllTrim(cPortEnv))
           	TMPCFG->ESTRUT_ZXM		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("ESTRUT_ZXM"))>0, 	TMPCFG_OLD->ESTRUT_ZXM,		lEstrut_Xml	)
		   	TMPCFG->AUTENTIFIC		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("AUTENTIFIC"))>0, 	TMPCFG_OLD->AUTENTIFIC,		lAutent )
		   	TMPCFG->SSL				:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SSL"))>0		, 	TMPCFG_OLD->SSL ,			lSSL )
		   	TMPCFG->PATH_XML		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PATH_XML"))>0	, 	TMPCFG_OLD->PATH_XML ,		AllTrim(cPath_XML) )
		   	TMPCFG->PROB_EMAIL		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("PROB_EMAIL"))>0, 	TMPCFG_OLD->PROB_EMAIL,		AllTrim(cProbEmail))
		   	TMPCFG->FOR_CANC		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("FOR_CANC"))>0	, 	TMPCFG_OLD->FOR_CANC,		AllTrim(cXmlCancFor))
		   	TMPCFG->DOMINIO			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("DOMINIO"))>0	, 	TMPCFG_OLD->DOMINIO,		AllTrim(cDominio) )
			TMPCFG->SEM_XML			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SEM_XML"))>0	, 	TMPCFG_OLD->SEM_XML,		AllTrim(cSem_XML) )
			TMPCFG->GRV_DIF_A		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("GRV_DIF_A"))>0	, 	TMPCFG_OLD->GRV_DIF_A,		AllTrim(cGrv_Dif_A) )
			TMPCFG->SEGURA    		:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("SEGURA"))>0	, 	TMPCFG_OLD->SEGURA,			lSegura )
		   	TMPCFG->TLS				:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("TLS"))>0		, 	TMPCFG_OLD->TLS ,			lTLS )
			TMPCFG->ESPECIE			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("ESPECIE"))>0	, 	TMPCFG_OLD->ESPECIE,		cVerifEsp )
			TMPCFG->TAGPROT			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("TAGPROT"))>0	, 	TMPCFG_OLD->TAGPROT,		cTagProt  )			
			TMPCFG->DESCFOR			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("DESCFOR"))>0	, 	TMPCFG_OLD->DESCFOR,		cDescFor  )
			TMPCFG->DESCEMP			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("DESCEMP"))>0	, 	TMPCFG_OLD->DESCEMP,		cDescEmp  )
			TMPCFG->TAMDFOR			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("TAMDFOR"))>0	, 	TMPCFG_OLD->TAMDFOR,		cTamDFor  )
			TMPCFG->TAMDEMP			:=	IIF(lNewVersao .And. TMPCFG_OLD->(FieldPos("TAMDEMP"))>0	, 	TMPCFG_OLD->TAMDEMP,		cTamDEmp  )
	   MsUnLock()
	   
       DbSelectArea('SM0')
       DbSkip()
   EndDo

   	If lNewVersao
	   IIF(Select('TMPCFG_OLD') != 0, TMPCFG_OLD->(DbCLoseArea()), )
	EndIf
	
EndIf


//���������������������������������Ŀ
//�        ALIMENTA aConfig   		�
//| aConfig == Itens MsNewGetDados	|
//�����������������������������������
nRecno := Recno()
DbSelectArea('SM0');DbGoTop()
Do While !Eof()
   DbSelectArea('TMPCFG');DbGoTop()
   If DbSeek(SM0->M0_CODIGO + SM0->M0_CODFIL, .F.)
		cCfgVersao	:= 	TMPCFG->VERSAO
       	cPathTables	:= 	TMPCFG->PATCHTABLE
       	cEmail		:=	TMPCFG->EMAIL
       	cPassword	:=	TMPCFG->SENHA
       	cPOrigem	:=	TMPCFG->PASTA_ORIG
       	cPDestino	:=	TMPCFG->PASTA_DEST
       	cTpMail		:=	TMPCFG->TIPOEMAIL
       	cServerEnv	:=	TMPCFG->SERVERENV
       	cServerRec	:=	TMPCFG->SERVERREC
       	cNFe9Dig	:=	TMPCFG->NFE_9DIG
       	cSer3Dig	:=	TMPCFG->SER_3DIG
       	cSerTransf	:=	TMPCFG->SER_TRANSF
       	cPreDoc		:=	TMPCFG->PRE_DOC
       	cPortRec	:=	TMPCFG->PORTA_REC
       	cPortEnv	:=	TMPCFG->PORTA_ENV    
        lEstrut_Xml	:=	TMPCFG->ESTRUT_ZXM
		lAutent		:=	TMPCFG->AUTENTIFIC
		lSegura		:=	TMPCFG->SEGURA
		lSSL		:=	TMPCFG->SSL
		lTLS		:=	TMPCFG->TLS		
		cPath_XML	:=	TMPCFG->PATH_XML
		cProbEmail	:=	TMPCFG->PROB_EMAIL
		cXmlCancFor	:=	TMPCFG->FOR_CANC
		cDominio	:=	TMPCFG->DOMINIO
		cSem_XML	:=	TMPCFG->SEM_XML
		cGrv_Dif_A	:=	TMPCFG->GRV_DIF_A
		cVerifEsp 	:= 	TMPCFG->ESPECIE
		cTagProt	:=	TMPCFG->TAGPROT
		cDescFor	:=	TMPCFG->DESCFOR
		cDescEmp	:=	TMPCFG->DESCEMP
		cTamDFor	:=	TMPCFG->TAMDFOR
		cTamDEmp	:=	TMPCFG->TAMDEMP
   Else

       	cPathTables :=  _cPathTable  
       	cEmail      :=  Space(100)
       	cPassword   :=  Space(100)
       	cPOrigem    :=  Space(100)
       	cPDestino   :=  Space(100)
       	cServerEnv	:=  Alltrim(GetMv('MV_RELSERV'))+    Space(100)
       	cServerRec	:=  Alltrim(GetMv('MV_RELSERV'))+    Space(100)       	
       	cTpMail     :=  _cMail
       	cNFe9Dig    :=  'SIM'
       	cSer3Dig    :=  'SIM'
       	cSerTransf  :=  Space(TamSX3('F1_SERIE')[1])
       	cPreDoc     :=  'PRE'
       	cPortEnv    :=  Space(03)
       	cPortRec    :=  Space(03)
        lEstrut_Xml	:=	.F.
		cPath_XML	:=	Space(100)
		cProbEmail	:=	Space(100)
		cXmlCancFor	:=	Space(100)
		lAutent		:=	.F.
		lSegura		:=	.F.
		lSSL		:=	.F.
		lTLS		:=	.F.
		cDominio	:=	Space(100)
		cSem_XML	:=	'N�O'
		cGrv_Dif_A	:=	'SIM'
		cVerifEsp 	:= 	Space(100)
		cTagProt	:=	'N�O'
		cDescFor	:=	'SIM'
		cDescEmp	:=	'N�O'
		cTamDFor	:=	'120'
		cTamDEmp	:=	AllTrim(Str(TamSX3('B1_DESC')[1]))
   EndIf                                           

	//��������������������������Ŀ
	//�      ARRAY aConfig       �
	//����������������������������
   DbSelectArea('SM0')
   Aadd(aConfig, {     SM0->M0_CODIGO,    SM0->M0_NOME,        SM0->M0_CODFIL,    SM0->M0_FILIAL,    SM0->M0_CGC,        SM0->M0_INSC,;    //06
                       SM0->M0_ENDENT,    SM0->M0_COMPENT,    SM0->M0_BAIRENT,    SM0->M0_CIDENT,    SM0->M0_ESTENT,    SM0->M0_CEPENT,;  //12
                       SM0->M0_TEL,        SM0->M0_FAX,        SM0->M0_ENDCOB,    SM0->M0_BAIRCOB,    SM0->M0_CIDCOB,    SM0->M0_ESTCOB,;  //18
                       SM0->M0_CEPCOB,    SM0->M0_CNAE,        SM0->M0_CODMUN,    cTpMail, cPathTables  ,cEmail ,cPassword ,cPOrigem,cPDestino,;//27
                       cServerEnv,  cNFe9Dig,cSer3Dig,cSerTransf,cPreDoc, cPortEnv,cPortRec, lAutent , lSSL , cPath_XML,;	//	37
                       cProbEmail,  cXmlCancFor, cDominio, cSem_XML, cGrv_Dif_A, lSegura, lTLS, cServerRec,,cVerifEsp,;	//	47
                       cTagProt, 	cDescFor, 	cDescEmp,  cTamDFor, cTamDEmp, .F. })    // 52 + .F.

	DbSkip()
EndDo

DbGoTo(nRecno)


//��������������������������Ŀ
//�   TELA DE CONFIGURACAO   �
//����������������������������
oFont1     := TFont():New( "Arial",0,IIF(nHRes<=800,-10,-12),,.T.,0,,700,.F.,.F.,,,,,, )
oFont2     := TFont():New( "Arial",0,IIF(nHRes<=800,-15,-17),,.T.,0,,700,.F.,.F.,,,,,, )
oFont3     := TFont():New( "Arial",0,IIF(nHRes<=800,-08,-10),,.T.,0,,700,.F.,.F.,,,,,, )

oDlgSM0    := MSDialog():New( D(050),D(050),D(485),D(720),"Configura��o Empresas Importa��o XML",,,.F.,,,,,,.T.,,oFont1,.T. )

oGrp1      := TGroup():New( D(004),D(004),D(088),D(330),"",oDlgSM0,CLR_BLACK,CLR_WHITE,.T.,.F. )
oBrwSM0    := MsNewGetDados():New( D(010),D(008),D(084),D(320),GD_UPDATE,'AllwaysTrue()','AllwaysTrue()','',,0,999,,'','AllwaysTrue()',oGrp1,aMyHeader,aConfig )


                              
//��������������������������Ŀ
//�   	    FOLDERs			 �
//����������������������������
oFolder    := TFolder():New( D(092),D(008),{"Config. Empresa","Config. XML Fornec.","Config. e-mail","Config.Extras"},{},oGrp1,,,,.T.,.F.,D(320),D(108),)
oFolder:bSetOption	:=	{|| IIF(oFolder:nOption==1, oCBox4:SetFocus(), IIF(oFolder:nOption==2, oCBox9:SetFocus(), oCBox14:SetFocus()))}



//��������������������������Ŀ
//�     CONFIG.EMPRESA       �
//����������������������������
//[ EMPRESA \ FILIAL ]
oSay1NomeFil := TSay():New( D(004),D(008),{|| Alltrim(aConfig[oBrwSM0:nAT][02])+' \ '+AllTrim(aConfig[oBrwSM0:nAT][04]) },oFolder:aDialogs[1],,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(008) )

oGrp2      := TGroup():New( D(012),D(006),D(095),D(170),"Config. Empresa",oFolder:aDialogs[1],CLR_BLACK,CLR_WHITE,.T.,.F. )


cToolTip  := 'Informar a quantidade de horas, conforme a SEFAZ de cada estado determina, para possibilitar o cancelamento da NFe.'+ENTER
cToolTip  += 'Par�metro utilizado para verificar se fornecedor cancelou o XML.'+ENTER
cToolTip  += 'Caso o XML esteje Importado\Gravado, e o fornecedor cancelou o XML ANTES desse per�odo, � enviado um e-mail,'+ENTER
cToolTip  += 'para determinado(s) usu�rio(s) informando o cancelamento (e-mail disparado pelo workflow).'+ENTER
cToolTip  += 'Par�metro MV_SPEDEXC.'
oSay1  := TSay():New( D(020),D(008),{||"Num. Horas Canc.XML"},oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(100),D(008) )
oGet1  := TGet():New( D(026),D(008),{|u| If(PCount()>0,nHoraCanc:=u,nHoraCanc)},oFolder:aDialogs[1],D(060),D(008),'',/*{||PutMv( 'XM_SPEDEXP', nHoraCanc)}*/,CLR_HGRAY,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)
oGet1:Disable()
                            

cToolTip  := 'Caminho dos arquivos XML.'+ENTER+'Utilizado na op��o de carregar XML para selecionar o diret�rio.'
oSay2  := TSay():New( D(020),D(070),{||"Caminho arq. XML"},oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(100),D(008) )
oBtn2  := TButton():New( D(026),D(160),"...",oFolder:aDialogs[1],{|| cPath_XML  := cGetFile("Anexos (*xml)|*xml|","Arquivos (*xml)",0,'',.T.,GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_MULTISELECT+GETF_RETDIRECTORY), aConfig[oBrwSM0:nAT][37]:= AllTrim(cPath_XML), cPath_XML+=Space(100), oGet2:Refresh() },D(010),D(010),,,,.T.,,"",,,,.F. )
oGet2  := TGet():New( D(026),D(070),{|u| If(PCount()>0,cPath_XML:=u,cPath_XML)},oFolder:aDialogs[1],D(090),D(008),'',{|| aConfig[oBrwSM0:nAT][37]:= AllTrim(cPath_XML), cPath_XML+=Space(100),oGet2:Refresh() },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)


cToolTip   := 'S�rie da NF Sa�da que a Filial utiliza.'+ENTER+'Par�metro utiliza os XML�s que forem de transferencia.'
oSay3 := TSay():New( D(040),D(008),{|| "S�rie NF Sa�da" },oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008) )
oGet3 := TGet():New( D(046),D(008),{|u| If(PCount()>0,cSerTransf:=u,cSerTransf) },oFolder:aDialogs[1],D(060),D(008),'', {|| aConfig[oBrwSM0:nAT][31]:=cSerTransf, oGet3:Refresh() }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)

oSay4 := TSay():New( D(040),D(070),{|| 'Gerar Pr�-Nota\Doc.Entr' },oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(090),D(008) )
oCBox4:= TComboBox():New( D(046),D(070),{|u| If(PCount()>0,cPreDoc:=u,IIF(cPreDoc=='PRE','PRE-NOTA','DOC.ENTRADA'))},{'PRE-NOTA','DOC.ENTRADA'},D(060),D(012),oFolder:aDialogs[1],,,{|| aConfig[oBrwSM0:nAT][32]:=cPreDoc},CLR_BLACK,CLR_WHITE,.T.,oFont1,"Pre-Nota\Doc.Entrada",,,,,,, )
oCBox4:cToolTip := 'Gerar Pr�-Nota ou Doc.Entrada ap�s realizar o vinculo Produto X Fornecedor'                    


oSay5	:= TSay():New( D(060),D(008),{|| 'Inclui Pr�\Doc sem XML?' },oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008) )
//oCBox5  	:= TComboBox():New( D(066),D(008),{|u| If(PCount()>0,cSem_XML:=u,cSem_XML)},{"SIM","N�O"},D(060),D(012),oFolder:aDialogs[1],,,{|| aConfig[oBrwSM0:nAT][41]:=cSem_XML, IIF(cSem_XML=='SIM',(oGetTp:Enable(),oGetTp:SetColor(CLR_BLACK,CLR_WHITE),aConfig[oBrwSM0:nAT][47]:=cVerifEsp), (oGetTp:Disable(),oGetTp:SetColor(CLR_BLACK,CLR_HGRAY),aConfig[oBrwSM0:nAT][47]:='',cVerifEsp:=Space(100))), oGetTp:Refresh() },CLR_BLACK,CLR_WHITE,.T.,oFont1,"",,,,,,, )
oCBox5  	:= TComboBox():New( D(066),D(008),{|u| If(PCount()>0,cSem_XML:=u,cSem_XML)},{"SIM","N�O"},D(060),D(012),oFolder:aDialogs[1],,,{|| aConfig[oBrwSM0:nAT][41]:=cSem_XML },CLR_BLACK,CLR_WHITE,.T.,oFont1,"",,,,,,, )
cToolTip	:=	'Inclui Pr�-Nota\Doc.Entrada sem XML ?'+ENTER+'Utilizado na inclus�o da Pr�-Nota\Doc.Entrada.'+ENTER+'Dever� ser informado um motivo da inclus�o da Pr�-Nota\Doc.Entrada sem o XML.'
oCBox5:cToolTip := cToolTip

oSay6	:= TSay():New( D(060),D(070),{|| 'Pr�\Doc com Status <> Aprovado?' },oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(100),D(008) )
oCBox6  	:= TComboBox():New( D(066),D(070),{|u| If(PCount()>0,cGrv_Dif_A:=u,cGrv_Dif_A)},{"SIM","N�O"},D(060),D(012),oFolder:aDialogs[1],,,{|| aConfig[oBrwSM0:nAT][42]:=cGrv_Dif_A },CLR_BLACK,CLR_WHITE,.T.,oFont1,"",,,,,,, )
cToolTip := 'Grava Pr�-Nota\Doc.Entrada com Status <> Aprovado\Cancelado?'+ENTER+'Caso ocorra problema na consulta do XML na SEFAZ e n�o retornar nenhum status � possivel incluir a Pr�-Nota\Doc.Entrada.'
oCBox6:cToolTip := cToolTip

// CASO NAO = INCLUI DOC. SEM XML
cToolTip:= "Especifique as Esp�cies de Documento (Ex.: SPED) que o sistema deve verificar para que n�o haja Pr�-Nota\Doc.Entrada sem XML. Separe por ; "
oSayTp:= TSay():New( D(077),D(008),{|| "Verificar apenas a(s) seguinte(s) Esp�cies:" 	},oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(100),D(008) )
oGetTp:= TGet():New( D(083),D(008),{|u| If(PCount()>0,cVerifEsp:=u,cVerifEsp)	},oFolder:aDialogs[1],D(152),D(008),'', {|| aConfig[oBrwSM0:nAT][47]:=cVerifEsp, oGetTp:Refresh() }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)



oGrp3      := TGroup():New( D(012),D(176),D(095),D(312),"Recebem E-mail quando...",oFolder:aDialogs[1],CLR_BLACK,CLR_WHITE,.T.,.F. )

cToolTip  := 'Quando � disparado um e-mail e ocorre algum problema (e-mail destinat�rio inv�lido),'+ENTER
cToolTip  += '� poss�vel enviar um e-mail, para determinado(s) usu�rio(s), e informar que o e-mail n�o foi enviado para o(s) destinat�rio(s).'+ENTER
cToolTip  += 'Ex.: Ao recusar um XML e enviado um e-mail ao fornecedor e ocorrer algum erro no envio.'+ENTER
cToolTip  += 'Obs.: colocar apenas o nome da conta sem @. Coloque ";" para adicionar mais contas.'
oSay7  := TSay():New( D(020),D(180),{||"Ocorrer problema no envio dos e-mails"},oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(170),D(008) )
oGet7  := TGet():New( D(026),D(180),{|u| If(PCount()>0,cProbEmail:=u,cProbEmail)},oFolder:aDialogs[1],D(130),D(008),'',{|| aConfig[oBrwSM0:nAT][38]:= AllTrim(cProbEmail), cProbEmail+=Space(100), oGet7:Refresh()  /*PutMv( 'XM_ADMIN_E', cProbEmail )*/  },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)

cToolTip  := 'Conta de e-mail do(s) usu�rio(s) para receber e-mail informando que XML foi cancelado pelo fornecedor.'
cToolTip  += 'Obs.: colocar apenas o nome da conta sem @. Coloque ";" para adicionar mais contas.'
oSay8  := TSay():New( D(040),D(180),{||"Fornecedor cancelar XML antes do prazo"},oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(170),D(008) )
oGet8  := TGet():New( D(046),D(180),{|u| If(PCount()>0,cXmlCancFor:=u,cXmlCancFor)},oFolder:aDialogs[1],D(130),D(008),'',{|| aConfig[oBrwSM0:nAT][39]:= AllTrim(cXmlCancFor), cXmlCancFor+=Space(100),oGet8:Refresh() /*PutMv( 'XM_XMLCANC', cEmailCancXML )*/ },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)

cToolTip  := 'Dom�nio da Empresa.'+ENTER+'Utilizado para enviar e-mail.'+ENTER
cToolTip  += 'Ex.: colocar empresaXYZ.com.br sem o nome da conta, sem @.'
oSay9  := TSay():New( D(060),D(180),{||"Dominio Empresa"},oFolder:aDialogs[1],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(170),D(008) )
oGet9  := TGet():New( D(066),D(180),{|u| If(PCount()>0,cDominio:=u,cDominio)},oFolder:aDialogs[1],D(130),D(008),'',{|| aConfig[oBrwSM0:nAT][40]:= AllTrim(cDominio), cXmlCancFor+=Space(100),oGet9:Refresh() },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)



//��������������������������Ŀ
//�   CONFIG. FORNECEDOR	 |
//����������������������������
//[ EMPRESA \ FILIAL ]
oSay2NomeFil := TSay():New( D(004),D(008),{|| Alltrim(aConfig[oBrwSM0:nAT][02])+' \ '+AllTrim(aConfig[oBrwSM0:nAT][04]) },oFolder:aDialogs[2],,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(008) )

oGrp4      := TGroup():New( D(012),D(006),D(045),D(170),"XML Fornecedor",oFolder:aDialogs[2],CLR_BLACK,CLR_WHITE,.T.,.F. )

oSay9      := TSay():New( D(020),D(008),{||"Nota com 9 D�gitos"},oFolder:aDialogs[2],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008) )
oCBox9  := TComboBox():New( D(026),D(008),{|u| If(PCount()>0,cNFe9Dig:=u,cNFe9Dig)},{"SIM","N�O"},076,012,oFolder:aDialogs[2],,,{|| aConfig[oBrwSM0:nAT][29]:=cNFe9Dig },CLR_BLACK,CLR_WHITE,.T.,oFont1,"Nota com 9 Digitos",,,,,,, )
oCBox9:cToolTip := 'Completa com "0" Zeros o restante dos caracteres da Pre-Nota\Doc.Entrada.'

oSayS10  := TSay():New( D(020),D(070),{|| "S�rie com 3 D�gitos"},oFolder:aDialogs[2],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008) )
oCBox10  := TComboBox():New( D(026),D(070),{|u| If(PCount()>0,cSer3Dig:=u,cSer3Dig)},{"SIM","N�O"},076,012,oFolder:aDialogs[2],,,{|| aConfig[oBrwSM0:nAT][30]:=cSer3Dig },CLR_BLACK,CLR_WHITE,.T.,oFont1,"S�rie com 3 Digitos",,,,,,, )
oCBox10:cToolTip := 'Completa com "0" Zeros o restante dos caracteres da Pre-Nota\Doc.Entrada.'



//��������������������������Ŀ
//�      CONFIG. EMAIL       �
//����������������������������
//	[ EMPRESA \ FILIAL  ]
oSay3NomeFil := TSay():New( D(004),D(008),{|| Alltrim(aConfig[oBrwSM0:nAT][02])+' \ '+AllTrim(aConfig[oBrwSM0:nAT][04]) },oFolder:aDialogs[3],,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(008) )



oGrp5      := TGroup():New( D(012),D(006),D(095),D(315),"",oFolder:aDialogs[3],CLR_BLACK,CLR_WHITE,.T.,.F. )

oCBox11 := TCheckBox():New( D(014),D(142),"Autentifica��o?",{|u| If(PCount()>0,lAutent:=u,lAutent)},oFolder:aDialogs[3],D(068),D(008),,		{|| aConfig[oBrwSM0:nAT][35]:= lAutent 	},,,CLR_BLACK,CLR_WHITE,,.T.,"",, )
oCBox11:cToolTip := 'Utiliza Autentifica��o ?'
oCBox12    := TCheckBox():New( D(014),D(215),"Conex�o segura?",{|u| If(PCount()>0,lSegura:=u,lSegura)},oFolder:aDialogs[3],D(088),D(008),,	{|| aConfig[oBrwSM0:nAT][43]:= lSegura, PortPadrao(), IIF(lSegura,(oCBox12b:Show(),oCBox12c:Show()),(oCBox12b:Hide(),oCBox12c:Hide()) ),(lSSL:=.F.,aConfig[oBrwSM0:nAT][36]:=.F.,lTLS:=.F.,aConfig[oBrwSM0:nAT][44]:=.F.), oGet21:Refresh(), oGet20:Refresh()	},,,CLR_BLACK,CLR_WHITE,,.T.,"",, )
oCBox12:cToolTip := 'Utiliza conex"ao segura ?'

oCBox12b    := TCheckBox():New( D(014),D(275),"SSL",{|u| If(PCount()>0,lSSL:=u,lSSL)},oFolder:aDialogs[3],D(088),D(008),,{|| IIF( !VerBuild(),lSSL:=.F., (PortPadrao(), IIF(lSSL,lTLS:=.F.,IIF(lSSL,lSSL:=.F.,lSSL:=.T.)), aConfig[oBrwSM0:nAT][36]:= lSSL, oCBox12b:Refresh(), oCBox12c:Refresh(), oGet21:Refresh(), oGet20:Refresh()) )},,,CLR_BLACK,CLR_WHITE,,.T.,"",, )
oCBox12c    := TCheckBox():New( D(014),D(295),"TLS",{|u| If(PCount()>0,lTLS:=u,lTLS)},oFolder:aDialogs[3],D(088),D(008),,{|| IIF( !VerBuild(),lTLS:=.F., (PortPadrao(), IIF(lTLS,lSSL:=.F.,IIF(lTLS,lTLS:=.F.,lTLS:=.T.)), aConfig[oBrwSM0:nAT][44]:= lTLS, oCBox12b:Refresh(), oCBox12c:Refresh(), oGet21:Refresh(), oGet20:Refresh()) )},,,CLR_BLACK,CLR_WHITE,,.T.,"",, )

oSay13  := TSay():New( D(022),D(008),{||"Path Table"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(060),D(008) )
oGet13  := TGet():New( D(028),D(008),{|u| If(PCount()>0,cPathTables:=u,cPathTables)},oFolder:aDialogs[3],D(060),D(008),'',{|| aConfig[oBrwSM0:nAT][23]:=cPathTables },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"Path Table - Diret�rio onde esta localizado os arquivos SX�s",,,.F.,.F.,,.F.,.F.,"","cPathTables",,)


oSay14:= TSay():New( D(022),D(075),{||"Protocolo de E-mail"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008))
oCBox14  := TComboBox():New( D(028),D(075),{|u| If(PCount()>0,cTpMail:=u,cTpMail)},{"POP","IMAP","MAPI"},D(060),D(012),oFolder:aDialogs[3],,,{|| PortPadrao(), aConfig[oBrwSM0:nAT][22]:=cTpMail, IIF(cTpMail=='IMAP',(oGet18:Enable(),oGet18:SetColor(CLR_BLACK,CLR_WHITE)),(oGet18:Disable(),aConfig[oBrwSM0:nAT][27]:=cPDestino:=Space(100),oGet18:SetColor(CLR_BLACK,CLR_HGRAY)) ), oGet18:Refresh() },CLR_BLACK,CLR_WHITE,.T.,oFont1,"",,,,,,, )
oCBox14:cToolTip := 'Indica o protocolo de e-mail que ser� usado para envio de e-mail.'

cToolTip   := 'O par�metro permite informar a conta de e-mail que ser� acessada para baixar os XML e enviar e-mail de notifica��o. '+ENTER
cToolTip   += 'No entanto, dependendo da configura��o do servidor, � necess�rio informar'+ENTER
cToolTip   += 'a conta de e-mail completa (usuario@provedor.com.br) ou apenas o nome do usu�rio.
oSay15  := TSay():New( D(022),D(142),{|| "E-mail"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008) )
oGet15  := TGet():New( D(028),D(142),{|u| If(PCount()>0,cEmail:=u,cEmail) },oFolder:aDialogs[3],D(125),D(008),'', {|| aConfig[oBrwSM0:nAT][24]:=cEmail }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)

oSay16  := TSay():New( D(022),D(270),{||"Senha"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008))                                                                                                         //.T.== ****
oGet16  := TGet():New( D(028),D(270),{|u| If(PCount()>0,cPassword:=u,cPassword)},oFolder:aDialogs[3],D(045),D(008),'',{|| aConfig[oBrwSM0:nAT][25]:=cPassword },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"Senha da conta de e-mail",,,.F.,.F.,,.F.,.T.,"","",,)

cToolTip   := 'Pasta de Origem.'+ENTER+'Onde o programa ir� LER os e-mails e buscar os XML�s.'+ENTER
oSay17  := TSay():New( D(042),D(008),{|| "Pasta Origem E-mail"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008))
oGet17  := TGet():New( D(048),D(008),{|u| If(PCount()>0,cPOrigem:=u,cPOrigem) },oFolder:aDialogs[3],D(060),D(008),'', {|| aConfig[oBrwSM0:nAT][26]:=cPOrigem }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)

cToolTip   := 'Pasta de Destino.'+ENTER+'Onde o programa ir� MOVER os e-mails ap�s importa-los para o sistema. '+ENTER+'Op��o apenas para Protocolo IMAP.'
oSay18  := TSay():New( D(042),D(075),{|| "Pasta Destino E-mail"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008) )
oGet18  := TGet():New( D(048),D(075),{|u| If(PCount()>0,cPDestino:=u,cPDestino) },oFolder:aDialogs[3],D(060),D(008),'', {|| aConfig[oBrwSM0:nAT][27]:=cPDestino, oGet18:Refresh() }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,cToolTip,,,.F.,.F.,,.F.,.F.,"","",,)


	//���������������������Ŀ
	//�  SERVIDOR DE ENVIO  �
	//�����������������������
oSay19 := TSay():New( D(042),D(142),{|| "Servidor de Envio"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008) )
oGet19 := TGet():New( D(048),D(142),{|u| If(PCount()>0,cServerEnv:=u,cServerEnv) },oFolder:aDialogs[3],D(125),D(008),'', {|| aConfig[oBrwSM0:nAT][28]:=cServerEnv }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,'Indica o endere�o ou alias do servidor de e-mail IMAP/POP/MAPI/SMTP.',,,.F.,.F.,,.F.,.F.,"","",,)

oSay20  := TSay():New( D(042),D(270),{||"Porta SMTP" },oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008) )
oGet20  := TGet():New( D(048),D(270),{|u| If(PCount()>0,cPortEnv:=u,cPortEnv)},oFolder:aDialogs[3],D(020),D(008),'',{|| aConfig[oBrwSM0:nAT][33]:= cPortEnv },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,'Indica a porta de comunica��o para conex�o IMAP/POP/MAPI/SMTP.',,,.F.,.F.,,.F.,.F.,"","",,)


	//���������������������������Ŀ
	//�  SERVIDOR DE RECEBIMENTO  �
	//�����������������������������
oSay19b := TSay():New( D(060),D(142),{|| "Servidor de Recebimento"},oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(088),D(008))
oGet19b := TGet():New( D(066),D(142),{|u| If(PCount()>0,cServerRec:=u,cServerRec) },oFolder:aDialogs[3],D(125),D(008),'', {|| aConfig[oBrwSM0:nAT][45]:=cServerRec }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,'Indica o endere�o ou alias do servidor de e-mail IMAP/POP/MAPI/SMTP.',,,.F.,.F.,,.F.,.F.,"","",,)

oSay21  := TSay():New( D(060),D(270),{||"Porta " + aConfig[oBrwSM0:nAT][22] },oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008) )
oGet21  := TGet():New( D(066),D(270),{|u| If(PCount()>0,cPortRec:=u,cPortRec)},oFolder:aDialogs[3],D(020),D(008),'',{|| aConfig[oBrwSM0:nAT][34]:= cPortRec },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,'Indica a porta de comunica��o para conex�o SMTP (Padr�o 25).',,,.F.,.F.,,.F.,.F.,"","",,)


oCBox22b    := TCheckBox():New( D(070),D(010),"Envio",{|u| If(PCount()>0,lTesteE:=u,lTesteE)},oFolder:aDialogs[3],D(088),D(008),,	{|| IIF(lTesteE, lTesteR:=.F.,), (cConect:='',oOkConect:Refresh(),oNoConect:Refresh()), oCBox22b:Refresh(),oCBox22c:Refresh() },,,CLR_BLACK,CLR_WHITE,,.T.,"",, )
oCBox22c    := TCheckBox():New( D(070),D(030),"Recebimento",{|u| If(PCount()>0,lTesteR:=u,lTesteR)},oFolder:aDialogs[3],D(088),D(008),,	{|| IIF(lTesteR, lTesteE:=.F.,), (cConect:='',oOkConect:Refresh(),oNoConect:Refresh()), oCBox22b:Refresh(),oCBox22c:Refresh() 	},,,CLR_BLACK,CLR_WHITE,,.T.,"",, )
oBtnTest    := TButton():New( D(080),D(008),"Testar Conex�o",oFolder:aDialogs[3],{|| IIF(lTesteE.Or.lTesteR, MsgRun('Testando Conex�o...', 'Aguarde... ',{|| cConect := TestConect(IIF(lTesteE,'E','R')), IIF(cConect=='Conectado com Sucesso',(oOkConect:Show(),oNoConect:Hide()),(oNoConect:Show(),oOkConect:Hide())) }), MsgInfo('Marque uma op��o - Envio ou Recebimento')) },D(057),D(012),,oFont3,,.T.,,"",,,,.F. )

oOkConect  := TSay():New( D(085),D(070),{|u| If(PCount()>0,cConect:=u,cConect) },oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(200),D(008) )
oNoConect  := TSay():New( D(085),D(070),{|u| If(PCount()>0,cConect:=u,cConect) },oFolder:aDialogs[3],,oFont1,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE, D(200),D(008) )
oOkConect:Hide()
oNoConect:Hide()



//��������������������������Ŀ
//�***** CONFIG.EXTRAS ******�
//����������������������������
//[ EMPRESA \ FILIAL ]
oSay1NomeFil := TSay():New( D(004),D(008),{|| Alltrim(aConfig[oBrwSM0:nAT][02])+' \ '+AllTrim(aConfig[oBrwSM0:nAT][04]) },oFolder:aDialogs[4],,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(008) )

oGrp6       := TGroup():New( D(012),D(006),D(095),D(315),"Config. Extras",oFolder:aDialogs[4],CLR_BLACK,CLR_WHITE,.T.,.F. )

oSayE2      := TSay():New( D(020),D(015),{||"Verifica TAG de Protocolo no XML ?"},oFolder:aDialogs[4],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(098),D(008) )
oCBoxE21  	:= TComboBox():New( D(026),D(015),{|u| If(PCount()>0,cTagProt:=u,cTagProt)},{"SIM","N�O"},076,012,oFolder:aDialogs[4],,,{|| aConfig[oBrwSM0:nAT][48]:= cTagProt },CLR_BLACK,CLR_WHITE,.T.,oFont1,"XML com TAG Protocolo",,,,,,, )
oCBoxE21:cToolTip := 'Verifica TAG ProtNFe no XML. Utilizado para verificar a validade do XML'


oGrp6       := TGroup():New( D(038),D(012),D(085),D(155),"Descri��es, Tamanhos",oFolder:aDialogs[4],CLR_BLACK,CLR_WHITE,.T.,.F. )
oSayE3      := TSay():New( D(045),D(015),{||"Descri�ao Prod.Fornec ?"},oFolder:aDialogs[4],,oFont1,.F.,.F.,.F.,.T.,CLR_HGRAY,CLR_WHITE,D(098),D(008) )
oCBoxE31  	:= TComboBox():New( D(051),D(015),{|u| If(PCount()>0,cDescFor:=u,cDescFor)},{"SIM","N�O"},076,012,oFolder:aDialogs[4],,,{|| aConfig[oBrwSM0:nAT][49]:= cDescFor },CLR_BLACK,CLR_WHITE,.T.,oFont1,"Apresentar Descri��o do Produto do Fornecedor contido no XML?",,,,,,, )
oCBoxE31:cToolTip := 'Apresentar Descri��o do Produto do Fornecedor contido no XML?'+ENTER+'Op��o necess�ria para funcionamento do porgrama.'
oCBoxE31:Disable()

oSayE31 	:= TSay():New( D(045),D(085),{|| "Tam. Descr. Fornec"},oFolder:aDialogs[4],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(088),D(008))
oGetE31 	:= TGet():New( D(051),D(085),{|u| If(PCount()>0,cTamDFor:=u,cTamDFor) },oFolder:aDialogs[4],D(045),D(008),'', {|| aConfig[oBrwSM0:nAT][51]:= cTamDFor }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,'Tamanho Descri��o Produto do Fornecedor',,,.F.,.F.,,.F.,.F.,"","",,)

oSayE4      := TSay():New( D(065),D(015),{||"Descri�ao Prod.Empresa ?"},oFolder:aDialogs[4],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(098),D(008) )
oCBoxE41  	:= TComboBox():New( D(071),D(015),{|u| If(PCount()>0,cDescEmp:=u,cDescEmp)},{"SIM","N�O"},076,012,oFolder:aDialogs[4],,,{|| aConfig[oBrwSM0:nAT][50]:= cDescEmp },CLR_BLACK,CLR_WHITE,.T.,oFont1,"Apresentar Descri��o do Produto da sua Empresa?",,,,,,, )
oCBoxE41:cToolTip := 'Apresentar Descri��o do Produto da sua Empresa?'

oSayE41 	:= TSay():New( D(065),D(085),{|| "Tam. Descr. Empresa"},oFolder:aDialogs[4],,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(088),D(008))
oGete41 	:= TGet():New( D(071),D(085),{|u| If(PCount()>0,cTamDEmp:=u,cTamDEmp) },oFolder:aDialogs[4],D(045),D(008),'', {|| aConfig[oBrwSM0:nAT][52]:= cTamDEmp }, CLR_BLACK,CLR_WHITE,oFont1,,,.T.,'Tamanho Descri��o Produto da Empresa',,,.F.,.F.,,.F.,.F.,"","",,)


//��������������������������Ŀ
//�  BOTOES OK - CANCELA     �
//����������������������������
oBtnOK     := TButton():New( D(205),D(120),"&OK",oDlgSM0,{|| MsgRun("Gravando dados...",,{|| GravaConfig()}), oDlgSM0:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )
oBtnCanc   := TButton():New( D(205),D(200),"&Cancela",oDlgSM0,{|| oDlgSM0:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )


oBrwSM0:oBrowse:bChange := {|| AtuGets() }

oBrwSM0:oBrowse:Refresh()
oBrwSM0:oBrowse:SetFocus()

oDlgSM0:Activate(,,,.T.)


Return(.T.)
***********************************************************************
Static Function VerBuild()
***********************************************************************
//���������������������������������Ŀ
//�	   VERIFICA VERSAO DA BUILD		�
//�����������������������������������
Local lRet := .T.

If Left(GetBuild(),12) < '7.00.101220A' // 7.00.111010p-20120120
	MsgInfo('O suporte aos protocolos  (SSL e/ou TLS) foram implementados a partir da build  7.00.101220A.'+ENTER+'Build uitilizada: '+AllTrim(GetBuild()))
	lRet := .F.	
	lSSL:=.F.;oCBox12b:Refresh()
	lTLS:=.F.;oCBox12c:Refresh()	
EndIf	

Return(lRet)
***********************************************************************
Static Function PortPadrao()
***********************************************************************
//���������������������������������Ŀ
//�	   INFORMA A PARTA PADRAO  		�
//�����������������������������������
cPortEnv	:=	'25 '

If Left(cTpMail,3) =='POP'
	cPortRec	:=	IIF(!lSegura,'110','995')
ElseIf  cTpMail =='IMAP'
	cPortRec	:=	IIF(!lSegura,'143','993')
ElseIf cTpMail =='SMTP'
	cPortRec	:=	IIF(!lSegura,'25 ', IIF(lSSL,'587','465') )
Else
	cPortRec	:=	'## '
EndIf

Return()
***********************************************************************
Static Function Get_MV_ESPECIE()
***********************************************************************
//�����������������������������������������Ŀ
//�	 BUSCA NO SX6 O PARAMETRO MV_ESPECIE	�
//�	 E SEPARA APENAS AS SERIES == SPED  	�
//�������������������������������������������
Local lSX6 	:= .F.
Local aAreaSX6:= GetArea()

If File(_StartPath+'SX6'+SM0->M0_CODIGO+'0.DTC')  //.DBF')

	If cEmpAnt != SM0->M0_CODIGO
		lSX6 := .T.
		IIF(Select('SX6') != 0, SX6->(DbCLoseArea()), )
		DbUseArea(.T.,,_StartPath+'SX6'+SM0->M0_CODIGO+'0', 'SX6',.T.,.F.)
	EndIf
	
	SX6->(DbGoTop(), DbSetOrder(1))
	If SX6->(DbSeek(SM0->M0_CODFIL+'MV_ESPECIE',.F.))
		cMV_Especie := AllTrim(SX6->X6_CONTEUD)
		aMV_Especie := StrTokArr( cMv_Especie, ";" )
		
	Else
		aMV_Especie := StrTokArr( GetMv('MV_ESPECIE'), ";" )
	EndIf                                            
	
	For nX:=1 To Len(aMV_Especie)
		If 'SPED' $ aMV_Especie[nX]
			nPos := AT('=',aMV_Especie[nX])
			cSerTransf := AllTrim(Left(aMV_Especie[nX], nPos-1))
		Else
			cSerTransf := Space(TamSX3('F1_SERIE')[1])
		EndIf
	Next
	
Else
	cSerTransf := Space(TamSX3('F1_SERIE')[1])
EndIf

If lSX6
	If File(_StartPath+'SX6'+cFilAnt+'0')
		IIF(Select('SX6') != 0, SX6->(DbCLoseArea()), )
		DbUseArea(.T.,,_StartPath+'SX6'+cFilAnt+'0', 'SX6',.T.,.F.)
	EndIf
EndIf
      
RestArea(aAreaSX6)
Return()
***********************************************************************
Static Function AtuGets()
***********************************************************************
//������������������������������������������������������������������������Ŀ
//� ATUALIZA aConfig QDO TROCAR DE LINHA DO BROWSER DE CONFIG.             �
//� Chamada -> TelaConfig()                                                �
//��������������������������������������������������������������������������

// [Config.Empresa]
cPath_XML		:=	aConfig[oBrwSM0:nAT][37]; oGet2:Refresh()
cSerTransf    	:=	aConfig[oBrwSM0:nAT][31]; oGet3:Refresh()
cPreDoc         :=	aConfig[oBrwSM0:nAT][32]; oCBox4:Refresh()
cSem_XML		:=	aConfig[oBrwSM0:nAT][41]; oCBox5:Refresh()
cGrv_Dif_A		:=	aConfig[oBrwSM0:nAT][42]; oCBox6:Refresh()
cProbEmail		:=	aConfig[oBrwSM0:nAT][38]; oGet7:Refresh()
cXmlCancFor		:=	aConfig[oBrwSM0:nAT][39]; oGet8:Refresh()
cDominio		:=	aConfig[oBrwSM0:nAT][40]; oGet9:Refresh()
cVerifEsp		:=	aConfig[oBrwSM0:nAT][47]; oGetTp:Refresh()

                                                                  
// [Config.XML Fornecedor]
cNFe9Dig       :=	aConfig[oBrwSM0:nAT][29]; oCBox9:Refresh()
cSer3Dig       :=	aConfig[oBrwSM0:nAT][30]; oCBox10:Refresh()
    
   
// [Config.e-mail]
lAutent    		:=	aConfig[oBrwSM0:nAT][35]; oCBox11:Refresh()
lSegura     	:=	aConfig[oBrwSM0:nAT][43]; oCBox12:Refresh()
cPathTables		:=	aConfig[oBrwSM0:nAT][23]; oGet13:Refresh()
cTpMail			:=	aConfig[oBrwSM0:nAT][22]; oCBox14:Refresh()
cEmail        	:=	aConfig[oBrwSM0:nAT][24]; oGet15:Refresh()
cPassword    	:=	aConfig[oBrwSM0:nAT][25]; oGet16:Refresh()
cPOrigem       	:=	aConfig[oBrwSM0:nAT][26]; oGet17:Refresh()
cPDestino   	:=	aConfig[oBrwSM0:nAT][27]; oGet18:Refresh()
cServerEnv		:=	aConfig[oBrwSM0:nAT][28]; oGet19:Refresh()
cServerRec		:=	aConfig[oBrwSM0:nAT][45]; oGet19b:Refresh()
cPortEnv    	:=	aConfig[oBrwSM0:nAT][33]; oGet20:Refresh()
cPortRec    	:=	aConfig[oBrwSM0:nAT][34]; oGet21:Refresh()
lSSL        	:=	aConfig[oBrwSM0:nAT][36]; oCBox12b:Refresh()
lTLS        	:=	aConfig[oBrwSM0:nAT][44]; oCBox12c:Refresh()
        
// [Config.Extra]                                                  
cTagProt		:=	aConfig[oBrwSM0:nAT][48]; oCBoxE21:Refresh()
cDescFor		:=	aConfig[oBrwSM0:nAT][49]; oCBoxE31:Refresh()
cDescEmp		:=	aConfig[oBrwSM0:nAT][50]; oCBoxE41:Refresh()
cTamDFor		:=	aConfig[oBrwSM0:nAT][51]; oGetE31:Refresh()
cTamDEmp		:=	aConfig[oBrwSM0:nAT][52]; oGetE41:Refresh()
			

oSay1NomeFil:Refresh()
oSay2NomeFil:Refresh()
oSay3NomeFil:Refresh()

cConect :=    ''
lTesteE	:=	.F.
lTesteR	:=	.F.
oOkConect:Hide()
oNoConect:Hide()
oBtnTest:Refresh()


oGet18:SetColor(CLR_BLACK,IIF(cTpMail=='IMAP',CLR_WHITE, CLR_HGRAY ))
IIF(cTpMail=='IMAP',oGet18:Enable(),oGet18:Disable())
oGet18:Refresh()

IIF(lSegura,(oCBox12b:Show(),oCBox12c:Show()),(oCBox12b:Hide(),oCBox12c:Hide()) )
oCBox12b:Refresh()
oCBox12c:Refresh()
oDlgSM0:Refresh()

Return()
***********************************************************************
Static Function GravaConfig()
***********************************************************************
//������������������������������������������������������������������������Ŀ
//� ATUALIZA ARQUIVO DE CONFIGURACAO (ZXM_CFG.DBF)                         �
//� Chamada -> TelaConfig()                                                �
//��������������������������������������������������������������������������
DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()    //    EMPRESA+FILIAL
For nX:=1 To Len(aConfig)
	If DbSeek(aConfig[nX][01]+aConfig[nX][03],.F.)
		RecLock('TMPCFG',.F.)
			//	[Config.Empresa]                   
			TMPCFG->PATH_XML	:=	AllTrim(aConfig[nX][37])
			TMPCFG->SER_TRANSF	:=	AllTrim(aConfig[nX][31])
			TMPCFG->PRE_DOC		:=	AllTrim(aConfig[nX][32])
			TMPCFG->SEM_XML		:=	AllTrim(aConfig[nX][41])
			TMPCFG->GRV_DIF_A	:=	AllTrim(aConfig[nX][42])
			TMPCFG->FOR_CANC	:=	AllTrim(aConfig[nX][39])
			TMPCFG->DOMINIO		:=	AllTrim(aConfig[nX][40])
			TMPCFG->PROB_EMAIL	:=	AllTrim(aConfig[nX][38])
			TMPCFG->ESPECIE   	:=	AllTrim(aConfig[nX][47])
						
			//	Config.XML Fornecedor]
			TMPCFG->NFE_9DIG	:=	AllTrim(aConfig[nX][29])
			TMPCFG->SER_3DIG	:=	AllTrim(aConfig[nX][30])
			
			// [Config.e-mail]
			TMPCFG->AUTENTIFIC	:=	aConfig[nX][35]
			TMPCFG->SEGURA		:=	aConfig[nX][43]
			TMPCFG->PATCHTABLE	:=	AllTrim(aConfig[nX][23])
			TMPCFG->TIPOEMAIL	:=	AllTrim(aConfig[nX][22])
			TMPCFG->EMAIL		:=	AllTrim(aConfig[nX][24])
			TMPCFG->SENHA		:=	AllTrim(aConfig[nX][25])
			TMPCFG->PASTA_ORIG	:=	AllTrim(aConfig[nX][26])
			TMPCFG->PASTA_DEST	:=	AllTrim(aConfig[nX][27])
			TMPCFG->SERVERREC	:=	AllTrim(aConfig[nX][45])			
			TMPCFG->SERVERENV	:=	AllTrim(aConfig[nX][28])
			TMPCFG->PORTA_REC	:=	AllTrim(aConfig[nX][34])
			TMPCFG->PORTA_ENV	:=	AllTrim(aConfig[nX][33])
			TMPCFG->SSL 		:=	aConfig[nX][36]
			TMPCFG->TLS    		:=	aConfig[nX][44]			

			// [Config.Extra]
			TMPCFG->TAGPROT		:=	aConfig[nX][48]
			TMPCFG->DESCFOR		:=	aConfig[nX][49]
			TMPCFG->DESCEMP		:=	aConfig[nX][50]
			TMPCFG->TAMDFOR		:=	aConfig[nX][51]
			TMPCFG->TAMDEMP		:=	aConfig[nX][52]

		
		MsUnLock()
	Endif
   DbSelectArea('TMPCFG');DbGoTop()
Next

Return()
***********************************************************************
Static Function TestConect()
***********************************************************************
//������������������������������������������������������������������������Ŀ
//� TESTA SE CONFIGURACAO DE EMAIL, SENHA, SERVIDOR ESTAO CORRETAS         �
//� Chamada -> TelaConfig()                                                |
//��������������������������������������������������������������������������
cRetorno    :=	''
nNumMsg		:=	0
cTpConexao	:=	AllTrim(aConfig[oBrwSM0:nAT][22])   
lAutent		:=	aConfig[oBrwSM0:nAT][35]
lSegura		:=	aConfig[oBrwSM0:nAT][43] 
lSSL		:=	aConfig[oBrwSM0:nAT][36] 
lTLS 		:=	aConfig[oBrwSM0:nAT][44]

oServer    :=    TMailManager():New()

If lSegura .And. lTesteR
	oServer:SetUseSSL(lSSL)					//		Define o envio de e-mail utilizando uma comunica��o segura atrav�s do SSL - Secure Sockets Layer.
	oServer:SetUseTLS(lTLS)					//		Define no envio de e-mail o uso de STARTTLS durante o protocolo de comunica��o.
EndIf	

							//	SERVER RECEBIMENTO,	 SERVER ENVIO	   ,		EMAIL		  ,	    SENHA	  	  , PORTA RECEBIMENTO  ,  PORTA ENVIO
nConfig    :=    oServer:Init( AllTrim(cServerRec), AllTrim(cServerEnv), AllTrim(cEmail), AllTrim(cPassword), IIF(lTesteR, Val(cPortRec),), Val(cPortEnv) )

If lAutent .And. lTesteE
	oServer:SMTPAuth(oServer:GetUser(), AllTrim(cPassword))
EndIf



If lTesteE
	nTimeConect	:=	 IIF(oServer:GetSMTPTimeOut()!=0, oServer:GetSMTPTimeOut(), 60 )
	oServer:SetSMTPTimeout(nTimeConect)  
Else
	nTimeConect	:=	 IIF(oServer:GetPOPTimeOut()!=0, oServer:GetPOPTimeOut(), 60 )
	oServer:SetPOPTimeout(nTimeConect)
EndIf



//�������������������������������Ŀ
//�	CONEXAO DE ENVIO == SMTP      �
//���������������������������������
If lTesteE
	nConecTou    :=    oServer:SmtpConnect()  
Else

	If cTpConexao == 'IMAP'
	   nConecTou    :=    oServer:IMAPConnect()
	ElseIf Left(cTpConexao,3) == 'POP'
	   nConecTou    :=    oServer:PopConnect()
	ElseIf cTpConexao == 'MAPI'
	     nConecTou    :=    oServer:ImapConnect()
	ElseIf cTpConexao == 'SMTP'  
	     nConecTou    :=    oServer:SmtpConnect()  
	EndIf
EndIf  

If lTesteR
	nRet := oServer:GetNumMsgs(@nNumMsg)
	//Alert( oServer:GetErrorString(nRet) +ENTER+ 'NUM. Msg: '+AllTrim(Str(nNumMsg)))
EndIf

   

#IFDEF WINDOWS
   If nConectou == 0
       If cTpConexao == 'IMAP'
           oServer:SmtpDisconnect()
       ElseIf Left(cTpConexao,3) == 'POP'
           oServer:PopDisconnect()
       ElseIf cTpConexao == 'MAPI'
             oServer:ImapDisconnect()
       ElseIf cTpConexao == 'SMTP'
             oServer:SmtpDisconnect()
       EndIf
       cRetorno    +=    'Conectado com Sucesso'
   Else
       cRetorno    +=    'ERRO: '+ Alltrim(oServer:GetErrorString(nConecTou))
   EndIf
#ELSE
   If nConectou == 0 .And. cTpConexao != 'MAPI'
       If cTpConexao == 'IMAP'
           oServer:SmtpDisconnect()
       ElseIf Left(cTpConexao,3) == 'POP'
           oServer:PopDisconnect()
       ElseIf cTpConexao == 'SMTP'
             oServer:SmtpDisconnect()   
       EndIf
       cRetorno    +=    'Conectado com Sucesso'
   ElseIf nConecTou == 1 .And. cTpConexao == 'MAPI'
         oServer:ImapDisconnect()
       cRetorno    +=    'Conectado com Sucesso.'
   Else
       cRetorno    +=    'ERRO: '+ Alltrim(oServer:(nConecTou))
   EndIf
#ENDIF

Return(cRetorno)
*****************************************************************
User Function Visual_XML()    //    [3.0]
*****************************************************************
//������������������������������������Ŀ
//� ABRE XML NO BROSWER                �
//� Chamada -> Rotina Visualizar       �
//��������������������������������������
Local   aArea3		:= GetArea()
Local   lRetorno	:=	.F.
Local	lValidDlg	:=	.T.
Local	aEditCpo	:=	{}                                                                                                                              

Private	aMyHeader	:=  {}
Private aMyCols    	:=  {}
Private aProdxFor   :=  {}
Private lGravado    := 	.F.
Private nTotalNFe   :=	0
Private cPreDoc    	:= 	''
Private cXMLStatus  :=	''
Private aClixFor	:=	{}	
Private cMsgDevRet 	:= ''
Private aPCxItem	:=	{}

PRIVATE aHeader 	:=	{}
PRIVATE aCols   	:=	{}
PRIVATE cTipo  		:=	'N'
PRIVATE cA100For	:= 	ZXM->ZXM_FORNEC

PRIVATE cLoja    	:=	ZXM->ZXM_LOJA          // Benhur 23/10/2019                                            
PRIVATE cFormul   	:=	IIF(ZXM->ZXM_FORMUL=='S', 'S', 'N')
PRIVATE cNFiscal	:=	IIF(cFormul!='S', ZXM->ZXM_DOC,   ZXM->ZXM_DOCREF )//IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC,   ZXM->ZXM_DOCREF )
PRIVATE cSerie    	:= 	IIF(cFormul!='S', ZXM->ZXM_SERIE, ZXM->ZXM_SERREF )//IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_SERIE, ZXM->ZXM_SERREF )

PRIVATE cEspecie	:=	'SPED'
PRIVATE cCondicao 	:= 	''
PRIVATE cForAntNFE	:= 	''
PRIVATE dDEmissao	:= 	dDataBase
PRIVATE n			:= 	1
PRIVATE nMoedaCor	:= 	1
PRIVATE L103AUTO 	:=	.F.
PRIVATE lConsLoja	:= .F.


Static lNFeDev 	 	:=  .F.
Static lImportacao	:=	.F.

Static aXMLOriginal	:= {}
Static cTpFormul 	:= ''


SetPrvt('oDlgV','oGrp1','oSay7','oSay10','oSay8','oSay9','oSay11','oSay12','oSay13','oSay14','oSay15','oSay16','oSay17')
SetPrvt('oSay18','oGrp2','oSayNF','oSayNFSeri','oSay1','oSay2','oSay3','oSay4','oSay5','oSay6','oSay71','oSay8','oGrp3')
SetPrvt('oBtn1','oBtn2','oBtn6SA1','oBtn4SA1','oBtn4SA2','oBtn4SA2','oBtnPreDoc','oBtn5','oSayF6','oSayStatus','oBrwV','oBtnPC')
SetPrvt('oSayDevRet')

oFont1    := TFont():New( "Arial",0,IIF(nHRes<=800,-12,-13),,.T.,0,,700,.F.,.F.,,,,,, )
oFont2    := TFont():New( "Arial",0,IIF(nHRes<=800,-14,-16),,.T.,0,,700,.F.,.F.,,,,,, )
oFont3    := TFont():New( "Arial",0,IIF(nHRes<=800,-17,-19),,.T.,0,,700,.F.,.F.,,,,,, )
oFont0    := TFont():New( "Arial",0,IIF(nHRes<=800,-09,-11),,.F.,0,,400,.F.,.F.,,,,,, )
oFontP    := TFont():New( "Arial",0,IIF(nHRes<=800,-08,-10),,.F.,0,,400,.F.,.F.,,,,,, )
oFontP2   := TFont():New( "Arial",0,IIF(nHRes<=800,-10,-12),,.F.,0,,400,.F.,.F.,,,,,, )

//���������������������������������Ŀ
//�  ABRE ARQUIVO DE CONFIGURACAO   |
//�����������������������������������
If Select('TMPCFG') == 0
   DbUseArea(.T.,,_StartPath+cArqConfig, 'TMPCFG',.T.,.F.)
   DbSetIndex(_StartPath+cIndConfig)
EndIf

DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
If DbSeek(cEmpAnt+cFilAnt, .F.)
   cPreDoc    :=    TMPCFG->PRE_DOC
EndIf

//	aMyHeader = CABEC do MsNewGetDados
DbSelectArea('SX3');DbSetOrder(2);DbGoTop()
DbSeek('D1_ITEM'); 	Aadd(aMyHeader,{ 'Item  ',           X3_CAMPO, X3_PICTURE, X3_TAMANHO, X3_DECIMAL, '', X3_USADO, X3_TIPO, X3_F3      })

//Benhur
// 09/10/2019
DbSeek('C7_PRODUTO'); 	Aadd(aMyHeader,{ 'Prod.Fornec',      X3_CAMPO, X3_PICTURE, X3_TAMANHO, X3_DECIMAL, '', X3_USADO, X3_TIPO, X3_F3      })	                                  
If TMPCFG->DESCFOR == 'SIM'			//	DESCRICAO PRODUTO FORNECEDOR - XML
	DbSeek('B1_DESC'); 	Aadd(aMyHeader,{ Trim(X3_TITULO)+' Fornec',    AllTrim(X3_CAMPO)+'FOR', X3_PICTURE, Val(TMPCFG->TAMDFOR), X3_DECIMAL, '', X3_USADO, X3_TIPO, X3_F3      })
EndIf

DbSeek('D1_COD'); 	Aadd(aMyHeader,{ Trim(X3_TITULO)+'-'+TMPCFG->NOME_EMP,  X3_CAMPO, X3_PICTURE, X3_TAMANHO, X3_DECIMAL, 'Vazio().Or.ExistCpo("SB1")', X3_USADO, X3_TIPO, 'SB1' })

If TMPCFG->DESCEMP == 'SIM'			//	DESCRICAO PRODUTO EMPRESA
	DbSeek('B1_DESC'); 	Aadd(aMyHeader,{ Trim(X3_TITULO)+'-'+TMPCFG->NOME_EMP,  X3_CAMPO, X3_PICTURE, Val(TMPCFG->TAMDEMP), X3_DECIMAL, '', X3_USADO, X3_TIPO, X3_F3  })
EndIf

//DbSeek('B1_RMS'); 	Aadd(aMyHeader,{ Trim(X3_TITULO),  X3_CAMPO, X3_PICTURE, X3_TAMANHO, X3_DECIMAL, '', X3_USADO, X3_TIPO, X3_F3  })
//DbSeek('B1_DTRMS');	Aadd(aMyHeader,{ Trim(X3_TITULO),  X3_CAMPO, '', X3_TAMANHO, X3_DECIMAL, '', X3_USADO, X3_TIPO, X3_F3  })

//	Campos para EDICAO do MsNewGetDados
Aadd( aEditCpo,	'D1_COD')	


//	aMyHeader == TODO O SX3 DO SD1 - BUSCA TODOS OS CAMPOS DO SD1
DbSelectArea('SX3');DbSetOrder(1);DbGoTop()
DbSeek('SD101', .F.) 
Do While !Eof() .And. SX3->X3_ARQUIVO == 'SD1'
    
    If 	!(AllTrim(SX3->X3_CAMPO) $ 'D1_FILIAL#D1_ITEM#D1_COD') .And. AllTrim(SX3->X3_CAMPO) != 'D1_DESCR' .And.;
		  X3USO(SX3->X3_USADO) .And. cNivel >= SX3->X3_NIVEL
		
		//If ( cPreDoc == 'PRE' .And. AllTrim(SX3->X3_CAMPO) $ 'D1_TES#D1_CF#D1_OPER' )
			// NAO MOSTRA NO HEADER
		//Else
		
			Aadd(aMyHeader,{ Trim(SX3->X3_TITULO), SX3->X3_CAMPO, SX3->X3_PICTURE,;
							  SX3->X3_TAMANHO, SX3->X3_DECIMAL, SX3->X3_VALID, SX3->X3_USADO, SX3->X3_TIPO, SX3->X3_F3 })
						  
			Aadd( aEditCpo,	AllTrim(SX3->X3_CAMPO))			// CAMPOS PARA EDICAO NO MSNEWGETDADOS
		
		//EndIf
		
	EndIf
	
	DbSkip()
EndDo

DbSelectArea('ZXM')

   ***************************************************************************************
       Processa ({|| CarregaACols()  },'Aguarde Carregando XML',  'Processando...', .T.)    //    [3.1]
   ***************************************************************************************



//��������������������������������������������Ŀ
//�	VERIFICA STATUS DO XML IMPORTADO           �
//����������������������������������������������
DbSelectArea('ZXM')
Do Case
	Case ZXM->ZXM_SITUAC == 'C'
	   cXMLStatus    :=    'Status XML: CANCELADO NA SEFAZ'
	Case ZXM->ZXM_STATUS == 'G'
	       cXMLStatus    :=  'Status XML:' +IIF(lGravado, IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+"  GRAVADO", '')
	Case ZXM->ZXM_STATUS == 'I'
	       cXMLStatus    :=    'Status XML: IMPORTADO'
	Case ZXM->ZXM_STATUS == 'X'
	       cXMLStatus    :=    'Status XML: CANCELADO'
	Case ZXM->ZXM_STATUS == 'R'
	       cXMLStatus    :=    'Status XML: RECUSADO'
	Case Empty(ZXM->ZXM_SITUAC)
		cXMLStatus    :=    'Status XML inv�lido. N�o houve retorno da SEFAZ.
EndCase




//�������������������������������������������������������������������������Ŀ
//�	                      TELA PRINCIPAL - VISUALIZACAO             		| 
//���������������������������������������������������������������������������

                            //   L1     C1      L2     C2
oDlgV      := MSDialog():New( D(090),D(300),D(545),D(1313),"XML Fornecedor",,,.F.,,,,,,.T.,,oFont0,.T. )

***************************
	AtuClixFor()
***************************

oGrp1      := TGroup():New( D(004),D(008),D(060),D(247),"",oDlgV,CLR_BLACK,CLR_WHITE,.T.,.F.	)
oSay71     := TSay():New( D(020),D(012),{|| aClixFor[1] },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(144),D(008)  )		
oSay14     := TSay():New( D(038),D(054),{|| aClixFor[5]	},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(160),D(008) )

oSay10     := TSay():New( D(020),D(054),{|| aClixFor[2] },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(136),D(008) )

oSay8      := TSay():New( D(011),D(012),{|| "C�digo:" },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(032),D(008) )
oSay9      := TSay():New( D(011),D(054),{|| aClixFor[3] },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )
oSay11     := TSay():New( D(029),D(012),{|| "CNPJ:" },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(032),D(008) )
oSay12     := TSay():New( D(029),D(054),{|| aClixFor[4] 	},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008) )
oSay13     := TSay():New( D(038),D(012),{|| "Endere�o:" },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(040),D(008) )
oSay14     := TSay():New( D(038),D(054),{|| aClixFor[5]	},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(160),D(008) )
oSay15     := TSay():New( D(048),D(012),{|| "Cidade:" },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(032),D(008) )
oSay16     := TSay():New( D(048),D(054),{|| aClixFor[6] },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(068),D(008) )
oSay17     := TSay():New( D(048),D(180),{|| "Fone:" },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(024),D(008) )
oSay18     := TSay():New( D(048),D(200),{|| aClixFor[7] },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(062),D(008) )



oGrp2      := TGroup():New( D(004),D(252),D(060),D(505),"",oDlgV,CLR_BLACK,CLR_WHITE,.T.,.F. )
oSayNF     := TSay():New( D(012),D(260),{|| "Nota Fiscal:"},oGrp2,,oFont3,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(060),D(012) )
oSayNFSeri := TSay():New( D(012),D(320),{|| ZXM->ZXM_DOC +' - '+ ZXM->ZXM_SERIE},oGrp2,,oFont3,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(150),D(012) )

oSayNNFRef := TSay():New( D(021),D(260),{|| IIF(ZXM->ZXM_FORMUL == 'S',"NF Refer�ncia:",'')},oGrp2,,oFontP2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(060),D(012) )
oSaySNFRef := TSay():New( D(021),D(320),{|| IIF(ZXM->ZXM_FORMUL == 'S',PadL(ZXM->ZXM_DOCREF, TamSx3('F1_DOC')[1],'') +' - '+ PadL(ZXM->ZXM_SERREF, TamSx3('F1_SERIE')[1],''),'')},oGrp2,,oFontP2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(012) )

oSay1      := TSay():New( D(012),D(397),{|| "Emiss�o:"},oGrp2,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(040),D(008) )
//oSay2      := TSay():New( D(012),D(421),{|| StoD(cDataNFe)  },oGrp2,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )
oSay2      := TSay():New( D(012),D(421),{|| StoD(cDataNFe)  },oGrp2,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )                                             // Substr(cDataNFe,1,4)+Substr(cDataNFe,6,2)+Substr(cDataNFe,9,2)
oSay3      := TSay():New( D(027),D(260),{|| "Total:"},oGrp2,,oFont3,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(035),D(023) )
oSay4      := TSay():New( D(027),D(320),{|| "R$ "+Transform( nTotalNFe, '@E 999,999,999.99') },oGrp2,,oFont3,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(150),D(012) )
oSay5      := TSay():New( D(039),D(260),{|| "Nat.Opera��o:"},oGrp2,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )
oSay6      := TSay():New( D(039),D(320),{|| cNatOper },oGrp2,,oFont1,.F.,.F.,.F.,.T.,IIF(lNFeDev,CLR_HRED, CLR_BLACK),CLR_WHITE,D(170),D(008) )
oSay7      := TSay():New( D(048),D(260),{|| "Chave:"},oGrp2,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )
oSay8      := TSay():New( D(048),D(320),{|| ZXM->ZXM_CHAVE},oGrp2,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(170),D(008) )



oBtn1      := TButton():New( D(064),D(008),"Visualizar XML",oDlgV, {|| View_XML()	}, D(045),D(014),,oFont0,,.T.,,"Visualiza XML no Broswer",,,,.F. )
oBtn2      := TButton():New( D(064),D(055),"Mensagem Nota",oDlgV,  {|| MsgInfCpl()}, D(045),D(014),,oFont0,,.T.,,"Mensagem da Nota Fiscal",,,,.F. )

nDif := (055-008) // 47
nCol :=  055 + nDif

oBtn6SA2     := TButton():New( D(064),D(nCol),"Hist.Fornecedor",oDlgV,{|| HistFornec()},D(045),D(014),,oFont0,,.T.,,"Hist�rico do Fornecedor",,,,.F. )
oBtn6SA1     := TButton():New( D(064),D(nCol),"Posi��o Cliente",oDlgV,{|| a450F4Con() },D(045),D(014),,oFont0,,.T.,,"Posi��o Cliente",,,,.F. )

nCol +=  nDif
	
oBtn4SA2   := TButton():New( D(064),D(nCol),"Cad."+"Fornecedor",oDlgV, {|| MostraCadFor(SA2Recno) },D(045),D(014),,oFont0,,.T.,,"Cadastro Fornecedor",,,,.F. )
oBtn4SA1   := TButton():New( D(064),D(nCol),"Cad."+"Cliente",oDlgV, {|| MostraCadCli(SA2Recno) },D(045),D(014),,oFont0,,.T.,,"Cadastro Cliente",,,,.F. )

If !lNFeDev
	oBtn6SA2:Show()
	oBtn6SA1:Hide()
	oBtn4SA2:Show()
	oBtn4SA1:Hide()
EndIf

nCol +=  nDif
oBtnPC     := TButton():New( D(064),D(nCol),"Ped.Compras"   ,oDlgV, {|| SelItemPC() },D(045),D(014),,oFont0,,.T.,,"Pedido de Compras - Item - F6 ",,,,.F. )
nCol +=  nDif
oBtnReplic := TButton():New( D(064),D(nCol),"Replicar Produtos",oDlgV,{|| ReplicaProd('PROD')},D(045),D(014),,oFont0,,.T.,,"Replica para os itens selecionados um �nico Produto - F7 ",,,,.F. )
nCol +=  nDif
oBtnPreDoc := TButton():New( D(064),D(nCol),IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada") , oDlgV, {|| IIF(lGravado, VisualNFE(), IIF(GeraPreDoc(), AtuPreDoc(), ) ) , VerifStatus() },D(045),D(014),,oFont0,,.T.,,"Gera "+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada"),,,,.F. )
//oBtnPreDoc := TButton():New( D(064),D(nCol),IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada") , oDlgV, {|| /*CARGA_ACOLS(),*/ IIF(cPreDoc=='PRE', (IIF(ExistBlock('MT140TOK'),ExecBlock('MT140TOK'),)), (IIF(ExistBlock('MT100TOK'),ExecBlock('MT100TOK'),)) ), IIF(lGravado, VisualNFE(), IIF(GeraPreDoc(), AtuPreDoc(), ) ) , VerifStatus() },D(045),D(014),,oFont0,,.T.,,"Gera "+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada"),,,,.F. )

oBtn5      := TButton():New( D(064),D(460),"&Sair",oDlgV,{|| oDlgV:End() },D(045),D(014),,oFont0,,.T.,,"",,,,.F. )



Do Case
	Case ZXM->ZXM_STATUS == 'G'
	   oSayStatus:= TSay():New( D(082),D(013),{|| cXMLStatus },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_GREEN,CLR_WHITE,D(120),D(008) )
	Case ZXM->ZXM_STATUS == 'I'.And. ZXM->ZXM_SITUAC == 'A'
	   oSayStatus:= TSay():New( D(082),D(013),{|| cXMLStatus },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(100),D(008) )
	OtherWise
	   oSayStatus:= TSay():New( D(082),D(013),{|| cXMLStatus },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,D(100),D(008) )
EndCase


cMsgF6	:=  IIF(ZXM->ZXM_STATUS$'I' , 'F6 - Seleciona PC.' , '')
cMsgF7	:=  IIF(ZXM->ZXM_STATUS$'I' , 'F7 - Replica Prod.' , '')
cMsgF8	:=  IIF(ZXM->ZXM_STATUS$'I' , 'F8 - Alterar Almox.', '')
cMsgF9	:=  IIF(ZXM->ZXM_STATUS$'I' .And. cPreDoc == 'DOC', 'F9 - Replica TES.  ', '')
cMsgF10	:=  IIF(ZXM->ZXM_STATUS$'I' .And. lNFeDev, 'F10 - Doc.Original', '')

If Empty(cMsgDevRet) .And. !Empty(ZXM->ZXM_TIPO)
	Do Case
		Case ZXM->ZXM_TIPO ==  'N'
	  		cMsgDevRet	:=	'  Tipo Nota: Normal'
	 	Case ZXM->ZXM_TIPO == 'D'
	 		cMsgDevRet	:=	'  Tipo Nota: Devolu��o'
	 	Case ZXM->ZXM_TIPO ==  'B'
	 		cMsgDevRet	:=	'  Tipo Nota: Beneficiamento'
	 	Case ZXM->ZXM_TIPO ==  'I'
	 		cMsgDevRet	:=	'  Tipo Nota: Compl. ICMS' 				
	 	Case ZXM->ZXM_TIPO == 'P'
	 		cMsgDevRet	:=	'  Tipo Nota: Compl. IPI' 				
	 	Case ZXM->ZXM_TIPO == 'C'
	 		cMsgDevRet	:=	'  Tipo Nota: Compl. Pre�o/Frete'
 	EndCase        
 			
 	cMsgDevRet += IIF(lImportacao,' - Importa��o','')
 			
EndIf 		      

		 		
oSayPedCom 	:= TSay():New( D(082),D(184),{|| cMsgF6 },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_GRAY,CLR_WHITE,D(120),D(008))
oSayReplic 	:= TSay():New( D(082),D(228),{|| cMsgF7 },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_GRAY,CLR_WHITE,D(120),D(008))
oSayLocal	:= TSay():New( D(082),D(278),{|| cMsgF8 },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_GRAY,CLR_WHITE,D(120),D(008))
oSay_TES 	:= TSay():New( D(082),D(328),{|| cMsgF9 },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_GRAY,CLR_WHITE,D(120),D(008))
oSayNFOrig	:= TSay():New( D(082),D(378),{|| cMsgF10 },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,D(120),D(008))
oSayDevRet	:= TSay():New( D(082),D(415),{|| cMsgDevRet },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,D(120),D(008))
                                                               

                               // L1     C1      L2     C2
oGrp3      := TGroup():New( D(080),D(008),D(224),D(505),"",oDlgV,CLR_BLACK,CLR_WHITE,.T.,.F. )


oBrwV := MsNewGetDados():New( D(087),D(012),D(220),D(500),GD_UPDATE/*+GD_DELETE+GD_INSERT*/,'AllwaysTrue()','AllwaysTrue()','+D1_ITEM',aEditCpo,0,999,'U_ATURMS()','','AllwaysTrue()',oGrp3,aMyHeader,aMyCols )
oBrwV:oBrowse:bDelOk  := 	{|| DelLnPC() } 	// APENAS DELETA ITENS ADICIONADO ATRAVES DE UM PEDIDO DE COMPRA


/*oBrwV := MsNewGetDados():New( D(087),D(012),D(220),D(500),GD_UPDATE,	IIF(cPreDoc=='PRE', (IIF(ExistBlock("MT140LOK"), 'U_MT140LOK()', 'AllwaysTrue()')), (IIF(ExistBlock("MT103LOK"), 'U_MT103LOK()','AllwaysTrue()')) ),;
																		IIF(cPreDoc=='PRE', (IIF(ExistBlock("MT140TOK"), 'U_MT140TOK()', 'AllwaysTrue()')), (IIF(ExistBlock("MT100TOK"), 'U_MT100TOK()','AllwaysTrue()')) ),;
																		'+D1_ITEM',aEditCpo,0,999,,'','AllwaysTrue()',oGrp3,aMyHeader,aMyCols )
*/


//�����������������������������������������������Ŀ
//� VERIFICA SE EXISTE NO SA5 PROD.FORNECEDOR     �
//| aProdxFor=Array com ProdutoxFornecedor        |
//�������������������������������������������������
bSavDblClick :=	oBrwV:oBrowse:bLDblClick
oBrwV:oBrowse:bLDblClick :=	 {|| PreDocRefresh(), VerifStatus(), ViewTrigger(oBrwV:NAT, oBrwV:oBrowse:ColPos), DadosSB1(), GravaZXP() }

//�������������������������������������������������������������Ŀ
//� FUNCAO CHAMA PONTOS DE ENTRADA CASO PE ESTAJA COMPILADO 	|
//���������������������������������������������������������������
//oBrwV:oBrowse:bDrawSelect  :=	{|| CheckingPE()  }
//bSavDrawSelect := oBrwV:oBrowse:bDrawSelect
//oBrwV:oBrowse:bDrawSelect  :=	{|| IIF(oBrwV:oBrowse:ColPos == aScan(aMyHeader,{|x| Alltrim(x[2]) == "D1_COD" }), bSavDblClick := oBrwV:oBrowse:bLDblClick,), oBrwV:oBrowse:bDrawSelect := bSavDrawSelect }


oBrwV:oBrowse:SetFocus()
oBrwV:oBrowse:Refresh()

oDlgV:Activate(,,,.T.,{|| IIF(!lValidDlg,oDlgV:End(),)},, {|| VerifStatus(), IIF((!Empty(ZXM->ZXM_TIPO).Or.lNFeDev.Or.lImportacao), (lOpRet:= OpcaoRetorno(), IIF(!lOpRet,oDlgV:End(),) ), ) } )	// OpcaoRetorno() = Tela onde user define qual o tipo de retorno

	  
RestArea(aArea3)
Return()
************************************************************
Static Function DadosSB1()
************************************************************
nPosProd  	:=	aScan(aMyHeader,{|x| Alltrim(x[2]) == "D1_COD" })
cProduto  	:= 	oBrwV:aCols[oBrwV:NAT][nPosProd]
			
nPosALM		:= 	aScan(aMyHeader,{|x| Alltrim(x[2]) == 	'D1_LOCAL'	})
nPosTES		:= 	aScan(aMyHeader,{|x| Alltrim(x[2]) == 	'D1_TES' 	})
nPosCTA		:= 	aScan(aMyHeader,{|x| Alltrim(x[2]) == 	'D1_CONTA'	})
nPosCUS		:= 	aScan(aMyHeader,{|x| Alltrim(x[2]) == 	'D1_CC'  	})
nPosUM		:= 	aScan(aMyHeader,{|x| Alltrim(x[2]) == 	'D1_UM'  	})
nPosDesc	:= 	aScan(aMyHeader,{|x| Alltrim(x[2]) == 	'B1_DESC'	})	//	DESCRICAO PRODUTO EMPRESA


//�������������������������������������������������������������Ŀ
//� oBrwV:oBrowse:ColPos = POSICAO EM QUE SE ENCONTRA NO aCols  �
//���������������������������������������������������������������
If oBrwV:oBrowse:ColPos == nPosProd

	SB1->(DbGoTop(), DbSetOrder(1))
	If SB1->(DbSeek(xFilial('SB1') + cProduto ,.F.) )
                
	    cAlmox 	:= 	IIF(!Empty(SB1->B1_LOCPAD),	AllTrim(SB1->B1_LOCPAD), 	Space(TamSx3('B1_LOCPAD')[1]) 	)	//	Local Padrao 
	    cTes	:= 	IIF(!Empty(SB1->B1_TE), 	AllTrim(SB1->B1_TE), 		Space(TamSx3('B1_TE')[1]) 		)	//	Codigo de Entrada padrao
	    cCtaCont:= 	IIF(!Empty(SB1->B1_CONTA),  AllTrim(SB1->B1_CONTA), 	Space(TamSx3('B1_CONTA')[1]) 	)	//	Conta Contabil dn Prod.
		cCCusto	:=	IIF(!Empty(SB1->B1_CC), 	AllTrim(SB1->B1_CC), 		Space(TamSx3('B1_CC')[1]) 		)	//	Centro de Custo
		cUnidade:=	IIF(!Empty(SB1->B1_UM), 	AllTrim(SB1->B1_UM), 		Space(TamSx3('B1_UM')[1]) 		)	//	Centro de Custo

		If nPosDesc > 0
			oBrwV:aCols[oBrwV:NAT][nPosDesc]:=	AllTrim(SB1->B1_DESC)
		EndIf
		If nPosUM > 0
			oBrwV:aCols[oBrwV:NAT][nPosUM]:=	cUnidade
		EndIf
		If nPosALM > 0
			oBrwV:aCols[oBrwV:NAT][nPosALM]	:=	cAlmox
		EndIf
		If nPosTES > 0
			oBrwV:aCols[oBrwV:NAT][nPosTES]	:=	cTes         		
		EndIf
		If nPosCTA > 0
			oBrwV:aCols[oBrwV:NAT][nPosCTA]	:=	cCtaCont
		EndIf
		If nPosCUS > 0
			oBrwV:aCols[oBrwV:NAT][nPosCUS]	:=	cCCusto
		EndIf
	
	Else

		If nPosDesc > 0
			oBrwV:aCols[oBrwV:NAT][nPosDesc]:=	Space(TamSx3('B1_DESC')[1])
		EndIf
		If nPosUM > 0
			oBrwV:aCols[oBrwV:NAT][nPosUM]:=	Space(TamSx3('B1_UM')[1])
		EndIf
		If nPosALM > 0
			oBrwV:aCols[oBrwV:NAT][nPosALM]	:=	Space(TamSx3('B1_LOCPAD')[1])
		EndIf
		If nPosTES > 0
			oBrwV:aCols[oBrwV:NAT][nPosTES]	:=	Space(TamSx3('B1_TE')[1])         		
		EndIf                                                         
		If nPosCTA > 0
			oBrwV:aCols[oBrwV:NAT][nPosCTA]	:=	Space(TamSx3('B1_CONTA')[1])
		EndIf
		If nPosCUS > 0
			oBrwV:aCols[oBrwV:NAT][nPosCUS]	:=	Space(TamSx3('B1_CC')[1])
		EndIf
			
	EndIf
	
	oBrwV:oBrowse:Refresh()

EndIf

Return()
************************************************************
Static Function CARGA_ACOLS()
************************************************************

aHeader 	:=	aMyHeader
aCols   	:=	oBrwV:aCols

RETURN
************************************************************
Static Function CheckingPE()
************************************************************
Local aRetorno := {}
Local xRet	

//���������������������������������������������Ŀ
//� P.E. Utilizado para chamar PE do Cliente	�
//�����������������������������������������������
IF ExistBlock("ExecutaPE") 
	aRetorno := ExecBlock("ExecutaPE",.F.,.F.,)

	If 	ExistBlock(aRetorno[1])
		xRet := ExecBlock(aRetorno[1],.F.,.F.,)
	EndIf

Endif

Return(xRet)
************************************************************
Static Function ViewTrigger(nLin, nPosCampo)
************************************************************
//�����������������������������������������������������������������������������������������Ŀ
//� EXECUTADO APOS CLICAR NO CAMPO.                                                			�
//�	oBrwV:oBrowse:bLDblClick	:=	{|| ... ViewTrigger(oBrwV:NAT, oBrwV:oBrowse:ColPos) }	�
//�������������������������������������������������������������������������������������������
cCampo := AllTrim(oBrwV:aHeader[nPosCampo][2])

If ExistTrigger(cCampo) .And.  Left(cCampo,2) == 'D1' // VERIFICA SE EXISTE TRIGGER PARA ESTE CAMPO
	RunTrigger(2,nLin,Nil,,cCampo)
Endif	

Return()
************************************************************
Static Function AtuClixFor()
************************************************************

//�����������������������������������������������������������������Ŀ
//�	lNFeDev - VARIAVEL VERIFICA SE Eh FORNECEDOR - ENTRADA NORMAL	|
//|								    OU CLIENTE   - DEVOLUCAO		|
//�������������������������������������������������������������������
IF !lNFeDev .And. !lImportacao

	SA2->(DbSetOrder(3),DbGoTop(),DbSeek(xFilial('SA2')+ ZXM->ZXM_CGC, .F.) )
	SA2Recno :=    SA2->(Recno())

Else					//	DEVOLUCAO\RETORNO GRAVA FORNEC\LOJA e TIPO APOS USER SELECIONAR O TIPO [ NA ROTINA OpcaoRetorno ]	
       
	cTabela := IIF(ZXM->ZXM_TIPO $ 'D\B', 'SA1', 'SA2' )
  	*********************************************************************************
		ForCliMsBlql(cTabela, ZXM->ZXM_CGC, lImportacao, ZXM->ZXM_FORNECE, ZXM->ZXM_LOJA )
	*********************************************************************************
	SA2Recno := IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->(Recno()), SA2->(Recno()) )

EndIf
         

If Empty(ZXM->ZXM_FORNEC).And.Empty(ZXM->ZXM_LOJA) .And. !lImportacao
	Aadd(aClixFor, "Fornec\Cli: ")	//	[1]
	Aadd(aClixFor, "") 				//	[2]
	Aadd(aClixFor, "")				//	[3]
	Aadd(aClixFor, Transform( IIF(!lNFeDev,SA2->A2_CGC,SA1->A1_CGC), IIF(IIF(!lNFeDev,Len(SA2->A2_CGC)==14,Len(SA1->A1_CGC)==14),'@R 99.999.999/9999-99','@R 999.999.999-99')) )		
	Aadd(aClixFor, "")				//	[5]
	Aadd(aClixFor, "")				//	[6]
	Aadd(aClixFor, "")				//	[7]
Else
	Aadd(aClixFor, IIF(!lNFeDev.Or.lImportacao, "Fornecedor: ", "Cliente: ") )
	Aadd(aClixFor, IIF(!lNFeDev.Or.lImportacao, SA2->A2_NOME, SA1->A1_NOME ) )
	Aadd(aClixFor, IIF(!lNFeDev.Or.lImportacao, (SA2->A2_COD +' - '+SA2->A2_LOJA), (SA1->A1_COD +' - '+SA1->A1_LOJA)) )
	Aadd(aClixFor, Transform( IIF(!lNFeDev.Or.lImportacao,SA2->A2_CGC,SA1->A1_CGC), IIF(IIF(!lNFeDev,Len(SA2->A2_CGC)==14,Len(SA1->A1_CGC)==14),'@R 99.999.999/9999-99','@R 999.999.999-99')) )
	Aadd(aClixFor, IIF(!lNFeDev.Or.lImportacao, (AllTrim(SA2->A2_END)+IIF(!Empty(SA2->A2_COMPLEM),'-'+SA2->A2_COMPLEM,'')+IIF(Empty(SA2->A2_NR_END),'',', '+SA2->A2_NR_END )), ;
															    (AllTrim(SA1->A1_END)+IIF(!Empty(SA1->A1_COMPLEM),'-'+SA1->A1_COMPLEM,'')+IIF(SA1->(FieldPos("A1_NR_END"))>0, IIF(Empty(SA1->A1_NR_END),'',', '+SA1->A1_NR_END ),'')) )  )
	               
	Aadd(aClixFor, IIF(!lNFeDev.Or.lImportacao,(AllTrim(SA2->A2_MUN)+' - '+SA2->A2_EST), AllTrim(SA1->A1_MUN)+' - '+SA1->A1_EST) )
	Aadd(aClixFor, IIF(!lNFeDev.Or.lImportacao, ( IIF(!Empty(SA2->A2_DDD),'('+AllTrim(SA2->A2_DDD)+') - ','')+SA2->A2_TEL),;
																( IIF(!Empty(SA1->A1_DDD),'('+AllTrim(SA1->A1_DDD)+') - ','')+SA1->A1_TEL)) )
EndIf

Return()
************************************************************
Static Function PreDocRefresh()
************************************************************
//�����������������������������������������������������Ŀ
//� FUNCAO DISPARADA AO CLICAR NO GRID         		    �
//|	Chamada -> Visual_XML()->oBrwV:oBrowse:bLDblClick	|
//�������������������������������������������������������
nPosItem := Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_ITEM'	})
nPosProd := Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_COD'	})
nPosLocal:= Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_LOCAL'	})
nPosNfOri:= Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_NFORI'	})
   

//�������������������������������������������������������������Ŀ
//� oBrwV:oBrowse:ColPos = POSICAO EM QUE SE ENCONTRA NO aCols  �
//���������������������������������������������������������������
If oBrwV:oBrowse:ColPos == nPosProd  .And. Len(aProdxFor) > 0 .And. Ascan(aProdxFor, {|X| X[1] == oBrwV:aCols[oBrwV:NAT][nPosItem] }) > 0

	//���������������������������������������������������������Ŀ
	//� MOSTRA TELA COM OS PRODUTOS QUE JA ESTA AMARRADOS - SA5	|
	//�����������������������������������������������������������
	Processa ({|| TMPProdxFor()  },'Verificando Produto X Fornecedor','Processando...', .T.)

Else

	//���������������������������������������������������������������������Ŀ
	//� RETORNA O VALOR ORIGINAL DOS EVENTOS - PARA EDICAO NORMAL DO ACOLS	|
	//�����������������������������������������������������������������������
/*    oBrwV:oBrowse:bLDblClick:=	{||	IIF(	oBrwV:oBrowse:ColPos == Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_COD'}) .And.;
    										Len(aProdxFor) > 0 .And. ;
    										Ascan(aProdxFor, {|X| X[1] == oBrwV:aCols[oBrwV:NAT][Ascan(oBrwV:aHeader, {|X| AllTrim(X[2])=='D1_ITEM'})] }) > 0,;
    									 	(PreDocRefresh(), VerifStatus()), oBrwV:EditCell()) }
	oBrwV:Editcell()
	oBrwV:oBrowse:lModified	:= .T.
*/

	If	oBrwV:oBrowse:ColPos == Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_COD'}) .And.;
	    Len(aProdxFor) > 0 .And. ;
	    Ascan(aProdxFor, {|X| X[1] == oBrwV:aCols[oBrwV:NAT][Ascan(oBrwV:aHeader, {|X| AllTrim(X[2])=='D1_ITEM'})] }) > 0
	    
	    oBrwV:oBrowse:bLDblClick :=	{||	PreDocRefresh(), VerifStatus(), ViewTrigger(oBrwV:NAT, oBrwV:oBrowse:ColPos), DadosSB1(), GravaZXP()  }
	    
	EndIf

	oBrwV:Editcell()
	oBrwV:oBrowse:lModified	:= .T.
	
EndIf

oBrwV:oBrowse:Refresh() 
    
Return()
************************************************************
Static Function GravaZXP()
************************************************************

nPosItem:= aScan(aMyHeader,{|x| Alltrim(x[2]) == "D1_ITEM" })
cItem 	:= oBrwV:aCols[oBrwV:NAT][nPosItem]

nPosProd:= aScan(aMyHeader,{|x| Alltrim(x[2]) == "D1_COD"  })
cProdXML:= oBrwV:aCols[oBrwV:NAT][nPosProd]
                                                         
//�������������������������������������������������������������Ŀ
//� oBrwV:oBrowse:ColPos = POSICAO EM QUE SE ENCONTRA NO aCols  �
//���������������������������������������������������������������
If oBrwV:oBrowse:ColPos == nPosProd
	
	If !Empty(cProdXML)
	
		DbSelectArea('ZXP');DbSetOrder(1);DbGoTop()
		If DbSeek(xFilial('ZXP') + ZXM->ZXM_CHAVE + cItem, .F.)
			If AllTrim(ZXP->ZXP_PROD) != AllTrim(cProdXML) 
			   RecLock("ZXP", .F.)
			       ZXP->ZXP_PROD  := AllTrim(cProdXML)
			   MsUnLock()
			EndIf

		Else
		
			RecLock("ZXP", .T.)
				ZXP->ZXP_FILIAL	:=	xFilial('ZXP')
				ZXP->ZXP_DOC  	:= 	ZXM->ZXM_DOC
				ZXP->ZXP_SERIE 	:= 	ZXM->ZXM_SERIE
				ZXP->ZXP_CHAVE 	:= 	ZXM->ZXM_CHAVE
				ZXP->ZXP_ITEM  	:= 	cItem
				ZXP->ZXP_PROD  	:= 	AllTrim(cProdXML)
			MsUnLock()
			   
		EndIf
	
	Else

		DbSelectArea('ZXP');DbSetOrder(1);DbGoTop()
		If DbSeek(xFilial('ZXP') + ZXM->ZXM_CHAVE + cItem, .F.)
			RecLock("ZXP", .F.)
				DbDelete()
			MsUnLock()
		EndIf

	EndIf

EndIf
	
Return()
************************************************************
Static Function AtuPreDoc()
************************************************************
//�����������������������������������������������������������������Ŀ
//|	ATUALIZA VARIAVEIS												|
//� Apos Gerar Pre-Nota\Doc.Entrada                                	|
//� Chamada -> Visual_XML()->Btn Gera Pre-Nota\Doc.Entrada         	�
//�������������������������������������������������������������������

lGravado        := .T.
oBrwV:lUpdate   := .F.
cXMLStatus      := 'Status XML:' +IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+"  GRAVADO"
SetKey( VK_F6,  Nil)
SetKey( VK_F7,  Nil)
SetKey( VK_F8,  Nil)
SetKey( VK_F9,  Nil)
SetKey( VK_F10, Nil)
oBtnPreDoc:Refresh()
oBtnReplic:Refresh()
oBtnPC:Refresh()
oBrwV:oBrowse:Refresh()
oBrwV:Refresh()
FreeObj(oSayStatus)
oSayStatus:= TSay():New( D(082),D(013),{|| cXMLStatus },oGrp2,,oFontP,.F.,.F.,.F.,.T.,CLR_GREEN,CLR_WHITE,100,008)
oSayStatus:Refresh()

If ZXM->ZXM_FORMUL == 'S'
	FreeObj(oSayNNFRef)
	FreeObj(oSaySNFRef)
	oSayNNFRef := TSay():New( D(021),D(260),{|| IIF(ZXM->ZXM_FORMUL == 'S',"NF Refer�ncia:",'')},oGrp2,,oFontP2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(060),D(012) )
	oSaySNFRef := TSay():New( D(021),D(320),{|| IIF(ZXM->ZXM_FORMUL == 'S',PadL(ZXM->ZXM_DOCREF, TamSx3('F1_DOC')[1],'') +' - '+ PadL(ZXM->ZXM_SERREF, TamSx3('F1_SERIE')[1],''),'')},oGrp2,,oFontP2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(012) )
	oSayNNFRef:Refresh()
	oSaySNFRef:Refresh()
EndIf               


Return()
************************************************************
Static Function VerifStatus()
************************************************************
//�����������������������������������������������������Ŀ
//�   ATUALIZA STATUS e VARIAVEIS                 		�
//|	Chamada -> Visual_XML()->oBrwV:oBrowse:bLDblClick	|
//�������������������������������������������������������

Do Case
	Case ZXM->ZXM_STATUS $ 'I\G' .And. ZXM->ZXM_SITUAC != 'C'//.And. ZXM->ZXM_SITUAC == 'A'
	   //�����������������������������������������������Ŀ
	   //�    XML - GRAVADO PRE-NOTA \ DOC.ENTRADA       �
	   //�������������������������������������������������
	   oBtnPreDoc:Enable()                                //    Habilita Botao
	   oBtnReplic:Enable()
	   oBtnPC:Enable()
	   oBrwV:lUpdate := IIF(lGravado,.F.,.T.)     //    Ativa\Desativa edicao Linha
	   
	   If lGravado
	       	oBrwV:oBrowse:bLDblClick := {|| MsgAlert( IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+' j� gravado... N�o � poss�vel edi��o!!!'+ENTER+'Para VISUALIZAR o documento click no bot�o '+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada") ) }
	   		SetKey( VK_F6, {|| MsgAlert( IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+' j� gravado... N�o � poss�vel edi��o!!!'+ENTER+'Para VISUALIZAR o documento click no bot�o '+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada")) })
	   		SetKey( VK_F7, {|| MsgAlert( IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+' j� gravado... N�o � poss�vel edi��o!!!'+ENTER+'Para VISUALIZAR o documento click no bot�o '+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada")) })
	   		SetKey( VK_F8, {|| MsgAlert( IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+' j� gravado... N�o � poss�vel edi��o!!!'+ENTER+'Para VISUALIZAR o documento click no bot�o '+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada")) })
	   		SetKey( VK_F9, {|| MsgAlert( IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+' j� gravado... N�o � poss�vel edi��o!!!'+ENTER+'Para VISUALIZAR o documento click no bot�o '+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada")) })
	   		SetKey( VK_F10,{|| MsgAlert( IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+' j� gravado... N�o � poss�vel edi��o!!!'+ENTER+'Para VISUALIZAR o documento click no bot�o '+IIF(cPreDoc=='PRE',"Pr� Nota","Doc.Entrada")) })

		   oBtnReplic:Disable()
		   oBtnPC:Disable()
		   oBrwV:lUpdate := .F.    //    Ativa\Desativa edicao Linha
	

	   Else                                                                     
		   	SetKey( VK_F6, {|| SelItemPC() })    //    Click - F6   
			SetKey( VK_F7, {|| Processa ( ReplicaProd('PROD')	,'Selecionando Registros','Processando...', .T.) } )    //    Click - F7
			SetKey( VK_F8, {|| Processa ( ReplicaProd('ALMOX')	,'Selecionando Registros','Processando...', .T.) } )    //    Click - F8
		  	If cPreDoc=='DOC'
				SetKey( VK_F9, {|| Processa ( ReplicaProd('TES')	,'Selecionando Registros','Processando...', .T.) } )    //    Click - F9
			Else
				SetKey( VK_F9,{|| Nil })
			EndIf
			If lNFeDev
				SetKey( VK_F10, {|| IIF(lNFeDev, MsgRun('Verificando NF Origem... ', 'Aguarde... ',{|| Doc_DevRet() }) , ), oBrwV:oBrowse:Refresh() })
			Else
				SetKey( VK_F10,{|| Nil })
			EndIf
		EndIf

	Case ZXM->ZXM_STATUS $ 'X\R' .And. ZXM->ZXM_SITUAC != 'C'
	   //�����������������������������������������������Ŀ
	   //�    XML - CANCELADO \ RECUSADO                 �
	   //�������������������������������������������������
	   oBtnPreDoc:Disable()    //    Desabilita Botao
	   oBtnReplic:Disable()
	   oBtnPC:Disable()
	   oBrwV:lUpdate := .F.    //    Desabilita Edicao Linha
       oBrwV:oBrowse:lModified := .F.
	   oBrwV:oBrowse:bLDblClick :=    {|| MsgAlert( cXMLStatus+'.   N�o � poss�vel edi��o !!!' ) }
	
	   SetKey( VK_F6, {|| MsgAlert('XML '+IIF(ZXM->ZXM_STATUS=='C','CANCELADO','RECUSADO' )+'... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F7, {|| MsgAlert('XML '+IIF(ZXM->ZXM_STATUS=='C','CANCELADO','RECUSADO' )+'... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F8, {|| MsgAlert('XML '+IIF(ZXM->ZXM_STATUS=='C','CANCELADO','RECUSADO' )+'... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F9, {|| MsgAlert('XML '+IIF(ZXM->ZXM_STATUS=='C','CANCELADO','RECUSADO' )+'... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F10,{|| MsgAlert('XML '+IIF(ZXM->ZXM_STATUS=='C','CANCELADO','RECUSADO' )+'... N�o � poss�vel edi��o!!!') })
	   		
	Case ZXM->ZXM_SITUAC == 'C'
	   //�����������������������������������������������Ŀ
	   //�    XML - CANCELADO PELO FORNECEDOR            �
	   //�������������������������������������������������
	   oBtnPreDoc:Disable()    //    Desabilita Botao
	   oBtnReplic:Disable()
	   oBtnPC:Disable()
	   oBrwV:lUpdate := .F.    //    Desabilita Edicao Linha
       oBrwV:oBrowse:lModified := .F.
	   oBrwV:oBrowse:bLDblClick :=    {|| MsgAlert( cXMLStatus+'.   N�o � poss�vel edi��o !!!' ) }
		
	   SetKey( VK_F6, {|| MsgAlert('XML CANCELADO... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F7, {|| MsgAlert('XML CANCELADO... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F8, {|| MsgAlert('XML CANCELADO... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F9, {|| MsgAlert('XML CANCELADO... N�o � poss�vel edi��o!!!') })
	   SetKey( VK_F10,{|| MsgAlert('XML CANCELADO... N�o � poss�vel edi��o!!!') })
	   	   
	   MsgAlert('XML Cancelado pelo Fornecedor.  N�o � poss�vel edi��o !!!' )
      
	Case Empty(ZXM->ZXM_SITUAC)
		lValidDlg	:=	.F.
		If MsgYesNo('Status XML inv�lido. Deseja consultar XML na SEFAZ ?') 
			cRet := ExecBlock("ConsNFeSefaz",.F.,.F.,{""})
			lValidDlg	:=	.T.
		EndIf
	
EndCase
                                               
//�����������������������������������������������Ŀ
//�    VERIFICA SE EH CLIENTE OU FORNECEDOR		  |
//�������������������������������������������������
If lNFeDev .And. !Empty(ZXM->ZXM_TIPO)
	aClixFor[1] := IIF(ZXM->ZXM_TIPO$'D\B','Cliente: ','Fornecedor: ') 
EndIf
                                  

oBrwV:oBrowse:Refresh()
oBrwV:Refresh()
Return
************************************************************
Static Function TMPProdxFor()
************************************************************
//������������������������������������������������������������������Ŀ
//� Ao clicar no campo Produto e existir mais de 1 Produto X Cod.For |
//| Browse para usuario informar qual produto ira amarrar com Cod.For|
//� Chamada -> Visual_XML()->bLDblClick                              �
//��������������������������������������������������������������������
Local	nPosPFor    :=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'C7_PRODUTO' })
Local	nTamProd    :=	TamSX3('B1_COD')[1]
Local	cProduto    :=	Space(nTamProd)
Private	cMarca		:= 	''

SetPrvt("oFont1","oDlgSA5","oSay1","oSay2","oSay3","oSay4","oSay5","oGrp1","oBrwSA5","oBtn1","oBtn2")

oFont1     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-09,-11),,.T.,0,,700,.F.,.F.,,,,,, )

oDlgSA5    := MSDialog():New( D(095),D(232),D(475),D(702),"Produto X Fornecedor",,,.F.,,,,,,.T.,,oFont1,.T. )
oSay1      := TSay():New( D(004),D(008),{||"Os Produtos abaixo est�o relacionados com o produto deste fornecedor."},oDlgSA5,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(210),D(008) )
oSay3      := TSay():New( D(019),D(008),{||"Produto: "+ oBrwV:aCols[oBrwV:NAT][nPosPFor] },oDlgSA5,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(150),D(008) )
oGet1      := TGet():New( D(026),D(008),{|u| If(PCount()>0,cProduto :=u,cProduto)},oDlgSA5,D(072),D(008),'',,CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SB1","",,)

oBtn4      := TButton():New( D(024),D(090),"Adicionar",oDlgSA5,{|| AddProdxFor(cProduto), cProduto:= Space(nTamProd), oDlgSA5:Refresh() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )

*******************************
   oTblSA5()
*******************************
cMarca     := GetMark()
DbSelectArea("TMPSA5")
oGrp1      := TGroup():New( D(038),D(004),D(164),D(228),"",oDlgSA5,CLR_BLACK,CLR_WHITE,.T.,.F. )
oBrwSA5    := MsSelect():New( "TMPSA5","OK","",{{"OK","","",""},{"PRODUTO","","Produto",""},{"DESCR","","Descri��o",""}},.F.,cMarca,{D(042),D(008),D(157),D(220)},,, oGrp1 )

oBtn1      := TButton():New( D(170),D(068),"&OK",oDlgSA5, {|| OKProdxFor(), oDlgSA5:End() } ,D(037),D(012),,oFont1,,.T.,,"",,,,.F. )
oBtn2      := TButton():New( D(170),D(116),"&Cancelar",oDlgSA5,{|| oDlgSA5:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )

oBrwSA5:oBrowse:bLDblClick := {|| MarcaSA5(), oBrwSA5:oBrowse:Refresh() }
oBrwSA5:oBrowse:SetFocus()
oBrwSA5:oBrowse:Refresh()

oDlgSA5:Activate(,,,.T.)


IIF(Select("TMPSA5") != 0, TMPSA5->(DbCLoseArea()), )

Return
************************************************************
Static Function oTblSA5()
************************************************************
//���������������������������������������������Ŀ
//� Cria\Limpa\Atualiza Tabela Temporaria       |
//� Chamada -> TMPProdxFor                      �
//�����������������������������������������������
Local aFds := {}
Local cTmp
Local nPosItem:=    Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_ITEM' })


If Select("TMPSA5") == 0
   Aadd( aFds , {"OK"      ,"C",002,000} )
   Aadd( aFds , {"PRODUTO" ,"C",015,000} )
   Aadd( aFds , {"DESCR"   ,"C",030,000} )
   Aadd( aFds , {"_RECNO"  ,"N",100,000} )

   cTmp := CriaTrab( aFds, .T. )
   Use (cTmp) Alias TMPSA5 New Exclusive
   Aadd(aArqTemp, cTmp)
Else
   DbSelectArea("TMPSA5")
   RecLock('TMPSA5', .F.)
       Zap
   MsUnLock()
EndIf

DbSelectArea("TMPSA5")
For nX := 1 To Len(aProdxFor)
	If oBrwV:aCols[oBrwV:NAT][nPosItem] == aProdxFor[nX][1]
   		RecLock("TMPSA5",.T.)  
       		TMPSA5->PRODUTO	:= aProdxFor[nX][3]
       		TMPSA5->DESCR   := aProdxFor[nX][4]
       		TMPSA5->_RECNO	:= nX
   		MsUnLock()
 	EndIf
Next

TMPSA5->(DbGoTop())

IIF(Type('oBrwSA5:oBrowse:Refresh()')!='U', (oBrwSA5:oBrowse:Refresh(),oBrwSA5:oBrowse:Setfocus()), )
oBrwV:oBrowse:Refresh()
oBrwV:Refresh()


Return
************************************************************
Static Function MarcaSA5()
************************************************************
//���������������������������������������������Ŀ
//� Marca apenas 1 para selecao                 |
//� Chamada -> TMPProdxFor                      �
//�����������������������������������������������
_nRecno    :=    TMPSA5->_RECNO

DbSelectArea('TMPSA5');DbGoTop()
Do While !Eof()
   RecLock("TMPSA5",.F.)
       TMPSA5->OK  := IIF(_nRecno==TMPSA5->_RECNO, cMarca, '')
   MsUnLock()
   DbSkip()
EndDo

DbGoTo(_nRecno)
IIF(Type('oBrwSA5:oBrowse:Refresh()')!='U', oBrwSA5:oBrowse:Refresh(), )

Return
************************************************************
Static Function OKProdxFor()
************************************************************
//���������������������������������������������Ŀ
//� Grava no aCols o item selecionado           |
//� Chamada -> TMPProdxFor                      �
//�����������������������������������������������
Local    nPosProd    :=    Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_COD' })

oBrwV:aCols[oBrwV:NAT][nPosProd] := TMPSA5->PRODUTO

oBrwV:oBrowse:Refresh()

Return
************************************************************
Static Function AddProdxFor(cProduto)
************************************************************
//������������������������������������������������������������Ŀ
//� Usuario podera adicionar outro produto para amarrar - SA5  |
//� Chamada -> TMPProdxFor                                     �
//��������������������������������������������������������������
nTotRecno	:= 	0
nPosPFor    :=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'C7_PRODUTO' })
nPDescFor   :=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'B1_DESCFOR' })		// DESCRICAO DO CODIGO DO FORNECEDOR [GRAVO NO SA5]
_nRecno     :=	TMPSA5->_RECNO

DbGoTop()
Do While !Eof()
   If AllTrim(TMPSA5->PRODUTO) == AllTrim(cProduto)
       Alert('PRODUTO '+SB1->B1_COD+' J� RELACIONADO !!!')
       Return()
   EndIf

   RecLock("TMPSA5",.F.)
       TMPSA5->OK  := ''
   MsUnLock()

   nTotRecno++
   DbSkip()
EndDo

RecLock("TMPSA5",.T.)
   TMPSA5->OK  		:= cMarca
   TMPSA5->PRODUTO  := SB1->B1_COD
   TMPSA5->DESCR    := SB1->B1_DESC
   TMPSA5->_RECNO   := nTotRecno++
MsUnLock()

DbSelectArea('ZXM')
DbSelectArea('SA5');DbSetOrder(1);DbGoTop()
If !Dbseek(xFilial("SA5")+ ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA + SB1->B1_COD, .F.)
	Reclock("SA5",.T.)
       SA5->A5_FILIAL    :=    xFilial("SA5")
       SA5->A5_FORNECE   :=    ZXM->ZXM_FORNEC
       SA5->A5_LOJA      :=    ZXM->ZXM_LOJA
       SA5->A5_CODPRF    :=    oBrwV:aCols[oBrwV:NAT][nPosPFor]
       SA5->A5_PRODUTO   :=    SB1->B1_COD
       SA5->A5_NOMPROD   :=    IIF(nPDescFor>0, SubStr(oBrwV:aCols[oBrwV:NAT][nPDescFor],1,30),'') //SubStr(SB1->B1_DESC,1,30)
       SA5->A5_NOMEFOR   :=    Posicione("SA2",3,xFilial("SA2")+ZXM->ZXM_CGC,"A2_NOME")
	   MsUnlock()
EndIf

MsgInfo('PRODUTO RELACIONADO COM SUCESSO !!!'+ENTER+ENTER+ AllTrim(SB1->B1_COD)+' - '+AllTrim(SB1->B1_DESC) )

DbGoTop()

Return()
************************************************************
Static Function CarregaACols()
************************************************************
//���������������������������������Ŀ
//� Carrega valores para aMyCols    �
//� Chamada -> Visual_XML()         �
//�����������������������������������
Local aArea3_2	:= GetArea()
Local cNumPC	:=	''
Local cItemPC	:=	''
Local cNFOrig	:=	''
Local cSerOrig	:=	''
Local cItemOrig	:=	''
Local cAlmox   	:=	''
Local cProdSD1	:=	''
Local cTes		:=	''
Local cAlmox  	:=	''
Local cProdSA5	:=	''
Local cEanCod	:=	''
Local cCompSB1	:= 	''
Local cCtaCont	:= 	''
Local cCCusto	:=	''		           		
Local _cTipo	:=	''
Local cXML		:=	''
Local aItens 	:=	{}
Local nSD1Recno := 	0
Local nItemXML	:= 	0
Local nPrecoNFe	:= 	0

aXMLOriginal	:=	{}
cTpFormul 		:= 	''


//���������������������������������Ŀ
//� Verifica se SB1 eh compartilhado|
//�����������������������������������
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('SB1'))
	cCompSB1	:= SX2->X2_MODO
EndIf

Private aItemProdxFor 	:= {}

//���������������������������������Ŀ
//� Var Private - Visual_XML        |
//�����������������������������������
lGravado	:=	.F.
nTotalNFe	:=	0



cXML	:=	AllTrim(ZXM->ZXM_XML)
cXML	+=	ExecBlock("VerificaZXN",.F.,.F., {ZXM->ZXM_FILIAL, ZXM->ZXM_CHAVE})	//	VERIFICA SE O XML ESTA NA TABELA ZXN


//������������������������������������������Ŀ
//�  CabecXmlParser -   ABRE XML - CABEC     �
//�������������������������������������������� 
*******************************************************************
   aRet	:=	ExecBlock("CabecXmlParser",.F.,.F., {cXML})
*******************************************************************

If aRet[1] == .T.	//	aRet[1] == .F. SIGNIFICA XML COM PROBLEMA NA ESTRUTURA

	ProcRegua(nItensXML)
	
   	For nX := 1 To nItensXML
                               
	   	IncProc('Verificando Item '+AllTrim(Str(nX)) +' de '+ AllTrim(Str(nItensXML))  ) 
		//�����������������������������������������Ŀ
		//�  ItemXmlParser -  ABRE XML - ITEM       �
		//������������������������������������������� 
       	****************************************
			ItemXmlParser(nX)   
       	****************************************


           	nItemXML := PadL(AllTrim(Str(nX)),4,'0')
		
		
           //�����������������������������������������������������������������Ŀ
           //� BUSCA DADOS DA PRE-NOTA \ DOC.ENTRADA, CASO JA TENHA GRAVADO    �
           //�������������������������������������������������������������������
           	lGravado		:= 	IIF(lGravado, .T., .F.) // CASO TENHA INCLUSO 1 ITEM E OS OUTROS NAO.. EH MARCADO COMO GRAVADO.
           	cNumPC 			:=	cItemPC  := cAlmox := cProdSD1 :=  cTes	:=	''
			cCtaCont    	:=  cCCusto  := _cTipo := cProdSA5 :=  ''
           	cCodFor 		:= ''
           	aItemProdxFor	:= 	{}
           	aProdFil		:=	{}


			//��������������������������������������������������Ŀ
			//�VERIFICA SE XML ESTA GRAVADO NO DOC.ENTRADA       �
			//����������������������������������������������������
			DbSelectArea('SD1');DbSetOrder(1);DbGoTop()    //  1 - D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
			If DbSeek( ZXM->ZXM_FILIAL + IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC+ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF+ZXM->ZXM_SERREF) + ZXM->ZXM_FORNEC+ ZXM->ZXM_LOJA, .F.)
				Do While !Eof() .And. SD1->D1_DOC + SD1->D1_SERIE + SD1->D1_FORNECE+ SD1->D1_LOJA == IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC+ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF+ZXM->ZXM_SERREF) + ZXM->ZXM_FORNEC+ ZXM->ZXM_LOJA
					If  SD1->D1_ITEM == nItemXML
						_cTipo		:=	SD1->D1_TIPO
						cNumPC		:=	SD1->D1_PEDIDO
						cItemPC		:=	SD1->D1_ITEMPC
						cAlmox		:=	SD1->D1_LOCAL
						cProdSD1	:=	SD1->D1_COD
						cTes		:=	SD1->D1_TES
			       		cCtaCont	:= 	SD1->D1_CONTA		//	Conta Contabil dn Prod.
		           		cCCusto		:=	SD1->D1_CC			//	Centro de Custo
						cNFOrig		:=	SD1->D1_NFORI
						cSerOrig	:=	SD1->D1_SERIORI
						cItemOrig	:=	SD1->D1_ITEMORI
						nSD1Recno	:=	SD1->(Recno())
						lGravado  	:=	.T.

						Exit
					EndIf
					DbSkip()
				EndDo
			EndIf


 
           nPrecoNFe := (nTotal) //-nDescont)
           nTotalNFe += (nTotal) //-nDescont)
	                        
	
           //�����������������������������Ŀ
           //�        XML - NORMAL         �
           //�������������������������������
			If !lNFeDev

		       ************************************
					ForCliMsBlql('SA2', ZXM->ZXM_CGC)
			   ************************************
               cCodFor    :=   SA2->A2_COD
               cLojaFor   :=   SA2->A2_LOJA

			Else

	           //�����������������������������Ŀ
	           //�        XML - DEVOLUCAO      �
	           //�������������������������������   
	           cTabela := IIF(ZXM->ZXM_TIPO $ 'D\B' , 'SA1', 'SA2' )
	           
				************************************
					ForCliMsBlql(cTabela, ZXM->ZXM_CGC)
				************************************
				cCodFor    	:=   IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->A1_COD, 	SA2->A2_COD)
				cLojaFor   	:=   IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->A1_LOJA, 	SA2->A2_LOJA )
				SA2Recno	:=   IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->(Recno()), SA2->(Recno()) )
				
			EndIf


           //������������������������������������������������������������������Ŀ
           //�  ATUALIZA STATUS ZXM - CASO XML ESTEJA GRAVADO NO DOC.ENTRADA    �
           //|	CASO XML NAO ESTEJA CANCELADO NA SEFAZ							|
           //��������������������������������������������������������������������
           If lGravado
				DbSelectArea("ZXM")
                If ZXM->ZXM_STATUS != 'G' .And. ZXM->ZXM_SITUAC != 'C'
                	RecLock('ZXM',.F.)
        	           	ZXM->ZXM_STATUS := 'G'
                  		ZXM->ZXM_TIPO	:=	_cTipo
                	MsUnlock()
				EndIf
           
           Else

			   //������������������������������������������������������������������Ŀ
			   //� VERIFICA SE JA HOUVE ALGUMA GRAVACAO PARCIAL DOS ITENS DO XML	|
			   //| UTILIZADO PARA QUE USUARIO NAO PRECISE DIGITAR TUDO NOVAMENTE	|
			   //��������������������������������������������������������������������
				DbSelectArea('ZXP');DbSetOrder(1);DbGoTop()
				If DbSeek(xFilial('ZXP') + ZXM->ZXM_CHAVE + nItemXML, .F.) .And. !lGravado
	
					cProdSA5 :=  AllTrim(ZXP->ZXP_PROD)
				
				Else
				
					//�������������������������������������������������������������������������������������Ŀ
					//| VERIFICA SE EH UMA TRANSFERENCIA E VERIFICA SE A TABELA SB1 E COMPARTIILHADA		|
					//| PEGO OS CODIGOS DE PRODUTO QUE ESTAO NO XML E JOGA PARA ACOLS                       |
		           	//���������������������������������������������������������������������������������������
	               	If	Ascan(_aEmpFil, {|X| X[1] == ZXM->ZXM_CGC } ) != 0 .And. cCompSB1 == 'C'
						Aadd(aProdFil, cProdFor ) 
							
					Else
		               
					   //�����������������������������������������������Ŀ
					   //� BUSCA O CODIGO DO PRODUTO PELO EAN DO XML	 �
					   //�������������������������������������������������
		               DbSelectArea('SB1');DbSetOrder(5);DbGoTop()	
		               If DbSeek( xFilial('SB1') + cEAN, .F. ) .And. !Empty(cEAN)
	                        cEanCod	:=	SB1->B1_COD
		               Else
	
	
						//�����������������������������������������������Ŀ
						//� VERIFICA SE EXISTE NO SA5 PROD.FORNECEDOR     �
						//| aProdxFor=Array com ProdutoxFornecedor        |
						//�������������������������������������������������
			               /*/*DbSelectArea('SA5');DbSetOrder(1);DbGoTop()
			               If Dbseek(xFilial("SA5")+ ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA, .T.)
			                   Do While !Eof().And. ZXM->ZXM_FORNEC == cCodFor .And. ZXM->ZXM_LOJA == cLojaFor 
			                   		If AllTrim(SA5->A5_CODPRF) == AllTrim(cProdFor)
				                        If Ascan(aItemProdxFor, {|X|X[3] == SA5->A5_PRODUTO } ) == 0
											Aadd(aItemProdxFor, {nItemXML, SA5->A5_CODPRF, SA5->A5_PRODUTO, SA5->A5_NOMPROD} )
										EndIf
									EndIf
			                      	DbSkip()
			                   EndDo
				         	EndIf
				     		*/                                      
				     		
			               	//DbSelectArea('SA5');DbSetOrder(5);DbGoTop()
			               	DbSelectArea('SA5');DbSetOrder(1);DbGoTop()
							cArquivo    	:= CriaTrab(NIL,.F.)
							cChave			:= 'A5_FILIAL+A5_FORNECE+A5_CODPRF' //IndexKey(6)
							Aadd(aArqTemp, cArquivo )
				
							cCondicao 		:= 'SA5->A5_FILIAL  == "'+ xFilial("SA5") + '" .And. '
							cCondicao 		+= 'SA5->A5_FORNECE == "'+ ZXM->ZXM_FORNEC+ '" .And. '
							cCondicao 		+= 'SA5->A5_LOJA    == "'+ ZXM->ZXM_LOJA  + '" .And. '
							cCondicao 		+= 'SA5->A5_CODPRF  == "'+ PadR( cProdFor,  TamSx3("A5_CODPRF")[1], '')+'" '
							IndRegua("SA5",cArquivo,cChave,,cCondicao,'Filtro Produto X Fornecedor')  // INDREGUA( cAlias, cIndice, cExpress, [ xOrdem] , [ cFor ], [ cMens ], [ lExibir ] ) -> nil
	                                 
						
		                   Do While !Eof().And. SA5->A5_FORNECE == cCodFor .And. SA5->A5_LOJA == cLojaFor 
		                   
		                   		If AllTrim(SA5->A5_CODPRF) == AllTrim(cProdFor)
			                        If Ascan(aItemProdxFor, {|X|X[3] == SA5->A5_PRODUTO } ) == 0
										Aadd(aItemProdxFor, {nItemXML, SA5->A5_CODPRF, SA5->A5_PRODUTO, SA5->A5_NOMPROD} )
									EndIf
								EndIf
		                   
			                   DbSelectArea('SA5')
			                   DbSkip()
		                   EndDo
	
					
						EndIf
	
					EndIf
						
	
				   	If !Empty(cEanCod)				
					   //�����������������������������������Ŀ
					   //� CODIGO PRODUTO BUSCADO PELO EAN.	 �
					   //�������������������������������������
						cProdSA5 := cEanCod							
	               
	               	ElseIf Len(aItemProdxFor) > 1 .And. ZXM->ZXM_STATUS == 'I'
					   //��������������������������������������������������������������������������������������������������Ŀ
					   //� QDO XML ESTA IMPORTADO E TEM MAIS DE UM PRODUTO NO SA5...	 									�
					   //| UTILIZADO AO CLICAR NO GRID DA COLUNA PRODUTO... MOSTRAR TELA COM AMARRACAO JA EXISTENTES - SA5	|	
					   //����������������������������������������������������������������������������������������������������
						cProdSA5 := ''
						For nY:=1 To Len(aItemProdxFor)
						   //��������������������������������������������������������������������������������������������������Ŀ
						   //� GUARDA EM aProdxFor(ITEM DO GRID, A5_CODPRF, COD.EMPRESA, DESCRICAO) OS PRODUTOS Q TEM NO SA5	|
						   //| UTILIZADO AO CLICAR NO GRID DA COLUNA PRODUTO... MOSTRAR TELA COM AMARRACAO JA EXISTENTES - SA5	|	
						   //����������������������������������������������������������������������������������������������������
	
							// 	Aadd(aItemProdxFor, {nItemXML, SA5->A5_CODPRF, SA5->A5_PRODUTO, SA5->A5_NOMPROD} )
							Aadd(aProdxFor, aItemProdxFor[nY])	//	UTILIZO aProdxFor no Duplo click
	
						Next
				
				   	ElseIf Len(aProdFil) > 0
			           //�����������������������������������������������������������������������Ŀ
			           //� CASO FOR TRANSFERENCIA E SB1 COMPARTILHADO PEGA O COD. PRODUTO DO XML �
			           //�������������������������������������������������������������������������
						cProdSA5 := aProdFil[1]
	               
	               	Else
						cProdSA5 :=  IIF(Len(aItemProdxFor)==1, aItemProdxFor[1][3], Space(TamSX3('B1_DESC')[1]) )
					EndIf
	
	            EndIf
	            
	            
		           //�����������������������������Ŀ
		           //�        ARMAZEM PADRAO       �
		           //| Busca primeiro do Produto   |
		           //�������������������������������
	
					
			           	SB1->(DbGoTop(), DbSetOrder(1))
			           	If SB1->(DbSeek(xFilial('SB1') + cProdSA5 ,.F.) )
			           		cAlmox 		:= 	AllTrim(SB1->B1_LOCPAD)		//	Local Padrao 
			           		cTes		:= 	AllTrim(SB1->B1_TE)			//	Codigo de Entrada padrao
			           		cCtaCont	:= 	AllTrim(SB1->B1_CONTA)		//	Conta Contabil dn Prod.
			           		cCCusto		:=	AllTrim(SB1->B1_CC)			//	Centro de Custo
						ElseIf SX6->(DbGoTop(), DbSeek( cFilAnt+'MV_LOCPAD' ,.F.) )
			               	cAlmox := AllTrim(SX6->X6_CONTEUD)		               
			            EndIf
		            
			EndIf
            

           //�����������������������������Ŀ
           //�     ALIMENTA aMyACols       �
           //�������������������������������
			DbSelectArea('SD1')
			aItens := {}


			Aadd( aItens, {'D1_ITEM', 		nItemXML 	})

			//��������������������������������������������������������
			//�   CODIGO E DESCRICAO DO PRODUTO DO FORNECEDOR - XML  �
			//��������������������������������������������������������
				Aadd( aItens, {'C7_PRODUTO',  	cProdFor    })
			If TMPCFG->(FieldPos('DESCFOR')) > 0
   				If TMPCFG->DESCFOR == 'SIM'
                	Aadd( aItens, {'B1_DESCFOR', cDescrProd	 })
                EndIf
			Else
				Aadd( aItens, {'B1_DESCFOR', cDescrProd	})
			EndIf


			//��������������������������������������������������������
			//�   	 CODIGO E DESCRICAO DO PRODUTO DA EMPRESA        �
			//��������������������������������������������������������
			Aadd( aItens, {'D1_COD',  IIF(!Empty(cProdSD1), cProdSD1, cProdSA5)})
			If TMPCFG->(FieldPos('DESCEMP')) > 0
                	If TMPCFG->DESCEMP == 'SIM'
                	Aadd( aItens, {'B1_DESC', AllTrim(Posicione('SB1',1,xFilial('SB1')+ IIF(!Empty(cProdSD1), cProdSD1, cProdSA5) ,'B1_DESC')) })
                	EndIf
			EndIf


			//	VERIFICA SE UNIDADE DE MEDIDA DO PRODUTO DO XML EXISTE NO CADASTRO DE MEDIDAS
			//  CASO NAO EXISTA BUSCA DO CADASTRO DE PRODUTO
			If !Empty(Posicione("SAH",1,xFilial("SAH")+Upper(cUM),'AH_UNIMED')) .And. Len(cUm) == 2
				Aadd( aItens, {'D1_UM',		Upper(cUM)	}) 
	
			Else
	
				nPosCod  := Ascan( aItens, {|X| AllTrim(X[1]) == 'D1_COD'	})
				cUM		 := Posicione('SB1',1,xFilial('SB1')+ aItens[nPosCod][2], 'B1_UM')
				If !Empty(cUM)
					Aadd( aItens, {'D1_UM',	 cUM })
				EndIf
			
			EndIf

							                      

			Aadd( aItens, {'D1_LOCAL', 		cAlmox 		})
			Aadd( aItens, {'D1_PEDIDO', 	cNumPC		})
        	Aadd( aItens, {'D1_ITEMPC', 	cItemPC		})
            Aadd( aItens, {'D1_TES',		cTes		})
            Aadd( aItens, {'D1_NFORI',		cNFOrig		})
			Aadd( aItens, {'D1_SERIORI',	cSerOrig	})
			Aadd( aItens, {'D1_ITEMORI',	cItemOrig	})
			Aadd( aItens, {'D1_QUANT',		nQuant		})
			Aadd( aItens, {'D1_VUNIT',		nPreco		})
			Aadd( aItens, {'D1_VALDESC',    nDescont	})
			Aadd( aItens, {'D1_TOTAL', 		nPrecoNFe	})
	   		Aadd( aItens, {'D1_CC', 		cCCusto		})
	   		Aadd( aItens, {'D1_CONTA', 		cCtaCont	})
	   		Aadd( aItens, {'D1_VALFRE', 	vFreteIt	})
	   	//	Aadd( aItens, {'B1_RMS', 		Posicione("SB1",1,xFilial("SB1")+IIF(!Empty(cProdSD1), cProdSD1, cProdSA5),"B1_RMS")	})
	   		Aadd( aItens, {'D1_LOTECTL', 	cLote	 })	   		
	   		Aadd( aItens, {'D1_DTVALID', 	Stod(cDtValid) })  

//	   		Aadd( aItens, {'B1_DTRMS', 		Posicione("SB1",1,xFilial("SB1")+IIF(!Empty(cProdSD1), cProdSD1, cProdSA5),"B1_DTRMS")	})
		
			//	Aadd(aMyHeader,{ Trim(X3_TITULO)+'-'+TMPCFG->NOME_EMP,    X3_CAMPO, X3_PICTURE, X3_TAMANHO, X3_DECIMAL, '', X3_USADO, X3_TIPO, 'SB1' })
           //��������������������������������������������������������������������������������������������������������������Ŀ
           //�     ALIMENTA aMyACols com os dados de aMyHeader, SE NAO EXISTE NO aItens CRIA COM O CONTEUDO PADRAO DO SX3	|
		   //| 	   aMyHeader == TODO O SX3 DO SD1																			|
           //����������������������������������������������������������������������������������������������������������������
			Aadd(aMyCols, Array(Len(aMyHeader)))				//	GERA aMyCols do tamannho de aMyHeader
			nTam := Len(aMyCols)
			
			
			//�����������������������������������������������������������Ŀ
			//�  PRIMEIRO VERIFICA E CARREGA aMyCols COM O ARRAY aItens   �
			//�������������������������������������������������������������
			For _nX:=1 To Len(aItens)

                SX3->(DbSetOrder(2), DbGoTop())                //	PROCURA NO SX3 OS CAMPOS DA aMyHeader
                SX3->(DbSeek(aItens[_nX][1], .F.) )
				
				nPos := Ascan(aMyHeader, {|X| AllTrim(X[2]) == aItens[_nX][1] })
				If nPos > 0
					If aMyHeader[nPos][8] == 'C'                            //           TAMANHO DO CONTUDO DA TAG PODERA SER MAIOR QUE O TAMANHO PADRAO DO CAMPO.... EX.: PRODUTO
						aMyCols[nTam][nPos] := IIF(!Empty(aItens[_nX][2]), IIF(Len(aItens[_nX][2])>aMyHeader[nPos][4], Left(aItens[_nX][2],aMyHeader[nPos][4]), aItens[_nX][2]), Space(aMyHeader[nPos][4]) )
					ElseIf aMyHeader[nPos][8] == 'N'

						If ValType(aItens[_nX][2]) == 'N'
							aMyCols[nTam][nPos] := IIF( aItens[_nX][2]!=0, aItens[_nX][2], CriaVar(aMyHeader[nPos][2],IIF(SX3->X3_CONTEXT=="V",.F.,.T.)) )
                            Else
							aMyCols[nTam][nPos] := IIF(!Empty(aItens[_nX][2]), IIF(Len(aItens[_nX][2])>aMyHeader[nPos][4], Left(aItens[_nX][2],aMyHeader[nPos][4]), aItens[_nX][2]), Space(aMyHeader[nPos][4]) )                            
                            EndIf
					EndIf
				Else
					aMyCols[nTam][_nX] := CriaVar(aMyHeader[_nX][2],IIF(SX3->X3_CONTEXT=="V",.F.,.T.))
				EndIf

			Next
			                
			//��������������������������������������������������������Ŀ
			//�  SEGUNDO CARREGA OS VALORES DEFAULT no aMyCols         �
			//����������������������������������������������������������	                
            For _nX:=1 To Len(aMyHeader)

				If Ascan(aItens, {|X| AllTrim(X[1]) == AllTrim(aMyHeader[_nX][2])} ) == 0

	                SX3->(DbSetOrder(2), DbGoTop())                //	PROCURA NO SX3 OS CAMPOS DA aMyHeader
	                SX3->(DbSeek(aMyHeader[_nX][2], .F.) )

					aMyCols[nTam][_nX] := CriaVar(aMyHeader[_nX][2],IIF(SX3->X3_CONTEXT=="V",.F.,.T.))

			    EndIf

			Next

			//�����������������������������������������������������������������Ŀ
			//�  ADICIONA +1 COLUNA NO aMyCols PARA CONTROLE DE ITEM DELETADO   �
			//�������������������������������������������������������������������
			Aadd(aMyCols[nTam], .F. )
                            
			//�����������������������������������������������������������������Ŀ
			//�  GRAVA DADOS DO XML PARA DEPOIS COMPARAR COM DOC.ENTRADA	 	�
			//�������������������������������������������������������������������
			Aadd(aXMLOriginal, {nItemXML, Right(cProdFor, TamSx3('D1_COD')[1]), cDescrProd, cNCM, nQuant, nPreco } )
	
	Next

EndIf

RestArea(aArea3_2)
Return()
************************************************************
User Function CabecXmlParser()
************************************************************
//���������������������������������Ŀ
//� ABRE XML - CABEC                �
//� Chamada -> CarregaAcols()       �
//� Chamada -> Grava_Xml()          �
//�����������������������������������
Local cArquivo 		:=	ParamIxb[01]
Local cError    	:=	''
Local cWarning    	:=	''
Local aRetorno   	:=	{}

Static nItensXML    := 	0
Static cInfAdic     := 	''
Static cDataNFe    	:=	''
Static cEmitCnpj    :=	''
Static cEmitNome    :=	''
Static cDestNome    :=	''
Static cDestCnpj    :=	''
Static cNotaXML    	:=	''
Static cSerieXML	:=	''
Static cChaveXML	:=	''
Static cNatOper		:=	''  

Static cTranspCgc	:=	''
Static cPlaca		:=	''
Static cTpFrete		:=	''
Static vFrete		:=  0
Static cEspecie		:=	''
Static nVolume		:=	''
Static nPLiquido	:=	''
Static nPBruto		:=	''

Static oXml			:=	''
Static cLote        :=  ''
Static dDataVal     :=  '' //StoD('  \  \  ')
Static cDataVal     :=  '' //StoD('  \  \  ')    
Static cDtValid     :=  ''


lNFeDev    			:=  .F.
lImportacao			:=	.F.

//���������������������������������������Ŀ
//� 			PARSE DO XML              |
//�����������������������������������������
oXml  := 	XmlParser(cArquivo,"_",@cError, @cWarning )


	*************************************
		aRetorno	:=	ReadTagXml(cError)	//	LENDO TAGs DO XML
	*************************************


Return(aRetorno)
************************************************************
Static Function ReadTagXml(cError)
************************************************************
Local aColsXML	:=	{}
Local aRetXML	:=	{}
Local lEstrut	:=	.T.      
Local lSf1 :=.F.                         



If TMPCFG->TAGPROT == 'SIM'

   //��������������������������������������Ŀ
   //� 	VERIFICA SE EXISTE A TAG PROTNFE	|
   //����������������������������������������
   If AllTrim(Type("oXml:_NFEPROC:_PROTNFE")) != "O"

	   	Aadd(aRetXML, .F., 'T')
   		Aadd(aRetXML, 'XML SEM TAG PROTOCOLO DE AUTORIZA��O.')

		Return(aRetXML)
	EndIf

EndIf


						// NAO IMPORTA CT-e
//If Empty(cError) .And. !(AllTrim(TYPE("oXML:_CTEPROC")) == "O") .And. !(AllTrim(TYPE("oXML:_NFES")) == "O")

If Empty(cError) .And. ( 	AllTrim(Type("oXml:_INFNFE")) 	== "O" 	   .Or.;
							AllTrim(Type("oXml:_NFE:_INFNFE")) 	== "O" .Or.;
							AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE")) == "O" )

   //������������������������������Ŀ
   //� QUANTIDADE DE ITEMS DO XML   �
   //��������������������������������  	   
   If AllTrim(Type("oXml:_INFNFE")) == "O"
       aColsXML := aClone(oXml:_INFNFE:_DET)
   ElseIf AllTrim(Type("oXml:_NFE:_INFNFE")) == "O"
       aColsXML := aClone(oXml:_NFE:_INFNFE:_DET)
   ElseIf AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE")) == "O"
       aColsXML := aClone(oXml:_NFEPROC:_NFE:_INFNFE:_DET)
   Else
  
		//������������������������������������������������������������������Ŀ
		//�NAO ENCONTROU NENHUM DAS TAG ACIMA... ENTAO PROCURA A TAG _NFE	 �
		//�SE ENCONTROU CHAMA NOVAMENTE CabecXmlParser          			 �
		//��������������������������������������������������������������������
		nCount	:=	XmlChildCount(oXml) 
		oXml	:=	XmlGetChild(oXML, nCount)
			***********************************
				ReadTagXml()
			***********************************	
   Endif                                            
   

   If aColsXML == Nil
       nItensXML := 1
   Else
       nItensXML := Len(aColsXML)
   Endif


   //������������������������������Ŀ
   //�       VERSAO _INFNFE         |
   //��������������������������������
   If AllTrim(Type("oXml:_INFNFE")) == "O"

       If AllTrim(TYPE("oXml:_INFNFE:_INFADIC:_INFCPL:TEXT"))=="C"
           cInfAdic := AllTrim(oXml:_INFNFE:_INFADIC:_INFCPL:TEXT)
       Endif

	   //����������������������Ŀ
	   //�		EMITENTE		|
	   //������������������������
       cEmitNome    :=    AllTrim(oXml:_INFNFE:_EMIT:_NOME:TEXT)
       If oXml:_INFNFE:_EMIT:_ENDEREMIT:_UF:TEXT  == 'EX'
           cEmitCnpj    :=  'EXTERIOR'
           lImportacao	:=	.T.
       Else
           cEmitCnpj    :=    AllTrim(oXml:_INFNFE:_EMIT:_CNPJ:TEXT)
       EndIf


	   //����������������������Ŀ
	   //�		DESTINATARIO	|
	   //������������������������
       cDestNome    :=    AllTrim(oXml:_INFNFE:_DEST:_NOME:TEXT)
       If oXML:_INFNFE:_DEST:_ENDERDEST:_UF:TEXT  == 'EX'
       		cDestCnpj    :=    'EXTERIOR'
			lImportacao	:=	.T.
       ElseIf AllTrim(Type("oXml:_INFNFE:_DEST:_CNPJ:TEXT"))=='C'
           cDestCnpj      := AllTrim(oXml:_INFNFE:_DEST:_CNPJ:TEXT)
       Else
           cDestCnpj      := AllTrim(oXml:_INFNFE:_DEST:_CPF:TEXT)
	   Endif
       

		//	StoD(StrTran(oXml:_INFNFE:_DET[i]:_PROD:_DI:_DDI:TEXT, '-','',10))					 
       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf  */
      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf      
       
       
       //cDataNFe    :=    SubStr(cDataNFe,1,4)+SubStr(cDataNFe,6,2)+SubStr(cDataNFe,9,2)
       cNotaXML    :=    oXml:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML   :=    oXml:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    :=    oXml:_INFNFE:_IDE:_NATOP:TEXT


	   //��������������������������������������Ŀ
	   //� TAG NFREF:_REFNF	 - DEVOLUCAO 		|
	   //| TAG NFREF:_REFECF - CUPOM FISCAL		|
	   //����������������������������������������
	   If AllTrim(Type("oXml:_INFNFE:_IDE:_NFREF")) == "O" 
			If AllTrim(Type("oXml:_INFNFE:_IDE:_NFREF:_REFNF")) == "O" 
				lNFeDev	:= .T.
			EndIf
       EndIf
      

	   //������������������������������������������������������Ŀ
       //| 				GERA CHAVE DE ACESSO					| 
	   //� VERSAO ANTIGAS NAO TINHA A CHAVE DE ACESSO NO XML	|
	   //��������������������������������������������������������
       If AllTrim(Type('oXml:_INFNFE:_ID:TEXT')) != 'C'
           cChaveXML	:=     oXml:_INFNFE:_IDE:_CUF:TEXT                     //  Codigo UF
	       If AllTrim(Type("oXml:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cChaveXML    	:=	SubStr(oXml:_INFNFE:_IDE:_DEMI:TEXT,3,2)+SubStr(oXml:_INFNFE:_IDE:_DEMI:TEXT,6,2) 
	       Else
	    	    cChaveXML    	:=	SubStr(oXml:_INFNFE:_IDE:_DHEMI:TEXT,3,2)+SubStr(oXml:_INFNFE:_IDE:_DHEMI:TEXT,6,2)     
	       EndIf          
       
  	       
           // cChaveXML   	+=    SubStr(oXml:_INFNFE:_IDE:_DEMI:TEXT,3,2)+SubStr(oXml:_INFNFE:_IDE:_DEMI:TEXT,6,2)
           cChaveXML   	+=    AllTrim(oXml:_INFNFE:_EMIT:_CNPJ:TEXT)     		// 	CNPJ do Emitente
           cChaveXML   	+=    '55'												//	Modelo
           cChaveXML   	+=    PadL(oXml:_INFNFE:_IDE:_SERIE:TEXT,3,'0')    	//	Serie
           cChaveXML    +=    PadL(oXml:_INFNFE:_IDE:_NNF:TEXT,9,'0')        	//	Numero da NF-e
           cChaveXML    +=    oXml:_INFNFE:_IDE:_CNF:TEXT                      //	Codigo Num�rico

           cChaveXML   	+=    GeraDV(cChaveXML)                                 //	DV

       Else
           cChaveXML    :=    AllTrim(SubStr(oXml:_INFNFE:_ID:TEXT,4,200))
       EndIf


	   //����������������������Ŀ
	   //�	  TIPO FRETE    	|
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_MODFRETE:TEXT"))=='C'
	       cTpFrete    :=    AllTrim(oXml:_INFNFE:_TRANSP:_MODFRETE:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	  TRANSPORTADORA	|
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_TRANSPORTA:_CNPJ:TEXT"))=='C'
	       cTranspCgc    :=    AllTrim(oXml:_INFNFE:_TRANSP:_TRANSPORTA:_CNPJ:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     PLACA          |
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_VEICTRANSP:_PLACA:TEXT"))=='C'
	       cPlaca       :=    AllTrim(oXml:_INFNFE:_TRANSP:_VEICTRANSP:_PLACA:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     ESPECIE        |
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_VOL:_ESP:TEXT"))=='C'
	       cEspecie    :=    AllTrim(oXml:_INFNFE:_TRANSP:_VOL:_ESP:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     VOLUME         |
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_VOL:_QVOL:TEXT"))=='C'
	       nVolume    :=    Val(AllTrim(oXml:_INFNFE:_TRANSP:_VOL:_QVOL:TEXT))
	   Endif
	   //����������������������Ŀ
	   //�     PESO BRUTO    	|
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_VOL:_PESOB:TEXT"))=='C'
	       nPBruto    :=    Val(AllTrim(oXml:_INFNFE:_TRANSP:_VOL:_PESOB:TEXT))
	   Endif
	   //����������������������Ŀ
	   //�     PESO LIQUIDO    	|
	   //������������������������
       If AllTrim(Type("oXml:_INFNFE:_TRANSP:_VOL:_PESOL:TEXT"))=='C'
	       nPLiquido    :=    Val(AllTrim(oXml:_INFNFE:_TRANSP:_VOL:_PESOL:TEXT))
	   Endif
	   

   //������������������������������Ŀ
   //�     VERSAO _NFE:_INFNFE      �
   //��������������������������������
   ElseIf AllTrim(Type("oXml:_NFE:_INFNFE")) == "O"

       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT"))=="C"
           cInfAdic:=AllTrim(oXml:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT)
       Endif


	   //����������������������Ŀ
	   //�		EMITENTE		|
	   //������������������������
       cEmitNome    :=    AllTrim(oXml:_NFE:_INFNFE:_EMIT:_XNOME:TEXT)
       If oXml:_NFE:_INFNFE:_EMIT:_ENDEREMIT:_UF:TEXT  == 'EX'
           cEmitCnpj    :=    'EXTERIOR'
           lImportacao	:=	.T.
       Else
           cEmitCnpj    :=    AllTrim(oXml:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT)
       EndIf


	   //����������������������Ŀ
	   //�		DESTINATARIO	|
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_DEST:_XNOME")) == "O"
       		cDestNome    :=    AllTrim(oXml:_NFE:_INFNFE:_DEST:_XNOME:TEXT)
       Else
			cDestNome    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_DEST:_XNOME:TEXT)       
       EndIf
       
       If oXML:_NFE:_INFNFE:_DEST:_ENDERDEST:_UF:TEXT  == 'EX'
			cDestCnpj    :=    'EXTERIOR'
			lImportacao	:=	.T.
       ElseIf AllTrim(Type("oXml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT"))=='C'
           cDestCnpj      := AllTrim(oXml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
       Else
           cDestCnpj      := AllTrim(oXml:_NFE:_INFNFE:_DEST:_CPF:TEXT)
       Endif

      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       
       //cDataNFe    	:=	SubStr(cDataNFe,1,4)+SubStr(cDataNFe,6,2)+SubStr(cDataNFe,9,2)
       cNotaXML    	:=	oXml:_NFE:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML    :=	oXml:_NFE:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    	:=	oXml:_NFE:_INFNFE:_IDE:_NATOP:TEXT
       cChaveXML	:=	AllTrim(SubStr(oXml:_NFE:_INFNFE:_ID:TEXT,4,200))


	   //��������������������������������������Ŀ
	   //� TAG NFREF:_REFNF	 - DEVOLUCAO 		|
	   //| TAG NFREF:_REFECF - CUPOM FISCAL		|
	   //����������������������������������������
	   If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_NFREF")) == "O"
			If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_NFREF:_REFNF")) == "O" 
				lNFeDev	:= .T.
			EndIf
       EndIf
       
	   //����������������������Ŀ
	   //�	  TIPO FRETE    	|
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_MODFRETE:TEXT"))=='C'
	       cTpFrete    :=    AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_MODFRETE:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	  TRANSPORTADORA	|
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_TRANSPORTA:_CNPJ:TEXT"))=='C'
	       cTranspCgc    :=    AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_TRANSPORTA:_CNPJ:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     PLACA          |
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_VEICTRANSP:_PLACA:TEXT"))=='C'
	       cPlaca       :=    AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_VEICTRANSP:_PLACA:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     ESPECIE        |
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_VOL:_ESP:TEXT"))=='C'
	       cEspecie    :=    AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_VOL:_ESP:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     VOLUME         |
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_VOL:_QVOL:TEXT"))=='C'
	       nVolume    :=    Val(AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_VOL:_QVOL:TEXT))
	   Endif
	   //����������������������Ŀ
	   //�     PESO BRUTO    	|
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_VOL:_PESOB:TEXT"))=='C'
	       nPBruto    :=    Val(AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_VOL:_PESOB:TEXT))
	   Endif
	   //����������������������Ŀ
	   //�     PESO LIQUIDO    	|
	   //������������������������
       If AllTrim(Type("oXml:_NFE:_INFNFE:_TRANSP:_VOL:_PESOL:TEXT"))=='C'
	       nPLiquido    :=    Val(AllTrim(oXml:_NFE:_INFNFE:_TRANSP:_VOL:_PESOL:TEXT))
	   Endif
	          

   //������������������������������Ŀ
   //� VERSAO _NFEPROC:_NFE:_INFNFE �
   //��������������������������������
   ElseIf AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE")) == "O"

       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT"))=="C"
           cInfAdic := AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT)
       Endif

	   //����������������������Ŀ
	   //�		EMITENTE		|
	   //������������������������
       cEmitNome    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_EMIT:_XNOME:TEXT)
       If oXml:_NFEPROC:_NFE:_INFNFE:_EMIT:_ENDEREMIT:_UF:TEXT  == 'EX'
           cEmitCnpj    :=    'EXTERIOR'
           lImportacao	:=	.T.
       Else
           cEmitCnpj    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT)
       EndIf


	   //����������������������Ŀ
	   //�		DESTINATARIO	|
	   //������������������������
       cDestNome    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_DEST:_XNOME:TEXT)
       If oXML:_NFEPROC:_NFE:_INFNFE:_DEST:_ENDERDEST:_UF:TEXT  == 'EX'
       		cDestCnpj    :=    'EXTERIOR'
			lImportacao	:=	.T.
       ElseIf AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT"))=='C'
           cDestCnpj      := AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
       Else
           cDestCnpj      := AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_DEST:_CPF:TEXT)
       Endif


       cNotaXML    	:=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML    :=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    	:=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NATOP:TEXT
       
       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf                                            
      */
      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf 
		if type() <>'U'
		endif  
       Else
	   	   ChaveXML	:=	AllTrim(SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_ID:TEXT,4,200))
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       


	  

	   //��������������������������������������Ŀ
	   //� TAG NFREF:_REFNF	 - DEVOLUCAO 		|
	   //| TAG NFREF:_REFECF - CUPOM FISCAL		|
	   //����������������������������������������
	   If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NFREF")) == "O"
			If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NFREF:_REFNF")) == "O" 
				lNFeDev	:= .T.
			EndIf
       EndIf




	   //����������������������Ŀ
	   //�	  TIPO FRETE    	|
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_MODFRETE:TEXT"))=='C'
	       cTpFrete    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_MODFRETE:TEXT)
	   Endif
	   
	   If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TOTAL:_ICMSTOT:_VFRETE:TEXT"))=='C' 
	       vFrete	   := Val(oXml:_NFEPROC:_NFE:_INFNFE:_TOTAL:_ICMSTOT:_VFRETE:TEXT)
	   EndIf    			
	   //����������������������Ŀ
	   //�	  TRANSPORTADORA	|
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_TRANSPORTA:_CNPJ:TEXT"))=='C'
	       cTranspCgc    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_TRANSPORTA:_CNPJ:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     PLACA          |
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VEICTRANSP:_PLACA:TEXT"))=='C'
	       cPlaca       :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VEICTRANSP:_PLACA:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     ESPECIE        |
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_ESP:TEXT"))=='C'
	       cEspecie    :=    AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_ESP:TEXT)
	   Endif
	   //����������������������Ŀ
	   //�	     VOLUME         |
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_QVOL:TEXT"))=='C'
	       nVolume    :=    Val(AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_QVOL:TEXT))
	   Endif
	   //����������������������Ŀ
	   //�     PESO BRUTO    	|
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_PESOB:TEXT"))=='C'
	       nPBruto    :=    Val(AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_PESOB:TEXT))
	   Endif
	   //����������������������Ŀ
	   //�     PESO LIQUIDO    	|
	   //������������������������
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_PESOL:TEXT"))=='C'
	       nPLiquido    :=    Val(AllTrim(oXml:_NFEPROC:_NFE:_INFNFE:_TRANSP:_VOL:_PESOL:TEXT))
	   Endif

         
   
   Else
	   	lEstrut	:=	.F.
   EndIf


   //����������������������������������Ŀ
   //� PROBLEMA COM ESTRUTURA DO XML	|
   //������������������������������������
   Aadd(aRetXML, lEstrut)
   Aadd(aRetXML, IIF(lEstrut, '' , 'ESTRUTURA DO XML INV�LIDA') )


Else
	aProc := {}
	aErros := {}
	aErroErp := {}
       If AllTrim(TYPE("oXML:_CTEPROC")) == "O"

			
			
	
			cError := "CTE-e. N�O � POSSIVEL IMPORTAR NOTA FISCAL DE Conhecimento de  tenasporte ."
			Aadd(aRetXML, .F. )
 		    Aadd(aRetXML, cError )
			

	   		
			
	   ElseIf AllTrim(TYPE("oXML:_NFES")) == "O"
   			cError := "NFS-e. N�O � POSSIVEL IMPORTAR NOTA FISCAL DE SERVI�O ELETRONICA."
			Aadd(aRetXML, .F. )
 		    Aadd(aRetXML, cError )
	   Else
		   //�����������������������������������������������Ŀ
		   //� ERRO NO PARSE - PROBLEMA COM ESTRUTURA DO XML |
		   //�������������������������������������������������
		   nXmlStatus := XMLError()
		   If ( nXmlStatus == XERROR_SUCCESS )
		       If FError() != 0
		           cError	:= "ERRO NO XML - Erro :" +AllTrim(Str(FError())) "
		       Endif
		   Else
		       cError :=	"ERRO NO XML  -  Erro (" + Str( nXmlStatus, 3 ) + ") no XML."
		   EndIf
			Aadd(aRetXML, .F. )
 		    Aadd(aRetXML, cError )
	   EndIf
	   
   

EndIf




//�����������������������������������������������Ŀ
//� 	VERIFICA SE CFOP EH DE DEVOLUCAO		  |
//�������������������������������������������������
If !lNFeDev .And. aRetXML[1] == .T.

   	Processa( {|| },"Verificando Devolu��o \ Retorno \ Importa��o' ",'Processando...', .T.) 
	ProcRegua(nItensXML)
	
   	For nI := 1 To nItensXML
                               
	   	IncProc('Verificando Item '+AllTrim(Str(nI)) +' de '+ AllTrim(Str(nItensXML)) + ' Devolu��o \ Retorno ' ) 
		
		cCFOP := ''

		If AllTrim(Type("oXml:_INFNFE")) == "O"
			If nItensXML > 1
				cCfOp	:=	oXml:_INFNFE:_DET[nI]:_PROD:_CFOP:TEXT
			Else
				cCfOp	:=	oXml:_INFNFE:_DET:_PROD:_CFOP:TEXT
			EndIf
			
		ElseIf AllTrim(Type("oXml:_NFE:_INFNFE")) == "O"
			If nItensXML > 1
			   cCFOP	:=	oXml:_NFE:_INFNFE:_DET[nI]:_PROD:_CFOP:TEXT
			Else
			    cCFOP  	:=	oXml:_NFE:_INFNFE:_DET:_PROD:_CFOP:TEXT
			EndIf
		
		ElseIf AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE")) == "O"
		   	If nItensXML > 1
		       cCFOP   	:=	oXml:_NFEPROC:_NFE:_INFNFE:_DET[nI]:_PROD:_CFOP:TEXT
			Else
			   cCFOP  	:=	oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CFOP:TEXT
			Endif
		
		EndIf
		

		cDevCFOP := '5201/5202/5208/5209/5210'
		cDevCFOP += '5410/5411/5412/5413/5503/5553/5555/5556/5660/5661/5662/5918/5919/5921'
		cDevCFOP += '6201/6202/6208/6209/6210/'
		cDevCFOP += '6410/6411/6412/6413/6503/6553/6555/6556/6660/6661/6662/6918/6919/6921/7201/7202/7210/7211'
		cDevCFOP += '7553/7556/7930'
		                            
		//	REMESSA PARA INDUSTRIALIZACAO
		cRemCFOP :=	'5414/5415/5451/5501/5502/5554/5657/5663/5901/5904/5905/5908/5910/5912/5914/5915/5920/5923/5924'        //5911 foi retirado por Diego pois barrou a entrada de uma nota valida em 15/10/12
		cRemCFOP +=	'6414/6415/6501/6502/6554/6657/6663/6901/6904/6905/6908/6910/6911/6912/6914/6915/6920/6923/6924'

		// SE CONTEM A TAG NFREF E NAO FOR DEVOLUCAO... CASO DE RETORNO DE CONSERTO. (PESQUISA PELO FORNECEDOR)
		If (cCFOP $ cDevCFOP) .Or. (cCFOP $ cRemCFOP)
			lNFeDev := .T.
			Exit
		Else
			lNFeDev := .F.
		EndIf
		    
    Next
    
EndIf



Return(aRetXML)
************************************************************
Static Function ItemXmlParser(i)
************************************************************
//���������������������������������Ŀ
//� ABRE XML - ITENS                �
//� Chamada -> CabecXmlParser()     �
//�����������������������������������
Static cProdFor    	:=    ''
Static cDescrProd	:=    ''
Static cCfOP    	:=    ''
Static cEAN			:=    ''
Static cNCM			:=    ''
Static cUM          :=    ''
Static nQuant       :=    0
Static nPreco       :=    0
Static nTotal       :=    0
Static nDescont    	:=    0
Static vFreteIt     :=    0

If AllTrim(Type("oXml:_INFNFE")) == "O"

   //������������������������������Ŀ
   //�       VERSAO _INFNFE         |
   //��������������������������������
   If nItensXML > 1

       	cProdFor	:=	oXml:_INFNFE:_DET[i]:_PROD:_CPROD:TEXT
       	cDescrProd	:=  oXml:_INFNFE:_DET[i]:_PROD:_PROD:TEXT
		nQuant		:= 	Val(oXml:_INFNFE:_DET[i]:_PROD:_QCOM:TEXT)
       	cUM         :=	oXml:_INFNFE:_DET[i]:_PROD:_UCOM:TEXT
	  
	   	cEAN		:=	IIF(AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_CEAN:TEXT"))=="C", oXml:_INFNFE:_DET[i]:_PROD:_CEAN:TEXT,'')
	   	cNCM		:=	IIF(AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_NCM:TEXT"))=="C", oXml:_INFNFE:_DET[i]:_PROD:_NCM:TEXT,'')
	   		                                                         
	   	cCfOp		:=	oXml:_INFNFE:_DET[i]:_PROD:_CFOP:TEXT
	   		
       	If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_VDESC:TEXT"))=="C"
       	    nDescont :=	Val(oXml:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT)
       	Endif

        //Bennhur 13/07/2020  1                                
		If (type('oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO') == 'C')
           cLote       :=           oXml:_INFNFE:_DET[i]:_PROD:_RASTRO:_NLOTE:TEXT
		    cDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)
           //dDataVal    :=   StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT, "-", "", 10) 
		   //cDataVal    :=   Substr(dDataVal,1,4)+Substr(dDataVal,6,2)+Substr(dDataVal,9,2)
        Endif
		
      	nPreco      :=	Val(oXml:_INFNFE:_DET[i]:_PROD:_VUNCOM:TEXT)
       	nTotal      := 	Val(oXml:_INFNFE:_DET[i]:_PROD:_VPROD:TEXT)
       	cNotaXML	:= 	oXml:_INFNFE:_IDE:_NNF:TEXT
       	cSerieXML   := 	oXml:_INFNFE:_IDE:_SERIE:TEXT
       	cNatOper    := 	oXml:_INFNFE:_IDE:_NATOP:TEXT
       
       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf                                 */
       
      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf      


       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif 
	   
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_INFNFE:_DET[i]:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_INFNFE:_DET[i]:_PROD:_DI:_DDESEMB:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_INFNFE:_DET[i]:_PROD:_DI:_DDI:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_INFNFE:_DET[i]:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_INFNFE:_DET[i]:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_INFNFE:_DET[i]:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif

	///Adicionado por Bruno Sperb
		If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_INFNFE:_DET[i]:_PROD:_DI:vAFRMM:TEXT)
	   Endif  
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:tpViaTransp:TEXT"))=="C" 
			_cTpvia := oXml:_INFNFE:_DET[i]:_PROD:_DI:tpViaTransp:TEXT
		else 
			_cTpvia := '1'
	   Endif
	    If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:tpIntermedio:TEXT"))=="C" 
			_cTpInter := oXml:_INFNFE:_DET[i]:_PROD:_DI:tpIntermedio:TEXT
		else 
			_cTpInter := '0'
	   Endif
		msgalert('Antes da DI 1')
//	  if ! Empty(_cNumDI)
	  	 	//IncluiCD5(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter, strzero(i,3), oXml:_INFNFE:IDE:nNF:TEXT,oXml:_INFNFE:IDE:serie:TEXT,_vAFRMM)
	  //endif

	  
	///Fim da adi��o 						
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VBC:TEXT"))=="C"
			_nXBCII := Val(oXml:_INFNFE:_DET[i]:_IMPOSTO:_II:_VBC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VII:TEXT"))=="C"
			_nXValII := Val(oXml:_INFNFE:_DET[i]:_IMPOSTO:_II:_VII:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VDESPADU:TEXT"))=="C"
			_nDespac := Val(oXml:_INFNFE:_DET[i]:_IMPOSTO:_II:_VDESPADU:TEXT)
	   Endif 
       If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VIOF:TEXT"))=="C"
			_nXValIOF := Val(oXml:_INFNFE:_DET[i]:_IMPOSTO:_II:_VIOF:TEXT)
       Endif


   Else                                                                            
   
       cProdFor		:= 	oXml:_INFNFE:_DET:_PROD:_CPROD:TEXT
       cDescrProd	:=	oXml:_INFNFE:_DET:_PROD:_PROD:TEXT
       nQuant		:=	Val(oXml:_INFNFE:_DET:_PROD:_QCOM:TEXT)
       cUM			:=	oXml:_INFNFE:_DET:_PROD:_UCOM:TEXT

   	   cEAN			:=	IIF(AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_CEAN:TEXT"))=="C", oXml:_INFNFE:_DET:_PROD:_CEAN:TEXT,'')
	   cNCM			:=	IIF(AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_NCM:TEXT"))=="C", oXml:_INFNFE:_DET:_PROD:_NCM:TEXT,'')
	              
	   cCfOp		:=		oXml:_INFNFE:_DET:_PROD:_CFOP:TEXT
	   
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_VDESC:TEXT"))=="C"
           nDescont := Val(oXml:_INFNFE:_DET:_PROD:_VDESC:TEXT)
       Endif

       nPreco   	:=	Val(oXml:_INFNFE:_DET:_PROD:_VUNCOM:TEXT)
       nTotal   	:=	Val(oXml:_INFNFE:_DET:_PROD:_VPROD:TEXT)
       cNotaXML 	:=	oXml:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML	:=	oXml:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper	 	:=	oXml:_INFNFE:_IDE:_NATOP:TEXT   

       //Benhur 13/07/2020  2
		If (type('oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO') == 'C')
	     	cLote       :=           oXml:_INFNFE:_DET:_PROD:_RASTRO:_NLOTE:TEXT
		    cDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)
          	//dDataVal    :=   StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT, "-", "", 10) 
		  	//cDataVal    :=   Substr(dDataVal,1,4)+Substr(dDataVal,6,2)+Substr(dDataVal,9,2)
        Endif

       
     /* If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf)*/
       If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       

       If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT,'-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT, '-','',10)) 
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif

	///Adicionado por Bruno Sperb
	If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT)
	   Endif 
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT"))=="C" 
			_cTpvia := oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT
		else
			_cTpvia :='1'
	   Endif
	    If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT"))=="C" 
			_cTpInter := oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT
		else 
			_cTpInter :='0'
	   Endif
	  // msgalert('Antes da DI 2')
	  //  if ! Empty(_cNumDI)
	  //	 	IncluiCD5(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter, strzero(i,3), oXml:_INFNFE:IDE:nNF:TEXT,oXml:_INFNFE:IDE:serie:TEXT,_vAFRMM)
	//	endif

	///Fim da adi��o 	
 
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_IMPOSTO:_II:_VBC:TEXT"))=="C"
			_nXBCII := Val(oXml:_INFNFE:_DET:_IMPOSTO:_II:_VBC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_IMPOSTO:_II:_VII:TEXT"))=="C"
			_nXValII := Val(oXml:_INFNFE:_DET:_IMPOSTO:_II:_VII:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_IMPOSTO:_II:_VDESPADU:TEXT"))=="C"
			_nDespac := Val(oXml:_INFNFE:_DET:_IMPOSTO:_II:_VDESPADU:TEXT)
	   Endif 
       If AllTrim(TYPE("oXml:_INFNFE:_DET:_IMPOSTO:_II:_VIOF:TEXT"))=="C"
			_nXValIOF := Val(oXml:_INFNFE:_DET:_IMPOSTO:_II:_VIOF:TEXT)
	   Endif
	   
	          
   Endif                
   

ElseIf AllTrim(Type("oXml:_NFE:_INFNFE")) == "O"
   //������������������������������Ŀ
   //�    VERSAO _NFE:_INFNFE       |
   //��������������������������������

   If nItensXML > 1
       cProdFor 	:= 	oXml:_NFE:_INFNFE:_DET[i]:_PROD:_CPROD:TEXT
       cDescrProd 	:= 	oXml:_NFE:_INFNFE:_DET[i]:_PROD:_XPROD:TEXT
       nQuant   	:=	Val(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_QCOM:TEXT)
       cUM 			:=	oXml:_NFE:_INFNFE:_DET[i]:_PROD:_UCOM:TEXT

	   cEAN			:=	IIF(AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_CEAN:TEXT"))=="C", oXml:_NFE:_INFNFE:_DET[i]:_PROD:_CEAN:TEXT,'')
	   cNCM			:=	IIF(AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_NCM:TEXT"))=="C", oXml:_NFE:_INFNFE:_DET[i]:_PROD:_NCM:TEXT,'')
	   	                                                           
	   cCFOP		:=	oXml:_NFE:_INFNFE:_DET[i]:_PROD:_CFOP:TEXT
	   	          
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_VDESC:TEXT"))=="C"
           nDescont := Val(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT)
       Endif
       nPreco      	:=	Val(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_VUNCOM:TEXT)
       nTotal      	:=	Val(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_VPROD:TEXT)
       cNotaXML    	:=	oXml:_NFE:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML    :=	oXml:_NFE:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    	:=	oXml:_NFE:_INFNFE:_IDE:_NATOP:TEXT

       //Benhur 13/07/2020   3
		If (type('oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO') == 'C')
       		cLote       :=           oXml:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_NLOTE:TEXT
		    cDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)
          	//dDataVal    :=   StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT, "-", "", 10) 
		  	//cDataVal    :=   Substr(dDataVal,1,4)+Substr(dDataVal,6,2)+Substr(dDataVal,9,2)
        Endif


       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf*/
       If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       


       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_DDESEMB:TEXT,'-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_DDI:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif
		///Adicionado por Bruno Sperb
		If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_INFNFE:_DET[i]:_PROD:_DI:vAFRMM:TEXT)
	   Endif 
	   If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:tpViaTransp:TEXT"))=="C" 
			_cTpvia := oXml:_INFNFE:_DET[i]:_PROD:_DI:tpViaTransp:TEXT
		else 
			_cTpvia :='1'
	   Endif
	    If AllTrim(TYPE("oXml:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:tpIntermedio:TEXT"))=="C" 
			_cTpInter := oXml:_INFNFE:_DET[i]:_PROD:_DI:tpIntermedio:TEXT
		else
			_cTpInter :='0'
	   Endif
//	   msgalert('Antes da DI 3')
//	   if ! Empty(_cNumDI)
//	  	 	IncluiCD5(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter, strzero(i,3), oXml:_INFNFE:IDE:nNF:TEXT,oXml:_INFNFE:IDE:serie:TEXT,_vAFRMM)
//	   endif
	///Fim da adi��o 	


       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VBC:TEXT"))=="C"
			_nXBCII := Val(oXml:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VBC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VII:TEXT"))=="C"
			_nXValII := Val(oXml:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VII:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VDESPADU:TEXT"))=="C"
			_nDespac := Val(oXml:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VDESPADU:TEXT)
	   Endif 
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VIOF:TEXT"))=="C"
			_nXValIOF := Val(oXml:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VIOF:TEXT)
	   Endif
	   	   
      
   Else
 
       cProdFor   	:= 	oXml:_NFE:_INFNFE:_DET:_PROD:_CPROD:TEXT
       cDescrProd	:=	oXml:_NFE:_INFNFE:_DET:_PROD:_XPROD:TEXT
       nQuant    	:= 	Val(oXml:_NFE:_INFNFE:_DET:_PROD:_QCOM:TEXT)
       cUM         	:=	oXml:_NFE:_INFNFE:_DET:_PROD:_UCOM:TEXT
       
	   cEAN			:=	IIF(AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_CEAN:TEXT"))=="C", oXml:_NFE:_INFNFE:_DET:_PROD:_CEAN:TEXT,'')
	   cNCM			:=	IIF(AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT"))=="C", oXml:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT,'')
	   	   
       cCFOP      	:=	oXml:_NFE:_INFNFE:_DET:_PROD:_CFOP:TEXT
       
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT"))=="C"
           nDescont := Val(oXml:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT)
       Endif


       nPreco    	:= 	Val(oXml:_NFE:_INFNFE:_DET:_PROD:_VUNCOM:TEXT)
       nTotal     	:= 	Val(oXml:_NFE:_INFNFE:_DET:_PROD:_VPROD:TEXT)
       cNotaXML    	:= 	oXml:_NFE:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML    := 	oXml:_NFE:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    	:= 	oXml:_NFE:_INFNFE:_IDE:_NATOP:TEXT

       //Benhur 13/07/2020   4
		If (type('oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO') == 'C')
	        cLote       :=           oXml:_NFE:_INFNFE:_DET:_PROD:_RASTRO:_NLOTE:TEXT
		    cDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)
          	//dDataVal    :=   StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT, "-", "", 10) 
		  	//cDataVal    :=   Substr(dDataVal,1,4)+Substr(dDataVal,6,2)+Substr(dDataVal,9,2)
        Endif


       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf*/
      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       

       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_NFE:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif
	   ///Adicionado por Bruno Sperb
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_INFNFE:_PROD:_DI:vAFRMM:TEXT)
	   Endif 
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT"))=="C" .And. Empty(_cTpvia)
			_cTpvia := oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT
		else
			_cTpvia :='1'
	   Endif
	    If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT"))=="C" .And. Empty(_cTpInter)
			_cTpInter := oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT
		else
			_cTpInter := '0'
	   Endif
	   //msgalert('Antes da DI 4')
	   //if ! Empty(_cNumDI)
	  //	 	IncluiCD5(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter, strzero(i,3), oXml:_INFNFE:IDE:nNF:TEXT,oXml:_INFNFE:IDE:serie:TEXT,_vAFRMM)
	  // endif
	///Fim da adi��o 	


       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VBC:TEXT"))=="C"
			_nXBCII := Val(oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VBC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VII:TEXT"))=="C"
			_nXValII := Val(oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VII:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VDESPADU:TEXT"))=="C"
			_nDespac := Val(oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VDESPADU:TEXT)
	   Endif 
       If AllTrim(TYPE("oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VIOF:TEXT"))=="C"
			_nXValIOF := Val(oXml:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VIOF:TEXT)
	   Endif

   Endif


ElseIf AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE")) == "O"
   //������������������������������Ŀ
   //� VERSAO _NFEPROC:_NFE:_INFNFE |
   //��������������������������������

   If nItensXML > 1

       cProdFor		:= 	oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_CPROD:TEXT
       cDescrProd	:= 	oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_XPROD:TEXT
       nQuant    	:= 	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_QCOM:TEXT)
       nPreco     	:=	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VUNCOM:TEXT)
       cUM        	:=	oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_UCOM:TEXT     
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_VFRETE:TEXT"))=="C"
       		vFreteIt	:=  Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VFRETE:TEXT)
       EndIf	
       
	   cEAN			:=	IIF(AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_CEAN:TEXT"))=="C", oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_CEAN:TEXT,'')
	   cNCM			:=	IIF(AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_NCM:TEXT"))=="C", oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_NCM:TEXT,'')

       cCFOP       	:=	oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_CFOP:TEXT
	   	   
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_VDESC:TEXT"))=="C"
           nDescont :=	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT)
       Endif
       nTotal		:=	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VPROD:TEXT)
       cNotaXML    	:= 	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML    :=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    	:=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NATOP:TEXT

	   nNota        :=  oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT

       //Benhur 13/07/2020   5
		If (type('oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO') == 'C')
          cLote       :=           oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_NLOTE:TEXT
		    cDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)
		  //dDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)+ "/"+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+"/"+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)
          //dDataVal    :=   StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT, "-", "", 10) 
		  //cDataVal    :=   Substr(dDataVal,1,4)+Substr(dDataVal,6,2)+Substr(dDataVal,9,2)
        Endif               


       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf)*/
      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       

/*       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_DDESEMB:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_DDI:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif
	   								
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VBC:TEXT"))=="C"
			_nXBCII := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VBC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VII:TEXT"))=="C"
			_nXValII := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VII:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VDESPADU:TEXT"))=="C"
			_nDespac := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VDESPADU:TEXT)
	   Endif 
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_IMPOSTO:_II:_VIOF:TEXT"))=="C"
			_nXValIOF := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_IMPOSTO:_II:_VIOF:TEXT)
	   Endif
*/

	Else

       cProdFor    	:=	oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CPROD:TEXT
       cDescrProd	:=	oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_XPROD:TEXT
       nQuant    	:=	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_QCOM:TEXT)
       cUM          :=	oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_UCOM:TEXT     
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VFRETE:TEXT"))=="C"
    	   	vFreteIt		:=  Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VFRETE:TEXT)
       EndIf
	   cEAN			:=	IIF(AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CEAN:TEXT"))=="C", oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CEAN:TEXT,'')
	   cNCM			:=	IIF(AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT"))=="C", oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT,'')
          
       cCFOP        :=	oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CFOP:TEXT
	   	   
       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT"))=="C"
           nDescont :=	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT)
       Endif
       nPreco   	:=	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VUNCOM:TEXT)
       nTotal    	:= 	Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VPROD:TEXT)
       cNotaXML   	:=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT
       cSerieXML    :=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_SERIE:TEXT
       cNatOper    	:=	oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_NATOP:TEXT

       //Benhur     6
		If (type('oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO') == 'C')
	        cLote       :=           oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_NLOTE:TEXT
		    cDtValid    :=    SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,1,4)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,6,2)+SubStr(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT,9,2)
            //dDataVal    :=   StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_RASTRO:_DVAL:TEXT, "-", "", 10) 
		    //cDataVal    :=   Substr(dDataVal,1,4)+Substr(dDataVal,6,2)+Substr(dDataVal,9,2)
        Endif


       /*If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
       Else                             ?
    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
       EndIf*/
      If AllTrim(Type("oXml:_NFEPROC:")) != "O"
	       If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    If AllTrim(Type("oXml:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
	    	    	cDataNFe    	:=	StrTran(oXml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	    	    Else
					If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI")) == "O" 
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     	    	    
					Else
						cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10)     	    	    					
					EndIf	
	    	    EndIf
	       EndIf  
       Else
	       If AllTrim(Type("oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI")) == "O" 
	       		cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DEMI:TEXT, "-", "", 10) 
	       Else
	    	    cDataNFe    	:=	StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT, "-", "", 10)     
	       EndIf  
		EndIf	       

       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT, '-','',10))
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif

		///Adicionado por Bruno Sperb
		If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT)
	   Endif 
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT"))=="C" .And. Empty(_cTpvia)
			_cTpvia := oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT
		else
			_cTpvia := '1'
	   Endif
	    If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT"))=="C" .And. Empty(_cTpInter)
			_cTpInter := oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT
		else
			_cTpInter:='0'
	   Endif
	  // msgalert('Antes da DI 5')
	   //if ! Empty(_cNumDI)
	  	// 	IncluiCD5(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter, strzero(i,3), oXml:_INFNFE:IDE:nNF:TEXT,oXml:_INFNFE:IDE:serie:TEXT,_vAFRMM)
       //endif
	///Fim da adi��o 	
       
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VBC:TEXT"))=="C"
			_nXBCII := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VBC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VII:TEXT"))=="C"
			_nXValII := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VII:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VDESPADU:TEXT"))=="C"
			_nDespac := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VDESPADU:TEXT)
	   Endif 
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VIOF:TEXT"))=="C"
			_nXValIOF := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_IMPOSTO:_II:_VIOF:TEXT)
	   Endif
	   
	   
   Endif

Endif

cDataNfe:=	Substr(cDataNFe,1,4)+Substr(cDataNFe,6,2)+Substr(cDataNFe,9,2)   // Diego em 25/06/15, para atender ao chamado 

Return()
************************************************************
Static Function View_XML()
************************************************************
//�������������������������������������������������Ŀ
//� Visualiza arquivo XML no Broswer                |
//� Chamada -> Visual_XML()                         �
//���������������������������������������������������
Local cMsgMotivo := ''

DbSelectArea('ZXM')

cArqViewXml  := GetTempPath()+AllTrim(ZXM->ZXM_CHAVE)+'.XML'    //    DIRETORIO TEMPORARIO WINDOWS
MemoWrit(AllTrim(cArqViewXml), AllTrim(ZXM->ZXM_XML) )
Aadd(aArqTemp, cArqViewXml)

MsgRun("Aguarde... Abrindo XML...",,{|| ShellExecute("open",cArqViewXml,"","",1) } )

Return()
************************************************************
Static Function MsgInfCpl()
************************************************************
//������������������������������������������������Ŀ
//� Visualiza Mensagem do XML                      |
//� Chamada -> Visual_XML()                        �
//��������������������������������������������������

oFont     := TFont():New( "Arial",0,-12,,.T.,0,,700,.F.,.F.,,,,,, )

Define MsDialog oMensNF From 0,0 To 290,415 Pixel Title "Mensagem da Nota Fiscal"
@ 005,005 Get oMemo Var cInfAdic Memo Size 200,135 FONT oFont Pixel Of oMensNF
Activate MsDialog oMensNF Center

Return()
************************************************************
Static Function HistFornec()
************************************************************
//�������������������������������������Ŀ
//� Visualiza Historico do Fornecedor   |
//� Chamada -> Visual_XML()             �
//���������������������������������������

Local aArea3_4  := GetArea()

//���������������������������������Ŀ
//�    Historico do Fornecedor      �
//�����������������������������������

DbSelectArea('ZXM')
cCadastro := 'Hist�rico Fornecedor'

aRotina2  :=    {  	{"Perdas",					"U_PR_PERD()", 	0,2},;
                   	{"Trocas Produtos",       	"U_PR_TRO()", 	0,2},;
                   	{"Diverg�ncias Pedidos",    "U_PR_DIV()", 	0,2},;
                   	{"Pedidos com cortes",     	"U_PR_COR()", 	0,2},;
                   	{"Pedidos N�o Entregues", 	"U_PR_NAO()", 	0,2},;
                   	{"Saldos por Grupo",		"U_PR_SALG()",	0,2}}

aRotina   :=    { 	{"Pesquisar",             	'AxPesqui',		00,01},;
					{"Comprar",           		'U_PR_COM()',	00,08},;
	                {"TOTALizar",         		'U_PR_TOT()',	00,03},;
	    			{"Gerar Pedidos",          	'U_PR_PED()',	00,08},;
	    			{"F4-Hist�rico",       		'U_PR_HIS()',	00,08},;
                  	{"Consultas",               aRotina2,		00,02,0 ,Nil},;
                  	{"Sugest�o",               	'U_PR_SUG()' ,	00,06},;
                  	{"Pre�o Fam�lia",         	'U_PR_FAM()',	00,07},;
                  	{"Incluir Produto",     	'U_PR_INCP()',	00,07},;
                  	{"Fora de Linha",        	'U_PR_FORL()',	00,07},;
                  	{"Legenda",           		'U_PR_LEG()',	00,09}}
	
If Pergunte("FIC030",.T.)
   ALTERA   	:=	.T.
   INCLUI  		:= 	.F.
   LF030TITAB	:=	.F.
   LF030TITPG	:=	.F.
   NVLGERALNF	:=	0  
   LF030TITCOM  :=  .F.
   LF030TITFAT 	:=	.F.    
   FC030CON(ZXM->ZXM_FORNEC,ZXM->ZXM_LOJA)
EndIf

RestArea(aArea3_4)
Return()
************************************************************
Static Function MostraCadFor(SA2Recno)
************************************************************
//���������������������������������Ŀ
//� Visualiza Cadastro do Fornecedor|
//� Chamada -> Visual_XML()         �
//�����������������������������������

aArea3_5    :=	GetArea()
cCadastro	:=	'Fornecedor'
cAlias    	:=	'SA2'
nReg        :=	SA2Recno
nOpc        :=	2


DbselectArea('SA2');DbGoTo(nReg)
AxVisual( cAlias, nReg, nOpc,,,,,)


RestArea(aArea3_5)
Return()
************************************************************
Static Function MostraCadCli(SA2Recno)
************************************************************
//���������������������������������Ŀ
//� Visualiza Cadastro do Cliente 	|
//� Chamada -> Visual_XML()         �
//�����������������������������������

aArea3_5    :=	GetArea()
cCadastro	:=	'Cliente'
cAlias    	:=	'SA1'
nReg        :=	SA2Recno
nOpc        :=	2

DbselectArea('SA1');DbGoTo(nReg)
AxVisual( cAlias, nReg, nOpc,,,,,)


RestArea(aArea3_5)
Return()
************************************************************
Static Function VisualNFE()
************************************************************
//�����������������������������������������������������Ŀ
//|	Quando ja foi gerado o Doc.Entrada do XML  apenas	|
//� Visualiza Pre-Nota\Doc.Entrada  					|
//� Chamada -> Visual_XML()         					�
//�������������������������������������������������������
Local aArea3_6  := GetArea()

DbSelectArea('ZXM')                
cSF1Chave := IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC+ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF+ZXM->ZXM_SERREF)

DbSelectArea('SF1');DbSetOrder()
If DbSeek(xFilial('SF1')+ cSF1Chave + ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA, .F.)

   //���������������������������������Ŀ
   //�    VISUALIZA NOTA FISCAL        �
   //�����������������������������������
   MsgRun("Localizando "+IIF(cPreDoc=='PRE',"PR� NOTA","DOC.ENTRADA")+" para VISUALIZA��O...",,{|| A103NFiscal('SF1',,1)  } )

Endif

RestArea(aArea3_6)
Return()
*****************************************************************
Static Function GeraPreDoc()
*****************************************************************
//���������������������������������Ŀ
//� Gera Pre-Nota\Doc.Entrada       |
//� Chamada -> Visual_XML()         �
//�����������������������������������
Local aArea3_7  	:=	GetArea()
Local cCondPag   	:= 	Space(3)
Local cDuplic		:=	Space(1)
Local _cNatureza	:=	TamSx3('A2_NATUREZ')[1]
Local cNaoIdent  	:=	''
Local cUfOrig    	:=	''
//Local cTipo			:=	'N'
//Local cFormul		:=	'N'
Local aCabec    	:=	{}
Local aItens    	:=	{}
Local aLinha    	:=	{}
Local cRet       	:=	.F.
Local lMsErroAuTo	:=	.F.
Local lRetorno    	:=	.F.
Local lRet 			:= 	.F.
Local lCondPag		:=	.F.
Local nPosItem   	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_ITEM'   })
Local nPosPFor   	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'C7_PRODUTO'})
Local nPosProd   	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_COD'    })
Local nPosLocal  	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_LOCAL'	 })
Local nPosQuant  	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_QUANT'	 })
Local nPosVunit  	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_VUNIT'	 })
Local nPosDesc   	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_DESC'   })
Local nPosTotal  	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_TOTAL'	 })
Local nPosPC     	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_PEDIDO' })
Local nPosPCItem 	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_ITEMPC' })
Local nPosTES		:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_TES'	 })
Local nPNFOrig		:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_NFORI'	 })
Local nPSeriOri		:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_SERIORI'})
Local nPItemOri		:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_ITEMORI'})
Local nPCC			:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_CC'	 })

Local   nZXMRecno	:=	ZXM->(Recno())
Private aDiverg 	:=	{}
Private aFormul 	:=	{ ZXM->ZXM_DOC, ZXM->ZXM_SERIE }  // VARIAVEL PARA FORMULARIO PROPRIO

Private a140Desp  	:= 	{0,0,0,0,0,0,0,0}
Private lConsMedic 	:= 	.F.
Private lNfMedic   	:= 	.F. 
Private aPedC 		:= 	{}



//�����������������������������������������������������Ŀ
//| Campos Obrigatorios	e verificacao do NCM, QTD, R$	|
//�������������������������������������������������������
For nX:=1 To Len(oBrwV:aCols)
   
   If Empty(oBrwV:aCols[nX][nPosProd])
       Msgbox("Campo C�digo-Produtos n�o esta identificado, corrija primeiro!"+ENTER+ENTER+'Item: '+oBrwV:aCols[nX][nPosItem]+'  -  Prod.Fornec: '+oBrwV:aCols[nX][nPosPFor],"Aten��o...","ALERT")
       Return(lRetorno)
	ElseIf !Empty(oBrwV:aCols[nX][nPosPC]) .And. Empty(oBrwV:aCols[nX][nPosPCItem])
       Msgbox("Campo Item do Ped. esta em branco, corrija primeiro!"+ENTER+ENTER+'Item: '+oBrwV:aCols[nX][nPosItem]+'  -  Prod.Fornec: '+oBrwV:aCols[nX][nPosPFor],"Aten��o...","ALERT")
       Return(lRetorno)			
	ElseIf cPreDoc=='DOC' 
		If Empty(oBrwV:aCols[nX][nPosTES])
			Msgbox("Campo TES n�o esta identificado, corrija primeiro!"+ENTER+ENTER+'Item: '+oBrwV:aCols[nX][nPosItem]+'  -  Prod.Fornec: '+oBrwV:aCols[nX][nPosPFor],"Aten��o...","ALERT")
			Return(lRetorno)
		EndIf
   EndIf
   
   If cPreDoc=='DOC'
  		If AllTrim(Posicione("SF4",1,xFilial("SF4")+oBrwV:aCols[1][nPosTES],"F4_DUPLIC")) == 'S'
			lCondPag	:=	.T.   
		EndIf	
	EndIf
                                                               /*
	If lNFeDev .And. ( 	Empty(oBrwV:aCols[nX][nPNFOrig])  .Or.;
						Empty(oBrwV:aCols[nX][nPSeriOri]) .Or.;
						Empty(oBrwV:aCols[nX][nPItemOri]) )
		                                                    
		
			Msgbox("Campo NF.\ S�rie \ item ORIGINAL n�o esta identificado, corrija primeiro!"+ENTER+ENTER+'Item: '+oBrwV:aCols[nX][nPosItem]+'  -  Prod.Fornec: '+oBrwV:aCols[nX][nPosPFor],"Aten��o...","ALERT")
			Return(lRetorno)
			
    EndIf            
	                                                             */

	//����������������������������������������������Ŀ
	//| VERIFICA DIVERGENCIA ENTRE XML E DOC.ENTRADA |
	//������������������������������������������������
	//	Aadd(aXMLOriginal, {nItemXML, Right(cProdFor, TamSx3('D1_COD')[1]), cDescrProd, cNCM, nQuant, nPreco } )
	//							1                   2                            3        4      5       6
	          
	lDiverg	:=	.F.
	nPosXML	:=	Ascan(aXMLOriginal, {|X| X[1] == oBrwV:aCols[nX][nPosItem]} )
	
	If nPosXML > 0
		If oBrwV:aCols[nX][nPosQuant] != aXMLOriginal[nPosXML][5]			//	QUANTIDADE
			lDiverg	:=	.T.
		ElseIf oBrwV:aCols[nX][nPosVunit] != aXMLOriginal[nPosXML][6]		//	VALOR UNITARIO
			lDiverg	:=	.T.
		EndIf
                                                                 
		cProdNCM := AllTrim(Posicione('SB1',1,xFilial('SB1')+ oBrwV:aCols[nX][nPosProd] ,'B1_POSIPI'))
		If AllTrim(aXMLOriginal[nPosXML][4]) != cProdNCM					//	NCM
			lDiverg	:=	.T.
		EndIf		

					
		If lDiverg
			Aadd(aDiverg, {	aXMLOriginal[nPosXML][1],;		//	ITEM  - 1
						 	aXMLOriginal[nPosXML][2],;		//	PROD.FORNECEDOR - 2
							oBrwV:aCols[nX][nPosProd],;		//	PROD.EMPRESA - 3
							aXMLOriginal[nPosXML][3],;		//	DESCRICAO PRODUTO - FORNECEDOR - 4
							aXMLOriginal[nPosXML][4],;		//	NCM - XML - 5
							cProdNCM,;						//	NCM - ACOLS - 6
							aXMLOriginal[nPosXML][5],;		//	QTD - XML - 7 
							oBrwV:aCols[nX][nPosQuant],;	//	QTD - ACOLS - 8 
							aXMLOriginal[nPosXML][6],;		//	PRECO - XML - 9 
							oBrwV:aCols[nX][nPosVunit],;	//	PRECO - ACOLS - 10
							.F. })	                        //  11
							
		EndIf
        
	EndIf
			
Next


//���������������������������������������������Ŀ
//| Informa DIVERGENCIA ENTRE XML E DOC.ENTRADA |
//�����������������������������������������������
If Len(aDiverg) > 0 

	//������������������������������Ŀ
	//| TELA INFORMANDO DIVERGENCIAS |
	//��������������������������������
	If !TelaDiverg()                                          
		IIF(Select("TMPDIV") != 0, TMPDIV->(DbCLoseArea()), )
		Return(lRetorno)
	EndIf

EndIf



//���������������������������������������������Ŀ
//| Grava status no fornecedor que manda o XML  |
//�����������������������������������������������
//���������������������������������������������������������������������Ŀ
//| BUSCA COND.PAGAMENTO CADSTRADO NO FORNECEDOR						|
//| CASO NAO TENHA CADASTRADO A COND.PAG - MOSTRA TELA PARA INFORMAR	|
//�����������������������������������������������������������������������
IF !lNFeDev .And. !lImportacao

	SA2->(DbSetOrder(3),DbGoTop(),DbSeek(xFilial('SA2')+ ZXM->ZXM_CGC, .F.) )
	cUfOrig  	:=	SA2->A2_EST
	cCondPag 	:= 	SA2->A2_COND
	_cNatureza 	:=	SA2->A2_NATUREZ
	SA2Recno 	:=	SA2->(Recno())

Else					//	DEVOLUCAO\RETORNO GRAVA FORNEC\LOJA e TIPO APOS USER SELECIONAR O TIPO [ NA ROTINA OpcaoRetorno ]	
       
	cTabela := IIF(ZXM->ZXM_TIPO $ 'D\B', 'SA1', 'SA2' )
  	*********************************************************************************
		ForCliMsBlql(cTabela, ZXM->ZXM_CGC, lImportacao, ZXM->ZXM_FORNECE, ZXM->ZXM_LOJA )
	*********************************************************************************
	cUfOrig  	:=	IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->A1_EST,  	SA2->A2_EST		)
	cCondPag 	:= 	IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->A1_COND, 	SA2->A2_COND	)
	_cNatureza 	:=	IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->A1_NATUREZ,SA2->A2_NATUREZ	)
	SA2Recno := IIF(ZXM->ZXM_TIPO $ 'D\B', SA1->(Recno()), SA2->(Recno()) )

EndIf



//�������������������������������������������Ŀ
//�	TELA PARA INFORMAR A COND.PAGAMENTO       �
//���������������������������������������������
If lCondPag
	cDescCondP := Space(3)
	oFont1     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-09,-11),,.T.,0,,700,.F.,.F.,,,,,, )

	oDlgCondP  := MSDialog():New( D(095),D(232),D(276),D(624),"",,,.F.,,,,,,.T.,,oFont1,.T. )
	oGrp1      := TGroup():New( D(004),D(004),D(064),D(190),"",oDlgCondP,CLR_BLACK,CLR_WHITE,.T.,.F. )
	oSay1      := TSay():New( D(012),D(008),{||"N�o h� uma Cond.Pagamento cadastrado para este Fornecedor."},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(200),D(008) )
	oSay2      := TSay():New( D(020),D(008),{||"Selecione a Cond.Pagamento."},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(100),D(008) )
	oSay3      := TSay():New( D(048),D(016),{||"Cond.Pag.:"},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(032),D(008) )

	oGet1      := TGet():New( D(044),D(056), {|u| If(PCount()>0,cCondPag :=u,cCondPag)},oGrp1,D(050),D(008),'',,CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"",,{|| IIF(!Empty(cCondPag), cDescCondP:=Posicione('SE4',1,xFilial('SE4')+cCondPag,'E4_DESCRI'),), oSay4:Refresh() },.F.,.F.,,.F.,.F.,"SE4","cCondPag",,)
	oSay4      := TSay():New( D(048),D(110), {|| cDescCondP },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,050,008)
		
	oBtn1      := TButton():New( D(068),D(048),"OK",oDlgCondP,{|| IIF(ExistCpo("SE4",cCondPag), oDlgCondP:End(),) },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )
	oBtn2      := TButton():New( D(068),D(098),"Cancelar",oDlgCondP,{|| lRet := .T., oDlgCondP:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )
		 
	oGet1:SetFocus()
	oDlgCondP:Activate(,,,.T.)

EndIf

If lRet
	Return(lRetorno)
EndIf






//���������������������������������������������������������Ŀ
//�         PRIMEIRO ALIMENTA aITENS.           			�
//| PEDIDO DE COMPRA VINCULADO COM PRE-NOTA \ DOC.ENTRADA	|
//|	NAO PODERA INFORMAR O TIPO DE FRETE.					|
//|															|
//| HELP: A103FRETE                                         |
//|				Campo Tipo de Frete na aba: DANFE n�o       |
//|				poder� ser preenchido quando houver         |
//|				pedido vinculado a nota !                   |
//|                                                         |
//�����������������������������������������������������������

lPedCom := .F.
nItem	:= 0

	
//���������������������������������������������Ŀ
//�  ARRAY ITEM  PARA UTILIZAR NO MSExecAuto	�
//�����������������������������������������������
 For nLin := 1 To Len(oBrwV:aCols)                      
 
 	//	NAO ADICIONA ITENS DELETADOS...
	If oBrwV:aCols[nLin][Len(aMyHeader)+1]
    	Loop
    EndIf
    
    aLinha 	 := {}                      
	nTam 	 := Len(oBrwV:aCols[nLin])
	nItem++
	For nPaCols := 1 To nTam   
		
		If nPaCols < nTam .And. Left(aMyHeader[nPaCols][2], 2) == 'D1'
 
			If AllTrim(aMyHeader[nPaCols][2]) == 'D1_ITEM'
				//Aadd(aLinha,{'D1_ITEM',	PadL(AllTrim(Str(nLin)), TamSx3('D1_ITEM')[1],'0'), Nil })
				Aadd(aLinha,{'D1_ITEM',	PadL(AllTrim(Str(nItem)), TamSx3('D1_ITEM')[1],'0'), Nil })
			
			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_LOCAL' .And. Empty(oBrwV:aCols[nLin][nPaCols])
			    _cLocal := AllTrim(Posicione('SB1', 1, xFilial("SB1") + oBrwV:aCols[nLin][nPosProd], 'B1_LOCPAD' ))
				Aadd(aLinha,{'D1_LOCPAD', IIF(!Empty(_cLocal), _cLocal, Space(TamSx3('D1_LOCPAD')[1]) ), Nil })
									
			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_PEDIDO' .And. !Empty(oBrwV:aCols[nLin][nPosPC])
				Aadd(aLinha,{'D1_PEDIDO',	oBrwV:aCols[nLin][nPosPC],	  	Nil })
				lPedCom := .T.

			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_ITEMPC' .And. !Empty(oBrwV:aCols[nLin][nPosPCItem])
				Aadd(aLinha,{'D1_ITEMPC',	oBrwV:aCols[nLin][nPosPCItem],	Nil })  
			
			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_TES' .And. cPreDoc == 'DOC'
   				Aadd(aLinha,{'D1_TES',		oBrwV:aCols[nLin][nPosTES],		Nil	})

   			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_NFORI' .And. !Empty(oBrwV:aCols[nLin][nPNFOrig])
			   	Aadd(aLinha,{'D1_NFORI', 	oBrwV:aCols[nLin][nPNFOrig], 	Nil	})     

   			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_SERIORI' .And. !Empty(oBrwV:aCols[nLin][nPSeriOri])
			   	Aadd(aLinha,{'D1_SERIORI', 	oBrwV:aCols[nLin][nPSeriOri],	Nil	})                       
   			
   			ElseIf AllTrim(aMyHeader[nPaCols][2]) == 'D1_ITEMORI' .And. !Empty(oBrwV:aCols[nLin][nPItemOri])
			   	Aadd(aLinha,{'D1_ITEMORI', 	oBrwV:aCols[nLin][nPItemOri],	Nil	})

			ElseIf !Empty(oBrwV:aCols[nLin][nPaCols])
				Aadd(aLinha,{ AllTrim(aMyHeader[nPaCols][2]),	oBrwV:aCols[nLin][nPaCols], Nil }) 
			EndIf
			
		EndIf   
	Next   

   	If Len(aLinha) > 0
   		Aadd(aItens,aLinha)
   	EndIf
   	
Next



//���������������������������������������������Ŀ
//�  ARRAY CABEC PARA UTILIZAR NO MSExecAuto	�
//�����������������������������������������������
DbSelectArea('SF1')
_cTipo 		:= 	IIF(Empty(ZXM->ZXM_TIPO),'N',ZXM->ZXM_TIPO)
_cFormul	:=	IIF(Empty(cTpFormul),'N', cTpFormul)
_cFormul	:=	IIF(lImportacao,'S', _cFormul)	//	NOTA DE IMPORTACAO FORMULARIO Eh PROPRIO

Aadd(aCabec,{"F1_TIPO",			_cTipo          	, Nil })
Aadd(aCabec,{"F1_DOC",			IIF(_cFormul!='S'	, ZXM->ZXM_DOC,	Space(TamSx3('F1_DOC')[1]) ),  Nil })
Aadd(aCabec,{"F1_SERIE",       	IIF(_cFormul!='S'	, ZXM->ZXM_SERIE, Space(TamSx3('F1_SERIE')[1])), Nil })
Aadd(aCabec,{"F1_FORMUL",       _cFormul        	, Nil })
Aadd(aCabec,{"F1_EMISSAO",      StoD(cDataNFe)		, Nil })
Aadd(aCabec,{"F1_FORNECE",      ZXM->ZXM_FORNEC		, Nil })
Aadd(aCabec,{"F1_LOJA",         ZXM->ZXM_LOJA		, Nil })
Aadd(aCabec,{"F1_EST",          cUfOrig				, Nil })
Aadd(aCabec,{"F1_ESPECIE",      'SPED'				, Nil })
Aadd(aCabec,{"F1_COND",         cCondPag			, Nil })
Aadd(aCabec,{"F1_CHVNFE",       ZXM->ZXM_CHAVE		, Nil })
Aadd(aCabec,{"F1_HORA",         Left(Time(),5)		, Nil })

If !Empty(cTranspCgc)
	cCodTransp := Posicione("SA4",3,xFilial("SA4")+cTranspCgc,"A4_COD")
	If !Empty(cCodTransp)
		Aadd(aCabec,{"F1_TRANSP", cCodTransp , Nil })
	EndIf
EndIf
If !Empty(cPlaca)
	Aadd(aCabec,{"F1_PLACA",  cPlaca , Nil })
EndIf
If !Empty(cTpFrete) .And. !lPedCom
	Aadd(aCabec,{"F1_TPFRETE" ,  IIF(AllTrim(cTpFrete)=='1','C','F') 	, Nil })
EndIf          
If !Empty(vFrete)
	Aadd(aCabec,{"F1_FRETE" ,  vFrete 	, Nil })
EndIf
If  SF1->(FieldPos("F1_ESPECI1")) > 0 .And.  !Empty(cEspecie)										//	Especie [C-10]	-	<vol><esp>BAU DE METAL</esp>
	Aadd(aCabec,{"F1_ESPECI1" ,   cEspecie	, Nil })
EndIf
If  SF1->(FieldPos("F1_PBRUTO")) > 0 .And.  !Empty(nPBruto)											//	Peso Bruto da N.F.[N-11]	-	<vol><pesoB>3212.000</pesoB>
	Aadd(aCabec,{"F1_PBRUTO" ,   nPBruto	, Nil })
EndIf
If  SF1->(FieldPos("F1_PLIQUI")) > 0 .And.  !Empty(nPLiquido)										//	Peso Liquido da N.F.[N-11]	-	<vol><pesoL>2777.338</pesoL>
	Aadd(aCabec,{"F1_PLIQUI" ,   nPLiquido	, Nil })
EndIf
If  SF1->(FieldPos("F1_PESOL")) > 0 .And.  !Empty(nPLiquido)										//	Peso Liquido[N-11]	-	<vol><pesoL>2777.338</pesoL>
	Aadd(aCabec,{"F1_PESOL" ,   nPLiquido	, Nil })
EndIf
If  SF1->(FieldPos("F1_VOLUME1")) > 0 .And.  !Empty(nVolume)											//	Volume[N-05]	-	<vol><qVol>1</qVol>
	Aadd(aCabec,{"F1_VOLUME1" ,   nVolume	, Nil })
EndIf 



//�������������������������Ŀ
//�  GRAVA DOC. ENTRADA		�
//���������������������������
If Len(aCabec) > 0 .And. Len(aItens) > 0
                      

	// BEGIN TRANSACTION		//	FORMULARIO PROPRIO + BEGIN TRANSCTION OCORRE PROBLEMA
   
		DbSelectArea('SF1')
		If cPreDoc=='PRE'
			MsAguarde( {|| MSExecAuto({|x, y, z, w, a | MATA140(x, y, z, w, a )}, aCabec,  aItens, 3, .F., 1) },'Processando','Gerando Pr� Nota...' )
		Else
			MsAguarde( {|| MSExecAuto({|x, y, z, w, a | MATA103(x, y, z, w, a )}, aCabec,  aItens, 3, .T.)    },'Processando','Gerando Documento de Entrada...' )
		EndIf
		
		
		//�������������������������������������Ŀ
		//�  ERRO AO TENTAR GRAVAR DOC.ENTRADA	|
		//���������������������������������������
		If lMsErroAuto

			MostraErro()
			DisarmTransaction()
			MsgAlert('DOCUMENTO N�O GRAVADO !!!' )
                                           
		Else


			//�����������������������������Ŀ
			//�  TENTA GRAVAR DOC.ENTRADA	|
			//�������������������������������
			DbSelectArea('ZXM')
			If ZXM->(Recno()) != nZXMRecno
				DbGoTo(nZXMRecno)
			EndIf
			

			******************************
				XML_ValNumDoc()
			******************************

			IncluiCD5(SF1->F1_SERIE,SF1->F1_DOC,SF1->F1_FORNECE ,SF1->F1_LOJA,ZXM->ZXM_XML)

			
			//���������������������������������������������������������Ŀ
			//� VERIFICA SE USUARIO CONFIRMOU A PRE-NOTA \ DOC.ENTRADA  �
			//�����������������������������������������������������������

			cSF1ChaveXML := IIF(Empty(ZXM->ZXM_FORMUL), ZXM->ZXM_DOC + ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF + ZXM->ZXM_SERREF ) + ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA + cTipo
			
			DbSelectArea('SF1');DbSetOrder(1);DbGoTop()    //    F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNEC +F1_LOJA+F1_TIPO
			If DbSeek( xFilial('SF1') + cSF1ChaveXML, .F.)

				DbSelectArea('ZXM')
				Reclock('ZXM',.F.)
					lGravado		:=	.T.
					ZXM->ZXM_STATUS	:=	'G'    // XML GRAVADO + NFENTRADA
					ZXM->ZXM_DTNFE	:=	dDataBase
		            ZXM->ZXM_TIPO   := 	cTipo
				MsUnlock() 
				

				//�������������������������������������������Ŀ
				//�  VERIFICA SA5 - PRODUTO X FORNECEDOR      �
				//���������������������������������������������
				nPosPFor	:= 	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'C7_PRODUTO'})
				nPosProd    :=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_COD'    })
				nPDescFor   :=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'B1_DESCFOR'})	// DESCRICAO DO CODIGO DO FORNECEDOR [GRAVO NO SA5]
				nPosItem   	:=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_ITEM'   })


				//���������������������������������������������������������������������������������������Ŀ
				//�  FOR EM TODOS OS ITENS DO GRID - VERIFICA SE Eh NECESSARIO FAZER AMARRACAO COM SA5    �
				//�����������������������������������������������������������������������������������������
				For nX:= 1 To Len(oBrwV:aCols)
                       

					lExiste := .F.
					DbSelectArea('ZXM')
					
					#IFDEF TOP
						
						IIF(Select("SQL")!= 0, SQL->(DbCLoseArea()), )

					    // CHAVE UNICA SA5 -> A5_FILIAL+A5_FORNECE+A5_LOJA+A5_PRODUTO+A5_FABR+A5_FALOJA+A5_REFGRD
						cQuery 	:= "SELECT 	* "+ENTER
						cQuery 	+= " FROM  "+ RetSqlName('SA5')+ " SA5 " 								+ENTER
						cQuery 	+= " WHERE	A5_FILIAL	=	'"+xFilial('SA5')+"'"						+ENTER
						cQuery 	+= " AND	A5_FORNECE 	= 	'"+ZXM->ZXM_FORNEC+"'" 						+ENTER
						cQuery 	+= " AND	A5_LOJA    	= 	'"+ZXM->ZXM_LOJA+"'"						+ENTER
						cQuery 	+= " AND	A5_PRODUTO 	= 	'"+AllTrim(oBrwV:aCols[nX][nPosProd])+"'"	+ENTER
						// cQuery  += " AND	A5_CODPRF  	= 	'"+AllTrim(oBrwV:aCols[nX][nPosPFor])+"'" 	+ENTER
						cQuery 	+= " AND	D_E_L_E_T_  = 	' ' "										+ENTER
						
						MemoWrit('C:\QUERY\IMPXMLNFE_SA5.TXT', cQuery )
						dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), 'SQL', .F., .T.)
						
						nCount := 0
						SQL->(DbGotop())
						SQL->( dbEval( {||nCount++},,,1 ) )
						SQL->(DbGotop())
						
						lExiste := IIF(nCount>=1,.T.,.F.)

					#ELSE
					
						DbSelectarea("SA5");DbSetOrder(1);DbGoTop()    //    A5_FILIAL+A5_FORNECE+A5_LOJA+A5_PRODUTO
						If Dbseek(xFilial("SA5")+ ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA + PadR(oBrwV:aCols[nX][nPosProd],TamSx3('A5_PRODUTO')[1],'') ,.F.)
							Do While !Eof() .And. AllTrim(SA5->A5_CODPRF) == AllTrim(oBrwV:aCols[nX][nPosPFor])
								lExiste :=    .T.
								Exit
							DbSkip()
							EndDo
						EndIf
					
					#ENDIF
					
					
					If !lExiste 
						DbSelectArea("SA5")
						Reclock("SA5",.T.)     
							SA5->A5_FILIAL	:=	xFilial("SA5")
							SA5->A5_FORNECE :=	ZXM->ZXM_FORNEC
							SA5->A5_LOJA    :=	ZXM->ZXM_LOJA
							SA5->A5_CODPRF  :=	AllTrim(oBrwV:aCols[nX][nPosPFor])
							SA5->A5_PRODUTO :=	AllTrim(oBrwV:aCols[nX][nPosProd])
						    SA5->A5_NOMPROD :=	IIF(nPDescFor>0,SubStr(oBrwV:aCols[nX][nPDescFor],1,30),'') 
							SA5->A5_NOMEFOR :=	Posicione("SA2",1,xFilial("SA2")+ZXM->ZXM_FORNEC+ZXM->ZXM_LOJA,"A2_NOME")
						MsUnlock()
					Else

						//��������������������������������������������������������Ŀ
						//�  VERIFICA SA5 CASO A5_CODPRF ESTA EM BRANCO            �
						//����������������������������������������������������������
					    // CHAVE UNICA SA5 -> A5_FILIAL+A5_FORNECE+A5_LOJA+A5_PRODUTO+A5_FABR+A5_FALOJA+A5_REFGRD
					
						DbSelectArea("SA5");DbGoTop()
						DbGoTo(SQL->R_E_C_N_O_)
						If Empty(SQL->A5_CODPRF) .And. SA5->(Recno()) == SQL->R_E_C_N_O_
							Reclock("SA5", .F.)
								SA5->A5_CODPRF  :=	oBrwV:aCols[nX][nPosPFor]
							MsUnlock()
						EndIf
											
					EndIf
                    
					IIF(Select("SQL")!= 0, SQL->(DbCLoseArea()), )
						
				Next  
				 
				
				IIF(Select("SQL")!= 0, SQL->(DbCLoseArea()), )
				
				lRetorno := .T.

				MsgInfo( 	ENTER+Space(10)+;
							IIF( cPreDoc=='PRE','PR� NOTA: ','DOC.ENTRADA: ') +AllTrim(SF1->F1_DOC)+' - S�rie: '+AllTrim(SF1->F1_SERIE)+;
						 	IIF( !Empty(ZXM->ZXM_FORMUL),  ENTER+Space(10)+'DOC.XML: '+AllTrim(ZXM->ZXM_DOC) +' - S�rie: '+AllTrim(ZXM->ZXM_SERIE)+ENTER, '')+;
						 	ENTER+Space(10)+'GERADO COM SUCESSO !!!'+Space(30))
			
			Else                                                  
				//���������������������������������������������Ŀ
				//�		CANCELOU A GRAVACAO DO DOC.ENTRADA      �
				//�����������������������������������������������
				If Aviso('Documento n�o gravado. NoFoundSF1.',ENTER+'Deseja verifica se houve erro?',{'Erro','Sair'}, 2) == 1
					MostraErro()
				EndIf
				
			EndIf
				
		Endif
	
	// END TRANSACTION
		
Endif

oBrwV:oBrowse:Refresh()
oBrwV:Refresh()

RestArea(aArea3_7)
Return(lRetorno)  
************************************************************
Static Function TelaDiverg()
************************************************************
//������������������������������Ŀ
//| TELA INFORMANDO DIVERGENCIAS |
//| GeraPreDoc()				 |
//��������������������������������

Local lRet := .T.

SetPrvt("oFont1","oDlgDiverg","oGrp1","oBrwD","oBtnOK","oBtnRet",'oSayClik')

oFont1     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-10,-12),,.F.,0,,700,.F.,.F.,,,,,, )

oDlgDiverg := MSDialog():New( D(095),D(232),D(363),D(1000),"Diverg�ncia",,,.F.,,,,,,.T.,,oFont1,.T. )
oGrp1      := TGroup():New( D(004),D(004),D(110),D(380),"",oDlgDiverg,CLR_BLACK,CLR_WHITE,.T.,.F. )

oSayClik 	:= TSay():New( D(006),D(008),{|| 'Duplo click para alterar' },oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(120),D(008))


oTblDiverg()
DbSelectArea("TMPDIV")

oBrw1      := MsSelect():New( "TMPDIV","","", {{"ITEM","","Item",""},{"PRODFOR","","Prod.Fornec",""},{"PRODEMP","","Produto",""},{"DESCR","","Descri��o",""},;
												{"NCM_XML","","NCM no XML",""},{"NCM","","NCM Informado",""},;
												{"QTD_XML","","Quant. no XML","@E 99999999.9999"},{"QUANT","","Quant.Informado","@E 99999999.9999"},;	
												{"PRECOXML","","Pre�o no XML","@E 9,999,999.999999"},{"PRECO","","Pre�o Informado","@E 9,999,999.999999"}};
												,.F.,,{ D(012),D(008),D(108),D(378)},,, oGrp1 )

oBrw1:oBrowse:bLDblClick	:=	{|| AlteraDiverg()  }

/*
oBrw1:oBrowse:aColumns[1]:bClrBack := {|| 255 }
oBrw1:oBrowse:aColumns[1]:bClrFore := {|| 8388608 }
oBrwD := MsNewGetDados():New( D(012),D(008),D(100),D(300),GD_UPDATE,'AllwaysTrue()','AllwaysTrue()','',,0,99,,'','AllwaysTrue()',oGrp1,aCabecDiv,aDiverg )
oBrwD:oBrowse:aColumns[1]:bClrFore := {||	IIF( oBrwD:aCols[1][5] <> oBrwD:aCols[1][6],;
								 	 		IIF(ALLTRIM(oBrwD:AHEADER[5][2]) == "B1_POSIPI", 255,   8388608 ), 16711935 )}
*/

oBtnOK     := TButton():New( D(120),D(140),"Confirma",oDlgDiverg,{|| lRet:=.T., oDlgDiverg:End() },037,012,,oFont1,,.T.,,"",,,,.F. )
oBtnRet    := TButton():New( D(120),D(204),"Retornar",oDlgDiverg,{|| lRet:=.F., oDlgDiverg:End() } ,037,012,,oFont1,,.T.,,"",,,,.F. )

oDlgDiverg:Activate(,,,.T.)

Return(lRet)
************************************************************
Static Function oTblDiverg()
************************************************************
Local aFds := {}
Local cTmp


If Select("TMPDIV") != 0
   DbSelectArea("TMPDIV") 
   RecLock('TMPDIV', .F.)
       Zap
   MsUnLock()

Else

	Aadd( aFds , {"X_NCM"   ,"L",001,000} )
	Aadd( aFds , {"X_QTD"   ,"L",001,000} )
	Aadd( aFds , {"X_PRECO" ,"L",001,000} )
	Aadd( aFds , {"ITEM"    ,"C",004,000} )
	Aadd( aFds , {"PRODFOR" ,"C",015,000} )
	Aadd( aFds , {"PRODEMP" ,"C",015,000} )
	Aadd( aFds , {"DESCR"   ,"C",030,000} )
	Aadd( aFds , {"NCM_XML" ,"C",010,000} )
	Aadd( aFds , {"NCM"     ,"C",010,000} )
	Aadd( aFds , {"DESC_NCM","C",040,000} )	
	Aadd( aFds , {"QTD_XML" ,"N",014,003} )
	Aadd( aFds , {"QUANT"   ,"N",014,003} )
	Aadd( aFds , {"PRECOXML","N",016,004} )
	Aadd( aFds , {"PRECO"   ,"N",016,004} )
	
	cTmp := CriaTrab( aFds, .T. )
	Use (cTmp) Alias TMPDIV New Exclusive
	Aadd(aArqTemp, cTmp)
EndIf


For nX:=1 To Len(aDiverg)                      

   cDescNCM :=	Posicione("SYD",1,xFilial("SYD") + aDiverg[nX][06], "YD_DESC_P") // cDesNCM 		 // YD_FILIAL+YD_TEC+YD_EX_NCM+YD_EX_NBM


   RecLock('TMPDIV', .T.)
		TMPDIV->ITEM		:=	aDiverg[nX][01]
		TMPDIV->PRODFOR		:=	aDiverg[nX][02]
		TMPDIV->PRODEMP		:=	aDiverg[nX][03]
		TMPDIV->DESCR		:=	aDiverg[nX][04]
		TMPDIV->NCM_XML		:=	aDiverg[nX][05]
		TMPDIV->NCM			:=	aDiverg[nX][06]
		TMPDIV->DESC_NCM	:=	cDescNCM
		TMPDIV->QTD_XML		:=	aDiverg[nX][07]
		TMPDIV->QUANT		:=	aDiverg[nX][08]
		TMPDIV->PRECOXML	:=	aDiverg[nX][09]
		TMPDIV->PRECO		:=	aDiverg[nX][10]
   MsUnLock()

Next

TMPDIV->(DbGoTop())    
Return()
************************************************************
Static Function AlteraDiverg()
************************************************************
SetPrvt("oFont1","oDlg","oGrp0","oSayProd","oSay1","oSay2","oGrp1","oSay3","oSay4","oGrp2","oSay5","oSay6")
SetPrvt("oBtn2")

oFont2     := TFont():New( "MS Sans Serif",0,-11,,.T.,0,,700,.F.,.F.,,,,,, )

oDlg       := MSDialog():New( 199,300,440,1018,"Diverg�ncia NCM",,,.F.,,,,,,.T.,,oFont2,.T. )
oGrp0      := TGroup():New( 005,005,104,365,"",oDlg,CLR_BLACK,CLR_WHITE,.T.,.F. )
 
oSay1      := TSay():New( 010,011,{|| "ITEM:"},oGrp0,,oFont2,.F.,.F.,.F.,.T.,CLR_GRAY,CLR_WHITE,030,010)
oSay2      := TSay():New( 010,030,{|| TMPDIV->ITEM },oGrp0,,oFont2,.F.,.F.,.F.,.T.,CLR_GRAY,CLR_WHITE,020,010)
                                

oGrp1      := TGroup():New( 020,010,053,350,"XML",oDlg,CLR_HBLUE,CLR_WHITE,.T.,.F. )
oSay7      := TSay():New( 030,013,{|| 'Produto:  '+AllTrim(TMPDIV->PRODFOR) +' - '+ AllTrim(TMPDIV->DESCR) },oGrp1,,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,250,008)
oSay3      := TSay():New( 040,013,{|| "NCM:"},oGrp1,,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,250,010)
oSay4      := TSay():New( 040,033,{|| AllTrim(TMPDIV->NCM_XML) +'  -  '+Posicione("SYD",1,xFilial("SYD") + TMPDIV->NCM_XML, "YD_DESC_P") },oGrp1,,oFont2,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,250,010)


oGrp2      := TGroup():New( 060,010,097,350,"Cad.Poduto",oDlg,CLR_HRED,CLR_WHITE,.T.,.F. )
oSay8      := TSay():New( 070,013,{|| 'Produto:  '+AllTrim(TMPDIV->PRODEMP) +' - '+ AllTrim(Posicione('SB1',1,xFilial('SB1')+TMPDIV->PRODEMP,'B1_DESC')) },oGrp2,,oFont2,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,250,008)
oSay5      := TSay():New( 080,013,{|| "NCM:"},oGrp2,,oFont2,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,250,008)
oSay6      := TSay():New( 080,033,{|| AllTrim(TMPDIV->NCM) +'  -  '+Posicione("SYD",1,xFilial("SYD") + TMPDIV->NCM, "YD_DESC_P") },oGrp2,,oFont2,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,250,008)
       

oBtn1      := TButton():New( 108,140,"Alterar",oDlg, {|| AlteraNCM(),oDlg:End() },036,012,,oFont2,,.T.,,"",,,,.F. )
oBtn2      := TButton():New( 108,208,"Cancelar",oDlg,{|| oDlg:End()  },037,012,,oFont2,,.T.,,"",,,,.F. )


oDlg:Activate(,,,.T.)


Return()
************************************************************
Static Function AlteraNCM()
************************************************************
Local lOk := .F.

If MsgYesNo('Deseja ALTERAR a NCM que esta cadastrada no Produto: '+AllTrim(TMPDIV->NCM)+'  -  '+Posicione("SYD",1,xFilial("SYD") + TMPDIV->NCM, "YD_DESC_P")+ENTER+;
			'para '+ENTER+;
			'NCM que esta no XML: '+AllTrim(TMPDIV->NCM_XML) +'  -  '+Posicione("SYD",1,xFilial("SYD") + TMPDIV->NCM_XML, "YD_DESC_P") +' ???' )
	
	DbSelectArea('SB1');DbSetOrder(1);DbGoTop()
	If DbSeek(xFilial('SB1')+ TMPDIV->PRODEMP, .F.)
		Do While !lOk
				RecLock('SB1',.F.)
					SB1->B1_POSIPI := TMPDIV->NCM_XML
			   	MsUnLock()
				lOk := .T.                                                                                                
			
				MsgInfo('Produto: '+ AllTrim(TMPDIV->PRODEMP) +' - '+ AllTrim(Posicione('SB1',1,xFilial('SB1')+TMPDIV->PRODEMP,'B1_DESC'))+ENTER+;
						'NCM: '+ AllTrim(TMPDIV->NCM) +'  -  '+Posicione("SYD",1,xFilial("SYD") + TMPDIV->NCM, "YD_DESC_P") +ENTER+ENTER+;
						'ALTERADO para'+ENTER+ENTER+;
						'NCM: '+AllTrim(TMPDIV->NCM_XML) +'  -  '+Posicione("SYD",1,xFilial("SYD") + TMPDIV->NCM_XML, "YD_DESC_P")+ENTER+;
						'COM SUCESSO !!!')
		EndDo
		
	EndIf


Else
	MsgInfo('Altera��o N�O realizada.')
EndIf

Return()
************************************************************
Static Function SelItemPC()
************************************************************
//���������������������������������������������������������Ŀ
//� Tela para vincular Ped.Compra com Pre-Nota\Doc.Entrada  |
//� Chamada -> Visual_XML()                                 �
//| Padrao Microsiga - A103ItemPC							|
//�����������������������������������������������������������
Local aArea3_8 	:=	GetArea()
Local n        	:= 	oBrwV:nAT
Local aHeader 	:=	aMyHeader
Local aCols   	:=	aMyCols
Local cTipo  	:=	'N'
Local cA100For	:= 	ZXM->ZXM_FORNEC
Local cLoja    	:=	ZXM->ZXM_LOJA
Local cNFiscal	:=	IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC,   ZXM->ZXM_DOCREF)
Local cSerie    := 	IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_SERIE, ZXM->ZXM_SERREF)
Local cEspecie	:=	'SPED'
Local cFormul   :=	'N'
Local cCondicao := 	''
Local cForAntNFE:= 	''
Local dDEmissao	:= 	dDataBase


Local lConsLoja     := .T.
Local cSeek            :=    ''
Local nOpca            := 0
Local aArea            := GetArea()
Local aAreaSA2        := SA2->(GetArea())
Local aAreaSC7        := SC7->(GetArea())
Local aAreaSB1        := SB1->(GetArea())
Local aRateio        := {0,0,0}
Local aCab            := {}
Local aNew            := {}
Local aArrSldo        := {}
Local aTamCab        := {}
Local lGspInUseM    := If(Type('lGspInUse')=='L', lGspInUse, .F.)
Local aButtons        := {    {'PESQUISA',{||A103VisuPC(aArrSldo[oQual:nAt][2])},OemToAnsi("Visualiza Pedido"),OemToAnsi("Pedido")},;
                               {'Pesquisa',{||A103PesqP(aCab,aCampos,aArrayF4,oQual)},OemToAnsi("Pesquisar")} }

Local aEstruSC7    := SC7->( dbStruct() )
Local bSavSetKey    := SetKey(VK_F4, Nil )
Local bSavKeyF5    := SetKey(VK_F5,Nil)
Local bSavKeyF7    := SetKey(VK_F7,Nil)
Local bSavKeyF8    := SetKey(VK_F8,Nil)
Local bSavKeyF9    := SetKey(VK_F9,Nil)
Local bSavKeyF10    := SetKey(VK_F10,Nil)
Local bSavKeyF11    := SetKey(VK_F11,Nil)

Local nFreeQt	:= 0
Local nPosPRD	:= aScan(aHeader,{|x| Alltrim(x[2]) == "D1_COD" })
Local nPosPDD	:= aScan(aHeader,{|x| Alltrim(x[2]) == "D1_PEDIDO" })
Local nPosITM	:= aScan(aHeader,{|x| Alltrim(x[2]) == "D1_ITEMPC" })
Local nPosQTD	:= aScan(aHeader,{|x| Alltrim(x[2]) == "D1_QUANT" })
Local nPosIte	:= aScan(aHeader,{|x| Alltrim(x[2]) == "D1_ITEM" })
Local cVar		:= oBrwV:aCols[n][nPosPrd]

Local cQuery   	:= ""
Local cAliasSC7	:= "SC7"
Local cCpoObri 	:= ""
Local nSavQual
Local nPed    	:= 0
Local nX      	:= 0
Local nAuxCNT 	:= 0
Local lMt103Vpc    := ExistBlock("MT103VPC")
Local lMt100C7D    := ExistBlock("MT100C7D")
Local lMt100C7C    := ExistBlock("MT100C7C")
Local lMt103Sel    := ExistBlock("MT103SEL")
Local nMT103Sel    := 0
Local nSelOk       := 1
Local lRet103Vpc   := .T.
Local lContinua    := .T.
Local lQuery       := .F.
Local oQual
Local oDlg
Local oPanel
Local aUsButtons  := {}
Local aLineNew    := {}
Local aNewF4      := {}
Local aLineF4     := {}
Local bLine       := {||.T.}
Local cLine       :=" "
Local oOk            := LoadBitMap(GetResources(), "LBOK")
Local oNo            := LoadBitMap(GetResources(), "LBNO")
Local cItem       := Strzero(0,Len(SD1->D1_ITEM ) )
Local lZeraAcols  := .T.
Local lReferencia := .F.
Local nPQtd       := 0
Local nLinha      := 0
Local nColuna     := 0
Local lUsaFiscal     := .T.
Local aPedido        := {}
Local lNfMedic       := .F.
Local lConsMedic     := .F.
Local aHeadSDE       := {}
Local aColsSDE       := {}
Local aGets          := {}

Private    aArrayF4    := {}
Private    aCampos    := {}


//���������������������������������������������������������������������Ŀ
//� Impede de executar a rotina quando a tecla F3 estiver ativa         �
//�����������������������������������������������������������������������
If Type("InConPad") == "L"
   lContinua := !InConPad
EndIf

//������������������������������������������������������������������������Ŀ
//� Adiciona botoes do usuario na EnchoiceBar                              �
//��������������������������������������������������������������������������
If ExistBlock( "MTIPCBUT" )
   If ValType( aUsButtons := ExecBlock( "MTIPCBUT", .F., .F. ) ) == "A"
       AEval( aUsButtons, { |x| AAdd( aButtons, x ) } )
   EndIf
EndIf

If lContinua
   lReferencia := MatgrdPrrf(@cVar)
   If lReferencia
       nPQtd     := aScan(ograde:acposCtrlGrd,{|x| AllTrim(x[1])=="D1_QUANT"})
   Endif

       If cTipo == 'N'
           #IFDEF TOP
               DbSelectArea("SC7")
               If TcSrvType() <> "AS/400"

                   lQuery    := .T.
                   cAliasSC7 := "QRYSC7"

                   cQuery      := "SELECT "
                   For nAuxCNT := 1 To Len( aEstruSC7 )
                       If nAuxCNT > 1
                           cQuery += ", "
                       EndIf
                       cQuery += aEstruSC7[ nAuxCNT, 1 ]
                   Next
                   cQuery += ", R_E_C_N_O_ RECSC7 FROM "
                   cQuery += RetSqlName("SC7") + " SC7 "
                   cQuery += "WHERE "
                   cQuery += "C7_FILENT = '"+xFilEnt(xFilial("SC7"))+"' AND "

                   If Empty(cVar)
                       If lConsLoja
                           cQuery += "C7_FORNECE = '"+cA100For+"' AND "
                           cQuery += "C7_LOJA = '"+cLoja+"' AND "
                       Else
                           cQuery += "C7_FORNECE = '"+cA100For+"' AND "
                       Endif
                   Else
                       If lConsLoja
                           cQuery += "C7_FORNECE = '"+cA100For+"' AND "
                           cQuery += "C7_LOJA = '"+cLoja+"' AND "
                           cQuery += "C7_PRODUTO LIKE'"+cVar+"%' AND "
                       Else
                           cQuery += "C7_FORNECE = '"+cA100For+"' AND "
                           cQuery += "C7_PRODUTO LIKE'"+cVar+"%' AND "
                       Endif
                   Endif


                   //���������������������������������������������������������������������Ŀ
                   //� Filtra os pedidos de compras de acordo com os contratos             �
                   //�����������������������������������������������������������������������

                   If lConsMedic
                       If lNfMedic
                           //���������������������������������������������������������������������Ŀ
                           //� Traz apenas os pedidos oriundos de medicoes                         �
                           //�����������������������������������������������������������������������
                           cQuery += "C7_CONTRA<>'"  + Space( Len( SC7->C7_CONTRA ) )  + "' AND "
                           cQuery += "C7_MEDICAO<>'" + Space( Len( SC7->C7_MEDICAO ) ) + "' AND "
                       Else
                           //���������������������������������������������������������������������Ŀ
                           //� Traz apenas os pedidos que nao possuem medicoes                     �
                           //�����������������������������������������������������������������������
                           cQuery += "C7_CONTRA='"  + Space( Len( SC7->C7_CONTRA ) )  + "' AND "
                           cQuery += "C7_MEDICAO='" + Space( Len( SC7->C7_MEDICAO ) ) + "' AND "
                       EndIf
                   EndIf
                   //����������������������������������������������������������Ŀ
                   //� Filtra os Pedidos Bloqueados e Previstos.                �
                   //������������������������������������������������������������
                   cQuery += "C7_TPOP <> 'P' AND "
                   If SuperGetMV("MV_RESTNFE") == "S"
                       cQuery += "C7_CONAPRO <> 'B' AND "
                   EndIf
                   cQuery += "SC7.C7_ENCER='"+Space(Len(SC7->C7_ENCER))+"' AND "
                   cQuery += "SC7.C7_RESIDUO='"+Space(Len(SC7->C7_RESIDUO))+"' AND "
                             
                   // ImpXmlNfe
                   //cQuery += " SC7.C7_NUM "

                   cQuery += "SC7.D_E_L_E_T_ = ' '"

                   cQuery := ChangeQuery(cQuery)

                   dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSC7,.T.,.T.)
                   For nX := 1 To Len(aEstruSC7)
                       If aEstruSC7[nX,2]<>"C"
                           TcSetField(cAliasSC7,aEstruSC7[nX,1],aEstruSC7[nX,2],aEstruSC7[nX,3],aEstruSC7[nX,4])
                       EndIf
                   Next nX
               Else
           #ENDIF

               If Empty(cVar)
                   DbSelectArea("SC7")
                   DbSetOrder(9)
                   If lConsLoja
                       cCond := "C7_FILENT+C7_FORNECE+C7_LOJA"
                       cSeek := cA100For+cLoja
                       MsSeek(xFilEnt(xFilial("SC7"))+cSeek)
                   Else
                       cCond := "C7_FILENT+C7_FORNECE"
                       cSeek := cA100For
                       MsSeek(xFilEnt(xFilial("SC7"))+cSeek)
                   EndIf
               Else
                   If !lReferencia
                       DbSelectArea("SC7")
                       DbSetOrder(6)
                       If lConsLoja
                           cCond := "C7_FILENT+C7_PRODUTO+C7_FORNECE+C7_LOJA"
                           cSeek := cVar+cA100For+cLoja
                           MsSeek(xFilEnt(xFilial("SC7"))+cSeek)
                       Else
                           cCond := "C7_FILENT+C7_PRODUTO+C7_FORNECE"
                           cSeek := cVar+cA100For
                           MsSeek(xFilEnt(xFilial("SC7"))+cSeek)
                       EndIf
                   Else


                       If lConsLoja
                           DbSelectArea("SC7")
                           DbSetOrder(17)
                           cCond := "C7_FILENT+C7_FORNECE+C7_LOJA"
                           cSeek := cA100For+cLoja
                           MsSeek(xFilEnt(xFilial("SC7"))+cA100For+cLoja+cVar,.F.)
                       Else
                           DbSelectArea("SC7")
                           DbSetOrder(18)
                           cCond := "C7_FILENT+C7_FORNECE"
                           cSeek := cA100For
                           MsSeek(xFilEnt(xFilial("SC7"))+cA100For+cVar,.F.)
                       EndIf
                   Endif
               EndIf
               #IFDEF TOP
               EndIf
               #ENDIF

           If Empty(cVar)
               cCpoObri := "C7_LOJA|C7_PRODUTO|C7_QUANT|C7_DESCRI|C7_TIPO|C7_LOCAL|C7_OBS"
           Else
               cCpoObri := "C7_LOJA|C7_QUANT|C7_DESCRI|C7_TIPO|C7_LOCAL|C7_OBS"
           Endif

           If (cAliasSC7)->(!Eof())
               DbSelectArea("SX3"); DbSetOrder(2)

               If lNfMedic .And. lConsMedic

                   MsSeek("C7_MEDICAO")

                   AAdd(aCab,x3Titulo())
                   Aadd(aCampos,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_CONTEXT,SX3->X3_PICTURE})
                   aadd(aTamCab,CalcFieldSize(SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_PICTURE,X3Titulo()))

                   MsSeek("C7_CONTRA")

                   AAdd(aCab,x3Titulo())
                   Aadd(aCampos,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_CONTEXT,SX3->X3_PICTURE})
                   aadd(aTamCab,CalcFieldSize(SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_PICTURE,X3Titulo()))

                   MsSeek("C7_PLANILH")

                   AAdd(aCab,x3Titulo())
                   Aadd(aCampos,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_CONTEXT,SX3->X3_PICTURE})
                   aadd(aTamCab,CalcFieldSize(SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_PICTURE,X3Titulo()))

               EndIf

               MsSeek("C7_NUM")
               AAdd(aCab,x3Titulo())
               Aadd(aCampos,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_CONTEXT,SX3->X3_PICTURE})
               aadd(aTamCab,CalcFieldSize(SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_PICTURE,X3Titulo()))


               DbSelectArea("SX3")
               DbSetOrder(1)
               MsSeek("SC7")
               While !Eof() .And. SX3->X3_ARQUIVO == "SC7"
                   IF ( SX3->X3_BROWSE=="S".And.X3Uso(SX3->X3_USADO).And. /*AllTrim(SX3->X3_CAMPO)<>"C7_PRODUTO" .And. */AllTrim(SX3->X3_CAMPO)<>"C7_NUM" .And.;
                           If( lConsMedic .And. lNfMedic, AllTrim(SX3->X3_CAMPO)<>"C7_MEDICAO" .And. AllTrim(SX3->X3_CAMPO)<>"C7_CONTRA" .And. AllTrim(SX3->X3_CAMPO)<>"C7_PLANILH", .T. )).Or.;
                           (AllTrim(SX3->X3_CAMPO) $ cCpoObri)
                       AAdd(aCab,x3Titulo())
                       Aadd(aCampos,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_CONTEXT,SX3->X3_PICTURE})
                       aadd(aTamCab,CalcFieldSize(SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_PICTURE,X3Titulo()))
                   EndIf
                   dbSkip()
               Enddo

               DbSelectArea(cAliasSC7)
               Do While If(lQuery, ;
                       (cAliasSC7)->(!Eof()), ;
                       (cAliasSC7)->(!Eof()) .And. xFilEnt(cFilial)+cSeek == &(cCond))

                   If lReferencia .And. !(Alltrim(cVar)==LEFT((cAliasSC7)->C7_PRODUTO,Len(cVar)))
                       dbSkip()
                       Loop
                   Endif
                   //��������������������������������������������������������������������Ŀ
                   //� Filtra os Pedidos Bloqueados, Previstos e Eliminados por residuo   �
                   //����������������������������������������������������������������������
                   If !lQuery
                       If (SuperGetMV("MV_RESTNFE") == "S" .And. (cAliasSC7)->C7_CONAPRO == "B") .Or. ;
                               (cAliasSC7)->C7_TPOP == "P" .Or. !Empty((cAliasSC7)->C7_RESIDUO)
                           dbSkip()
                           Loop
                       EndIf
                   Endif

                   nFreeQT := 0

                   nPed    := aScan(aPedido,{|x| x[1] = (cAliasSC7)->C7_NUM+(cAliasSC7)->C7_ITEM})

                   nFreeQT -= If(nPed>0,aPedido[nPed,2],0)
                   If MaGrade() .And. !lUsaFiscal
                       If lReferencia
                           nAchou:= Ascan(aCols,{|x| x[nPosPDD] == SC7->C7_NUM .And. x[nPosITM] == SC7->C7_ITEM   })
                           If nAchou > 0
                               For nLinha:=1 to len(oGrade:aColsGrade[nAchou])
                                   For nColuna:=2 to len(oGrade:aHeadGrade[nAchou])

                                       If ( oGrade:aColsFieldByName("D1_QUANT",nAchou,nLinha,nColuna) <> 0) .And. !aCols[nAchou][Len(aHeader)+1]
                                           oGrade:nPosLinO:=nAchou
                                           cProdRef := oGrade:GetNameProd(Alltrim(cVar),nLinha,nColuna)
                                           If Alltrim(cProdRef) == Alltrim((cAliasSC7)->C7_PRODUTO)
                                               nFreeQT += oGrade:aColsFieldByName("D1_QUANT",nAchou,nLinha,nColuna)
                                           Endif
                                       Endif
                                   Next
                               Next
                           Endif
                       Else

                           For nAuxCNT := 1 To Len( aCols )
                               If (nAuxCNT # n) .And. ;
                                       (aCols[ nAuxCNT,nPosPRD ] == (cAliasSC7)->C7_PRODUTO) .And. ;
                                       (aCols[ nAuxCNT,nPosPDD ] == (cAliasSC7)->C7_NUM) .And. ;
                                       (aCols[ nAuxCNT,nPosITM ] == (cAliasSC7)->C7_ITEM) .And. ;
                                       !ATail( aCols[ nAuxCNT ] )
                                   nFreeQT += aCols[ nAuxCNT,nPosQTD ]
                               EndIf
                           Next

                       Endif
                   Else

                       For nAuxCNT := 1 To Len( aCols )
                           If (nAuxCNT # n) .And. ;
                                   (aCols[ nAuxCNT,nPosPRD ] == (cAliasSC7)->C7_PRODUTO) .And. ;
                                   (aCols[ nAuxCNT,nPosPDD ] == (cAliasSC7)->C7_NUM) .And. ;
                                   (aCols[ nAuxCNT,nPosITM ] == (cAliasSC7)->C7_ITEM) .And. ;
                                   !ATail( aCols[ nAuxCNT ] )
                               nFreeQT += aCols[ nAuxCNT,nPosQTD ]
                           EndIf
                       Next
                   Endif

                   lRet103Vpc := .T.

                   If lMt103Vpc
                       If lQuery
                           ('SC7')->(dbGoto((cAliasSC7)->RECSC7))
                       EndIf
                       lRet103Vpc := Execblock("MT103VPC",.F.,.F.)
                   Endif

                   If lRet103Vpc
                       If ((nFreeQT := ((cAliasSC7)->C7_QUANT-(cAliasSC7)->C7_QUJE-(cAliasSC7)->C7_QTDACLA-nFreeQT)) > 0)
                           Aadd(aArrayF4,Array(Len(aCampos)))

                           SB1->(DbSetOrder(1))
                           SB1->(MsSeek(xFilial("SB1")+(cAliasSC7)->C7_PRODUTO))
                           For nX := 1 to Len(aCampos)

                               If aCampos[nX][3] != "V"
                                   If aCampos[nX][2] == "N"
                                       If Alltrim(aCampos[nX][1]) == "C7_QUANT"
                                           aArrayF4[Len(aArrayF4)][nX] :=Transform(nFreeQt,PesqPict("SC7",aCampos[nX][1]))
                                       ElseIf Alltrim(aCampos[nX][1]) == "C7_QTSEGUM"
                                           aArrayF4[Len(aArrayF4)][nX] :=Transform(ConvUm(SB1->B1_COD,nFreeQt,nFreeQt,2),PesqPict("SC7",aCampos[nX][1]))
                                       Else
                                           aArrayF4[Len(aArrayF4)][nX] := Transform((cAliasSC7)->(FieldGet(FieldPos(aCampos[nX][1]))),PesqPict("SC7",aCampos[nX][1]))
                                       Endif
                                   Else
                                       aArrayF4[Len(aArrayF4)][nX] := (cAliasSC7)->(FieldGet(FieldPos(aCampos[nX][1])))
                                   Endif
                               Else
                                   aArrayF4[Len(aArrayF4)][nX] := CriaVar(aCampos[nX][1],.T.)
                                   If Alltrim(aCampos[nX][1]) == "C7_CODGRP"
                                       aArrayF4[Len(aArrayF4)][nX] := SB1->B1_GRUPO
                                   EndIf
                                   If Alltrim(aCampos[nX][1]) == "C7_CODITE"
                                       aArrayF4[Len(aArrayF4)][nX] := SB1->B1_CODITE
                                   EndIf
                               Endif

                           Next

                           aAdd(aArrSldo, {nFreeQT, IIF(lQuery,(cAliasSC7)->RECSC7,(cAliasSC7)->(RecNo()))})

                           If lMT100C7D
                               If lQuery
                                   ('SC7')->(dbGoto((cAliasSC7)->RECSC7))
                               EndIf
                               aNew := ExecBlock("MT100C7D", .f., .f., aArrayF4[Len(aArrayF4)])
                               If ValType(aNew) = "A"
                                   aArrayF4[Len(aArrayF4)] := aNew
                               EndIf
                           EndIf
                       EndIf
                   Endif
                   (cAliasSC7)->(dbSkip())
               EndDo

               If !Empty(aArrayF4)


                   cLine := " { aArrayF4[oQual:nAt,1]
                   For nX := 2 To Len( aCampos )
                       cLine += ",aArrayF4[oQual:nAT][" + AllTrim( Str( nX ) ) + "]"
                   Next nX

                   cLine += " } "


                   //����������������������������������������������������������Ŀ
                   //� Monta dinamicamente o bline do CodeBlock                 �
                   //������������������������������������������������������������
                   bLine := &( "{ || " + cLine + " }" )
                   DEFINE MSDIALOG oDlg FROM 30,20  TO 265,521 TITLE "Selecionar Pedido de Compra ( por item ) - <F6> " Of oMainWnd PIXEL

                   If lMT100C7C
                       aNew := ExecBlock("MT100C7C", .f., .f., aCab)
                       If ValType(aNew) == "A"
                           aCab := aNew
                       EndIf
                   EndIf

                   @ 12,0 MSPANEL oPanel PROMPT "" SIZE 100,19 OF oDlg CENTERED LOWERED //"Botoes"
                   oPanel:Align := CONTROL_ALIGN_TOP

                   oQual := TWBrowse():New( 29,4,243,85,,aCab,aTamCab,oDlg,,,,,,,,,,,,.F.,,.T.,,.F.,,,)
                   oQual:SetArray(aArrayF4)
                   oQual:bLDblClick := {|| AtuPedCom(nSavQual:=oQual:nAT), oDlg:End() }
                   oQual:bLine := bLine

                   oQual:Align := CONTROL_ALIGN_ALLCLIENT

                   oQual:nFreeze := 1


                   If !Empty(cVar)
                       @ 6  ,4   SAY OemToAnsi("Produto") Of oPanel PIXEL SIZE 47 ,9 //"Produto"
                       @ 4  ,30  MSGET cVar PICTURE PesqPict('SB1','B1_COD') When .F. Of oPanel PIXEL SIZE 80,9
                   Else
                       @ 6  ,4   SAY OemToAnsi("Selecione o Pedido de Compra") Of oPanel PIXEL SIZE 120 ,9 //"Selecione o Pedido de Compra"
                   EndIf

                   ACTIVATE MSDIALOG oDlg CENTERED ON INIT EnchoiceBar(oDlg,{|| AtuPedCom(nSavQual:=oQual:nAT), oDlg:End() },{||oDlg:End()},,aButtons)

               Else
                   Help(" ",1,"A103F4")
               EndIf
           Else
               Help(" ",1,"A103F4")
           EndIf
       Else
           Help('   ',1,'A103TIPON')
       EndIf

Endif

If MaGrade() .And. Len(acols) >0 .And. !lUsaFiscal
   aCols := aColsGrade(oGrade, aCols, aHeader, "D1_COD", "D1_ITEM", "D1_ITEMGRD")
   aGets[SEGURO] := aRateio[1]
   aGets[VALDESP]:= aRateio[2]
   aGets[FRETE]  := aRateio[3]
Endif

If lQuery
   DbSelectArea(cAliasSC7)
   dbCloseArea()
   DbSelectArea("SC7")
Endif
SetKey(VK_F5,bSavKeyF5)
SetKey(VK_F7,bSavKeyF7)
SetKey(VK_F8,bSavKeyF8)
SetKey(VK_F9,bSavKeyF9)
SetKey(VK_F10,bSavKeyF10)
SetKey(VK_F11,bSavKeyF11)
RestArea(aAreaSA2)
RestArea(aAreaSC7)
RestArea(aAreaSB1)
RestArea(aArea)
RestArea(aArea3_8)

Return()
***********************************************************************
Static Function a103PesqP(aCab,aCampos,aArrayF4,oQual)
***********************************************************************
//�������������������������������������������������������������������������Ŀ
//� Tela para vincular Ped.Compra com Pre-Nota\Doc.Entrada - Botao Pesquisa |
//� Chamada -> Visual_XML()->SelItemPC()                                 �
//���������������������������������������������������������������������������

Local aCpoBusca    := {}
Local aCpoPict        := {}
Local aComboBox    := { AllTrim( "Exata" ) , AllTrim( "Parcial" ) , AllTrim( "Contem" ) } //"Exata"###"Parcial"###"Contem"
Local bAscan        := { || .F. }
Local cPesq            := Space(30)
Local cBusca        := ""
Local cTitulo        := OemtoAnsi("Pesquisar")  //"Pesquisar"
Local cOpcAsc        := aComboBox[1]    //"Exata"
Local cAscan        := ""
Local nOpca            := 0
Local nPos        := 0
Local nx            := 0
Local nTipo         := 1
Local nBusca        := Iif(oQual:nAt == Len(aArrayF4) .Or. oQual:nAt == 1, oQual:nAt, oQual:nAt+1 )
Local oDlg
Local oBusca
Local oPesq1
Local oPesq2
Local oPesq3
Local oPesq4
Local oComboBox


For nX := 1 to Len(aCampos)
   AAdd(aCpoBusca,aCab[nX])
   AAdd(aCpoPict,aCampos[nX][4])
Next

If Len(aCampos) > 0 .And. Len(aArrayF4) > 0

   DEFINE MSDIALOG oDlg TITLE OemtoAnsi(cTitulo)  FROM 00,0 TO 100,490 OF oMainWnd PIXEL

   @ 05,05 MSCOMBOBOX oBusca VAR cBusca ITEMS aCpoBusca SIZE 206, 36 OF oDlg PIXEL ON CHANGE (nTipo := oBusca:nAt,A103ChgPic(nTipo,aCampos,@cPesq,@oPesq1,@oPesq2,@oPesq3,@oPesq4))

   @ 022,005 MSGET oPesq1 VAR cPesq Picture "@!" SIZE 206, 10 Of oDlg PIXEL

   @ 022,005 MSGET oPesq2 VAR cPesq Picture "@!" SIZE 206, 10 Of oDlg PIXEL

   @ 022,005 MSGET oPesq3 VAR cPesq Picture "@!" SIZE 206, 10 Of oDlg PIXEL

   @ 022,005 MSGET oPesq4 VAR cPesq Picture "@!" SIZE 206, 10 Of oDlg PIXEL

   oPesq1:Hide()
   oPesq2:Hide()
   oPesq3:Hide()
   oPesq4:Hide()

   Do Case
   Case aCampos[1][2] == "C"

       DbSelectArea("SX3")
       DbSetOrder(2)
       If MsSeek(aCampos[1][1])
           If !Empty(SX3->X3_F3)
               oPesq2:cF3 := SX3->X3_F3
               oPesq1:Hide()
               oPesq2:Show()
               oPesq3:Hide()
               oPesq4:Hide()
           Else
               oPesq1:Show()
               oPesq2:Hide()
               oPesq3:Hide()
               oPesq4:Hide()
           Endif
       Endif

   Case aCampos[1][2] == "D"
       oPesq1:Hide()
       oPesq2:Hide()
       oPesq3:Show()
       oPesq4:Hide()
   Case aCampos[1][2] == "N"
       oPesq1:Hide()
       oPesq2:Hide()
       oPesq3:Hide()
       oPesq4:Show()
   EndCase

   cPesq := CriaVar(aCampos[1][1],.F.)
   cPict := aCampos[1][4]

   DEFINE SBUTTON oBut1 FROM 05, 215 TYPE 1 ACTION ( nOpca := 1, oDlg:End() ) ENABLE of oDlg
   DEFINE SBUTTON oBut1 FROM 20, 215 TYPE 2 ACTION ( nOpca := 0, oDlg:End() )  ENABLE of oDlg

   @ 037,005 SAY OemtoAnsi("Tipo") SIZE 050,10 OF oDlg PIXEL //Tipo
   @ 037,030 MSCOMBOBOX oComboBox VAR cOpcAsc ITEMS aComboBox SIZE 050,10 OF oDlg PIXEL

   ACTIVATE MSDIALOG oDlg CENTERED

   If nOpca == 1

       Do Case

       Case aCampos[nTipo][2] == "C"
           IF ( cOpcAsc == aComboBox[1] )    //Exata
               cAscan := Padr( Upper( cPesq ) , TamSx3(aCampos[nTipo][1])[1] )
               bAscan := { |x| cAscan == Upper( x[ nTipo ] ) }
           ElseIF ( cOpcAsc == aComboBox[2] )    //Parcial
               cAscan := Upper( AllTrim( cPesq ) )
               bAscan := { |x| cAscan == Upper( SubStr( Alltrim( x[nTipo] ) , 1 , Len( cAscan ) ) ) }
           ElseIF ( cOpcAsc == aComboBox[3] )    //Contem
               cAscan := Upper( AllTrim( cPesq ) )
               bAscan := { |x| cAscan $ Upper( Alltrim( x[nTipo] ) ) }
           EndIF

           nPos := Ascan( aArrayF4 , bAscan )
       Case aCampos[nTipo][2] == "N"
           nPos := Ascan(aArrayF4,{|x| Transform(cPesq,PesqPict("SC7",aCampos[nTipo][1])) == x[nTipo]},nBusca)
       Case aCampos[nTipo][2] == "D"
           nPos := Ascan(aArrayF4,{|x| Dtos(cPesq) == Dtos(x[nTipo])},nBusca)
       EndCase

       If nPos > 0
           oQual:bLine := { || aArrayF4[oQual:nAT] }
           oQual:nFreeze := 1
           oQual:nAt := nPos
           oQual:Refresh()
           oQual:SetFocus()
       Else
           Help(" ",1,"REGNOIS")
       Endif

   EndIf

Endif

Return()
***********************************************************************
Static Function A103VisuPC(nRecSC7)
***********************************************************************
//��������������������������������������������������������������������������Ŀ
//� Tela para vincular Ped.Compra com Pre-Nota\Doc.Entrada - Botao Visualizar|
//� Chamada -> Visual_XML()->SelItemPC()                                  	 �
//����������������������������������������������������������������������������

Local n    :=    1
Local aArea        :=    GetArea()
Local aAreaSC7    := SC7->(GetArea())
Local nSavNF    := MaFisSave()
Local cSavCadastro:= cCadastro
Local cFilBak    := cFilAnt
Local nBack        :=    n
Private nTipoPed    := 1
Private cCadastro    := OemToAnsi("Consulta ao Pedido de Compra")
Private l120Auto    :=    .F.
Private LVLDHEAD     :=    .T.
Private LGATILHA  :=    .T.
Private aBackSC7    := {}

MaFisEnd()

DbSelectArea("SC7")
MsGoto(nRecSC7)

cFilAnt := IIf(!Empty(SC7->C7_FILIAL),SC7->C7_FILIAL,cFilAnt)

A120Pedido(Alias(),RecNo(),2)

cFilant     := cFilBak
n             := nBack
cCadastro:=    cSavCadastro

MaFisRestore(nSavNF)
RestArea(aAreaSC7)
RestArea(aArea)

Return(.T.)
********************************************************************************
Static Function A103ChgPic(nTipo,aCampos,cPesq,oPesq1,oPesq2,oPesq3,oPesq4)
********************************************************************************
//������������������������������������������������������������Ŀ
//� Chamada -> Visual_XML()->Seleciona()->a103PesqP()          �
//��������������������������������������������������������������
Local cPict   := ""
Local aArea   := GetArea()
Local aAreaSX3:= SX3->(GetArea())
Local bRefresh


DbSelectArea("SX3")
DbSetOrder(2)
If MsSeek(aCampos[nTipo][1])

   Do case

   Case aCampos[nTipo][2] == "C"
       If !Empty(SX3->X3_F3)
           oPesq2:cF3 := SX3->X3_F3
           oPesq1:Hide()
           oPesq2:Show()
           oPesq3:Hide()
           oPesq4:Hide()
           bRefresh := { || oPesq2:oGet:Picture := cPict,oPesq2:Refresh() }
       Else
           oPesq1:Show()
           oPesq2:Hide()
           oPesq3:Hide()
           oPesq4:Hide()
           bRefresh := { || oPesq1:oGet:Picture := cPict,oPesq1:Refresh() }
       Endif

   Case aCampos[nTipo][2] == "D"
       oPesq1:Hide()
       oPesq2:Hide()
       oPesq3:Show()
       oPesq4:Hide()
       bRefresh := { || oPesq3:oGet:Picture := cPict,oPesq3:Refresh() }
   Case aCampos[nTipo][2] == "N"
       oPesq1:Hide()
       oPesq2:Hide()
       oPesq3:Hide()
       oPesq4:Show()
       bRefresh := { || oPesq4:oGet:Picture := cPict,oPesq4:Refresh() }
   EndCase

Endif

If nTipo > 0
   cPesq := CriaVar(aCampos[nTipo][1],.F.)
   cPict := aCampos[nTipo][4]
EndIf

Eval(bRefresh)

RestArea(aAreaSX3)
RestArea(aArea)

Return()
***********************************************************************
Static Function AtuPedCom(nLnoQual)
***********************************************************************
//���������������������������������������������������������Ŀ
//� Atualiza aMyCols com Pedido\Item do Compra              |
//� Chamada -> Visual_XML()->SelItemPC()                    �
//�����������������������������������������������������������
nLnaCols := oBrwV:NAT
lDeletado:=	oBrwV:aCols[oBrwV:NAT][Len(aMyHeader)+1]

nPos1    	:=	Ascan( aCampos, {|X| AllTrim(X[1])  == 'C7_NUM'    })
nPos2    	:=	Ascan( aCampos, {|X| AllTrim(X[1])  == 'C7_ITEM'   })
nPos4    	:=	Ascan( aCampos, {|X| AllTrim(X[1])  == 'C7_PRODUTO'})
nPos5    	:=	Ascan( aCampos, {|X| AllTrim(X[1])  == 'C7_QUANT'	})

nPosItem  	:=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_ITEM'   })
nPosProd  	:=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_COD'    })
nPosPC     	:=	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_PEDIDO' })
nPosPCItem	:= 	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_ITEMPC' })
nPosQuant	:= 	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_QUANT'	})
nPosPreco	:= 	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_VUNIT' 	})
nPosTotal	:= 	Ascan(aMyHeader, {|X| Alltrim(X[2]) == 'D1_TOTAL'  })

If !lDeletado
	
	nPosPcom := Ascan( oBrwV:aCols, {|X|  X[nPosPC] == aArrayF4[nLnoQual][nPos1] .And.  X[nPosPCItem] == aArrayF4[nLnoQual][nPos2] })
	If 	nPosPcom != 0
		MsgInfo('Pedido de Compra j� utilizado no item '+ PadL(AllTrim(Str(nPosPcom)), TamSx3('D1_ITEM')[1],'0') )
	EndIf
	
	If nPos1 != 0
	   	If !Empty(oBrwV:aCols[nLnaCols][nPosPC]) .And.;
   			( oBrwV:aCols[nLnaCols][nPosPC] 	!= aArrayF4[nLnoQual][nPos1]	.Or.;	//	NUM.PC
			  oBrwV:aCols[nLnaCols][nPosPCItem]	!= aArrayF4[nLnoQual][nPos2] 	.Or.;	//	ITEM PC
			  oBrwV:aCols[nLnaCols][nPosProd] 	!= aArrayF4[nLnoQual][nPos4]   ) 		//	PRODUTO

			If MsgYesNo('Item  '+oBrwV:aCols[nLnaCols][nPosItem]+' j� vinculado com Pedido de Compra '+oBrwV:aCols[nLnaCols][nPosPC]+' Item: '+oBrwV:aCols[nLnaCols][nPosPCItem]+ENTER+;
						'Deseja adicionar novo Item ?')  
                                  
	   			If oBrwV:AddLine(.T.,.F.)	//	ADICIONA NOVA LINHA NO ACOLS

	   				oBrwV:aCols[Len(oBrwV:aCols)] := aClone(oBrwV:aCols[nLnaCols])		//	COPIA TODO CONTEUDO DA LINHA POSICIONADA PARA LINHA ADICIONADA
					oBrwV:aCols[Len(oBrwV:aCols)][nPosItem] := PadL(AllTrim(Str(Len(oBrwV:aCols))), TamSx3('D1_ITEM')[1],'0')
					nLnaCols := Len(oBrwV:aCols)										//	nLnaCols RECEBE O VALOR DO LEN() PARA GRAVAR DADOS... NA SEQUENCIA ABAIXO
	            
	   		         If Ascan(aPCxItem, Len(oBrwV:aCols) ) == 0
						Aadd(aPCxItem, Len(oBrwV:aCols) )
	        	    EndIf
	            
					oBrwV:Refresh()   
					oBrwV:oBrowse:Refresh()
					GetDRefresh()
				EndIf
		
			EndIf
		
		EndIf
	EndIf


	If nPos4 != 0
	   oBrwV:aCols[nLnaCols][nPosProd]  := aArrayF4[nLnoQual][nPos4]    //    PRODUTO
	EndIf
	If nPos2 != 0
	   oBrwV:aCols[nLnaCols][nPosPCItem]:= aArrayF4[nLnoQual][nPos2]    //    ITEMPC
	EndIf
	If nPos1 != 0
	   oBrwV:aCols[nLnaCols][nPosPC]    := aArrayF4[nLnoQual][nPos1]    //    NUM.PC
	EndIf
	
	If nPos5 != 0
		oBrwV:aCols[nLnaCols][nPosQuant]:= Val( StrTran(StrTran(aArrayF4[nLnoQual][nPos5], '.',''), ',','.') )    //    QUANT.PC    
		oBrwV:aCols[nLnaCols][nPosTotal]:= Round( oBrwV:aCols[nLnaCols][nPosQuant]	* oBrwV:aCols[nLnaCols][nPosPreco], 2 )
	EndIf
	
	
	oBrwV:Refresh()   
	oBrwV:oBrowse:Refresh()
	GetDRefresh()
	oBrwV:ForceRefresh()
    
	oBrwV:oBrowse:SetFocus()
	oBrwV:oBrowse:ColPos  := nPosPC

EndIf
	
Return()
*****************************************************************
Static Function DelLnPC()
*****************************************************************

nPosDel := Ascan(aPCxItem, oBrwV:NAT )
       
If nPosDel > 0
	// oBrwV:DelLine()
	oBrwV:aCols[oBrwV:NAT][Len(aMyHeader)+1] := .T.     
	aPCxItem[nPosDel] := 'XXXX'		//	FLAG
	oBrwV:Refresh()   
	oBrwV:oBrowse:Refresh()
	GetDRefresh()
	oBrwV:ForceRefresh()
Else
	Alert('Este item � original do XML N�O pode ser DELETADO !!!') 
EndIf


Return()
*****************************************************************
Static Function ReplicaProd(cParam)
*****************************************************************
//�����������������������������������������������������Ŀ
//� REPLICA CODIGO PRODUTO PARA OS ITENS SELECIONADOS   �
//|	Chamada -> Visual_XML()-> Bota Replica				|
//�������������������������������������������������������
Local cDescProd	:=	''
Local nPosProd 	:= Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_COD'	})
Local nPosLocal	:= Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_LOCAL'	})
Local nPosTES	:= Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_TES'	})

Private cOpcao 		:=	cParam
Private cProdAglut	:=	''
Private cMarca 		:=	''
Private _cTitulo	:=	''


If cOpcao == 'PROD'
	cProdAglut	:=	Space(TamSX3('D1_COD')[1])
	_cTitulo		:=	oBrwV:oBrowse:aColumns[nPosProd]:cHeading
ElseIf cOpcao == 'ALMOX'
	cProdAglut	:=	Space(TamSX3('D1_LOCAL')[1])
	_cTitulo		:=	oBrwV:oBrowse:aColumns[nPosLocal]:cHeading
ElseIf cOpcao == 'TES'
	cProdAglut	:=	Space(TamSX3('D1_TES')[1])
	_cTitulo		:=	'TES' //oBrwV:oBrowse:aColumns[nPosTES]:cHeading
EndIf	
                        

                                                     
//����������������������Ŀ
//�   TELA - REPLICAR    �
//������������������������
SetPrvt("oFont1","oDlg1","oSay1","oGrp1","oBrw1","oGet1","oBtn1","oGrp2","oBrw2","oBtn2","oBtn3")
oFont0     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-08,-10),,.T.,0,,700,.F.,.F.,,,,,, )
oFont1     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-11,-13),,.T.,0,,700,.F.,.F.,,,,,, )
oFont2     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-13,-15),,.T.,0,,700,.F.,.F.,,,,,, )

oDlg1      := MSDialog():New( D(091),D(232),D(400),D(700),'Replica C�digo',,,.F.,,,,,,.T.,,oFont1,.T. )
oGrp1      := TGroup():New( D(004),D(008),D(028),D(230),"",oDlg1,CLR_BLACK,CLR_WHITE,.T.,.F. )

oSay1      := TSay():New( D(008), D(012),{|| _cTitulo },oDlg1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(092),D(008) )



If cOpcao == 'PROD'
	//����������������������Ŀ
	//� REPLICA - PRODUTO    �
	//������������������������
	oGet1      := TGet():New( D(016),D(012),{|u| If(PCount()>0,cProdAglut :=u,cProdAglut)},oDlg1,D(074),D(008),'',{|| IIF(Empty(cProdAglut).Or.!ExistCpo("SB1",cProdAglut), (cDescProd:=''), (cDescProd := AllTrim(Posicione('SB1',1,xFilial('SB1')+cProdAglut,'B1_DESC')),oGet1:Refresh() ) ) },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SB1","",,)

ElseIf cOpcao == 'ALMOX'
	//�������������������������Ŀ
	//� REPLICA - ALMOXARIFADO	|
	//���������������������������
	cF3Local	  := ''
	If SX3->(DbSetOrder(2),DbGoTop(),DbSeek('D1_LOCAL',.F.))
		cF3Local	  := SX3->X3_F3
	EndIf	
	oGet1      := TGet():New( D(016),D(012),{|u| If(PCount()>0,cProdAglut :=u,cProdAglut)},oDlg1,D(074),D(008),'',,CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"",,,.F.,.F.,,.F.,.F.,cF3Local,"",,)

ElseIf cOpcao == 'TES'
	//��������������������Ŀ
	//� REPLICA - TES	   �
	//����������������������
	oGet1      := TGet():New( D(016),D(012),{|u| If(PCount()>0,cProdAglut :=u,cProdAglut)},oDlg1,D(074),D(008),'',{|| IIF(Empty(cProdAglut).Or.!ExistCpo("SF4",cProdAglut), (cDescProd:=''), (cDescProd := AllTrim(Posicione('SF4',1,xFilial('SF4')+cProdAglut,'F4_TEXTO')),oGet1:Refresh() ) ) },CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SF4","",,)
	
EndIf	

oSay1      := TSay():New( D(019),D(090),{|| Left(cDescProd,50) },oDlg1,,oFont0,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(150),D(008) )


**********************
oTblTRB2()              
**********************
cMarca 		:= GetMark()
DbSelectArea("TRB2")        

oGrp2      := TGroup():New( D(030),D(008),D(135),D(230),"",oDlg1,CLR_BLACK,CLR_WHITE,.T.,.F. )
oBrw2      := MsSelect():New( "TRB2","OK","",{{"OK","","",""},{"ITEM","","Item",""},{"CODFOR","","Cod.Fornec",""},{"DESCR","","Descri��o",""}},.F.,cMarca,{D(035),D(012),D(130),D(225)},,, oGrp2 ) 

oBrw2:oBrowse:bAllMark  :=  {|| MarcaTRB2(), oBrw2:oBrowse:Refresh() }

oBtn2      := TButton():New( D(140),D(080),"&OK",oDlg1,{|| ConfirmaTRB2(),oDlg1:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )
oBtn3      := TButton():New( D(140),D(120),"&Cancelar",oDlg1,{|| oDlg1:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )

oDlg1:Activate(,,,.T.)

IIF(Select("TRB2") != 0, TRB2->(DbCLoseArea()), )
oBrwV:oBrowse:Refresh()

Return()
*****************************************************************
Static Function oTblTRB2()
*****************************************************************
Local 	aFds := {}
Local 	cTmp
Local   nPosItem    := 	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_ITEM'    	})
Local   nPosPFor    :=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'C7_PRODUTO' 	})
Local	nPosDesc    :=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'B1_DESCFOR'  	})
Local	nPosLocal	:=	Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_LOCAL'   	})
Local	nPosTES		:= 	Ascan( aMyHeader, {|X| AllTrim(X[2]) == 'D1_TES'		})

If Select("TRB2") == 0
	Aadd( aFds , {"OK"      ,"C",002,000} )
	Aadd( aFds , {"ITEM"    ,"C",004,000} )
	If cOpcao == 'ALMOX'
		Aadd( aFds , {"LOCALP"	,"C",002,000} )	
	ElseIf cOpcao == 'TES'
		Aadd( aFds , {"TES"	,"C",003,000} )
	EndIf
	Aadd( aFds , {"CODFOR"  ,"C",015,000} )
	Aadd( aFds , {"DESCR"	,"C",030,000} )
	
	cTmp := CriaTrab( aFds, .T. )
	Use (cTmp) Alias TRB2 New Exclusive
    Aadd(aArqTemp, cTmp)
Else
   DbSelectArea("TRB2") 
   RecLock('TRB2', .F.)
       Zap
   MsUnLock()
EndIf                 

DbSelectArea("TRB2")
For nX:=1 To Len(oBrwV:aCols)                                                       
   RecLock("TRB2",.T.)
		TRB2->ITEM  		:= oBrwV:aCols[nX][nPosItem]
		If cOpcao == 'ALMOX'
			TRB2->LOCALP 	:= oBrwv:aCols[nX][nPosLocal]
		ElseIf cOpcao == 'TES'
			TRB2->TES 	:= oBrwv:aCols[nX][nPosTES]
		EndIf
			
		TRB2->CODFOR  	:= oBrwv:aCols[nX][nPosPFor]
		TRB2->DESCR		:= IIF(nPosDesc>0, oBrwV:aCols[nX][nPosDesc], '' )
   MsUnLock()
Next

TRB2->(DbGoTop())

IIF(Type('oBrw1:oBrowse')!='U', (oBrw1:oBrowse:Refresh(),oBrw1:oBrowse:Setfocus()), )


Return()                                             
*****************************************************************
Static Function ConfirmaTRB2()
*****************************************************************
Local   nPosItem    :=    Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_ITEM'	})
Local   nPosProd    :=    Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_COD'		})
Local   nPosLocal   :=    Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_LOCAL'	})
Local   nPosTES     :=    Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'D1_TES'		})
Local   nPosDesc    :=    Ascan(aMyHeader  ,{|X| Alltrim(X[2]) == 'B1_DESC'	})	//	ATUALIZA COLUNA DESCRICAO PRODUTO EMPRESA


DbSelectArea('TRB2');DbGoTop()
Do While !Eof()

lDeletado := oBrwV:aCols[Val(TRB2->ITEM)][Len(aMyHeader)+1]

	If IsMark('OK', cMarca ) .And. !lDeletado
	
		If cOpcao=='PROD' .And. nPosProd > 0
		
			oBrwv:aCols[Val(TRB2->ITEM)][nPosProd] :=  cProdAglut
			
			If nPosDesc > 0
				oBrwv:aCols[Val(TRB2->ITEM)][nPosDesc]:= Posicione('SB1', 1, xFilial("SB1") + cProdAglut, 'B1_DESC')			
			EndIf
						                            
			If nPosLocal > 0
				If Empty(oBrwv:aCols[Val(TRB2->ITEM)][nPosLocal])
					oBrwv:aCols[Val(TRB2->ITEM)][nPosLocal]:= Posicione('SB1', 1, xFilial("SB1") + cProdAglut, 'B1_LOCPAD')
				EndIf
			EndIf
	
			If 	nPosTES > 0
				If	Empty(oBrwv:aCols[Val(TRB2->ITEM)][nPosTES])
					oBrwv:aCols[Val(TRB2->ITEM)][nPosTES] 	:=   Posicione('SB1', 1, xFilial("SB1") + cProdAglut, 'B1_TE')
				EndIf
			EndIf						    
		
		ElseIf cOpcao=='ALMOX' .And. nPosLocal > 0
			oBrwv:aCols[Val(TRB2->ITEM)][nPosLocal]:=  cProdAglut
		
		ElseIf cOpcao=='TES' .And. nPosTES > 0
			oBrwv:aCols[Val(TRB2->ITEM)][nPosTES] 	:=  cProdAglut
		
		EndIf
		
    EndIf
    DbSkip()
    
EndDo

Return
***********************************************************
Static Function MarcaTRB2(cPar)    //    [4.1.3]
***********************************************************
//������������������������������������������������Ŀ
//� Desmarca TODOS os itens do Browse              |
//� Chamada -> LoadFiles()                         �
//��������������������������������������������������
TRB2->(DbGoTop())
_cMarca    :=    IIF(Empty(TRB2->OK), cMarca,'')
Do While TRB2->(!Eof())
   RecLock("TRB2",.F.)
       TRB2->OK  := _cMarca
   MsUnLock()
   DbSkip()
EndDo

TRB2->(DbGoTop())

Return()
*****************************************************************
User Function Carrega_XML()    //    [4.0]
*****************************************************************
//������������������������������������Ŀ
//� Seleciona XML - Pasta\Email        |
//� Chamada -> Rotina Carrega XML      �
//��������������������������������������
Local	aAlias4  := GetArea()
Private	cTpCarga :=	''


nOpc := Aviso('Carega XML','Deseja Carregar XML de uma Pasta ou do Email???',{'Pasta','Email','Sair'}, 2)

If nOpc == 1
   MsgRun("Recebendo XML PASTA ",,{|| LoadFiles(cTpOpen:='IMPORTAR') })    						// [4.1]
ElseIf nOpc == 2
   Processa ({|| RecebeEmail(cTpOpen:='EMAIL') },"Recebendo XML EMAIL ",'Processando...', .T.)   	// [4.2]
EndIf


RestArea(aAlias4)
Return()
************************************************************
Static Function LoadFiles()    //    [4.1]
************************************************************
//���������������������������������Ŀ
//� Seleciona XML PASTA             �
//� Chamada -> Carrega_XML()        �
//�����������������������������������
Private cMarca        	:=	GetMark()
Private nQtdArqXML  	:= 	0
Private aStatus			:= 	{}
Static cDiretorioXML	:=	Space(200)

SetPrvt('oDlg1','oGrp','oSay1','oGet1','oBrw1','oBtn1','oBtn2','oBtn3')



If Select('TMPCFG') == 0
   DbUseArea(.T.,,_StartPath+cArqConfig, 'TMPCFG',.T.,.F.)
   DbSetIndex(_StartPath+cIndConfig)
EndIf
DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
DbSeek(cEmpAnt+cFilAnt, .F.)     

//	COM "\" NO FINAL NAO ENCONTRA O DIRETORIO
cPathXML := AllTrim(TMPCFG->PATH_XML)
If 	Right(cPathXML,1)=='\'
	cPathXML := SubStr(cPathXML,1, Len(cPathXML)-1)
EndIf



//���������������������������������Ŀ
//�    TELA CARREGA ARQUIVO XML     �
//�����������������������������������
oDlg1      := MSDialog():New( D(095),D(232),D(427),D(740),"Carregar arquivo XML - IMPORTAR",,,.F.,,,,,,.T.,,,.T. )
oGrp       := TGroup():New( D(004),D(004),D(146),D(250),"",oDlg1,CLR_BLACK,CLR_WHITE,.T.,.F. )

oSay1      := TSay():New( D(012),D(008),{||"Caminho do(s) Arquivo(s)"},oGrp,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(064),D(008) )
oGet1      := TGet():New( D(020),D(008),{|u| If(PCount()>0,cDiretorioXML :=u,cDiretorioXML)},oGrp,D(230),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,{|| IIF(!Empty(cDiretorioXML),(AdicionaAnexo(), oBrw1:oBrowse:Refresh()),) },.F.,.F.,"","",,)
oBtn1      := TButton():New( D(018),D(238),"...",oGrp,{|| cDiretorioXML  := 	cGetFile("Anexos (*xml)|*xml|","Arquivos (*xml)",0,cPathXML,.T.,GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_MULTISELECT+GETF_RETDIRECTORY),; 
																		IIF(!Empty(cDiretorioXML), Processa ({||AdicionaAnexo(),'Processando....','Carregando Arquivos',.F.}),), IIF(nQtdArqXML==0, oSay2:Hide(), oSay2:Show()),oBrw1:oBrowse:Refresh() },D(011),D(011),,,,.T.,,"",,,,.F. )

oSay2      := TSay():New( D(034),D(008),{|| AllTrim(Str(nQtdArqXML))+" arquivo(s) selecionado(s)"},oGrp,,,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(150),D(008) )
IIF(nQtdArqXML==0, oSay2:Hide(), oSay2:Show())
oSay2:Refresh()

****************************
   oTbl()    // CRIAR ARQ. TEMP
****************************
DbSelectArea("TMP")
oBrw1      := MsSelect():New( "TMP","OK","",{{"OK","","",""},{"ARQUIVO","","Arquivo",""}},.F.,cMarca,{D(040),D(008),D(140),D(245) },,, oGrp )

oBtn2      := TButton():New( D(150),D(086),"Ok"  ,oDlg1,{|| Processa ({|| Open_Xml('XML_TMP','','', cDiretorioXML,0,0)},'Verificando Arquivos XML','Processando...', .F.) ,oDlg1:End() },D(037),D(012),,,,.T.,,"",,,,.F. )
oBtn3      := TButton():New( D(150),D(146),"Sair",oDlg1,{|| oDlg1:End(), lClose:=.T.},D(037),D(012),,,,.T.,,"",,,,.F. )

oBrw1:oBrowse:bAllMark      :=    {|| MarcaTMP(),         oSay2:Refresh(), oBrw1:oBrowse:Refresh() }
oBrw1:oBrowse:bLDblClick    :=    {|| Des_MarcaTMP(),     oSay2:Refresh(), oSay2:Refresh()}


oDlg1:Activate(,,,.T.,{||.T.})


IIF(Select("TMP")!= 0, TMP->(DbCLoseArea()), )

//����������������������������������������������Ŀ
//� MOSTRA TELA COM STATUS DA IMPORTACAO DO XML  �
//|	XML IMPORTADO VIA PASTA						 |
//������������������������������������������������
If Len(aStatus) > 0
   ******************************
       MensStatus()
   ******************************
EndIf


Return()
************************************************************
Static Function oTbl()    //    [4.1.1]
************************************************************
//���������������������������������������������Ŀ
//� Cria\Limpa Arq. Temp            			�
//� Chamada -> Carrega_XML()->LoadFiles()       �
//�����������������������������������������������
Local aFds := {}
Local cTmp

If Select("TMP") == 0
   Aadd( aFds , {"OK"      ,"C",002,000} )
   Aadd( aFds , {"ARQUIVO" ,"C",200,000} )

   cTmp := CriaTrab( aFds, .T. )
   Use (cTmp) Alias TMP New Exclusive
   Aadd(aArqTemp, cTmp)
Else
   LimpaTMP()
EndIf

Return()
***********************************************************
Static Function AdicionaAnexo()		//	[4.1.2]
***********************************************************
//������������������������������������������������Ŀ
//� Adiciona no Browser os itens XML de uma pasta  |
//� Chamada -> Carrega_XML()->LoadFiles()          �
//��������������������������������������������������
Local    aArqXml     :=    {}


If !Empty(cDiretorioXML)
	nQtdArqXML    := 0
	cDiretorioXML := cDiretorioXML+IIF(Right(AllTrim(cDiretorioXML),1)=='\','','\')
	aDir(AllTrim(cDiretorioXML)+'*.xml',aArqXml)
Else
	cDiretorioXML := Space(200)
	Return()
EndIf                

**********************
   LimpaTMP()
**********************

For _X:=1 To Len(aArqXml)
   nQtdArqXML++
   RecLock("TMP",.T.)
       TMP->OK 	     := cMarca
       TMP->ARQUIVO  := aArqXml[_X]
   MsUnLock()
Next

TMP->(DbGoTop())

Return()
***********************************************************
Static Function MarcaTMP(cPar)    //    [4.1.3]
***********************************************************
//������������������������������������������������Ŀ
//� Desmarca TODOS os itens do Browse              |
//� Chamada -> Carrega_XML()->LoadFiles()          �
//��������������������������������������������������
nQtdArqXML := 0
DbSelectArea('TMP');DbGoTop()
_cFlag    :=    IIF(!Empty(TMP->OK), cMarca,'')
Do While TMP->(!Eof())
   If !Empty(_cFlag)
       nQtdArqXML :=	0
	Else
		nQtdArqXML++	
   EndIf
   RecLock("TMP",.F.)
       TMP->OK  := IIF(!Empty(_cFlag),'', cMarca)
   MsUnLock()
   DbSkip()
EndDo

TMP->(DbGoTop())

Return()
***********************************************************
Static Function Des_MarcaTMP()    //    [4.1.4]
***********************************************************
//������������������������������������������������Ŀ
//� Marca\Desmarca itens do Browse                 |
//� Chamada -> Carrega_XML()->LoadFiles()          �
//��������������������������������������������������
_cMarca    :=    IIF(Empty(TMP->OK), cMarca,'')
If !Empty(_cMarca)
   nQtdArqXML++
Else
   nQtdArqXML--
EndIf

RecLock("TMP",.F.)
   TMP->OK  := _cMarca
MsUnLock()


Return()
***********************************************************
Static Function LimpaTMP()    //    [4.1.5]
***********************************************************
//������������������������������������������������Ŀ
//� Limpa Arq.Temp                                 |
//� Chamada -> Carrega_XML()->LoadFiles()          �
//�         -> oTbl(), AdicionaAnexo()             �
//��������������������������������������������������

If Select('TMP') > 0
   DbSelectArea("TMP")
   RecLock('TMP', .F.)
       Zap
   MsUnLock()
Endif

TMP->(DbGoTop())

Return()
*****************************************************************
Static Function RecebeEmail(cTpCarga)    //    [4.2]
*****************************************************************
//���������������������������������Ŀ
//� Busca XML do EMAIL              �
//� Chamada -> Carrega_XML()        �
//�����������������������������������


Local cTpConexao	:=	''
Local cServerEnv    :=	''
Local cServerRec    :=	''
Local cPassword    	:=	''
Local cAccount     	:=	''
Local cOrigFolder	:=	''
Local cDestFolder	:=	''
Local cPortEnv    	:=	''
Local cPortRec    	:=	''
Local nNumMsg		:= 	0
Local aAllFolder	:= 	{}
Local lConect		:=	.F.
Local lCreateFolder	:=	.F.
Local lMoveEmail	:=	.F.
Local lAutent
Local lSegura
Local lSSL
Local lTLS

Local oServer
Local oMsg
Local nConfig
Local nConecTou

Private aStatus    := {}

                      

//�����������������������������������������������Ŀ
//�BUSCA AS CONFIGURACOES DEFINIDAS PELO USUARIOS �
//�NO PAINEL DE CONFIGURACAO                      �
//�������������������������������������������������
DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
If DbSeek(cEmpAnt+cFilAnt, .F.)

	cTpConexao	:=	Left( AllTrim(TMPCFG->TIPOEMAIL),3)
	cServerEnv	:=	AllTrim(TMPCFG->SERVERENV)
	cServerRec	:=	AllTrim(TMPCFG->SERVERREC)
	cPassword	:=	AllTrim(TMPCFG->SENHA)
	cAccount	:=	AllTrim(TMPCFG->EMAIL)
	cOrigFolder	:=	AllTrim(TMPCFG->PASTA_ORIG)
	cDestFolder	:=	AllTrim(TMPCFG->PASTA_DEST)
	cPortEnv	:=	AllTrim(TMPCFG->PORTA_ENV)
	cPortRec	:=	AllTrim(TMPCFG->PORTA_REC)
	lAutent		:=	TMPCFG->AUTENTIFIC
	lSegura		:=	TMPCFG->SEGURA
	lSSL		:=	TMPCFG->SSL
	lTLS		:=	TMPCFG->TLS

Else                

   MsgInfo('N�o encontrado arquivo de configura��o da Empresa: '+cEmpAnt+' \ Filial: '+cFilAnt+ENTER+ENTER+'Utilize o Bot�o de Configura��o...')
   Return()

EndIf



oServer := TMailManager():New()

If lSegura
	oServer:SetUseSSL(lSSL)					//		Define o envio de e-mail utilizando uma comunica��o segura atrav�s do SSL - Secure Sockets Layer.
	oServer:SetUseTLS(lTLS)					//		Define no envio de e-mail o uso de STARTTLS durante o protocolo de comunica��o.
EndIf	

							//	SERVER RECEBIMENTO,	 SERVER ENVIO	   ,		EMAIL		  ,	    SENHA	  	  , PORTA RECEBIMENTO  ,  PORTA ENVIO
nConfig    :=    oServer:Init( AllTrim(cServerRec), AllTrim(cServerEnv), AllTrim(cAccount), AllTrim(cPassword), Val(cPortRec), Val(cPortEnv) )

If lAutent
	oServer:SMTPAuth(oServer:GetUser(), AllTrim(cPassword))
EndIf



If cTpConexao == 'IMAP'
   nConecTou     :=    oServer:IMAPConnect()
ElseIf Left(cTpConexao,3) == 'POP'
   nConecTou     :=    oServer:PopConnect()
ElseIf cTpConexao == 'MAPI'
     nConecTou   :=    oServer:ImapConnect()
ElseIf cTpConexao == 'SMTP'
     nConecTou   :=    oServer:SmtpConnect()
EndIf


#IFDEF WINDOWS
   lConect := IIF(nConectou != 0, .F., .T.)
#ELSE
   If nConectou == 0 .And. cTpConexao != 'MAPI'
       lConect := .T.
   ElseIf nConecTou == 1 .And. cTpConexao == 'MAPI'
       lConect := .T.
   Else
       lConect := .F.
   EndIf
#ENDIF


If  !lConect
       MsgBox(	"N�o foi poss�vel ler emails da conta..."+ENTER+; 
       			"Servidor Recebimento: "+cServerRec+ENTER+;
           		"Servidor Envio: "    +cServerEnv+ENTER+;
				"Usu�rio:  "    +cAccount+ENTER+;
				"Porta "    +cTpConexao+": "+cPortRec+ENTER+;
				"Porta SMTP: "+cPortEnv+ENTER+ENTER+;
				"ERRO: "    /*+AllTrim(oServer:GetErrorString(nConecTou))*/ +ENTER+ENTER+;
				"Verifique a configura��o de e-mail da filial.") 

Else


   //������������������������������Ŀ
   //� VERIFICA SE PASTAS EXISTEM   �
   //��������������������������������
   aAllFolder        :=    oServer:GetAllFolderList()        //    MOSTRA TODAS  AS PASTAS
   lCreateFolder    := .F.

   If cTpConexao == 'IMAP'

	   //������������������Ŀ
	   //� PASTA DE ORIGEM	�
	   //��������������������
       If !Empty(cOrigFolder)
           nPosFolder    :=    Ascan(aAllFolder, { |x| Upper(X[1]) == IIF(cOrigFolder=='Caixa de Entrada','INBOX', Upper(cOrigFolder) )} )
           If nPosFolder == 0
               If MsgYesNo('Pasta de Origem '+cOrigFolder+' n�o existe...'+ENTER+'Deseja criar essa pasta ???')
                   lCreateFolder := .T.
                   Processa ({|| },'Aguarde criando pasta de Origem...','Processando...', .T.)

                   oServer:CreateFolder(cOrigFolder)                    //    CRIA PASTA
                   oServer:SetFolderSubscribe(cOrigFolder, .T.)        //    ASSINA PASTA, DEIXA ELA VISIVEL
                   aAllFolder     := oServer:GetFolder()               //    MOSTRA APENAS AS PASTAS ASSINADAS
               EndIf
            Else                                                   
              	  cOrigFolder := IIF(cOrigFolder=='Caixa de Entrada','INBOX', cOrigFolder )
                  oServer:ChangeFolder(cOrigFolder)
           EndIf

       Else
       	  cOrigFolder := IIF(cOrigFolder=='Caixa de Entrada','INBOX', cOrigFolder )
          oServer:ChangeFolder(cOrigFolder)
       EndIf


	   //������������������Ŀ
	   //� PASTA DE DESTINO	�
	   //��������������������
       If !Empty(cDestFolder)
           lMoveEmail    :=    .T.
           nPosFolder    :=    Ascan(aAllFolder, { |x| Upper(X[1]) == Upper(cDestFolder) })
           If nPosFolder == 0
               If MsgYesNo('Pasta de Destino '+cDestFolder+' n�o existe...'+ENTER+'Deseja criar essa pasta ???')
                   lCreateFolder := .T.
                   Processa ({|| },'Aguarde criando pasta de Destino...','Processando...', .T.)

                   oServer:CreateFolder(cDestFolder)
                    oServer:SetFolderSubscribe(cDestFolder, .T.)
                   aAllFolder  := oServer:GetFolder()
               EndIf
           EndIf
      EndIf

       If lCreateFolder
           MsgAlert('Feche e abra novamente o gerenciador de e-mail para visualizar as pastas criadas...')
       EndIf
   EndIf


   // SELECIONA A PASTA DE E-MAIL DE ORIGEM
   cOrigFolder := IIF(cOrigFolder=='Caixa de Entrada','INBOX', cOrigFolder )
   oServer:ChangeFolder(cOrigFolder)


    If cTpConexao == 'IMAP'
       oServer:SetUseRealID( .T. )
    EndIf
   
   	nRet := oServer:GetNumMsgs(@nNumMsg)
	ProcRegua(nNumMsg)


   oMsg :=  TMailMessage():New()

   For nX :=1 To nNumMsg

       IncProc()
       oMsg:Clear()
       oMsg:Receive( oServer, nX )

       	//������������������������������Ŀ
       	//�  VERIFICA O TIPO DO ANEXO    �
       	//��������������������������������
		For nY := 1 To oMsg:GetAttachCount()

           	aInfo        :=    {}
			aInfo        :=    oMsg:GetAttachInfo(nY)
			If Right(aInfo[1], 4) == '.xml'

               	cAnexo :=  oMsg:GetAttach(nY)
/*
               	nIndex := nY
               	cPath  := 'C:\'
				oMsg:SaveAttach(nIndex, cPath )
*/                

               //��������������������������������������Ŀ
               //�    ABRE E GRAVA ARQUIVO XML          �
               //����������������������������������������
               ***********************************************
                   Open_Xml('XML_EMAIL', cAnexo, AllTrim(oMsg:cSubject)+' - '+AllTrim(aInfo[1]), '', nX, nNumMsg)
		       ***********************************************

               If cTpCarga == 'EMAIL' .And. cTpConexao == 'IMAP'

                   //������������������������������������������������������������Ŀ
                   //�Move uma mensagem da pasta em uso, do servidor IMAP         �
                   //�para outra pasta contida na conta de e-mail.                |
                   //��������������������������������������������������������������
                   If lMoveEmail
                       oServer:MoveMsg(nX, cDestFolder)

                   EndIf
				
				EndIf
			
			EndIf

		Next

	Next


Endif
                


//����������������������������������������������Ŀ
//� MOSTRA TELA COM STATUS DA IMPORTACAO DO XML  �
//|	XML IMPORTADO VIA E-MAIL					 |
//������������������������������������������������
If Len(aStatus) > 0
   ******************************
       MensStatus()	
   ******************************
EndIf


//�����������������������������Ŀ
//� DESCONECTA DE E-MAIL		|
//�������������������������������
If lConect
   If cTpConexao == 'IMAP'
       oServer:SmtpDisconnect()
   ElseIf Left(cTpConexao,3) == 'POP'
       oServer:PopDisconnect()
   ElseIf cTpConexao == 'MAPI'
       oServer:ImapDisconnect()
   ElseIf cTpConexao == 'SMTP'
       oServer:SmtpDisconnect()
   EndIf
EndIf

Return()
*****************************************************************
Static Function Open_Xml(cTpOpen, cAnexo, cNomeArq, cDiretorioXML, nImportados, nMarcados)    //    [4.1.6] - [4.2.1]
*******************************************************************
//�����������������������������������������Ŀ
//� Abre XML                                �
//� Chamada -> LoadFiles()-> RecebeEmail()  |
//�������������������������������������������

Private cArqXML := ''




//���������������������������Ŀ
//�BUSCA XML PELO DIRETORIO   �
//�����������������������������
If cTpOpen == 'XML_TMP'

	nMarcados	:=	0
	nImportados	:=	0	
	For nX:=1 To nQtdArqXML
		If !Empty(cMarca)
			nMarcados++
		EndIf
	Next

	ProcRegua(nMarcados)
	DbSelectArea('TMP');DbGoTop()
	Do While !Eof()
   
		If IsMark('OK', cMarca )

			nImportados++
			IncProc('Importando arquivos XML... '+ AllTrim(Str(nImportados))+' de '+ AllTrim(Str(nMarcados))  )

			cArqXML	:=	AllTrim(TMP->ARQUIVO)
			cAnexo	:=	''
           	ProcRegua(Len(cAnexo))

			cTeste := ''
			
			//���������������������������������Ŀ
			//�REALIZA A LEITURA DO ARQUIVO XML �
			//�����������������������������������
			If File(AllTrim(cDiretorioXML)+cArqXML)
				FT_FUSE(AllTrim(cDiretorioXML)+cArqXML)
				FT_FGOTOP()
				Do While !FT_FEOF()
					
					cLinha := FT_FREADLN()
					cLinha := StrTran(cLinha,'</','')
					cLinha := StrTran(cLinha,'<','')
					cLinha := StrTran(cLinha,'>','')					
					cTeste += cLinha
					
					cAnexo += FT_FREADLN()
					FT_FSKIP()
               	EndDo
               	FT_FUSE()
                          
    			MemoWrit('C:\QUERY\TESTE_XML.TXT', cTeste )



	
				//���������������������������������Ŀ
				//� REALIZA VERIFICACOES E GRAVA XML�
				//�����������������������������������
				
				Conout( "INICIO -------------------------------- "+ENTER )
				
				Conout( "cEmpAnt == "+ cEmpAnt +ENTER )
				Conout( " SM0->M0_CODIGO + SM0->M0_CODFIL == "+  SM0->M0_CODIGO+' - '+SM0->M0_CODFIL +ENTER )

				Conout( " cEmpAnt + cFilAnt == "+  cEmpAnt+' - '+cFilAnt +ENTER )
								
				Conout( "Grava_Xml"+ENTER )
				******************************************************
					Grava_Xml(cAnexo, cArqXML, nImportados, nMarcados )
				******************************************************

				Conout( "Len(aStatus) == "+STR(Len(aStatus)) +ENTER )
				
				Conout( "FIM -------------------------------- "+ENTER )
				
				If  Len(aStatus) > 0 
					If aStatus[Len(aStatus)][1] $ 'I/G'
						//�����������������������������������������Ŀ
						//� RENOMEA XML PARA NOMEXML_IMPORTADO		�
						//�������������������������������������������
							Renomea_XML(Len(aStatus))
					EndIf
				EndIf

			EndIf
		EndIf

		IncProc()
		DbSelectArea('TMP')
		DbSkip()

		
		//��������������������������������������������������Ŀ
		//� JA IMPORTOU TODOS OS ARQUIVOS MARCADOS...        �
		//����������������������������������������������������
		If nImportados == nMarcados
			Exit
		EndIf

	EndDo

	TMP->(DbGoTop())


ElseIf cTpOpen == 'XML_EMAIL'
	//���������������������������Ŀ
	//�BUSCA XML PELO E-MAIL	  �
	//�����������������������������
	
   //��������������������������������������������������������������������Ŀ
   //� NAO PRECISA ABRIR O XML E LER LINHA A LINHA...                     |
   //| NA FUNCAO QUE ABRE O ANEXO DO EMAIL JA VEM TODA A ESTRUTURA DO XML |
   //����������������������������������������������������������������������
   ****************************************
       Grava_Xml(cAnexo, AllTrim(cNomeArq),nImportados, nMarcados, cDiretorioXML)
   ****************************************

EndIf

Return()
***********************************************************
Static Function Grava_Xml(cAnexo, cArqXML, nImportados, nMarcados, cDiretorioXML )
***********************************************************
//�����������������������������������������Ŀ
//� Parse\Grava XML                         �
//� Chamada -> LoadFiles()->Open_Xml()      |
//�            RecebeEmail()->Open_Xml()    |
//�������������������������������������������

Local cDestEmpAnt	:=	''
Local cDestFilial	:=	''
Local cStatus		:=	''
Local cDocSF1		:=	''
Local cSerieSF1   	:=	''
Local cCodSA2    	:=	''
Local cLojaSA2    	:=	''
Local cNomeSA2     	:=	''
Local cUFSA2     	:=	''
Local cTipoNFe		:=	''
Local cError        :=	''
Local cWarning      :=	''
Local cRest			:=	''
Local cSA2Modo:=cSF1Modo:=cZXMModo:=cZXNModo:=''
Local dDtEntrada    :=	StoD('  \  \  ')
Local lStatus		:=	.F.
Local lEntrada		:=	.F.
Local lExterior		:=	.F.

Private oXml        :=	Nil
Private nItensXML   :=	0
Private cInfAdic    :=	''
Private cDataNFe    :=	''
Private cEmitCnpj   :=	''
Private cEmitNome   :=	''
Private cDestNome   :=	''
Private cDestCnpj   :=	''
Private cNotaXML    :=	''
Private cSerieXML   := 	''
Private cChaveXML   :=	''
Private cNatOper    :=	''

Private cLote       :=  ''
Private dDataVal    :=  '' //StoD('  \  \  ')
Private cDataVal    :=  '' //StoD('  \  \  ')
Private cDtValid    := ''


//���������������������������������������������������������Ŀ
//| VERIFICA SE TABELAS SAO COMPARTILHADAS OU EXCLUSIVAS	|
//�����������������������������������������������������������
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('SA2'))
   cSA2Modo	:=	AllTrim(SX2->X2_MODO)
EndIf
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('SF1'))
   cSF1Modo	:=	AllTrim(SX2->X2_MODO)
EndIf
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('ZXM'))
   cZXMModo	:=	AllTrim(SX2->X2_MODO)
EndIf
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('ZXN'))
   cZXNModo	:=	AllTrim(SX2->X2_MODO)
EndIf
					

	//�����������������������������Ŀ
	//�     ABRE XML - CABEC        �
	//�������������������������������
   **************************************************************************************
       aRet := ExecBlock("CabecXmlParser",.F.,.F., { IIF(Empty(cAnexo),'Erro',cAnexo)} )
   **************************************************************************************

If Type('_aEmpFil') == 'U'
	   //���������������������������������������Ŀ
	   //�Utilizado para criar variaveis Private |
	   //|Rotina xxxx nao cria essa var.         |
	   //�����������������������������������������
       //ExecBlock("IniConfig",.F.,.F.,)
EndIf

If aRet[1] == .T. 	//	aRet[1] == .F. SIGNIFICA XML COM PROBLEMA NA ESTRUTURA
	//nPos := Ascan(_aRetIni, {|X| X[8][3] == cDestCnpj} ) 
   	nPos := Ascan(_aEmpFil, {|X| X[3] == cDestCnpj} ) 
	If nPos == 0 .And. cDestCnpj != 'EXTERIOR'

		MSGALERT('CAMPO CNPJ DESTINO SPERB	'+cDestCnpj)
       	//���������������������������������������Ŀ
       	//� NAO ENCONTROU O CNPJ DO DESTINATARIO  �
       	//�����������������������������������������
       	cStatus	:=	'C'
       	lStatus	:=	.T.
		Aadd(aStatus, {cStatus, cArqXML, Transform(cDestCnpj, IIF(Len(cDestCnpj)==14,'@R 99.999.999/9999-99','@R 999.999.999-99'))  +' - '+ cDestNome, cDiretorioXML, cDestCnpj })
	
	ElseIf cDestCnpj == 'EXTERIOR'
	
		//��������������������������Ŀ
		//�  IMPORTACAO IMELTRON     �
		//����������������������������s
        	
       	cDestCnpj := cEmitCnpj
       	cEmitCnpj := 'EXTERIOR'
        msgalert('CnPj :'+cDestCnpj)
		for nAuxi :=1 to len (_aEmpFil)
			msgalert(_aEmpFil[nAuxi][3])
		next 
	   	nPos := Ascan(_aEmpFil, {|X| X[3] == cDestCnpj } ) 
       	If nPos == 0
		   msgalert('teste do cnpj dentro do Else Sperb')
       		//���������������������������������������Ŀ
       		//� NAO ENCONTROU O CNPJ DO DESTINATARIO  �
       		//�����������������������������������������
       		cStatus	:=	'C'
       		lStatus	:=	.T.
			Aadd(aStatus, {cStatus, cArqXML, 'IMPORTA��O.  '+Transform(cDestCnpj, IIF(Len(cDestCnpj)==14,'@R 99.999.999/9999-99','@R 999.999.999-99'))  +' - '+ cDestNome, cDiretorioXML, cDestCnpj })
         EndIf
         
    EndIf
    
	  
	
	If nPos > 0
         
		//���������������������������������������Ŀ
		//� SALVA EMPRESA\FILIAL\NOME DESTINO     �
		//�����������������������������������������
		cDestEmpAnt    :=    _aEmpFil[nPos][1]
		cDestFilial    :=    _aEmpFil[nPos][2]
		cNomeEmpFil    :=    AllTrim(SM0->M0_NOME)+' '+AllTrim(SM0->M0_FILIAL)//_aEmpFil[nPos][4]
		cNomeFilial    :=    AllTrim(SM0->M0_NOME)+' '+AllTrim(SM0->M0_FILIAL)//_aEmpFil[nPos][5]
              

	
		//������������������������������������������������������������������������������Ŀ
		//� VERIFICA SE XML Eh PARA A MESMA EMPRESA QUE ESTA POSICIONADO  				|
		//� CASO XML FOR IMPORTADO PELA EMPRESA 01 E O XML SEJA PARA EMPRESA 02			|
		//| ABRE-SE AS TABELAS DA EMPRESA 02... NO FINAL DA ROTINA VOLTA PARA EMPRESA 01	|
		//��������������������������������������������������������������������������������
		if len(alltrim(cFilAnt)) == 4
			_cVar := substr(cFilAnt,3,2)
			
		else 
			_cVar := cFilAnt
		endif
		If _cVar != cEmpAnt

		
			//������������������������������������������������������Ŀ
			//� VERIFICA SE TABELA ZXM ESTA COM TODOS CAMPOS CRIADOS |
			//��������������������������������������������������������
			DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
           	If DbSeek(cDestEmpAnt+cDestFilial, .F.)

	
				If !TMPCFG->ESTRUT_ZXM


					If !ExecBlock("CreateTable",.F.,.F.,{cDestEmpAnt} )

						//������������������������������������������Ŀ
                       	//� TABELA ZXM NAO EXISTE NA EMPRESA DESTINO |
                       	//��������������������������������������������
                       	cStatus :=  'X'
                       	lStatus	:=	.T.
                       	Aadd(aStatus, {cStatus, cArqXML, cDestEmpAnt, cNomeEmpFil, cNomeFilial, cDiretorioXML, cDestCnpj })

						//������������������������������������������Ŀ
						//� RETORNA COM TABELAS DA EMPRESA ATUAL     �
						//��������������������������������������������
						IIF(Select("SA2") != 0, SA2->(DbCLoseArea()), )
						IIF(Select("SF1") != 0, SF1->(DbCLoseArea()), )
						IIF(Select("ZXM") != 0, ZXM->(DbCLoseArea()), )
						IIF(Select("ZXN") != 0, ZXN->(DbCLoseArea()), )
						
						EmpOpenFile('SA2',"SA2",1,.T.,cEmpAnt,@cSA2Modo)
						EmpOpenFile('SF1',"SF1",1,.T.,cEmpAnt,@cSF1Modo)
						EmpOpenFile('ZXM',"ZXM",1,.T.,cEmpAnt,@cZXMModo)
						EmpOpenFile('ZXN',"ZXN",1,.T.,cEmpAnt,@cZXNModo)
						
						Return()

					EndIf
                Else
           			Conout( "TMPCFG->ESTRUT_ZXM"+ ENTER )
				EndIf

				//���������������������������������������Ŀ
				//� FECHA ARQUIVOS DA EMPRESA ATUAL       �
				//�����������������������������������������
				IIF(Select("SA2") != 0, SA2->(DbCLoseArea()), )
				IIF(Select("SF1") != 0, SF1->(DbCLoseArea()), )
				IIF(Select("ZXM") != 0, ZXM->(DbCLoseArea()), )
				IIF(Select("ZXN") != 0, ZXN->(DbCLoseArea()), )

				//���������������������������������������Ŀ
				//� ABRE ARQUIVOS DA EMPRESA DESTINO      �
				//�����������������������������������������
				EmpOpenFile('SA2',"SA2",1,.T.,cDestEmpAnt,@cSA2Modo)
				EmpOpenFile('SF1',"SF1",1,.T.,cDestEmpAnt,@cSF1Modo)
				EmpOpenFile('ZXM',"ZXM",1,.T.,cDestEmpAnt,@cZXMModo)
				EmpOpenFile('ZXN',"ZXN",1,.T.,cDestEmpAnt,@cZXNModo)
              			
             			
			Else
		
				MsgAlert('N�o encontrado Empresa: '+cDestEmpAnt+'\'+cNomeEmpFil+' Filial: '+cDestFilial+'-'+cNomeFilial+' no arquivo ZXM_CFG.DTC') //.DBF
				Aadd(aStatus, {'XX', cArqXML, 'N�o encontrado Empresa: '+cDestEmpAnt+'\'+cNomeEmpFil+' Filial: '+cDestFilial+'-'+cNomeFilial+' no arquivo ZXM_CFG.DTC','','' }) //.DBF
				Return()      
			EndIf

	EndIf



	//���������������������������������������������Ŀ
	//�  VERIFICA CONFIGURACAO DA EMPRESA ATUAL     �
	//�����������������������������������������������
    DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
    If DbSeek(cEmpAnt + cFilAnt, .F.) .And. !lStatus

		Conout('DbSeek(cEmpAnt + cFilAnt, .F.) .And. !lStatus'+ ENTER)
		
			//���������������������������������������������Ŀ
           	//� ATUALIZA VARIAVEIS CONFORME CNOFIGURACOES   �
           	//|	NOTA E SERIE - SE PREENCHE COM ZEROS OU NAO |	
           	//�����������������������������������������������       
           	
           	DbSelectArea("SA1")
           	DbSetOrder(3)
           	DbSeek(xFilial("SA1")+cEmitCnpj,.F.)
           	
           	If Substr(SA1->A1_COD,1,1) == "Z"
	          	cNotaXML     :=    PadL(AllTrim(cNotaXML),6,'0')
			Else
	           	cNotaXML     :=    IIF(TMPCFG->NFE_9DIG=='SIM',PadL(AllTrim(cNotaXML),9,'0'), cNotaXML    )
	        EndIf   	
           	//Benhur
           	// 09/10/2019
           	//cSerieXML    :=    IIF(Left(TMPCFG->CNPJ,8)==Left(cEmitCnpj,8), TMPCFG->SER_TRANSF , IIF(TMPCFG->SER_3DIG=='SIM',PadL(AllTrim(cSerieXML),3,'0'), cSerieXML) )
			
		If Left(TMPCFG->CNPJ,8) == Left(cEmitCnpj,8)
		
			// BUSCA A SERIE DA FILIAL DE SAIDA DA NOTA DE TRANSFERENCIA
			DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
			Do While !Eof()
 					If TMPCFG->CNPJ == cEmitCnpj
						cSerieXML := TMPCFG->SER_TRANSF
					Exit
				EndIf
				DbSkip()
			EndDo       
			
			//	 VOLTA PARA REGISTRO ANTERIOR
	       	DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
	       	DbSeek(cEmpAnt + cFilAnt, .F.)
		       	
		//ElseIf TMPCFG->SER_3DIG == 'SIM'	//	18/10/2011  bENH
		//	cSerieXML  := PadL(AllTrim(cSerieXML),3,'0')
		Else
			 cSerieXML := cSerieXML
		EndIf

   
        //�����������������������������������������������������������������������������Ŀ
        //�  SE FORNECEDOR FOR DO EXTERIOR USUARIOS DEVERA INFORMAR CODIGO DO FORNECE	|
        //�������������������������������������������������������������������������������
        If cEmitCnpj == 'EXTERIOR'
			
			lExterior := .T.
			/*
				MsgInfo('NF-e Importa��o. Selecione Fornecedor.')
           		**********************************
				cEmitCnpj := SA2_SA1ConsPad(IIF(!lNFeDev,'SA2','SA1'))	//	MONTA TELA PARA CONSULTA DO FORNECEDOR
           		**********************************
	        */
	    Else
	    	lExterior := .F.
		EndIf             
		

        //���������������������������������Ŀ
        //�  VERIFICA FORNECEDOR \ CLIENTE  �
        //�����������������������������������
	    lCliente  := .F.
	    lFornece  := .F.
      	cTipoNFe  := ''
             
		If lExterior
           	//�������������������������Ŀ
           	//�  XML - IMPORTACAO		�
           	//���������������������������
			cCodSA2    	:=  ''
           	cLojaSA2    :=  ''
           	cNomeSA2    := 	''
           	cUFSA2		:=	''
	
		ElseIf !lNFeDev
           	//���������������������������������Ŀ
           	//�  XML - TIPO DE ENTRADA NORMAL   �
           	//�����������������������������������
           	DbSelectarea("SA2");IIF(!lExterior,DbSetOrder(3),DbSetOrder(1));DbGoTop()
           	If Dbseek(xFilial("SA2") + cEmitCnpj, .F.)
				cCodSA2    	:=  SA2->A2_COD
               	cLojaSA2    :=  SA2->A2_LOJA
               	cNomeSA2    := 	AllTrim(SA2->A2_NOME)
	           	cUFSA2		:=	SA2->A2_EST
	           	lFornece	:= 	.T.
			EndIf
        
        	Conout(' !lNFeDev - ACHOU FORNECEDOR '+ ENTER)
        	    
		Else
            
           	//����������������������������������������������Ŀ
           	//� XML - TIPO DEVOLUCAO - lNFeDev == TAG NFREF  �
           	//������������������������������������������������
           	DbSelectarea("SA1");IIF(!lExterior,DbSetOrder(3),DbSetOrder(1));DbGoTop()
           	If Dbseek(xFilial("SA1") + cEmitCnpj, .F.)
               	cCodSA2    	:=  SA1->A1_COD
               	cLojaSA2    :=  SA1->A1_LOJA
               	cNomeSA2    :=  AllTrim(SA1->A1_NOME)
	           	cUFSA2		:=	SA1->A1_EST
	           	lCliente	:= .T.
           	EndIf

        	Conout(' lNFeDev - ACHOU CLIENTE '+ ENTER)

		EndIf

           	
        If !lExterior .And. (lCliente .Or. lFornece)
               
           	//������������������������������������������������������������������Ŀ
           	//� VERIFICA SE NOTA DO FORNECEDOR JA ESTA CADSATRADA NO SISTEMA     |
           	//��������������������������������������������������������������������
                                                                                     

			//���������������������������������������������������������������������������������������������Ŀ
           	//� SX2->X2_UNIC -> F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_FORMUL	 					|
           	//| REALIZA PESQUISA PELO F1_TIPO == N-NORMAL, SE FOR DEVOLUCAO\RETORNO PESQUISA SEM O F1_TIPO	|
           	//�����������������������������������������������������������������������������������������������
           	DbSelectArea("SF1");DbSetOrder(1);DbGoTop()    //    F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_TIPO
          	If Dbseek(cDestFilial + cNotaXML + cSerieXML + cCodSA2 + cLojaSA2, .F.)
				lEntrada    :=	.T.
              	dDtEntrada 	:= 	SF1->F1_EMISSAO
			   	cTipoNFe	:= 	SF1->F1_TIPO
	
			   	cStatus    	:= 	'G'
				lStatus		:=	.T.
				Aadd(aStatus, {cStatus, cArqXML, cNomeEmpFil, cNomeFilial, cDiretorioXML, cDestCnpj })
	        	
	        	Conout('PROCURA SF1 - VERIFICA SE JA HOUVE ENTRADA DA NF'+ ENTER)
        		               
			Else
				lEntrada    := 	.F.
				dDtEntrada  := 	StoD('  \  \  ')
        				
     			Conout('PROCURA SF1 - NAO HOUVE ENTRADA DA NF'+ ENTER)
        				
			EndIf
            

        	
		ElseIf !lExterior

   			Conout('NAO ENCONTROU O CNPJ DO FORNECEDOR'+ ENTER)


           	//���������������������������������������Ŀ
           	//� NAO ENCONTROU O CNPJ DO FORNECEDOR    �
          	//�����������������������������������������
          	cStatus	:= 'F'
	       	lStatus	:= .T.
			Aadd(aStatus, {cStatus, cArqXML, Transform(cEmitCnpj, IIF(Len(cEmitCnpj)>0,'@R 99.999.999/9999-99','@R 999.999.999-99') ) +' - '+ cEmitNome, cDiretorioXML, cEmitCnpj })
		
		Endif



		If !lStatus .Or. cStatus == 'G' 
           	//���������������������������������������������������������������������������������������������Ŀ
           	//� VERIFICA SE XML JA FOI IMPORTADO   															|
           	//| cStatus == 'G' -> CASO A NF-E JA TENHA INCLUSA NO SISTEMA (SF1) VERIFICA SE JA ESTA NO ZXM.	|
           	//�����������������������������������������������������������������������������������������������
           	DbSelectArea('ZXM');DbSetOrder(6);DbGoTop()
           	If DbSeek( cChaveXML , .F.)
				cStatus	:=	'D'       									//    XML JA IMPORTADO
		        lStatus	:=	.T.
				If cTpCarga !='EMAIL'  
					Aadd(aStatus, {cStatus, cArqXML, cNomeEmpFil+' \ '+cNomeFilial +'  Nota: '+cNotaXML+' S�rie: '+cSerieXML+' - Fornecedor: '+cCodSA2+'/'+cLojaSA2+'  -  '+cNomeSA2, cDiretorioXML, cDestCnpj })
				EndIf               
           	
           	Else
	           	//������������������������������������Ŀ
	           	//� 	IMPORTADO COM SUCESSO   	   |
	           	//��������������������������������������
               	cStatus :=	'I'
              	lStatus	:=	.T.
				Aadd(aStatus, {cStatus, cArqXML, cNomeEmpFil, cNomeFilial, cDiretorioXML, cDestCnpj, cNomeFilial })
				
        	EndIf

		EndIf                                                          
		
		
		

	EndIf
            
            
    //������������������������������������Ŀ
    //� CONSULTA XML-SEFAZ \ GRAVA ZXM     |
    //��������������������������������������
    If cStatus == 'I' .Or. cStatus == 'G' 

		Conout('CONSULTA XML-SEFAZ'+ ENTER)

		aRetorno := {}
    	**************************************************************************************
			 MsgRun("Aguarde... Consultando XML na SEFAZ...  "+IIF(cTpCarga!='EMAIL',AllTrim(Str(nImportados))+' de '+ AllTrim(Str(nMarcados)) ,'') ,,{|| aRetorno :=  U_ConsWebSG(cChaveXML)  })
    	**************************************************************************************
        // JAIME 05/01/2017 TRATAMENTO DE ERRO , POIS QUANDO A EMPRESA NAO ESTA COM ACESSO EXTERNO DA WEB NAO RETORNA NDADA NO ARRAY
        IF LEN(aRetorno) == 0
           Alert("Aten��o n�o houve retorno do Sefaz, verifique manualmente no site da Sefaz se a nota esta OK. A nota ser� incluida normalmente no sistema. ")

        ENDIF   
		DbSelectArea('ZXM')
		RecLock('ZXM',.T.)
			ZXM->ZXM_FILIAL	:=	xFilial('ZXM')
			ZXM->ZXM_STATUS	:=	cStatus
			ZXM->ZXM_FORNEC	:=	IIF(lNFeDev.Or.lExterior,'',cCodSA2 ) // DEVOLUCAO\RETORNO\IMPORTACAO GRAVA FORNEC e LOJA APOS USER SELECIONAR O TIPO [ NA ROTINA OpcaoRetorno ]
			ZXM->ZXM_LOJA	:=	IIF(lNFeDev.Or.lExterior,'',cLojaSA2) 
			If ZXM->(FieldPos("ZXM_NOMFOR")) > 0
				ZXM->ZXM_NOMFOR	:=	cNomeSA2
            EndIf
			If ZXM->(FieldPos("ZXM_UFFOR")) > 0
				ZXM->ZXM_UFFOR	:=	IIF(!Empty(cUFSA2), cUFSA2, IIF(lExterior, 'EX', cUFSA2) )
            EndIf
			ZXM->ZXM_CGC	:=	cEmitCnpj
			ZXM->ZXM_DOC	:=	cNotaXML
			ZXM->ZXM_SERIE	:=	cSerieXML
			ZXM->ZXM_DTIMP	:=	dDataBase
			ZXM->ZXM_CHAVE	:=	cChaveXML
			ZXM->ZXM_DTNFE	:=	dDtEntrada
			ZXM->ZXM_USER	:=	AllTrim(UsrRetName(__cUserId))
//			ZXM->ZXM_CODRET	:=	aRetorno[1]       // Retirados por Diego Cerioli em 05/01/17, pois o aRetorno estava retornando em branco, Eber vai arrumar link.
//			ZXM->ZXM_SITUAC	:=	IIF(Empty(aRetorno[1]),'',IIF(aRetorno[1]=='100','A',IIF(aRetorno[1]=='101','C','E')))
//			ZXM->ZXM_DTXML	:=	CTOD(aRetorno[2])
			ZXM->ZXM_TIPO	:=	cTipoNFe

	       	//�����������������������������������������������������Ŀ
	       	//� CAMPO MEMO EM SQL SUPORTA APENAS 65.535 CARACTERES	|
	       	//|	TABELA ZXN GRAVA O RESTANTE DO XML					|
	       	//�������������������������������������������������������
			#IFDEF TOP               
			
				If Len(AllTrim(cAnexo)) > 65535
				
			       	//�����������������������������������������������������Ŀ
			       	//�  GRAVA NA TABELA AUXILIAR, ZXN, O RESTANTE DO XML	|
			       	//�������������������������������������������������������
					**********************************************************************************
						TabSeqXML(cDestFilial, cChaveXML, SubStr(AllTrim(cAnexo), 65535, Len(AllTrim(cAnexo)))) 
					**********************************************************************************
			
					cAnexo	:=	SubStr(cAnexo, 1, 65534)
			
				EndIf
	
			#ENDIF
			
	     	ZXM->ZXM_XML := cAnexo
               
		MsUnlock()
	Else
		Conout('CSTATUS == '+ cStatus +  ENTER)	
	EndIf

   	EndIf

Else           

	//	aRet[3] -> FLAG XML SEM TAG PROTOCOLO
   cStatus    :=   IIF(Len(aRet)==2, 'E', aRet[3])
   Aadd(aStatus, {cStatus, cArqXML, aRet[2], cDiretorioXML, '' })
   
   Conout('CSTATUS == '+ cStatus +  ENTER)	
   
EndIf


//����������������������������������������Ŀ
//�  VOLTA AS TABELAS DA EMPRESA ATUAL     �
//������������������������������������������
If !Empty(cDestEmpAnt) .And. cDestEmpAnt != cEmpAnt 

	IIF(Select("SA2") != 0, SA2->(DbCLoseArea()), )
	IIF(Select("SF1") != 0, SF1->(DbCLoseArea()), )
	IIF(Select("ZXM") != 0, ZXM->(DbCLoseArea()), )
	IIF(Select("ZXN") != 0, ZXN->(DbCLoseArea()), )	
	
	EmpOpenFile('SA2',"SA2",1,.T.,cEmpAnt,@cSA2Modo)
	EmpOpenFile('SF1',"SF1",1,.T.,cEmpAnt,@cSF1Modo)
	EmpOpenFile('ZXM',"ZXM",1,.T.,cEmpAnt,@cZXMModo)
	EmpOpenFile('ZXN',"ZXN",1,.T.,cEmpAnt,@cZXNModo)
   
   DbGoTop()
   
EndIf
                      
Conout('FIM GRAVA_XML'+ENTER)	

Return()
*********************************************************************
Static Function TabSeqXML(cDestFilial, cChaveXML, cSeqXML)
*********************************************************************
//�����������������������������������������������������Ŀ
//� GRAVA NA TABELA AUXILIAR, ZXN, O RESTANTE DO XML	|
//� Chamada -> Grava XML()				 				|
//�������������������������������������������������������
Local nSeq := 0

Do While !Empty(cSeqXML)
	
	DbSelectArea('ZXN')
	Reclock('ZXN',.T.)             
		nSeq++
		ZXN->ZXN_FILIAL	:=	cDestFilial
		ZXN->ZXN_CHAVE	:=	cChaveXML
		ZXN->ZXN_SEQ  	:=	StrZero(nSeq,3)
		ZXN->ZXN_XML  	:=	SubStr(cSeqXML, 1, 65534) 
	MsUnlock()

	cSeqXML := SubStr(cSeqXML, 65535, Len(cSeqXML) ) 

EndDo

Return()
*********************************************************************
User Function VerificaZXN()
*********************************************************************
//�����������������������������������������������������Ŀ
//� GRAVA NA TABELA AUXILIAR, ZXN, O RESTANTE DO XML	|
//� Chamada -> CarregaACols()				 			|
//� 		-> ExpXML()									|
//� 		-> ConsWebNfe()								|
//�������������������������������������������������������
Local _cFilial	:=	ParamIxb[01]
Local cChave	:=	ParamIxb[02]
Local cRetorno 	:= ''

ZXN->(DbSetOrder(1), DbGoTop())
If ZXN->(DbSeek(_cFilial + cChave ,.F.))
	Do While ZXN->(!Eof()) .And. ZXN->ZXN_CHAVE == cChave
		cRetorno	+=	ZXN->ZXN_XML
	ZXN->(DbSkip())
	EndDo
EndIf

Return(cRetorno)
*********************************************************************
Static Function SA2_SA1ConsPad(cTabela)
*********************************************************************
//�����������������������������������������������������������������Ŀ
//� Monta tela consulta para usuario pesquisar codigo do fornecedor |
//| quando fornecedor == exteriror									|
//� Chamada -> Grava_Xml()     										|
//�������������������������������������������������������������������
Local nCBox 	
Local lOk		  := .F.
Private aArea_1   := GetArea()
Private cPesquisa := Space(100)
Private cRetorno  :=	''
Private cArqTmp

	
SetPrvt("oDlgSA2","oCBox","oGet1","oBrw1","oSBtn1","oSBtn2")

	
oDlgSA2	:= MSDialog():New( D(113),D(321),D(486),D(897),"Consulta "+IIF(cTabela=='SA2',"Fornecedor","Cliente"),,,.F.,,,,,,.T.,,,.T. )
oCBox	:= TComboBox():New( D(004),D(004),{|u| If(PCount()>0,nCBox:=u,nCBox)},{"C�DIGO+LOJA","NOME","NOME FANTASIA"},D(240),D(010),oDlgSA2,,{|| DbSelectArea("TMPSA2"),DbSetOrder(oCBox:nAT), DbGoTop(), oDlgSA2:Refresh(),  },,CLR_BLACK,CLR_WHITE,.T.,,"",,,,,,,nCBox )
oGet1	:= TGet():New( D(016),D(004),{|u| If(PCount()>0,cPesquisa:=u,cPesquisa)},oDlgSA2,D(240),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,{||  TMPSA2->(DbSeek(cPesquisa, .T.)), cRetorno := TMPSA2->CODIGO + TMPSA2->LOJA,oBrw1:oBrowse:Refresh() },.F.,.F.,"","",,)
oBtn1   := TButton():New( D(004),D(247),"&Pesquisar",oDlgSA2,{|| TMPSA2->(DbSetOrder(oCBox:nAT), DbSeek(cPesquisa, .T.)), cRetorno := TMPSA2->CODIGO + TMPSA2->LOJA, oBrw1:oBrowse:Refresh() },D(040),D(011),,,,.T.,,"",,,,.F. )

MsgRun("Consulta Fornecedor...",,{|| oTblSA2(cTabela) } )

DbSelectArea("TMPSA2") 
oBrw1	:= MsSelect():New( "TMPSA2","","",  {{"CODIGO","","C�digo",""},{"LOJA","","Loja",""},{"NOME","","Nome",""}, {"NFANT","","Nome Fantasia",""}},.F.,,{D(028),D(004),D(164),D(280)},,, oDlgSA2 ) 

oSBtn1	:= SButton():New( D(168),D(004),01,{|| cRetorno := TMPSA2->CODIGO + TMPSA2->LOJA , oDlgSA2:End() },oDlgSA2,,"", )
oSBtn3	:= SButton():New( D(168),D(040),15,{|| MostraRegSA2(cTabela) },oDlgSA2,,"", )

oDlgSA2:Activate(,,,.T., {|| .T.} )


IIF(Select("TMPSA2") != 0, TMPSA2->(DbCLoseArea()), )
FErase(cArqTmp)

RestArea(aArea_1)       

Return(cRetorno)
************************************************************
Static Function oTblSA2(cTabela)
************************************************************
Local aFds := {}
Local cTmp


If Select("TMPSA2") != 0
   DbSelectArea("TMPSA2")
   RecLock('TMPSA2', .F.)
       Zap
   MsUnLock()
   DbGoTop()

Else

	Aadd( aFds , {"CODIGO",	"C",006,000} )
	Aadd( aFds , {"LOJA",	"C",002,000} )
	Aadd( aFds , {"NOME",	"C",060,000} )
	Aadd( aFds , {"NFANT",	"C",035,000} )
	Aadd( aFds , {"MUN",	"C",025,000} )
	Aadd( aFds , {"EST",	"C",002,000} )
	Aadd( aFds , {"PAIS",	"C",003,000} )
	Aadd( aFds , {"CNPJ",	"C",014,000} )
	Aadd( aFds , {"_RECNO",	"N",010,000} )
	Aadd( aFds , {"TMPREC",	"N",010,000} )
	
	cArqTmp := CriaTrab( aFds, .T. )
	Use (cArqTmp) Alias TMPSA2 New Exclusive
	Index On CODIGO+LOJA	TAG 1 To &cArqTmp
	Index On NOME       	TAG 2 To &cArqTmp
	Index On NFANT 			TAG 3 To &cArqTmp
	Index On TMPREC			TAG 4 To &cArqTmp
EndIf


DbSelectArea("TMPSA2");DbSetOrder(1)

If cTabela == 'SA2'
	DbSelectArea('SA2');DbGoTop()        
	nCount := 0
	Do While !Eof()
		If SA2->A2_EST == 'EX'     
			nCount++
			RecLock("TMPSA2",.T.) 
		     	TMPSA2->CODIGO	:=	SA2->A2_COD
				TMPSA2->LOJA	:=	SA2->A2_LOJA	
				TMPSA2->NOME	:=	SA2->A2_NOME
				TMPSA2->NFANT	:=	IIF(SA2->(FieldPos("A2_NFANT"))>0, SA2->A2_NFANT, '' )
				TMPSA2->MUN		:=	SA2->A2_MUN
				TMPSA2->EST		:=	SA2->A2_EST
				TMPSA2->PAIS	:=	SA2->A2_PAIS
				TMPSA2->CNPJ	:=	SA2->A2_CGC
				TMPSA2->_RECNO	:=	SA2->(Recno())
				TMPSA2->TMPREC  :=	nCount
			MsUnLock()      	
		EndIf
		DbSelectArea('SA2') 
		DbSkip()
	EndDo
	
ElseIf cTabela == 'SA1'
	DbSelectArea('SA1');DbGoTop()
	Do While !Eof()
		If SA1->A1_EST == 'EX'
			RecLock("TMPSA2",.T.) 
		     	TMPSA2->CODIGO	:=	SA1->A1_COD
				TMPSA2->LOJA	:=	SA1->A1_LOJA	
				TMPSA2->NOME	:=	SA1->A1_NOME
				TMPSA2->NFANT	:=	SA1->A1_NFANT
				TMPSA2->MUN		:=	SA1->A1_MUN
				TMPSA2->EST		:=	SA1->A1_EST
				TMPSA2->PAIS	:=	SA1->A1_PAIS
				TMPSA2->CNPJ	:=	SA1->A1_CGC
				TMPSA2->_RECNO	:=	SA1->(Recno())
			MsUnLock()      	
		EndIf
		DbSelectArea('SA1') 
		DbSkip()
	EndDo

EndIf

DbSelectArea('TMPSA2');DbGoTop()

Return
************************************************************
Static Function MostraRegSA2()
************************************************************
//�����������������������������������������������������������������Ŀ
//� Bota de visualizar cadastro do Fornecedor \ Cliente             |
//| na consulta para usuario pesquisar codigo do fornecedor 		|
//� Chamada -> SA2_SA1ConsPad()     								|
//�������������������������������������������������������������������
cCadastro	:=	IIF(cTabela=='SA2','Fornecedor','Cliente') 
cAlias		:=	IIF(cTabela=='SA2','SA2','SA1')
nReg		:=	TMPSA2->_RECNO
nOpc		:=	2
aArea_2  	:= GetArea()


DbselectArea(cAlias);DbGoTo(nReg)
AxVisual( cAlias, nReg, nOpc,,,,,)

RestArea(aArea_2)
Return()
*****************************************************************
Static Function MensStatus()
*****************************************************************
//���������������������������������Ŀ
//� Mostra Retorno da Importacao    �
//� Chamada -> RecebeEmail()        �
//|                LoadFiles()      |
//�����������������������������������

Local 	cMsgXML  := cMsg_I := cMsg_D := cMsg_F := cMsg_C := cMsg_E := ''
Local	lRet		:=	.T.
Private	aStatusI 	:=	{}
Private	aStatusD 	:= 	{}
Private	aStatusF 	:= 	{}
Private	aStatusC 	:= 	{}
Private	aStatusE 	:= 	{}
Private	aStatusX 	:= 	{}
Private	aStatusT 	:= 	{}
Private	aStatusK 	:= 	{}
Private	aTmpStatus	:=	{}
Private	cMsgStatus	:=	''
Private	nQtdStatus	:=	0

SetPrvt("oDlgS","oGrp","oSayOK","oSayNO",'oSayQtd','oBrw1','oSayXml','oBtn2','oSayImp')


//���������������������������������������Ŀ
//�	SEPARA O ARRAY aStatus CONFORME TIPO  �
//�����������������������������������������
aSort(aStatus,,, { |x,y| x[1] > y[1] })
For nX := 1 To Len(aStatus)

   If aStatus[nX][1] ==     'I'
       Aadd(aStatusI,    {aStatus[nX][2],aStatus[nX][3]+' \ '+aStatus[nX][4], aStatus[nX][1], aStatus[nX][4], aStatus[nX][6], aStatus[nX][7] })  
   ElseIf aStatus[nX][1]     ==    'D'
       Aadd(aStatusD,    {aStatus[nX][2],aStatus[nX][3], aStatus[nX][1], aStatus[nX][4] })
   ElseIf aStatus[nX][1]     ==    'F'
       Aadd(aStatusF,    {aStatus[nX][2],aStatus[nX][3], aStatus[nX][1], aStatus[nX][4] })
   ElseIf aStatus[nX][1]     ==    'C'
       Aadd(aStatusC,    {aStatus[nX][2],aStatus[nX][3], aStatus[nX][1], aStatus[nX][4] })
   ElseIf aStatus[nX][1]     ==    'E'
       Aadd(aStatusE,    {aStatus[nX][2],aStatus[nX][3], aStatus[nX][1], aStatus[nX][4] })
   ElseIf aStatus[nX][1]     ==    'X'
       Aadd(aStatusX,    {aStatus[nX][2], aStatus[nX][4]+' \ '+ aStatus[nX][5], aStatus[nX][1], aStatus[nX][4] })
   ElseIf aStatus[nX][1]     ==    'T'
       Aadd(aStatusT,    {aStatus[nX][2],aStatus[nX][3], aStatus[nX][1], aStatus[nX][4] })
   EndIf

Next


//���������������������������������������Ŀ
//�	  BROSWER COM STATUS DA IMPORTACAO    �
//�����������������������������������������
oFontG     := TFont():New( "Arial",0,IIF(nHRes<=800,-16, -18),,.T.,0,,700,.F.,.F.,,,,,, )
oFontP     := TFont():New( "Arial",0,IIF(nHRes<=800,-09, -11),,.F.,0,,700,.F.,.F.,,,,,, )

oDlgS      := MSDialog():New( D(095),D(232),D(427),D(740),"STATUS ARQUIVOS IMPORTADOS...",,,.F.,,,,,,.T.,,,.T. )
oGrp       := TGroup():New( D(004),D(004),D(146),D(250),"",oDlgS,CLR_BLACK,CLR_WHITE,.T.,.F. )

oSayOK     := TSay():New( D(010),D(000),{|| cMsgStatus },oGrp,,oFontG,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(240),D(010))
oSayNO     := TSay():New( D(010),D(000),{|| cMsgStatus },oGrp,,oFontG,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,D(240), D(010))
oSayOK:Hide()
oSayNO:Hide()

oSayQtd    := TSay():New( D(022),D(010),{|| Alltrim(Str(nQtdStatus))+ ' arquivo(s) ' },oGrp,,oFontP,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(010) )

oSayImp    := TSay():New( D(022),D(055),{|| ' Arquivo(s) n�o importado ' },oGrp,,oFontP,.F.,.F.,.F.,.T.,CLR_HRED,CLR_WHITE,D(080),D(010) )
oSayImp:Hide()

*************************
   oTblStatus()
*************************

DbSelectArea("TMPSTATUS")
oBrw1 := MsSelect():New( "TMPSTATUS","","",{{"ARQUIVO","","Arquivo",""},{"CONTEUDO","","Observa��o",""}},.F.,,{ D(030),D(008),D(140),D(245) },,, oGrp )

*************************
   MostraStatus()
*************************
oSayOK:Refresh()
oSayNO:Refresh()

If cTpCarga != 'EMAIL'
   	oSayXml	:=	TSay():New( D(146),D(008),{|| ' Duplo Click para visualizar XML' },oGrp,,oFontP,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(080),D(010) )
	oBrw1:oBrowse:bLDblClick := {|| IIF(cTpCarga!='EMAIL', MsgRun("Aguarde... Abrindo XML...",,{|| ShellExecute("open",IIF(!(TMPSTATUS->_STATUS$'I/G'),cDiretorioXML+AllTrim(TMPSTATUS->ARQUIVO),AllTrim(TMPSTATUS->CAMINHO)),"","",1)}), )}
EndIf

oBtn2    := TButton():New( D(152),D(097),"&Ok",oDlgS, {|| MostraStatus() },D(037),D(012),,,,.T.,,"",,,,.F. )


oDlgS:Activate(,,,.T.,,,)


IIF(Select("TMPSTATUS") != 0, TMPSTATUS->(DbCLoseArea()), )
aStatus    :=    {}

Return()
*****************************************************************
Static Function oTblStatus()
*****************************************************************
//���������������������������������Ŀ
//� Cria Arq. Temp                  �
//� Chamada -> MensStatus()         �
//�����������������������������������
Local aFds := {}
Local cTmp

If Select("TMPSTATUS") == 0

   Aadd( aFds , {"_STATUS" ,"C",001,000} )
   Aadd( aFds , {"ARQUIVO" ,"C",080,000} )
   Aadd( aFds , {"CONTEUDO","C",200,000} )
   Aadd( aFds , {"CAMINHO" ,"C",300,000} )
   
   cTmp := CriaTrab( aFds, .T. )
   Use (cTmp) Alias TMPSTATUS New Exclusive
   Aadd(aArqTemp, cTmp)

Else
   DbSelectArea("TMPSTATUS")
   RecLock('TMPSTATUS', .F.)
       Zap
   MsUnLock()

   DbGoTop()
EndIf

Return()
*****************************************************************
Static Function MostraStatus()
*****************************************************************
//�������������������������������������������������Ŀ
//� ALIMENTA TABELA TEMPORARIA E ATUALIZA VARIAVEIS	|
//| SEPARA OS TIPOS E ALIMENTA aTmpStatus			|
//� Chamada -> MensStatus()         				�
//���������������������������������������������������

aTmpStatus := {}

If Len(aStatusI) > 0
                     
   // 				Aadd(aStatus, {cStatus, cArqXML, cNomeEmpFil, cNomeFilial, cDiretorioXML, cDestCnpj, cNomeFilial })
	aCopia	:=	{}
	aSort(aStatusI,,, { |x,y| x[5] > y[5] })
	cCnpjFil := aStatusI[1][5]
	cNomeFil := aStatusI[1][6]
	
	If SM0->M0_CGC == cCnpjFil
    	cMsgStatus  :=	PadC('ARQUIVO IMPORTADO COM SUCESSO',080,'')
    Else
    	cMsgStatus	:=	PadC('XML IMPORTADO COM SUCESSO - FILIAIL '+cNomeFil,080,'')
   	EndIf
   	
	For _nX:=1 To Len(aStatusI)
		If cCnpjFil == aStatusI[_nX][5] .Or. Len(aStatusI) == 1
			Aadd(aTmpStatus, {aStatusI[_nX][1], aStatusI[_nX][2], aStatusI[_nX][3], aStatusI[_nX][4]} )
   		Else
   			Aadd(aCopia, aStatusI[_nX])
        EndIf
    Next
         
	aStatusI := aCopia
   		
   oSayOK:Show()
   oSayNO:Hide()
   oSayImp:Hide()


			ElseIf Len(aStatusD) > 0
   cMsgStatus   :=	PadC('ARQUIVO J� IMPORTADO',080,'')
   aTmpStatus   := 	aClone(aStatusD)
   aStatusD    	:= 	{}
   oSayOK:Hide()
   oSayNO:Show()
   oSayImp:Show()
ElseIf Len(aStatusF) > 0
   cMsgStatus	:=	PadC('CNPJ DO EMITENTE N�O ENCONTRADO',080,'')
   aTmpStatus   := 	aClone(aStatusF)
   aStatusF    	:=	{}
   oSayOK:Hide()
   oSayNO:Show()
   oSayImp:Show()
ElseIf Len(aStatusC) > 0
   cMsgStatus	:=	PadC('CNPJ DO DESTINAT�RIO N�O ENCONTRADO',080,'')
   aTmpStatus	:=	aClone(aStatusC)
   aStatusC    	:=	{}
   oSayOK:Hide()
   oSayNO:Show()
   oSayImp:Show()
ElseIf Len(aStatusE) > 0  
   cMsgStatus	:=	PadC('XML COM FORMATA��O INCORRETA',080,'')
   aTmpStatus	:=	aClone(aStatusE)
   aStatusE    	:=	{}
   oSayOK:Hide()
   oSayNO:Show()
   oSayImp:Show()
ElseIf Len(aStatusX) > 0 
   cMsgStatus	:=	PadC('PROBLEMA COM ESTRUTURA DA TABELA',080,'')
   aTmpStatus	:=	aClone(aStatusX)
   aStatusX    	:=	{}
   oSayOK:Hide()
   oSayNO:Show()
   oSayImp:Show()
ElseIf Len(aStatusT) > 0 
   cMsgStatus	:=	PadC('XML SEM TAG PROTOCOLO DE AUTORIZA��O',080,'')
   aTmpStatus	:=	aClone(aStatusT)
   aStatusT    	:=	{}
   oSayOK:Hide()
   oSayNO:Show()
   oSayImp:Show() 
EndIf


lContinua := IIF( Len(aTmpStatus) > 0, .T., .F.)


If lContinua

   ********************
   oTblStatus()
   ********************

   For nX:=1 To Len(aTmpStatus)
       DbSelectArea("TMPSTATUS")
       RecLock('TMPSTATUS', .T.)
           TMPSTATUS->ARQUIVO     :=    aTmpStatus[nX][1]
           TMPSTATUS->CONTEUDO    :=    aTmpStatus[nX][2]
           TMPSTATUS->_STATUS     :=    aTmpStatus[nX][3]
           TMPSTATUS->CAMINHO     :=    aTmpStatus[nX][4]
       MsUnLock()
   Next

   TMPSTATUS->(DbGoTop())
Else
   oDlgS:End()
EndIf

nQtdStatus := Len(aTmpStatus)

Return()
*****************************************************************
Static Function Renomea_XML(nPos)
*****************************************************************
//���������������������������������Ŀ
//� Renomea Arq.XML -Importado      �
//� Chamada -> MensStatus()         �
//�����������������������������������


Local lCopia := .T.

//���������������������������������Ŀ
//� VERIFICO SE DIRETORIO EXISTE    �
//�����������������������������������
If !File(cDiretorioXML+'XML_IMPORTADO') 
	
	//��������������������������������������������������������������������������������Ŀ
	//� NAO EXISTINDO VERIFICO SE NO CAMINHO INFORMADO CONTEM A PALAVRA \XML_IMPORTADO �
	//� PARA QUE NAO SEJA NECESARIO CRIAR UMA PASTA \XML_IMPORTADO DENTRO DA MESMA.    �
	//����������������������������������������������������������������������������������
	nPosXML := RAT('\XML_IMPORTADO', Upper(cDiretorioXML) )
	If nPosXML == 0
		If MakeDir(cDiretorioXML+'XML_IMPORTADO') != 0
			MsgAlert('� necessario criar a pasta '+cDiretorioXML+'XML_IMPORTADO'+' para separar os arquivos j� importados.') 
		EndIf
	Else
		lCopia := .F.
	EndIf
EndIf


nTam       	:=	Len(aStatus[nPos][2])-4
cArquivo 	:=	SubStr(aStatus[nPos][2],1, nTam )
cExtensao	:= 	Right(AllTrim(aStatus[nPos][2]), 4)  


//����������������������Ŀ
//�   RENOMEA ARQUIVO    �
//������������������������
FRename( cDiretorioXML+aStatus[nPos][2], cDiretorioXML+cArquivo+'_IMPORTADO'+cExtensao)


If lCopia

	//�������������������������������������Ŀ
	//� COPIA PARA DIRETORIO \_IMPORTARDO	|
	//���������������������������������������
	_CopyFile(cDiretorioXML+cArquivo+'_IMPORTADO'+cExtensao, cDiretorioXML+'XML_IMPORTADO\'+cArquivo+'_IMPORTADO'+cExtensao )
	//�������������������������������������Ŀ
	//| APAGA ARQUIVO DA PASTA ORIGINAL		|
	//���������������������������������������
	FErase(cDiretorioXML+cArquivo+'_IMPORTADO'+cExtensao)

EndIf



//�����������������������������������������������������������������������������������������������������Ŀ
//� ALTERANDO O CAMINHO ANTIGO, COLOCANDO O NOVO CAMINHO, PARA QUE SEJA POSSIVEL ABRIR O XML COM 2 CLICK�
//�������������������������������������������������������������������������������������������������������
aStatus[nPos][4] := cDiretorioXML+'XML_IMPORTADO\'+cArquivo+'_IMPORTADO'+cExtensao

Return()

User Function ConsWebSG(cChaveNfe)

Local cURL     := PadR(GetNewPar("MV_SPEDURL","http://"),250)
Local cMensagem:= ""
Local oWS
Local lErro := .F.
Local aRetCon := {}


oWs:= WsNFeSBra():New()
oWs:cUserToken   := "TOTVS"
	cIdEnt:=U_WSAT01GetIdEnt()      
	//cChaveNFe:= cChaveNfe
	oWs:cID_ENT    := cIdEnt
	ows:cCHVNFE		 := cChaveNFe
	
oWs:_URL         := AllTrim(cURL)+"/NFeSBRA.apw"
If oWs:ConsultaChaveNFE()
  /*	cMensagem := ""
	If !Empty(oWs:oWSCONSULTACHAVENFE:cVERSAO)
		cMensagem += "Vers�o da Mensagem"+": "+oWs:oWSCONSULTACHAVENFE:cVERSAO+CRLF
	EndIf
	cMensagem += "Ambiente"+": "+IIf(oWs:oWSCONSULTACHAVENFE:nAMBIENTE==1,"Produ��o","Homologa��o")+CRLF //"Produ��o"###"Homologa��o"
	cMensagem += "Cod.Ret.NFe"+": "+oWs:oWSCONSULTACHAVENFE:cCODRETNFE+CRLF
	cMensagem += "Msg.Ret.NFe"+": "+oWs:oWSCONSULTACHAVENFE:cMSGRETNFE+CRLF
	If !Empty(oWs:oWSCONSULTACHAVENFE:cPROTOCOLO)
		cMensagem += "Protocolo"+": "+oWs:oWSCONSULTACHAVENFE:cPROTOCOLO+CRLF
	EndIf
	//QUANDO NAO ESTIVER OK NAO IMPORTA, CODIGO DIFERENTE DE 100           */
	If oWs:oWSCONSULTACHAVENFERESULT:cCODRETNFE # "100"         //funcao que consulta a nfe para verificar se esta ativa ou nao.
		lErro := .T.
	EndIf    
	if type('oWs:oWSCONSULTACHAVENFERESULT:dRECBTO ') == 'C'
		msgalert(oWs:oWSCONSULTACHAVENFERESULT:dRECBTO )
		dDatXML := oWs:oWSCONSULTACHAVENFERESULT:dRECBTO 
	else 
		dDatXML := stod('')
	endif 

	if type ('oWs:oWSCONSULTACHAVENFERESULT:cCODRETNFE') == 'C'
		CodRetNfe := oWs:oWSCONSULTACHAVENFERESULT:cCODRETNFE 
	else 
		cCodRetNfe:=' '
	endif 




	
	aadd( aRetCon, cCodRetNfe )	
	aadd( aRetCon, DTOC(dDatXML)) 
	
EndIf	
Return(aRetCon)


*****************************************************************
User Function ConsNFeSefaz()    //    [5.0]
*****************************************************************
//���������������������������������Ŀ
//� Consulta Status XML - SEFAZ     �
//� Chamada -> Rotina Status XML    �
//�����������������������������������
Local aArea5	:= GetArea()
Local aRetorno	:= {}
Local cRet		:= ''

DbSelectArea('ZXM')

MsgRun("Aguarde... Consultando XML na SEFAZ...",,{|| aRetorno := ExecBlock("ConsWebSG",.F.,.F.,{ZXM->ZXM_CHAVE, cEmpAnt, .T./*DtoS(ZXM->ZXM_DTXML)*/ }) } )

//���������������������������������Ŀ
//�	GRAVA RETORNO DO STATUS DO XML  �
//�����������������������������������
Reclock('ZXM',.F.)
	ZXM->ZXM_SITUAC	:=	IIF(Empty(aRetorno[1]),'',IIF(aRetorno[1]=='100','A',IIF(aRetorno[1]=='101','C','E')))
   	ZXM->ZXM_CODRET	:=	aRetorno[1]
   	ZXM->ZXM_DTXML	:=	aRetorno[2]
	cRet			:=	aRetorno[1]
MsUnlock()


	//�������������������������������������������������������������Ŀ
	//�	  FUNCAO MOSTRA MENSAGEM DO CODIGO DE RETORNO DA SEFAZ      �
	//���������������������������������������������������������������
	********************************************************
		IIF(!Empty(ZXM->ZXM_CODRET), ExecBlock("ErroNFe",.F.,.F.,{''}), MsgInfo('C�digo de Retorno do XML esta em branco.') )
	********************************************************


RestArea(aArea5)
Return(cRet)
*****************************************************************
User Function SiteSefaz()    //    [6.0]
*****************************************************************
//���������������������������������������Ŀ
//� Abre Portal da Nota Fiscal Eletronica �
//� Chamada -> Rotina Site Sefaz          �
//�����������������������������������������

DbSelectArea('ZXM')
ShellExecute("open",AllTrim(GetMv('XML_SEFAZ'))+AllTrim(ZXM->ZXM_CHAVE),"","",1)

Return()
*********************************************************************
User Function ErroNFe()    //    [7.0]
*********************************************************************
//�����������������������������������������������������Ŀ
//� Retorno mensagem do status do XML     				�
//|	\SYSTEM\ZXM_ERROS_NFE.TXT contem os erros de retorno|
//� Chamada -> Rotina Msg.Erro            				�
//�������������������������������������������������������
Local cErroXML	:= AllTrim(ZXM->ZXM_CODRET)
Local cLinha    :=	''
Local cLnErro  	:=	''
Local cMsgErro	:=	''

If Empty(cErroXML)
	Alert('Cod.Ret. XML esta em branco.  Verifique o Status do XML.'+ENTER+'Op��o: Consulta Sefaz-> Status XML') 
ElseIf File(_StartPath+cArqErro)

   FT_FUSE(_StartPath+cArqErro)
   FT_FGOTOP()
   Do While !FT_FEOF()
       cLinha	:= 	FT_FREADLN()
       cLnErro	:=	SubStr(cLinha,1 ,AT(';', cLinha)-1)
       cMsgErro	:= 	SubStr(cLinha, AT(';',cLinha)+1, Len(AllTrim(cLinha)) )
       If cLnErro == cErroXML
           Exit
       EndIf
       FT_FSKIP()
   EndDo
   FT_FUSE()

   MsgInfo(cMsgErro)

Else
   Alert('Arquivo de Erros n�o encontrado no caminho: '+_StartPath+cArqErro )
EndIf


Return()
*****************************************************************
User Function Exporta_XML()    //    [8.0]
*****************************************************************
//���������������������������������������Ŀ
//� Exporta XML para Arquivo              �
//� Chamada -> Rotina Exportar            �
//�����������������������������������������

Private cExpDiretorio:= Space(200)
Private cFor_Ini    :=	Space(TamSX3('A2_COD' )[1])
Private cFor_fin    :=	Space(TamSX3('A2_COD' )[1])
Private cForLj_in   :=	Space(TamSX3('A2_LOJA')[1])
Private cForLj_fin	:=	Space(TamSX3('A2_LOJA')[1])
Private cNF_Ini    	:=	Space(TamSX3('F2_DOC' )[1])
Private cNF_fin    	:=	Space(TamSX3('F2_DOC' )[1])
Private cSerie_Ini	:=	Space(TamSX3('F2_SERIE')[1])
Private cSerie_fin	:=	Space(TamSX3('F2_SERIE')[1])
Private dData1		:=	CtoD(" ")
Private dData2 		:=	CtoD(" ")

SetPrvt("oFont1","oDlg2","oGrp1","oSay1","oSay2","oSay3","oSay4","oSay5","oSay6","oSay7","oGetNF_Ini")
SetPrvt("oGetDir","oBtn1","oGetDt1","oGetDt2","oGetFor_ini","oGet2","oBtnOk","oBtnCanc","oForLj_in","oForLj_fin")


//���������������������������������������Ŀ
//�       BROSWER COM PARAMETROS          �
//�����������������������������������������
oFont1     := TFont():New( "Arial",0,IIF(nHRes<=800,-09, -11),,.F.,0,,400,.F.,.F.,,,,,, )
oDlg2      := MSDialog():New( D(151),D(347),D(496),D(791),"Exporta XML",,,.F.,,,,,,.T.,,oFont1,.T. )
oGrp1      := TGroup():New( D(000),D(001),D(144),D(220),"",oDlg2,CLR_BLACK,CLR_WHITE,.T.,.F. )

oSay1      := TSay():New( D(028),D(005),{||"Nota Fiscal Inicial"},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(052),D(008) )
oGetNF_Ini := TGet():New( D(024),D(065),{|u| If(PCount()>0,cNF_Ini:=u,cNF_I�ni)},oGrp1,D(060),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cNF_Ini",,)
oSay10     := TSay():New( D(024),D(130),{||"S�rie"},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(052),D(008) )
oGetSer_Ini:= TGet():New( D(024),D(144),{|u| If(PCount()>0,cSerie_Ini:=u,cSerie_Ini)},oGrp1,D(020),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cSerie_Ini",,)

oSay2      := TSay():New( D(044),D(005),{||"Nota Fiscal Final"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(048),D(008) )
oGetNF_fin := TGet():New( D(040),D(065),{|u| If(PCount()>0,cNF_fin:=u,cNF_fin)},oGrp1,D(060),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cNF_fin",,)
oSay11     := TSay():New( D(044),D(130),{||"S�rie"},oGrp1,,oFont1,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(052),D(008) )
oGetSer_fin:= TGet():New( D(040),D(144),{|u| If(PCount()>0,cSerie_fin:=u,cSerie_fin)},oGrp1,D(020),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cSerie_fin",,)

oSay3      := TSay():New( D(060),D(005),{||"Diret�rio de Destino"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_HBLUE,CLR_WHITE,D(052),D(008) )
oBtn1      := TButton():New( D(055),D(169),"Procurar",oGrp1,{|| cExpDiretorio  := cGetFile("Anexos (*)|*|","Arquivos (*)",0,'',.T.,GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_RETDIRECTORY) },D(030),D(012),,,,.T.,,"",,,,.F. )
oGetDir    := TGet():New( D(056),D(065),{|u| If(PCount()>0,cExpDiretorio:=u,cExpDiretorio)},oGrp1,D(100),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cGetDir",,)

oSay4      := TSay():New( D(076),D(005),{||"Data Imp. Inicio"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(032),D(008) )
oGetDt1    := TGet():New( D(072),D(065),{|u| If(PCount()>0,dData1:=u,dData1)},oGrp1,D(060),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","dGetData",,)
oSay5      := TSay():New( D(092),D(005),{||"Data Imp. Final"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(032),D(008) )
oGetDt2    := TGet():New( D(088),D(065),{|u| If(PCount()>0,dData2:=u,dData2)},oGrp1,D(060),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","dData2",,)

oSay6      := TSay():New( D(108),D(005),{||"Fornecedor Inicial"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )
oGetFor_in := TGet():New( D(108),D(065),{|u| If(PCount()>0,cFor_Ini:=u,cFor_Ini)},oGrp1,D(060),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA2","cFor_Ini",,)
oSay8      := TSay():New( D(108),D(130),{||"Loja"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008) )
oForLj_in  := TGet():New( D(108),D(144),{|u| If(PCount()>0,cForLj_in:=u,cForLj_in)},oGrp1,D(020),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA2","cForLj_Ini",,)

oSay7      := TSay():New( D(124),D(005),{||"Fornecedor Final"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(048),D(008) )
oGetFor_fi := TGet():New( D(121),D(065),{|u| If(PCount()>0,cFor_fin:=u,cFor_fin)},oGrp1,D(060),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA2","cForLj_fin",,)
oSay9      := TSay():New( D(124),D(130),{||"Loja"},oGrp1,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(052),D(008))
oForLj_fin := TGet():New( D(121),D(144),{|u| If(PCount()>0,cForLj_fin:=u,cForLj_fin)},oGrp1,D(020),D(008),'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA2","cForLj_fin",,)


oBtnOk     := TButton():New( D(152),D(160),"Ok",oDlg2,{|| Processa( {|| ExpXML(),'Exportando arquivos...','Processando...',.F. }),oDlg2:End() },D(024),D(012),,oFont1,,.T.,,"",,,,.F. )
oBtnCanc   := TButton():New( D(152),D(188),"Cancela",oDlg2,{|| oDlg2:End() },D(028),D(012),,oFont1,,.T.,,"",,,,.F. )

oDlg2:Activate(,,,.T.)

Return()
*****************************************************************
Static Function ExpXML()
*****************************************************************
//���������������������������������������Ŀ
//� Exporta XML para Arquivo              �
//| Seleciona os XML para exportar		  |
//� Chamada -> Rotina Exportar            �
//�����������������������������������������
Local _nCount :=    0


If Empty(cExpDiretorio )
   Alert('Informe o diret�rio de Destino !!!')
Else
   DbSelectArea('ZXM');DbGoTop()
   Do While !Eof()
           If ZXM->ZXM_DOC >= cNF_Ini      .And. ZXM->ZXM_DOC <= cNF_Fin     .And.;
              ZXM->ZXM_SERIE >= cSerie_Ini    .And. ZXM->ZXM_SERIE <= cSerie_fin


           If !Empty(cFor_Ini)    .Or. !Empty(cFor_Fin)
               If !(ZXM->ZXM_FORNEC >= cFor_Ini  .And. ZXM->ZXM_FORNEC <= cFor_Fin)
                   DbSkip();Loop
               EndIf
           EndIf

           If !Empty(cForLj_In)    .Or. !Empty(cForLj_Fin)
               If !(ZXM->ZXM_LOJA >= cForLj_in     .And. ZXM->ZXM_LOJA <= cForLj_in)
                   DbSkip();Loop
               EndIf
           EndIf

           If !Empty(dData1)    .Or. !Empty(dData2)
               If !( ZXM->ZXM_DTIMP >= dData1    .And. ZXM->ZXM_DTIMP <= dData2 )
                   DbSkip();Loop
               EndIf
           EndIf

           cNomeXML :=    AllTrim(ZXM->ZXM_FORNEC)+'_'+AllTrim(ZXM->ZXM_LOJA)+'_'+AllTrim(ZXM->ZXM_DOC)+'_'+AllTrim(ZXM->ZXM_SERIE)

           cXML	:=	AllTrim(ZXM->ZXM_XML)
           cXML	+=	ExecBlock("VerificaZXN",.F.,.F., {ZXM->ZXM_FILIAL, ZXM->ZXM_CHAVE})	//	VERIFICA SE O XML ESTA NA TABELA ZXN

           MemoWrit( AllTrim(cExpDiretorio)+AllTrim(cNomeXML)+'.XML', cXML )

           _nCount++

       EndIf
       DbSkip()
   EndDo


EndIf

IIF(_nCount==0,Alert('Nenhum XML foi exportado !!!'), MsgInfo('XML exportado com sucesso!!!'))

DbSelectArea('ZXM');DbGoTop()

Return()
*****************************************************************
User Function Recusar_XML()    //    [9.0]
*****************************************************************
//���������������������������������������������Ŀ
//� Recusa XML \ Envia E-mail para Fornecedor   |
//� Chamada ->  Rotina Recusar                  �
//�����������������������������������������������
Local cFornec	:=	Alltrim( Posicione('SA2',3,xFilial('SA2')+ ZXM->ZXM_CGC, 'A2_NOME') )
Local cChaveNFe	:=	AllTrim(ZXM->ZXM_CHAVE)

If ZXM->ZXM_STATUS=="R" 
	Alert('XML j� recusado !!!') 
EndIF

If MsgBox("Deseja recusar o recebimento da NOTA FISCAL: "+AllTrim(ZXM->ZXM_DOC)+"   S�rie: "+AllTrim(ZXM->ZXM_SERIE)+ENTER+;
            "Fornecedor: "+ZXM->ZXM_FORNEC+"-"+ZXM->ZXM_LOJA+" - "+AllTrim(cFornec)+"  ?","Aten��o...","YESNO")



	//���������������������������������������Ŀ
	//� VERIFICA SE DOC.ENTRADA FOI INCLUSO   �
	//�����������������������������������������
   	DbSelectArea('SF1');DbSetOrder(1);DbGoTop()
   	If !DbSeek(xFilial('SF1')+ IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC+ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF+ZXM->ZXM_SERREF) + ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA + 'N' ,.F.)


       If MsgBox("Deseja enviar um email para o fornecedor para ficar documentado esta recusa?","Aten��o...","YESNO")
			//���������������������������������������������������������Ŀ
			//� BROWSER PARA USER INFORMAR O MOTIVO DA RECUSA DO XML	|
			//�����������������������������������������������������������
           *************************
               MsgRecusarXML()
           *************************

       Endif


       DbSelectArea('ZXM');DbSetOrder(6);DbGoTop()
       If DbSeek( cChaveNFe, .F.)
           Reclock('ZXM',.F.)
              	ZXM->ZXM_STATUS := 'R'    // RECUSADO RECEBIMENTO
           	  MsUnlock()
       EndIf

       	MsgInfo('NOTA FISCAL: '+AllTrim(ZXM->ZXM_DOC)+' S�RIE: '+AllTrim(ZXM->ZXM_SERIE)+' RECUSADA COM SUCESSO!!!')

   	Else
		MsgAlert('XML n�o pode ser recusado!!!'+ENTER+'Primeiro exclua o Doc.Entrada vinculado a esse XML.'+ENTER+'Documento:'+SF1->F1_DOC+' - '+SF1->F1_SERIE)
   	EndIf
   	
Endif

Return()
*****************************************************************
Static Function MsgRecusarXML()
*****************************************************************
//���������������������������������������������Ŀ
//� Envia E-mail para Fornecedor                |
//� Chamada ->  Recusar_XML                     �
//�����������������������������������������������

SetPrvt("oFont1","oDlg1","oGrp","oSay1","oSay2","oGet1","oMGet1","oBtn2")

cSubject   :=    'Nota Fiscal: '+ZXM->ZXM_DOC+' Serie: '+ZXM->ZXM_SERIE+' recebimento recusado'
cHtml      :=    ''
cMsgUser   :=    ''
cForEmail  :=    AllTrim(Posicione('SA2',3,xFilial('SA2')+ZXM->ZXM_CGC,'A2_EMAIL'))+Space(200)

oFont1     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-09, -11),,.T.,0,,700,.F.,.F.,,,,,, )
oDlgRec    := MSDialog():New( D(095),D(232),D(462),D(829),"Carregar arquivo XML",,,.F.,,,,,,.T.,,oFont1,.T. )
oGrp       := TGroup():New( D(004),D(004),D(156),D(284),"",oDlgRec,CLR_BLACK,CLR_WHITE,.T.,.F. )

oSay1      := TSay():New( D(012),D(008),{||"Email Forncedor (Para adicionar mais e-mails separar por;) "},oGrp,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(208),D(008))
oGet1      := TGet():New( D(020),D(008),{|u| If(PCount()>0,cForEmail:=u,cForEmail)},oGrp,D(272),D(008),'',,CLR_BLACK,CLR_WHITE,oFont1,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","",,)

oSay2      := TSay():New( D(036),D(008),{||"Adicionar informa��es:"},oGrp,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(080),D(008))
oMGet1     := TMultiGet():New( D(044),D(008),{|u| If(PCount()>0,cMsgUser:=u,cMsgUser)},oGrp,D(272),D(108),oFont1,,CLR_BLACK,CLR_WHITE,,.T.,"",,,.F.,.F.,.F.,,,.F.,,  )
oBtn2      := TButton():New( D(160),D(120),"Ok",oDlgRec,{|| oDlgRec:End() },D(037),D(012),,,,.T.,,"",,,,.F. )

oDlgRec:Activate(,,,.T.)


//���������������������������������������������Ŀ
//� 			CORPO DO E-MAIL                 |
//�����������������������������������������������

cHtml += '<html>'
cHtml += '<head>'
cHtml += '<h3 align = Left><font size="3" color="#FF0000" face="Arial">Recusado Recebimento Nota Fiscal: '+AllTrim(ZXM->ZXM_DOC)+' S�rie: '+AllTrim(ZXM->ZXM_SERIE)+'</h3></font>'
cHtml += '</head>'

cHtml += '<body bgcolor = white text=black>'
cHtml += '<hr width=100% noshade>'
cHtml += '<br></br>'

cNomeFil    :=	AllTrim( Posicione('SM0', 1, cEmpAnt + cFilAnt,  'M0_FILIAL' ))
cCcgFil		:=	AllTrim( Posicione('SM0', 1, cEmpAnt + cFilAnt,  'M0_CGC'    ))
cEmpNome    :=	AllTrim( Posicione('SM0', 1, cEmpAnt + cFilAnt,  'M0_NOMECOM'))

cHtml += '<b><font size="3" face="Arial"> '+cEmpNome+'  -  '+ TransForm(cCcgFil,"@R 99.999.999/9999-99")+'</font></b>'
cHtml += '<br></br>'

cHtml += '<b><font size="3" face="Arial">Recusado  em: </font></b>'+'<b><font size="3" face="Arial"> '+Dtoc(Date())+' �s '+Time()+'</font></b>'
cHtml += '<br></br>'

cMsgPadrao :=    'Recusado Recebimento Nota Fiscal: '+AllTrim(ZXM->ZXM_DOC)+' S�rie: '+AllTrim(ZXM->ZXM_SERIE)
cHtml += '<br></br>'
cHtml += '<b><font size="3" face="Arial"><I>'+cMsgPadrao+'</font></b></I>'
cHtml += '<br></br>'
cHtml += '<b><font size="3" face="Arial"><I>'+cMsgUser+'</font></b></I>'
cHtml += '<br></br>'
cHtml += '<br></br>'

cHtml += '<b><font size="2" color="#9C9C9C" face="Arial"> E-mail enviado automaticamente, n�o responda este e-mail </font></b>'
cHtml += '</html>'


   ***********************************************************************************************
       lRetotno     := U_ENVIA_EMAIL('ImpXMLNFe', cSubject, AllTrim(cForEmail), '', '', cHtml, '', '')
   ***********************************************************************************************

If !lRetotno
   Alert('Problema no envio do email.'+ENTER+'Email n�o enviado!!!')
EndIf

Return()
*****************************************************************
User Function Cancelar_XML()    //    [10.0]
*****************************************************************
//���������������������������������������Ŀ
//� Cancela XML                           �
//� Chamada -> Rotina Cancelar            �
//�����������������������������������������
Local    cMsgCanc    :=    ''
Local    ZXMnRecno    :=    ZXM->(Recno())

DbSelectArea('ZXM')

If ZXM->ZXM_STATUS == 'X' 
	Alert('XML j� cancelado !!!') 
EndIF

If MsgYesNo('Deseja realmente CANCELAR este XML???'+ENTER+ENTER+'FORNECEDOR: '+ZXM->ZXM_FORNEC+'-'+ZXM->ZXM_LOJA +ENTER+' NOTA: '+Alltrim(ZXM->ZXM_DOC)+'  S�rie: '+AllTrim(ZXM->ZXM_SERIE) )

	//���������������������������������������Ŀ
	//� VERIFICA SE DOC.ENTRADA FOI INCLUSO   �
	//�����������������������������������������
	DbSelectArea('SF1');DbSetOrder(1);DbGoTop()
	If !DbSeek(xFilial('SF1')+ IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC+ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF+ZXM->ZXM_SERREF) + ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA + 'N' ,.F.)

		SetPrvt("oFont1","oDlgCanc","oGrp","oSay1","oSay2","oGet1","oMGet1","oBtn2")

		//���������������������������������������Ŀ
		//�       Motivo Cancelamento XML         �
		//�����������������������������������������
		oFont1     := TFont():New( "MS Sans Serif",0,IIF(nHRes<=800,-09, -11),,.T.,0,,700,.F.,.F.,,,,,, )
		oDlgCanc   := MSDialog():New( D(095),D(232),D(462),D(829),"Motivo Cancelamento XML",,,.F.,,,,,,.T.,,oFont1,.T. )
		oGrp       := TGroup():New( D(004),D(004),D(156),D(284),"",oDlgCanc,CLR_BLACK,CLR_WHITE,.T.,.F. )

		oSay2      := TSay():New( D(012),D(008),{||"Informe o Motivo Cancelamento XML:" },oGrp,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,D(150),D(008) )
		oMGet1     := TMultiGet():New(D(025),D(008),{|u| If(PCount()>0,cMsgCanc:=u,cMsgCanc)},oGrp,D(272),D(118),oFont1,,CLR_BLACK,CLR_WHITE,,.T.,"",,,.F.,.F.,.F.,,,.F.,,  )
		oBtn2      := TButton():New( D(160),D(120),"Ok",oDlgCanc,{|| IIF(Len(AllTrim(cMsgCanc))<5, Alert('Informe o motivo do cancelamento.') ,oDlgCanc:End()) },D(037),D(012),,,,.T.,,"",,,,.F. )

		oDlgCanc:Activate(,,,.T.)

		DbSelectArea('ZXM')
	    Reclock("ZXM",.F.)
			ZXM->ZXM_STATUS := 'X'
			ZXM->ZXM_OBS    := cMsgCanc
		MsUnlock()

		MsgInfo('XML cancelado com sucesso !!!') 
			
	Else
           MsgAlert('Arquivo n�o pode ser cancelado!!!'+ENTER+'Primeiro exclua o Doc.Entrada vinculado a esse XML.'+ENTER+'Documento:'+SF1->F1_DOC+' - '+SF1->F1_SERIE)
	EndIf

EndIf


DbSelectArea('ZXM');DbGoTo(ZXMnRecno)

Return()
*****************************************************************
User Function Excluir_XML()    //    [11.0]
*****************************************************************
//���������������������������������������Ŀ
//� Exclui  XML                           �
//� Chamada -> Rotina Excluir_XML         �
//�����������������������������������������
Local ZXMnRecno	:=  ZXM->(Recno())
Local _cFilial 	:=	ZXM->ZXM_FILIAL
Local cChave	:=	ZXM->ZXM_CHAVE

DbSelectArea('ZXM')


If MsgYesNo('Deseja realmente EXCLUIR este XML???'+ENTER+ENTER+'FORNECEDOR: '+ZXM->ZXM_FORNEC+'-'+ZXM->ZXM_LOJA +ENTER+' NOTA: '+Alltrim(ZXM->ZXM_DOC)+'  S�rie: '+AllTrim(ZXM->ZXM_SERIE) )

		//���������������������������������������Ŀ
		//� VERIFICA SE DOC.ENTRADA FOI INCLUSO   �
		//�����������������������������������������
		DbSelectArea('SF1');DbSetOrder(1);DbGoTop()
		If !DbSeek(xFilial('SF1')+ IIF(ZXM->ZXM_FORMUL!='S', ZXM->ZXM_DOC+ZXM->ZXM_SERIE, ZXM->ZXM_DOCREF+ZXM->ZXM_SERREF) + ZXM->ZXM_FORNEC + ZXM->ZXM_LOJA + 'N' ,.F.)
                         
			//���������������������������������Ŀ
			//� ATUALIZA STATUS TABELA ZXP		�
			//�����������������������������������
			DbSelectArea('ZXP'); DbSetOrder(1);DbGoTop()	//	ZXM_FILIAL+ZXM_FORNEC+ZXM_LOJA+ZXM_DOC+ZXM_SERIE
			If DbSeek( xFilial('ZXP') + ZXM->ZXM_CHAVE , .F.)
				Do While !Eof() .And. 	ZXM->ZXM_CHAVE == ZXP->ZXP_CHAVE
					DbSelectArea('ZXP')
					Reclock('ZXP',.F.)
						DbDelete()
					MsUnlock()
					DbSkip()
				EndDo
			EndIf
		

           DbSelectArea('ZXM')
           Reclock("ZXM",.F.)
            	DbDelete()
           MsUnlock()
           
      
			ZXN->(DbSetOrder(1), DbGoTop())
			If ZXN->(DbSeek(_cFilial + cChave ,.F.))
				Do While ZXN->(!Eof()) .And. ZXN->ZXN_CHAVE == cChave
					Reclock('ZXN',.F.)
						DbDelete()
					MsUnlock()
				ZXN->(DbSkip())
				EndDo
			EndIf
			      
      
			MsgInfo('XML Excluido com sucesso !!!') 

     Else
           MsgAlert('Arquivo n�o pode ser EXCLUIDO!!!'+ENTER+'Primeiro exclua o Doc.Entrada vinculado a esse XML.'+ENTER+'Documento:'+SF1->F1_DOC+' - '+SF1->F1_SERIE)
     EndIf

EndIf


DbSelectArea('ZXM');DbGoTo(ZXMnRecno)

Return()
*****************************************************************
User Function Filtro_XML()    //    [11.00]
*****************************************************************
//���������������������������������������Ŀ
//� Opcao de Filtro - apenas DBF          �
//� Chamada -> Rotina Filtro              �
//�����������������������������������������
Local aArea	  := GetArea()
Local aCampos := {}

Private	bFiltraBrw	:=	{|| Nil }
Static	cFiltroRet	:=	''
Static	aIndexXml	:=	''

DbSelectArea('SX3');DbSetOrder(1);DbGoTop()
Do While !Eof() .And. SX3->X3_ARQUIVO == 'ZXM'
	aAdd( aCampos, {SX3->X3_CAMPO	, SX3->X3_PICTURE, SX3->X3_TITULO })
	DbSkip()
EndDo                      


   cFiltroRet := BuildExpr('ZXM',,'',.T.,,,,'Filtro XML',,aCampos)
   EndFilBrw('ZXM',aIndexXml)
   aIndexXml	:=	{}
   bFiltraBrw	:= 	{|| FilBrowse('ZXM',@aIndexXml,@cFiltroRet) }
   Eval(bFiltraBrw)
   DbSelectArea('ZXM');DbGoTop()


RestArea(aArea)
Return(Nil)
*********************************************************************
Static Function OpcaoRetorno()
*********************************************************************
//���������������������������������������������������������������������Ŀ
//� DEVOLUCAO      - FUNCAO F4NFORI  PADRAO DO MICROSIGA				|
//� BENEFICIAMENTO - FUNCAO F4Poder3 PADRAO DO MICROSIGA				|
//|																		|
//|	Chamada -> Visual_XML()->oBrwV:oBrowse:bLDblClick->VerifStatus()	�
//�����������������������������������������������������������������������
                              
// 	NOVA VERSAO NAO PREENCHE COD.FORNECEDOR E LOJA DESSA MANEIRA APRESENTA A TELA A SEGUIR.

Local  lOpRet  := .T.
Local  lBtnSair:= .F.

Private nPosCod  	:= Ascan( oBrwV:aHeader, {|X| AllTrim(X[2]) == 'D1_COD'	})
Private aTpNota  	:= {"Normal","Devolu��o","Beneficiamento","Compl. ICMS","Compl. IPI","Compl. Pre�o/Frete" }
Private nTpNota  	:= 1
Private aTpForm  	:= {"Sim","N�o"}
Private nTpForm  	:= 2
Private cVarCliFor 	:= "Fornece:"
Private cNomeCliFor	:= ""
Private cCodCliFor 	:= Space(06)
Private cCodLoja 	:= Space(04)
Private cUFCliFor 	:= Space(02)

                    	
SetPrvt("oFont1","oDlgBD","oGrp1","oCBoxTpNF","oCBoxTpNF","oCBoxNor","oCBoxDev","oCBoxBen","oGrp2","oCBoxSim","oCBoxNao","oBtn1")


If Empty(ZXM->ZXM_TIPO)

	oFont1     := TFont():New( "Verdana",0,IIF(nHRes<=800,-09, -11),,.T.,0,,700,.F.,.F.,,,,,, )
	oDlgBD     := MSDialog():New( D(142),D(361),D(299),D(674),"Devolu��o\Retorno\Importa��o",,,.F.,,,,,,.T.,,oFont1,.T. )

	oGrp1      := TGroup():New( D(008),D(004),D(030),D(072),"Tipo Nota",oDlgBD,CLR_BLACK,CLR_WHITE,.T.,.F. ) 
	oCBoxTpNF  := TComboBox():New( D(018),D(010), {|u|if(PCount()>0,nTpNota:=u,nTpNota)},aTpNota,072,010,oDlgBD,,{|| AtualizaGet(), cCodCliFor:=Space(06), cCodLoja :=	Space(02), cNomeCliFor := "" },,CLR_BLACK,CLR_WHITE,.T.,,"",,,,,,,)
		
	oGrp2      := TGroup():New( D(008),D(080),D(030),D(148),"Formul�rio Pr�prio",oDlgBD,CLR_BLACK,CLR_WHITE,.T.,.F. )
	oCBoxTpFor := TComboBox():New( D(018),D(085), {|u|if(PCount()>0,nTpForm:=u,nTpForm)},aTpForm,062,010,oDlgBD,,,,CLR_BLACK,CLR_WHITE,.T.,,"",,,,,,, )	
                  
	oSayCgc    := TSay():New( D(034),D(010),{|| "CNPJ:  "+ Transform(AllTrim(ZXM->ZXM_CGC), IIF(Len(AllTrim(ZXM->ZXM_CGC))==14,'@R 99.999.999/9999-99','@R 999.999.999-99'))  },oDlgBD,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,096,008)

	oSayCliFor := TSay():New( D(046),D(010),{|| cVarCliFor  },oDlgBD,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,032,008)
	oGetSA2    := TGet():New( D(044),D(035),{|u| If(PCount()>0,cCodCliFor:=u,cCodCliFor) },oDlgBD,040,008,'', {|| AtualizaGet() }, CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA2","",,)
	oGetSA1    := TGet():New( D(044),D(035),{|u| If(PCount()>0,cCodCliFor:=u,cCodCliFor) },oDlgBD,040,008,'', {|| AtualizaGet() }, CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA1","",,)		 	
	oGetLoja   := TGet():New( D(044),D(070),{|u| If(PCount()>0,cCodLoja:=u,cCodLoja)     },oDlgBD,005,008,'', {|| AtualizaGet() }, CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","",,)		 	

	oSayNome   := TSay():New( D(056),D(010),{|| cNomeCliFor },oDlgBD,,,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,096,008)			
	
	oBtnOK     := TButton():New( D(065),D(040),"OK",  oDlgBD,{|| IIF(ValidaDados(cCodCliFor),(Atu_SayDev(),oDlgBD:End()),) },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )
	oBtnNO     := TButton():New( D(065),D(080),"Sair",oDlgBD,{|| lBtnSair:=.T.,lOpRet:=.F.,oDlgBD:End() },D(037),D(012),,oFont1,,.T.,,"",,,,.F. )

	                  
	oBtnOK:SetFocus()
		
	oDlgBD:Activate(,,,.T.,{|| IIF(!lBtnSair, (lOpRet:=ValidOpcDev(), lOpRet), .T.) },, {|| AtualizaGet(), oDlgBD:lEscClose := .F. } )

            	
EndIf 


// ATUALIZA DADOS DO FORNECEDOR \ CLIENTE
***************************                                           
	AtuClixFor()
***************************

		
Return(lOpRet)
*********************************************************************
Static Function ValidOpcDev()
*********************************************************************
Local lValid := .T.
If Empty(cCodCliFor).Or.Empty(cCodLoja)
	Alert('Preencha o C�digo-Loja do Fornecedor\Cliente')
	lValid := .F.
EndIf

Return(lValid)
*********************************************************************
Static Function AtualizaGet()
*********************************************************************

If oCBoxTpNF:NAT == 2 .Or. oCBoxTpNF:NAT == 3 

	cVarCliFor := "Cliente: "
	oGetSA1:Show()
	oGetSA2:Hide() 
	If !Empty(cCodCliFor) .And. !Empty(cCodLoja)
		SA1->(DbSetOrder(1),DbGoTop())
		If !SA1->(DbSeek(xFilial('SA1') + cCodCliFor + cCodLoja, .F. ))
			Alert('C�digo Cliente n�o existe!!!!') 
			cNomeCliFor := ""
			cCodLoja 	:= Space(02)
			cCodCliFor 	:= Space(06)
			cNomeCliFor := ""
			cUFCliFor 	:= ''
		Else                                    
			cNomeCliFor := AllTrim(SA1->A1_NOME)
			cCodCliFor 	:= SA1->A1_COD
			cCodLoja 	:= SA1->A1_LOJA         
			cUFCliFor	:= SA1->A1_EST
		EndIf
	EndIf
			
Else

	cVarCliFor := "Fornece: "
	oGetSA1:Hide() 
	oGetSA2:Show()
	If !Empty(cCodCliFor) .And. !Empty(cCodLoja)
		SA2->(DbSetOrder(1),DbGoTop())
		If !SA2->(DbSeek(xFilial('SA2') + cCodCliFor + cCodLoja, .F. ))
			Alert('C�digo Fornecedor n�o existe!!!!') 
			cNomeCliFor := ""
			cCodLoja 	:= Space(02)
			cCodCliFor 	:= Space(06)
			cNomeCliFor := ""
			cUFCliFor	:= ""
		Else                           
			cNomeCliFor := AllTrim(SA2->A2_NOME)
			cCodCliFor 	:= SA2->A2_COD
			cCodLoja 	:= SA2->A2_LOJA
			cUFCliFor	:= SA2->A2_EST
		EndIf
	EndIf
		
EndIf

oGetSA2:Refresh()
oGetSA1:Refresh()
oSayCliFor:Refresh()
oSayNome:Refresh()
oDlgBD:Refresh()


Return()
*********************************************************************
Static Function ValidaDados()
*********************************************************************
//���������������������������������������������Ŀ
//|	Chamada -> OpcaoRetorno - Botao OK			�
//�����������������������������������������������
Local lReturn := .T. 

If Empty(cCodCliFor)

	Alert('Informe o c�digo do '+IIF(oCBoxTpNF:NAT==2.Or.oCBoxTpNF:NAT==3,'Cliente','Fornecedor')+ '!!!' )
	lReturn := .F.

Else
       
	_cCnpj 	:= IIF(oCBoxTpNF:NAT==2.Or.oCBoxTpNF:NAT==3, AllTrim(SA1->A1_CGC), AllTrim(SA2->A2_CGC) )
	_cUF 	:= IIF(oCBoxTpNF:NAT==2.Or.oCBoxTpNF:NAT==3, AllTrim(SA1->A1_EST), AllTrim(SA2->A2_EST) )
	

	If AllTrim(ZXM->ZXM_CGC) != AllTrim(_cCnpj) .And. !lImportacao
	   	Alert(	'CNPJ informado ['+Transform( AllTrim(_cCnpj), IIF(Len(AllTrim(_cCnpj))==14,'@R 99.999.999/9999-99','@R 999.999.999-99'))+'] '+ENTER+;
				'DIFERENTE'+ENTER+;
				'do CNPJ do XML ['+Transform( AllTrim(ZXM->ZXM_CGC), IIF(Len(AllTrim(ZXM->ZXM_CGC))==14,'@R 99.999.999/9999-99','@R 999.999.999-99'))+']' )
		
		lReturn := .F.

	ElseIf lImportacao .And. _cUF != 'EX'
		Alert('NOTA DE IMPORTA��O � OBRIGADO ESTADO = EX ') 
		lReturn := .F.
	Else

 		cTipoNota := ''
 		Do Case
 			Case oCBoxTpNF:NAT ==  1
		 		cTipoNota	:=	'N'
		 		cMsgDevRet	:=	'- Tipo Nota: Normal' 
 			Case oCBoxTpNF:NAT == 2
 				cTipoNota	:=	'D'
				cMsgDevRet	:=	'- Tipo Nota: Devolu��o'
 			Case oCBoxTpNF:NAT ==  3
 				cTipoNota	:=	'B'
				cMsgDevRet	:=	'- Tipo Nota: Beneficiamento'
 			Case oCBoxTpNF:NAT ==  4
 				cTipoNota	:=	'I'
				cMsgDevRet	:=	'- Tipo Nota: Compl. ICMS' 				
 			Case oCBoxTpNF:NAT == 5
 				cTipoNota	:=	'P'
				cMsgDevRet	:=	'- Tipo Nota: Compl. IPI' 				
 			Case oCBoxTpNF:NAT == 6
 				cTipoNota	:=	'C'
				cMsgDevRet	:=	'- Tipo Nota: Compl. Pre�o/Frete'
 		EndCase
                                                      
		cMsgDevRet += IIF(lImportacao,' - Importa��o','')

		//���������������������������������������������������������������������Ŀ
		//�		GRAVA CLIENTE\FORNECEDOR - TIPO DE NOTA NA TABELA ZXM        	�
		//�����������������������������������������������������������������������	
		DbSelectArea('ZXM')
  		Reclock('ZXM',.F.)               
			ZXM->ZXM_TIPO	:=	cTipoNota
			ZXM->ZXM_FORNEC	:=	cCodCliFor  
			ZXM->ZXM_LOJA	:=	cCodLoja 
		
			If ZXM->(FieldPos("ZXM_NOMFOR")) > 0
				ZXM->ZXM_NOMFOR	:=	cNomeCliFor
            EndIf
			If ZXM->(FieldPos("ZXM_UFFOR")) > 0
				ZXM->ZXM_UFFOR	:=	cUFCliFor
            EndIf
					
		MsUnlock()
    			
		//���������������������������������������������������������������������Ŀ
		//�		ATUALIZA ALGUNS DADOS DO ACOLS, QDO EH CLIENTE OU FORNECEDOR	�
		//�		APENAS QDO O PRODUTO JA ESTA RELACIONADO                     	�
		//�����������������������������������������������������������������������
		For nX := 1 To Len(aMyCols)                    
   
			nPLocal		:= Ascan(aMyHeader,{|x| AllTrim(x[2])=="D1_LOCAL"	})
			nPTES		:= Ascan(aMyHeader,{|x| AllTrim(x[2])=="D1_TES"	})
			nPosCC		:= Ascan(aMyHeader,{|X| Alltrim(X[2])=="D1_CC"		})
			nPosConta	:= Ascan(aMyHeader,{|X| Alltrim(X[2])=="D1_CONTA"	})
			nPosCod		:= Ascan(aMyHeader,{|x| AllTrim(x[2])=="D1_COD"	}) 

	  		If !Empty(aMyCols[nX][nPosCod] )
				SB1->(DbGoTop(), DbSetOrder(1))
	           	If SB1->(DbSeek(xFilial('SB1') + oBrwV:aCols[nX][nPosCod] ,.F.) )
	           		oBrwV:aCols[nX][nPLocal] 	:= IIF(!Empty(SB1->B1_LOCPAD), AllTrim(SB1->B1_LOCPAD),)		//	Local Padrao 
	           		oBrwV:aCols[nX][nPTES]   	:= IIF(!Empty(SB1->B1_TE), 	AllTrim(SB1->B1_TE),)			//	Codigo de Entrada padrao
	           		oBrwV:aCols[nX][nPosCC] 	:= IIF(!Empty(SB1->B1_CONTA), 	AllTrim(SB1->B1_CONTA),)		//	Conta Contabil dn Prod.
	           		oBrwV:aCols[nX][nPosConta] 	:= IIF(!Empty(SB1->B1_CC), 	AllTrim(SB1->B1_CC),)			//	Centro de Custo
				ElseIf SX6->(DbGoTop(), DbSeek( cFilAnt+'MV_LOCPAD' ,.F.) )
					oBrwV:aCols[nX][nPLocal] 	:= IIF(!Empty(SX6->X6_CONTEUD), AllTrim(SX6->X6_CONTEUD),)
	            EndIf
            
            EndIf
     	Next
        
    
		oBrwV:oBrowse:Refresh()
	EndIf

EndIf

Return(lReturn)
*****************************************************************
Static Function Atu_SayDev()
*****************************************************************
lNFeDev := IIF(oCBoxTpNF:NAT == 2 .Or. oCBoxTpNF:NAT == 3, .T., .F.) 
	
aClixFor[01] := IIF(!lNFeDev, "Fornecedor: ", "Cliente: ") 
aClixFor[02] := IIF(!lNFeDev, SA2->A2_NOME, SA1->A1_NOME ) 
aClixFor[03] := IIF(!lNFeDev, (SA2->A2_COD +' - '+SA2->A2_LOJA), (SA1->A1_COD +' - '+SA1->A1_LOJA)) 
aClixFor[04] := Transform( IIF(!lNFeDev,SA2->A2_CGC,SA1->A1_CGC), IIF(IIF(!lNFeDev,Len(SA2->A2_CGC)==14,Len(SA1->A1_CGC)==14),'@R 99.999.999/9999-99','@R 999.999.999-99')) 
aClixFor[05] := IIF(!lNFeDev, (AllTrim(SA2->A2_END)+IIF(!Empty(SA2->A2_COMPLEM),'-'+SA2->A2_COMPLEM,'')+IIF(Empty(SA2->A2_NR_END),'',', '+SA2->A2_NR_END )), ;
														    (AllTrim(SA1->A1_END)+IIF(!Empty(SA1->A1_COMPLEM),'-'+SA1->A1_COMPLEM,'')+IIF(SA1->(FieldPos("A1_NR_END"))>0, IIF(Empty(SA1->A1_NR_END),'',', '+SA1->A1_NR_END ),'')) )  
               
aClixFor[06] := IIF(!lNFeDev,(AllTrim(SA2->A2_MUN)+' - '+SA2->A2_EST), AllTrim(SA1->A1_MUN)+' - '+SA1->A1_EST) 
aClixFor[07] := IIF(!lNFeDev, ( IIF(!Empty(SA2->A2_DDD),'('+AllTrim(SA2->A2_DDD)+') - ','')+SA2->A2_TEL),;
															( IIF(!Empty(SA1->A1_DDD),'('+AllTrim(SA1->A1_DDD)+') - ','')+SA1->A1_TEL)) 

If !lNFeDev
	oBtn6SA2:Show()
	oBtn6SA1:Hide()
	oBtn4SA2:Show()
	oBtn4SA1:Hide()

	oBtn6SA2:Refresh()
	oBtn6SA1:Refresh()
	oBtn4SA2:Refresh()
	oBtn4SA1:Refresh()
EndIf

oSay71:Refresh()
oSay10:Refresh()
oSay8:Refresh()
oSay9:Refresh()
oSay11:Refresh()
oSay12:Refresh()
oSay13:Refresh()
oSay14:Refresh()
oSay15:Refresh()
oSay16:Refresh()
oSay17:Refresh()
oSay18:Refresh()

oSayDevRet:Refresh()


oGrp1:Refresh()
oBrwV:oBrowse:Refresh()

Return()
*****************************************************************
Static Function Doc_DevRet()
*****************************************************************
lOk		  :=	.T.
nPTES     := Ascan(oBrwV:aHeader,{|x| AllTrim(x[2])=="D1_TES"		})
nPLocal   := Ascan(oBrwV:aHeader,{|x| AllTrim(x[2])=="D1_LOCAL"	})
nPosCod	  := Ascan(oBrwV:aHeader,{|x| AllTrim(x[2])=="D1_COD"		})
nPosPFor  := Ascan(oBrwV:aHeader,{|X| Alltrim(X[2])=="C7_PRODUTO"	})
                                                                      

aHeader := 	aMyHeader
aCols	:=	aMyCols
n		:=	oBrwV:NAT
nLinha	:=	1
nRecSD2	:=	1
nRecSD1	:=	1
cRotina	:=	''
cReadVar:=	'D1_NFORI'        
cTES	:=	oBrwV:aCols[n][nPTES]
cProduto:=	oBrwV:aCols[n][nPosCod]
cLocal	:= 	oBrwV:aCols[n][nPLocal]

cPrograma := 'IMPXMLNFE'


If Empty(cProduto) .Or. Empty(cTES)
	MsgAlert('Preencha os campos: PRODUTO e TIPO DE ENTRADA') 
	Return()
EndIf

SF4->(DbSetOrder(1), DbGoTop())
SF4->(DbSeek(xFilial('SF4') +  cTES ,.F.))

Do Case
	Case ZXM->ZXM_TIPO == "D" .And. SF4->F4_PODER3 == "N"
		MsgRun('Verificando NF Origem... Aguarde...', 'Aguarde... ',{|| F4NFORI('',,"_NFORI",ZXM->ZXM_FORNEC,ZXM->ZXM_LOJA,cProduto,'IMPXMLNFE',cLocal,1,1)  })

	Case ZXM->ZXM_TIPO$"NB" .And. SF4->F4_PODER3 == "D"
		MsgRun('Verificando NF Origem... Aguarde...', 'Aguarde... ',{|| F4Poder3(cProduto, cLocal, ZXM->ZXM_TIPO, 'E', ZXM->ZXM_FORNEC,ZXM->ZXM_LOJA, 1, '')  })
	
	OtherWise
		If Empty(aCols[oBrwV:NAT][nPosCod]) .Or. Empty(aCols[oBrwV:NAT][nPTES])
			Help('   ',1,'A103TPNFOR')
		ElseIf cTipo == "D" .And. SF4->F4_PODER3 <> "N"	
			Help('   ',1,'A103TESNFD')
		ElseIf cTipo$"NB" .And. SF4->F4_PODER3 <> "D"	
			Help('   ',1,'A103TESNFB')
		EndIf
		
		lOk := .F.
EndCase


    
//��������������������������������������������������������Ŀ
//� APOS BUSCAR OS DADOS, PELA ROTINA PADRAO DO MICROSIGA, �
//� ALIMENTO aMyCols COM DADOS DE RETORNO DA BUSCA         �
//����������������������������������������������������������
If lOk
	For _nX:=1 To Len(aHeader)
		nPos :=	Ascan(aMyHeader, {|X| AllTrim(X[2]) == AllTrim(aHeader[_nX][2]) })
		If nPos > 0
			If oBrwV:aCols[n][nPos]	!= aCols[n][nPos] .And. !Empty(aCols[n][nPos])   
				oBrwV:aCols[n][nPos]	:= aCols[n][nPos]
			EndIf
		EndIf
	Next

	oBrwV:oBrowse:Refresh()
EndIf

Return()
*****************************************************************
Static Function GeraDV(xChave)
*****************************************************************
//����������������������������������������������������������������Ŀ
//� Gera DV para XML (Versao Old) que nao tenham Chave de Acesso   |
//|	VERSAO _INFNFE												   |
//� Chamada -> CabecXmlParser                                      �
//������������������������������������������������������������������
Local aChave	:= {}
Local nSoma    	:= 0
Local nResult	:= 0
Local nMult		:= 2
Local DV

//�������������������������������������Ŀ
//� ALIMENTA aChave POSICAO a POSICAO	|
//���������������������������������������
Do While !Empty(xChave)
   Aadd(aChave, Left(xChave,1) )
   xChave := Right(xChave, Len(xChave)-1,)
EndDo


For X := 0 To Len(aChave)

   nTam    :=    Len(aChave) - X
   If nTam == 0
       Exit
   EndIf

   nSoma    +=     Val(aChave[nTam]) * nMult

   If nMult == 9
       nMult := 2
   Else
        nMult++
   EndIf

Next

//Somat�ria das pondera��es = 644
//Dividindo a somat�ria das pondera��es por 11 teremos, 644 /11 = 58 restando 6.
//Como o d�gito verificador DV = 11 - (resto da divis�o), portando 11 - 6 = 5
//Quando o resto da divis�o for 0 (zero) ou 1 (um), o DV dever� ser igual a 0 (zero)

nResto     :=     Mod(nSoma,11)
If nResto == 0 .Or. nResto == 1
   DV     :=    0
Else
   DV     :=  11 - nResto
EndIf

Return( AllTrim(Str(DV)) )
***********************************************************************
User Function CreateTable()
***********************************************************************
//����������������������������������������������������������������Ŀ
//� Cria SIX, SX2, SX3 da tabela ZXM, caso nao exista              |
//� Chamada -> Grava_Xml()                                         �
//������������������������������������������������������������������
Local   cField		:=	''
Local	aTabelaOK 	:=	{{'ZXM',.T.},{'ZXN',.T.}}
Local	lRet		:=	.F.
Private	cEmpDest	:=	ParamIxb[1]
Private	cNumEmpOrig	:=	cNumEmp    //    GUARDA VALOR ORIGINAL
Private	cEmpAntOrig	:=	cEmpAnt    //    GUARDA VALOR ORIGINAL
Private	cFilAntOrig	:=	cFilAnt    //    GUARDA VALOR ORIGINAL
Private	cNomeEmp	:=	''
Private cTabela		:=	''
Private cArqTab		:=	''
                                    

cZXMModo	:=  cZXNModo	:=    ''
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('ZXM'))
   cZXMModo	:=    AllTrim(SX2->X2_MODO)
EndIf
If SX2->(DbSetOrder(1),DbGoTop(),DbSeek('ZXN'))
   cZXNModo	:=    AllTrim(SX2->X2_MODO)
EndIf



For nI := 1 To 2

	cTabela	:=	IIF(nI==1,'ZXM','ZXN')                                   
	cArqTab :=  cTabela+cEmpDest +'0'
	nPosTab	:=	Ascan(aTabelaOK, {|X|X[1] == cTabela } )
	
	
	//���������������������������Ŀ
	//�  FECHA SX2 EMPRESA ATUAL  �
	//�����������������������������
	IIF(Select('SX2')!=0, SX2->(DbCLoseArea()  ), )
	//���������������������������Ŀ
	//�  FECHA ZXM EMPRESA ATUAL  �
	//�����������������������������
	IIF(Select(cArqTab)!=0, cTabela->(DbCLoseArea()  ), )
	
	//������������������������������������Ŀ
	//�  RECEBE VALOR DA EMPRESA DESTINO   |
	//��������������������������������������
	cNumEmp    :=    cEmpDest+_aEmpFil[Int(Val('01'))][3] //'01'
	cEmpAnt    :=    cEmpDest
	cFilAnt    :=    '01'
	
	//���������������������������������Ŀ
	//�  BUSCA NOME DA EMPRESA DESTINO  |
	//�����������������������������������
	nFilDest := Ascan(_aEmpFil, {|X| X[1] == cNumEmp })
	cNomeEmp := AllTrim(SM0->M0_NOME)+' '+AllTrim(SM0->M0_FILIAL)//Alltrim(_aEmpFil[Int(Val(cFilAnt))][1]+'-'+_aEmpFil[Int(Val(cFilAnt))][2]+' \ '+_aEmpFil[Int(Val(cFilAnt))][4])   //nFilDest][4]
	
	//�����������������������������Ŀ
	//� ABRE SX2 DA EMPRESA DESTINO �
	//�������������������������������
	DbUseArea(.T.,,_StartPath+'SX2'+ cEmpDest +'0', 'SX2' ,.T.,.F.)
	DbSetIndex(_StartPath+'SX2'+ cEmpDest +'0')
	
	#IFDEF TOP
	   If !TCCanOpen(cArqTab)
	       Processa( {|| CriaZXM(cTabela,cEmpDest)},'Aguarde...','Criando arquivos SX�s Tabela '+cTabela+', Empresa: '+cNomeEmp, .T.)
	   EndIf
	#ELSE
	   ChkFile(cTabela,.T.,cTabela,Processa({|| CriaZXM(cTabela,cEmpDest)},'Aguarde...','Criando arquivos SX�s Tabela '+cTabela+' Empresa: '+cNomeEmp, .T.),,,'',.T.)
	#ENDIF
	
	
	//������������������������������������������Ŀ
	//� VERIFICA SE TODOS CAMPOS FORAM CRIADOS   |
	//��������������������������������������������
	DbSelectArea('SX3');DbSetOrder(1);DbGoTop()
	If DbSeek(cTabela+'01',.F.)
	   Do While !Eof() .And. SX3->X3_ARQUIVO == cTabela         
	       cCampo   :=    Alltrim(SX3->X3_CAMPO)
	       If &(cTabela)->(FieldPos(cCampo)) <= 0
	           	cField += cCampo + ENTER
				aTabelaOK[nPosTab][2] := .F.	
	       EndIf
	
	       DbSelectArea('SX3')
	       DbSkip()
	   EndDo
	Else
        nPos :=	 Ascan(aTabelaOK, {|X|X[1] == cTabela } )
		aTabelaOK[nPos][2] := .F.	
	EndIf
	
	
	If !Empty(cField)
	   MsgAlert('N�O FOI POSS�VEL CRIAR TODOS OS CAMPOS DA TABELA '+cTabela+' !!!'+ENTER+;
	               'Empresa: '+cNomeEmp+ENTER+ENTER+;
	              'Verifique a estrutura da tabela.'+ENTER+;
	              'Entre no modulo Configurador e Crie\Recrie o(s) seguinte(s) campo(s):'+ENTER+ cField  )
	   cField	:=	''
	
	EndIf
		
Next


//���������������������������������Ŀ
//� ABRE ARQUIVO DE CONFIGURACAO    |
//�����������������������������������
For nX:=1 To Len(aTabelaOK)

	lRet	:=	IIF(aTabelaOK[nX][2], .T., .F. )
	If !lRet
		Exit
	EndIf
	
Next

//����������������������������������������������������������Ŀ
//� ATUALIZA CAMPO ESTRUT_ZXM DO ARQUIVO ZXM_CFG.DBF     	 |
//� UTILIZADO PARA VERIFICAR SE ESTRUTURA DA TABELA ESTA OK  |
//������������������������������������������������������������
DbSelectArea('TMPCFG');DbSetOrder(1);DbGoTop()
If DbSeek(cEmpAnt+cFilAnt, .F.)
   RecLock('TMPCFG',.F.)
       	TMPCFG->ESTRUT_ZXM := lRet
   MsUnLock()
EndIf

	
//���������������������������Ŀ
//� FECHA SX2 EMPRESA DESTINO �
//�����������������������������
IIF(Select('SX2')!=0, SX2->(DbCLoseArea()  ), )
//���������������������������Ŀ
//� FECHA ZXM EMPRESA DESTINO �
//�����������������������������
IIF(Select('ZXM')!=0, ZXM->(DbCLoseArea()  ), )
IIF(Select('ZXN')!=0, ZXN->(DbCLoseArea()  ), )


//���������������������������Ŀ
//�    VOLTA VALOR ORIGINAL   �
//�����������������������������
cNumEmp    :=    cNumEmpOrig
cEmpAnt    :=    cEmpAntOrig
cFilAnt    :=    cFilAntOrig


//���������������������������Ŀ
//�  ABRE SX2 EMPRESA ATUAL   �
//�����������������������������
DbUseArea(.T.,,_StartPath+'SX2'+ cEmpAnt +'0', 'SX2' ,.T.,.F.)
DbSetIndex(_StartPath+'SX2'+ cEmpAnt +'0')

//��������������������������������Ŀ
//�  ABRE ZZXM\ZXN EMPRESA ATUAL   �
//����������������������������������
EmpOpenFile('ZXM',"ZXM",1,.T.,cEmpAnt,@cZXMModo)
EmpOpenFile('ZXN',"ZXN",1,.T.,cEmpAnt,@cZXNModo)

Return(lRet)
***********************************************************************
Static Function CriaZXM()
***********************************************************************

Processa({||CriaSIX(cTabela)},'SIX... ','Aguarde... Tabela'+cTabela, .T.)
Processa({||CriaSX2(cTabela)},'SX2... ','Aguarde... Tabela'+cTabela, .T.)
Processa({||CriaSX3(cTabela)},'SX3... ','Aguarde... Tabela'+cTabela, .T.)


IIF(Select('ZXM')!=0, ZXM->(DbCLoseArea()  ), )
IIF(Select('ZXN')!=0, ZXN->(DbCLoseArea()  ), )

Return()
***********************************************************************
Static Function CriaSIX()	//	CRIA SIX ou SINDEX
***********************************************************************
Local lSindex	:=	.F.

ProcRegua(3)

IIF( Select('SIX_ORIG') !=0, SIX_ORIG->(DbCLoseArea()  ), )

//���������������������������Ŀ
//�       VERIFICA  SIX  	  �
//�����������������������������
If File(_StartPath+'SIX'+cEmpAntOrig+'0.DTC') //.DBF

	//���������������������������Ŀ
	//� ABRE SIX DA EMPRESA ATUAL �
	//�����������������������������
	DbUseArea(.T.,,_StartPath+'SIX'+cEmpAntOrig+'0','SIX_ORIG',.T.,.F.)
	DbSetIndex(_StartPath+'SIX'+cEmpAntOrig+'0')
	                              
    
//���������������������������Ŀ
//�  VERIFICA  S I N D E X    �
//�����������������������������
ElseIf File(_StartPath+'SINDEX.DTC') //.DBF
 
	//������������������������������Ŀ
	//� ABRE SINDEX DA EMPRESA ATUAL �
	//��������������������������������
	DbUseArea(.T.,,_StartPath+'SINDEX','SIX_ORIG',.T.,.F.)
	DbSetIndex(_StartPath+'SINDEX')
	lSindex	  := .T.

EndIf



DbSelectArea('SIX_ORIG');DbSetOrder(1);DbGoTop()
If DbSeek(cTabela+'1', .F.)    //    INDICE + ORDEM
   IncProc()

   //���������������������������Ŀ
   //�ABRE SIX DA EMPRESA DESTINO�
   //�����������������������������
   IIF( Select('SIX_DEST')!=0, SIX_DEST->(DbCLoseArea()  ), )
   If !lSindex
	   DbUseArea(.T.,,_StartPath+'SIX'+cEmpDest+'0','SIX_DEST',.T.,.F.)
	   DbSetIndex(_StartPath+'SIX'+cEmpDest+'0')
   Else
	   DbUseArea(.T.,,_StartPath+'SINDEX','SIX_DEST',.T.,.F.)
	   DbSetIndex(_StartPath+'SINDEX')   
   EndIf

   DbSelectArea('SIX_DEST');DbGoTop()
   If !DbSeek(cTabela+'1', .F.)    //    INDICE + ORDEM

       DbSelectArea('SIX_ORIG')
       Do While !Eof() .And. SIX_ORIG->INDICE == cTabela
           DbSelectArea('SIX_DEST')
           RecLock('SIX_DEST',.T.)
               SIX_DEST->INDICE     :=    SIX_ORIG->INDICE
               SIX_DEST->ORDEM     	:=    SIX_ORIG->ORDEM
               SIX_DEST->CHAVE     	:=    SIX_ORIG->CHAVE
               SIX_DEST->DESCRICAO	:=    SIX_ORIG->DESCRICAO
               SIX_DEST->DESCSPA	:=    SIX_ORIG->DESCSPA
               SIX_DEST->DESCENG	:=    SIX_ORIG->DESCENG
               SIX_DEST->PROPRI 	:=    SIX_ORIG->PROPRI
               SIX_DEST->F3       	:=    SIX_ORIG->F3
               SIX_DEST->NICKNAME 	:=    SIX_ORIG->NICKNAME
               SIX_DEST->SHOWPESQ 	:=    SIX_ORIG->SHOWPESQ
           MsUnLock()

           DbSelectArea('SIX_ORIG')
           DbSkip()
       EndDo
   EndIf
Else
   MsgAlert('N�o encontrado INDICE "'+cTabela+'" da Empresa atual.... Verifique tabela de indices...'+ENTER+'Ou crie o indice: '+cTabela)
EndIf


IIF( Select('SIX_ORIG') !=0, SIX_ORIG->(DbCLoseArea() ),)
IIF( Select('SIX_DEST') !=0, SIX_DEST->(DbCLoseArea() ),)

Return()
***********************************************************************
Static Function CriaSX2()
***********************************************************************

ProcRegua(3);IncProc()

//���������������������������Ŀ
//�ABRE SX2 DA EMPRESA ATUAL  �
//�����������������������������
IIF( Select('SX2_ORIG') !=0, SX2_ORIG->(DbCLoseArea()  ), )
DbUseArea(.T.,,_StartPath+'SX2'+cEmpAntOrig+'0','SX2_ORIG',.T.,.F.)
DbSetIndex(_StartPath+'SX2'+cEmpAntOrig+'0')

DbSelectArea('SX2_ORIG');DbSetOrder(1);DbGoTop()
If DbSeek(cTabela, .F.)    //    CHAVE
   IncProc()

   //���������������������������Ŀ
   //�ABRE SX2 DA EMPRESA DESTINO�
   //�����������������������������
   IIF( Select('SX2_DEST')  !=0, SX2_DEST->(DbCLoseArea()  ), )
   DbUseArea(.T.,,_StartPath+'SX2'+cEmpDest+'0','SX2_DEST',.T.,.F.)
   DbSetIndex(_StartPath+'SX2'+cEmpDest+'0')

   DbSelectArea('SX2_DEST');DbGoTop()
   If !DbSeek(cTabela, .F.)    //    INDICE + ORDEM

       DbSelectArea('SX2_ORIG')
       Do While !Eof() .And. SX2_ORIG->X2_CHAVE == cTabela
           DbSelectArea('SX2_DEST')
           RecLock('SX2_DEST',.T.)
               SX2_DEST->X2_CHAVE	:=	SX2_ORIG->X2_CHAVE
               SX2_DEST->X2_PATH	:=	SX2_ORIG->X2_PATH
               SX2_DEST->X2_ARQUIVO	:=	cTabela+cEmpDest+'0'
               SX2_DEST->X2_NOME	:=	SX2_ORIG->X2_NOME
               SX2_DEST->X2_NOMESPA	:=	SX2_ORIG->X2_NOMESPA
               SX2_DEST->X2_NOMEENG	:=	SX2_ORIG->X2_NOMEENG
               SX2_DEST->X2_ROTINA	:=	SX2_ORIG->X2_ROTINA
               SX2_DEST->X2_MODO	:=	SX2_ORIG->X2_MODO
               SX2_DEST->X2_DELET	:=	SX2_ORIG->X2_DELET
               SX2_DEST->X2_TTS		:=	SX2_ORIG->X2_TTS
               SX2_DEST->X2_UNICO	:=	SX2_ORIG->X2_UNICO
               SX2_DEST->X2_PYME	:=	SX2_ORIG->X2_PYME
               SX2_DEST->X2_TTS		:=	SX2_ORIG->X2_TTS
           MsUnLock()
           DbSelectArea('SX2_ORIG')
           DbSkip()
       EndDo
   EndIf
Else
   MsgAlert('N�o encontrado chave "'+cTabela+'" na tabela SX2 da Empresa atual.... Verifique tabela SX2...'+ENTER+'Ou crie a chave '+cTabela+' na tabela SX2.')
EndIf

IncProc()

IIF( Select('SX2_ORIG') !=0, SX2_ORIG->(DbCLoseArea() ),)
IIF( Select('SX2_DEST') !=0, SX2_DEST->(DbCLoseArea() ),)

Return()
***********************************************************************
Static Function CriaSX3()
***********************************************************************
ProcRegua(3);IncProc()

//���������������������������Ŀ
//�ABRE SX3 DA EMPRESA ATUAL  �
//�����������������������������
IIF( Select('SX3_ORIG') !=0, SX3_ORIG->(DbCLoseArea()  ), )
DbUseArea(.T.,,_StartPath+'SX3'+cEmpAntOrig+'0','SX3_ORIG',.T.,.F.)
DbSetIndex(_StartPath+'SX3'+cEmpAntOrig+'0')

DbSelectArea('SX3_ORIG');DbSetOrder(1);DbGoTop()
If DbSeek(cTabela+'01', .F.)        //    X3_ARQUIVO + Z3_ORDERM
   IncProc()

   //���������������������������Ŀ
   //�ABRE SX3 DA EMPRESA DESTINO�
   //�����������������������������
   IIF( Select('SX3_DEST') !=0, SX3_DEST->(DbCLoseArea()  ), )
   DbUseArea(.T.,,_StartPath+'SX3'+cEmpDest+'0','SX3_DEST',.T.,.F.)
   DbSetIndex(_StartPath+'SX3'+cEmpDest+'0')

   DbSelectArea('SX3_DEST');DbGoTop()
   If !DbSeek(cTabela, .F.)    //    INDICE + ORDEM
       DbSelectArea('SX3_ORIG')
       Do While !Eof() .And. SX3_ORIG->X3_ARQUIVO == cTabela
           DbSelectArea('SX3_DEST')
           RecLock('SX3_DEST',.T.)
               SX3_DEST->X3_ARQUIVO		:=    SX3_ORIG->X3_ARQUIVO
               SX3_DEST->X3_ORDEM     	:=    SX3_ORIG->X3_ORDEM
               SX3_DEST->X3_CAMPO     	:=    SX3_ORIG->X3_CAMPO
               SX3_DEST->X3_TIPO		:=    SX3_ORIG->X3_TIPO
               SX3_DEST->X3_TAMANHO		:=    SX3_ORIG->X3_TAMANHO
               SX3_DEST->X3_DECIMAL		:=    SX3_ORIG->X3_DECIMAL
               SX3_DEST->X3_TITULO		:=    SX3_ORIG->X3_TITULO
               SX3_DEST->X3_TITSPA		:=    SX3_ORIG->X3_TITSPA
               SX3_DEST->X3_TITENG		:=    SX3_ORIG->X3_TITENG
               SX3_DEST->X3_DESCRIC 	:=    SX3_ORIG->X3_DESCRIC
               SX3_DEST->X3_DESCSPA     :=    SX3_ORIG->X3_DESCSPA
               SX3_DEST->X3_DESCENG     :=    SX3_ORIG->X3_DESCENG
               SX3_DEST->X3_PICTURE     :=    SX3_ORIG->X3_PICTURE
               SX3_DEST->X3_VALID     	:=    SX3_ORIG->X3_VALID
               SX3_DEST->X3_USADO     	:=    SX3_ORIG->X3_USADO
               SX3_DEST->X3_RELACAO     :=    SX3_ORIG->X3_RELACAO
               SX3_DEST->X3_F3          :=    SX3_ORIG->X3_F3
               SX3_DEST->X3_NIVEL     	:=    SX3_ORIG->X3_NIVEL
               SX3_DEST->X3_RESERV     	:=    SX3_ORIG->X3_RESERV
               SX3_DEST->X3_CHECK     	:=    SX3_ORIG->X3_CHECK
               SX3_DEST->X3_TRIGGER     :=    SX3_ORIG->X3_TRIGGER
               SX3_DEST->X3_PROPRI     	:=    SX3_ORIG->X3_PROPRI
               SX3_DEST->X3_BROWSE     	:=    SX3_ORIG->X3_BROWSE
               SX3_DEST->X3_VISUAL     	:=    SX3_ORIG->X3_VISUAL
               SX3_DEST->X3_CONTEXT		:=    SX3_ORIG->X3_CONTEXT
               SX3_DEST->X3_OBRIGAT		:=    SX3_ORIG->X3_OBRIGAT
               SX3_DEST->X3_VLDUSER		:=    SX3_ORIG->X3_VLDUSER
               SX3_DEST->X3_CBOX      	:=    SX3_ORIG->X3_CBOX
               SX3_DEST->X3_CBOXSPA     :=    SX3_ORIG->X3_CBOXSPA
               SX3_DEST->X3_CBOXENG     :=    SX3_ORIG->X3_CBOXENG
               SX3_DEST->X3_PICTVAR     :=    SX3_ORIG->X3_PICTVAR
               SX3_DEST->X3_WHEN		:=    SX3_ORIG->X3_WHEN
               SX3_DEST->X3_INIBRW     	:=    SX3_ORIG->X3_INIBRW
               SX3_DEST->X3_GRPSXG		:=    SX3_ORIG->X3_GRPSXG
               SX3_DEST->X3_FOLDER		:=    SX3_ORIG->X3_FOLDER
               SX3_DEST->X3_PYME		:=    SX3_ORIG->X3_PYME
               SX3_DEST->X3_CONDSQL    	:=    SX3_ORIG->X3_CONDSQL
               SX3_DEST->X3_CHKSQL     	:=    SX3_ORIG->X3_CHKSQL
               SX3_DEST->X3_IDXSRV 		:=    SX3_ORIG->X3_IDXSRV
               SX3_DEST->X3_ORTOGRA		:=    SX3_ORIG->X3_ORTOGRA
               SX3_DEST->X3_IDXFLD     	:=    SX3_ORIG->X3_IDXFLD
               SX3_DEST->X3_TELA      	:=    SX3_ORIG->X3_TELA
           MsUnLock()
           DbSelectArea('SX3_ORIG')
           DbSkip()
       EndDo
   EndIf
Else
   MsgAlert('N�o encontrado arquivo "'+cTabela+'" na tabela SX3 na Empresa atual.... Verifique tabela SX3...'+ENTER+'Ou crie os campos na tabela: '+cTabela)
EndIf

IncProc()

IIF( Select('SX3_ORIG') !=0, SX3_ORIG->(DbCLoseArea() ),)
IIF( Select('SX3_DEST') !=0, SX3_DEST->(DbCLoseArea() ),)

Return()
***********************************************************************
Static Function ForCliMsBlql(cTabela, cCnpj, lImportacao, cCodFornece, cLojaFornece)
***********************************************************************
//�����������������������������������������������������������Ŀ
//�FUNCAO PARA VERIFICAR SE CADASTRO DE CLIENTE OU FORNECEDOR �
//�NAO ESTA BLOQUEADO                                         �
//�������������������������������������������������������������
If cTabela == 'SA2' 

	If !lImportacao
		SA2->(DbSetOrder(3),DbGoTop(),DbSeek(xFilial('SA2')+ cCnpj, .F.) )
		Do While !Eof() .And. AllTrim(SA2->A2_CGC) == AllTrim(cCnpj) .And. SA2->A2_MSBLQL == '1'
			SA2->(DbSkip())
		EndDo
	Else
		SA2->(DbSetOrder(1),DbGoTop(),DbSeek(xFilial('SA2')+ cCodFornece + cLojaFornece, .F.) )
		Do While !Eof() .And. AllTrim(SA2->A2_COD) == AllTrim(cCodFornece) .And. AllTrim(SA2->A2_LOJA) == AllTrim(cLojaFornece) .And. SA2->A2_MSBLQL == '1'
			SA2->(DbSkip())
		EndDo	
	EndIf
		
ElseIf cTabela == 'SA1'

	If !lImportacao	
		SA1->(DbSetOrder(3),DbGoTop(),DbSeek(xFilial('SA1')+ cCnpj, .F.) )
		Do While !Eof() .And. AllTrim(SA1->A1_CGC) == AllTrim(cCnpj) .And. SA1->A1_MSBLQL == '1' // BLOQUEADO
			SA1->(DbSkip())
		EndDo
	Else
		SA1->(DbSetOrder(1),DbGoTop(),DbSeek(xFilial('SA1')+  cCodFornece + cLojaFornece,, .F.) )
		Do While !Eof() .And. AllTrim(SA1->A1_COD) == AllTrim(cCodFornece) .And. AllTrim(SA1->A1_LOJA) == AllTrim(cLojaFornece) .And. SA1->A1_MSBLQL == '1' // BLOQUEADO
			SA1->(DbSkip())
		EndDo	
	EndIf
	
EndIf



Return()
***********************************************************************
Static Function XML_ValNumDoc()
***********************************************************************
Local   cXMLDoc		:=	ZXM->ZXM_DOC 
Local 	cXMLSerie 	:= 	ZXM->ZXM_SERIE

//������������������������������������������������������Ŀ
//�    VERIFICA SE XML ESTA GRAVADO NA TABELA ZXM        �
//��������������������������������������������������������
DbSelectArea('ZXM'); DbSetOrder(1);DbGoTop()	//	ZXM_FILIAL+ZXM_FORNEC+ZXM_LOJA+ZXM_DOC+ZXM_SERIE
If !DbSeek( xFilial('ZXM') + cA100For + cLoja + cNFiscal + cSerie, .F.)

	//���������������������������������������������������������������������Ŀ
	//�   VERIFICA SE O NUMERO + SERIE DA NOTA\PRE-NOTA EH DIFERENTE DO XML	|
	//�����������������������������������������������������������������������	
	If	cNFiscal != cXMLDoc .Or. cSerie != cXMLSerie			//	cOpRotina $ 'MT100TOK/MT140TOK'	//	PE valida Doc.Entrada\Pre-Nota
		
		MsgInfo( 'O numero '+ IIF(cOpRotina=='MT100TOK','desse Doc.Entrada' ,'dessa Pre-Nota')+' � diferente do XML Importado.'+ENTER+ENTER+;
				 IIF(cOpRotina=='MT100TOK','Doc.Entrada: ' ,'Pre-Nota: ')+ cNFiscal+' - '+ cSerie +ENTER+;
				 'XML: '+cXMLDoc+' - '+cXMLSerie +ENTER+ENTER+;
				 'O numero do Documento XML ser� alterado.' )


		DbSelectArea('ZXM'); DbSetOrder(1);DbGoTop()	//	ZXM_FILIAL+ZXM_FORNEC+ZXM_LOJA+ZXM_DOC+ZXM_SERIE
		If DbSeek( xFilial('ZXM') + cA100For + cLoja + cXMLDoc + cXMLSerie, .F.)
			Reclock('ZXM',.F.)
				ZXM->ZXM_DOC	:=	cNFiscal
				ZXM->ZXM_SERIE	:=	cSerie
			MsUnlock() 
		EndIf

	EndIf

EndIf

Return()
***********************************************************************
Static Function C(nTam)
***********************************************************************
//���������������������������������������Ŀ
//� Ajusta Tamanho dos Objetos            �
//�����������������������������������������
Local nHRes    :=    oMainWnd:nClientWidth 		// Resolucao horizontal do monitor

   If nHRes == 640                              // Resolucao 640x480 (soh o Ocean e o Classic aceitam 640)
       nTam *= 0.8
   ElseIf (nHRes == 798).Or.(nHRes == 800)    	// Resolucao 800x600
       nTam *= 1
   Else                                        	// Resolucao 1024x768 e acima
       nTam *= 1.28
   EndIf

   //���������������������������Ŀ
   //�Tratamento para tema "Flat"�
   //�����������������������������
   If "MP8" $ oApp:cVersion
       If (Alltrim(GetTheme()) == "FLAT") .Or. SetMdiChild()
           nTam *= 0.90
       EndIf
   EndIf
Return Int(nTam)
***********************************************************************
Static Function D(nTam)	//	QDO TELA Eh DESENVOLVIDA COM RESOLUCAO 1080
***********************************************************************
//���������������������������������������Ŀ
//� Ajusta Tamanho dos Objetos            �
//�����������������������������������������
Local nHRes    :=    oMainWnd:nClientWidth		// Resolucao horizontal do monitor

   If nHRes == 640                            	// Resolucao 640x480 (soh o Ocean e o Classic aceitam 640)
       nTam /= 1.28
   ElseIf (nHRes == 798).Or.(nHRes == 800)    	// Resolucao 800x600
       nTam /= 1.28
   Else                                        	// Resolucao 1024x768 e acima
      nTam *= 1.28
   EndIf

   //���������������������������Ŀ
   //�Tratamento para tema "Flat"�
   //�����������������������������
   If "MP8" $ oApp:cVersion .Or. '10' $ cVersao 
       If (Alltrim(GetTheme()) == "FLAT") .Or. SetMdiChild()
           nTam /= 1.04
       EndIf
   EndIf
Return Int(nTam)            

User Function WSAT01GetIdEnt()

Local aArea  := GetArea()
Local cIdEnt := ""
Local cURL   := PadR(GetNewPar("MV_SPEDURL","http://"),250)
Local oWs
//������������������������������������������������������������������������Ŀ
//�Obtem o codigo da entidade                                              �
//��������������������������������������������������������������������������
oWS := WsSPEDAdm():New()
oWS:cUSERTOKEN := "TOTVS"

oWS:oWSEMPRESA:cCNPJ       := IIF(SM0->M0_TPINSC==2 .Or. Empty(SM0->M0_TPINSC),SM0->M0_CGC,"")
oWS:oWSEMPRESA:cCPF        := IIF(SM0->M0_TPINSC==3,SM0->M0_CGC,"")
oWS:oWSEMPRESA:cIE         := SM0->M0_INSC
oWS:oWSEMPRESA:cIM         := SM0->M0_INSCM
oWS:oWSEMPRESA:cNOME       := SM0->M0_NOMECOM
oWS:oWSEMPRESA:cFANTASIA   := SM0->M0_NOME
oWS:oWSEMPRESA:cENDERECO   := FisGetEnd(SM0->M0_ENDENT)[1]
oWS:oWSEMPRESA:cNUM        := FisGetEnd(SM0->M0_ENDENT)[3]
oWS:oWSEMPRESA:cCOMPL      := FisGetEnd(SM0->M0_ENDENT)[4]
oWS:oWSEMPRESA:cUF         := SM0->M0_ESTENT
oWS:oWSEMPRESA:cCEP        := SM0->M0_CEPENT
oWS:oWSEMPRESA:cCOD_MUN    := SM0->M0_CODMUN
oWS:oWSEMPRESA:cCOD_PAIS   := "1058"
oWS:oWSEMPRESA:cBAIRRO     := SM0->M0_BAIRENT
oWS:oWSEMPRESA:cMUN        := SM0->M0_CIDENT
oWS:oWSEMPRESA:cCEP_CP     := Nil
oWS:oWSEMPRESA:cCP         := Nil
oWS:oWSEMPRESA:cDDD        := Str(FisGetTel(SM0->M0_TEL)[2],3)
oWS:oWSEMPRESA:cFONE       := AllTrim(Str(FisGetTel(SM0->M0_TEL)[3],15))
oWS:oWSEMPRESA:cFAX        := AllTrim(Str(FisGetTel(SM0->M0_FAX)[3],15))
oWS:oWSEMPRESA:cEMAIL      := UsrRetMail(RetCodUsr())
oWS:oWSEMPRESA:cNIRE       := SM0->M0_NIRE
oWS:oWSEMPRESA:dDTRE       := SM0->M0_DTRE
oWS:oWSEMPRESA:cNIT        := IIF(SM0->M0_TPINSC==1,SM0->M0_CGC,"")
oWS:oWSEMPRESA:cINDSITESP  := ""
oWS:oWSEMPRESA:cID_MATRIZ  := ""
oWS:oWSOUTRASINSCRICOES:oWSInscricao := SPEDADM_ARRAYOFSPED_GENERICSTRUCT():New()
oWS:_URL := AllTrim(cURL)+"/SPEDADM.apw"
If oWs:ADMEMPRESAS()
	cIdEnt  := oWs:cADMEMPRESASRESULT
Else
	Aviso("SPED",IIf(Empty(GetWscError(3)),GetWscError(1),GetWscError(3)),{"Ok"},3)
EndIf

RestArea(aArea)
Return(cIdEnt)

User Function AtuRMS()          

	xPosRMS     := Ascan(aHeader,{|x| Alltrim(x[2]) == "B1_RMS"})
	xPosProd    := Ascan(aHeader,{|x| Alltrim(x[2]) == "D1_COD"})
	if xPosRMS > 0
		aCols[n][xPosRMS]	:= Posicione("SB1",1,xFilial("SB1")+aCols[n][xPosProd],"B1_RMS")
	endif
	oBrwV:oBrowse:Refresh()

Return .T.




//static function IncluiCD52(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter,_cItem, _cNOta,_cSerie , _vAFRMM)
static function IncluiCD5(_cSerie,_cNOta,_cFornece , _cLoja ,cXml)
Local  cError :=' ' 																
Local lRet :=.T.
Local cWarning := ' '
Local cXML	:=	AllTrim(cXml)
Local _nAdicao := 0
Local _nSeq := 0
Local _cFabri := ' '
Local _cExpt := ' '
Local _dDtDesem :=  stod(' ')
Local _dDtDI := stod(' ')
Local _cNumDI := ' '
Local _cUFDesemb := ' '
Local _vAFRMM:=0
Local _cTpvia:=' ' 
Local _cTpInter:=' '

cXML	+=	ExecBlock("VerificaZXN",.F.,.F., {ZXM->ZXM_FILIAL, ZXM->ZXM_CHAVE})
oXml  := 	XmlParser(cXML,"_",@cError, @cWarning )

msgalert(cError)
msgalert( Type("oXml:_NFEPROC:_NFE:_INFNFE:_DET[1]:_PROD:_DI:_ADI:_NADICAO:TEXT") )
if type('oXml:_NFEPROC:_NFE:_INFNFE:_DET') =='C'
	    If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   else
		   _nAdicao:= 0
	   Endif                
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
		else
			_nSeq:= 0
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
		else
			_cFabri:=' '
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_CEXPORTADOR:TEXT 
		else
			_cExpt:= ' '
	   Endif  
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT"))=="C" 
			_dDtDesemb := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDESEMB:TEXT, '-','',10))
		else
			_dDtDesemb  := stod(' ')
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT"))=="C" 
			_dDtDI := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_DDI:TEXT, '-','',10))
		else
			_dDtDI:=stod(' ')
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT"))=="C" 
			_cNumDI := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_NDI:TEXT
		else
			_cNumDI:= ' '
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT"))=="C" 
			_cUFDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_UFDESEMB:TEXT
		else
			_cUFDesemb:= ' '	
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" 
			_cLocDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_DI:_XLOCDESEMB:TEXT
		else
			_cLocDesemb:=' '
	   Endif

		///Adicionado por Bruno Sperb
		If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_INFNFE:_DET:_PROD:_DI:vAFRMM:TEXT)
		else
			_vAFRMM:= 0
	   ENDIF 
	   If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT"))=="C" .And. Empty(_cTpvia)
			_cTpvia := oXml:_INFNFE:_DET:_PROD:_DI:tpViaTransp:TEXT
		else
			_cTpvia := '1'
	   Endif
	    If AllTrim(TYPE("oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT"))=="C" .And. Empty(_cTpInter)
			_cTpInter := oXml:_INFNFE:_DET:_PROD:_DI:tpIntermedio:TEXT
		else
			_cTpInter:='0'
	   Endif

	   if ! Empty(_cNumDI)
	  	 	IncluiCD52(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter,'001' , _cNOta,_cSerie,_vAFRMM)
       endif

elseif type('oXml:_NFEPROC:_NFE:_INFNFE:_DET')	== 'A' 
	for i :=  1 to len (oXml:_NFEPROC:_NFE:_INFNFE:_DET)
		
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NADICAO:TEXT"))=="C"
			_nAdicao := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NADICAO:TEXT)
	   Endif                
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_NSEQADIC:TEXT"))=="C"
			_nSeq := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_NSEQADIC:TEXT)
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT"))=="C"
			_cFabri := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_ADI:_CFABRICANTE:TEXT
	   Endif  
       If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_CEXPORTADOR:TEXT"))=="C"
			_cExpt := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_CEXPORTADOR:TEXT 
	   Endif  
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDESEMB:TEXT"))=="C" .And. Empty(_dDtDesemb)
			_dDtDesemb := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_DDESEMB:TEXT,'-',''))
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_DDI:TEXT"))=="C" .And. Empty(_dDtDI)
			_dDtDI := StoD(StrTran(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_DDI:TEXT, '-',''))
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_NDI:TEXT"))=="C" .And. Empty(_cNumDI)
			_cNumDI := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_NDI:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_UFDESEMB:TEXT"))=="C" .And. Empty(_cUFDesemb)
			_cUFDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_UFDESEMB:TEXT
	   Endif
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:_XLOCDESEMB:TEXT"))=="C" .And. Empty(_cLocDesemb)
			_cLocDesemb := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:_XLOCDESEMB:TEXT
	   Endif
		///Adicionado por Bruno Sperb
		If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:vAFRMM:TEXT"))=="C"
			_vAFRMM := Val(oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:vAFRMM:TEXT)
	   Endif 
	   If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:tpViaTransp:TEXT"))=="C" 
			_cTpvia := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:tpViaTransp:TEXT
		else 
			_cTpvia :='1'
	   Endif
	    If AllTrim(TYPE("oXml:_NFEPROC:_NFE:_INFNFE:_DET["+AllTrim(Str(i))+"]:_PROD:_DI:tpIntermedio:TEXT"))=="C" 
			_cTpInter := oXml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_DI:tpIntermedio:TEXT
		else
			_cTpInter :='0'
	   Endif

	   if ! Empty(_cNumDI)
	  	 	IncluiCD52(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter, strzero(i,3), _cNota,_cSerie,_vAFRMM, _cLoja, _cFornece)
	   endif
	next 
endif 


RETURN .t.

static function IncluiCD52(_dDtDI,_nAdicao,_nSeq,_cFabri,_cExpt,_dDtDesemb,_cNumDI,_cUFDesemb,_cLocDesemb,_cTpvia,_cTpInter,_cItem, _cNOta,_cSerie , _vAFRMM, _cLoja,_cFornece)

CD5->(DbSetOrder(4))
if !CD5->(DbSeek(xFilial('CD5')+_cNOta+_cSerie+_cFornece+_cLoja+_cItem))
		CD5->(RecLock('CD5', .T.))
		CD5->CD5_FILIAL :=xFilial('CD5')
		CD5->CD5_DOC	:=_cNOta
		CD5->CD5_SERIE	:=_cSerie
		CD5->CD5_ESPEC	:='SPED'
		CD5->CD5_FORNEC	:=_cFornece
		CD5->CD5_LOJA	:=_cLoja
		CD5->CD5_TPIMP	:= '1'//0
		CD5->CD5_DOCIMP	:=_cNumDI //NDI
		CD5->CD5_BSPIS	:=0//
		CD5->CD5_ALPIS	:=0
		CD5->CD5_VLPIS	:=0
		CD5->CD5_BSCOF	:=0
		CD5->CD5_ALCOF	:=0
		CD5->CD5_VLCOF	:=0
		CD5->CD5_LOCAL	:= '0'// 0
		CD5->CD5_NDI	:=_cNumDI
		CD5->CD5_DTDI	:=_dDtDI
		CD5->CD5_LOCDES	:=_cLocDesemb
		CD5->CD5_UFDES	:=_cUFDesemb
		CD5->CD5_DTDES	:=_dDtDesemb
		CD5->CD5_CODEXP	:=_cFornece
		CD5->CD5_NADIC	:= STRZERO(_nAdicao,3)// completar com 0 a esquerda
		CD5->CD5_SQADIC	:=  alltrim(str(_nSeq))
		CD5->CD5_CODFAB	:= _cFornece
		CD5->CD5_BCIMP	:=0
		CD5->CD5_DSPAD	:=0
		CD5->CD5_VDESDI	:=0
		CD5->CD5_VLRII	:=0
		CD5->CD5_VLRIOF	:=0
		CD5->CD5_LOJFAB	:=_cLoja
		CD5->CD5_LOJEXP	:=_cLoja
		CD5->CD5_VAFRMM	:=_vAFRMM
		CD5->CD5_INTERM	:='1'
		CD5->CD5_CNPJAE	:=' '
		CD5->CD5_UFTERC	:= ' '
		CD5->CD5_ITEM   := _cItem
		CD5->CD5_SDOC   :=_cNumDI
		CD5->(MsUnlock())
		lRet := .T.
	 
else
	alert('Aten��o , este numero de nota j� possu� registros na tabela CD5 para o Item :'+_cItem+'! ','Aten��o')
endif 

return lRet





static Function ImpXML_Cte(cFile,lJob,aProc,aErros,oXml,aErroErp,oXMLCanc , lSF1)
Local cPrdFrete  := ""
Local lRet       := .T.
Local lRemet 	 := .F.
Local lToma3Dest := .F.
Local lCliente	 := .F.
Local lDevSemSF1 := .F.
Local lToma4	 := .F.
Local nX		 := 0
Local nPesoBruto := 0
Local nPesoLiqui := 0    
Local nY		 := 0
Local nCount	 := 0
Local nPos			:= 0
Local cError     := ""
Local cCNPJ_CT	 := ""
Local cCNPJ_CF	 := ""
Local cFornCTe   := ""
Local cLojaCTe   := ""
Local cNomeCTe   := ""
Local cCodiRem   := ""
Local cLojaRem   := ""
Local cChaveNF   := ""
Local cTES_CT 	 := ""
Local cCPag_CT 	 := ""
Local cTipoFrete := ""
Local cTagDest	 := ""
local cTagRem	 := ""
Local cTagExped	 := ""
Local cIEExped	 := ""
Local cQuery	 := ""
Local cTabRem	 := ""
Local cRetPrd	 := ""
Local cMotivo		:= ""
Local aDadosAux  := {}
Local aDadosFor  := Array(2) //-- 1- Codigo; 2-Loja
Local aDadosCli  := Array(2) //-- 1- Codigo; 2-Loja
Local aCabec116	 := {}
Local aItens116	 := {}
Local aAux		 := {}
Local aEspecVol  := {}
Local aAux1		 := {}
Local aAuxCliFor	 := {}
Local aParamPE	 := {}
Local aChave	:= {}
Local lMt116XmlCt:= ExistBlock("MT116XMLCT")
Local lA116IFor	 := ExistBlock("A116IFOR")
Local lA116ITPCT := ExistBlock("A116ITPCT")
Local lA116ICHV	:= ExistBlock("A116ICHV")
Local lA116iComp	:= ExistBlock("A116ICOMP")
Local cVersao	:= oXML:_InfCte:_Versao:Text
Local nTamFil	:= 0
Local lComp	:= .F. // Indica se CTe � do tipo complementar
Local cTpCte := ""
Local lFornExt	:= .F.
Local lDevBen	:= .F. // Devolu��o ou Beneficiamento
Local cDoc		:= ""
Local cSerie	:= ""
Local aA116IDOC	:= {}
Local lCliForDup	:= .F.
Local cChvNFOr	:= ""
Local nDevOri	:= 0
Local nNorOri	:= 0
Local lFornOri := .T.
Local aFornChv	:= {}
Local nPFornChv	:= 0
Local aExcStat 	:= {"100","102","103","104","105","106","107","108","109","110","111","112","113","114","134","135","136","301"}
Local cHrEmis		:= ""
Local lVlrFrtZero	:= .F.
Local nVlrFrt		:= 0
Local cCFBN		:= SuperGetMv("MV_XMLCFBN",.F.,"")
Local lRateiaDev	:= SuperGetMv("MV_XMLRATD",.F.,.T.)
Local cUFOrigem		:= ""
Local cMunOrigem	:= ""
Local cUFDestino	:= ""
Local cMunDestin	:= ""

//lPreNota = .T. (N�o Classificado), lPreNota = .F. (Classificado)
//MV_CTECLAS = .F. (N�o Classificado), MV_CTECLAS = .T. (Classificado)
Local lPreNota	:= !SuperGetMV("MV_CTECLAS",.F.,.F.) 

// Variaveis para apuracao do ICMS
Local oICMS
Local oICMSTipo
Local oICMSNode
Local nZ		:= 0
Local nBaseICMS := 0
Local nValICMS	:= 0
Local nAliqICMS	:= 0
Local lBaseICMS := .F.
Local lValICMS	:= .F.
Local lAliqICMS	:= .F.
Local lT4DifDest:= .F.
Local lT3Exped	:= .F.
Local lT3Remet	:= .F.
Local lDSNotF1	:= .F.
Local lUnidMed	:= SDT->(ColumnPos("DT_UM")) > 0 .And. SDT->(ColumnPos("DT_SEGUM")) > 0 .And. SDT->(ColumnPos("DT_QTSEGUM")) > 0

Private lImpXML	  := SuperGetMv("MV_IMPXML",.F.,.F.) .And. CKO->(FieldPos("CKO_ARQXML")) > 0 .And. !Empty(CKO->(IndexKey(5)))

Default lJob    := .T.
Default aProc   := {}
Default aErros  := {} 
Default aErroERP := {} 
lSF1 :=  .F.

//-- Verifica se o arquivo pertence a filial corrente
lRet := CTe_VldEmp(oXML,SM0->M0_CGC,@lToma3Dest,@lToma4,lJob,cFile,,,@lT4DifDest,@lT3Exped,@lT3Remet,@aErros,@aErroERP)

//Verifica se XML � Cancelado ou Rejeitado.
If lRet .And. ValType(oXMLCanc) <> "U"
	If Valtype(XmlChildEx(oXMLCanc:_infProt,"_CSTAT")) <> "U"
		//Chave CT-e
		cChaveCte	:= Right(AllTrim(oXML:_InfCte:_Id:Text),44)
		
		//Motivo rejei��o
		If Valtype(XmlChildEx(oXMLCanc:_infProt,"_XMOTIVO")) <> "U"
			cMotivo := ConvASC(oXMLCanc:_infProt:_xMotivo:Text)  
		Endif
		
		//Busca status
		nPos := aScan(aExcStat,AllTrim(oXMLCanc:_infProt:_cStat:Text))
		
		//Status de cancelado ou rejeitado
		If nPos == 0
			If AllTrim(oXMLCanc:_infProt:_cStat:Text) == "101" //Cancelado
				If !lJob
					Aviso('STR0001',"CT-e cancelado: " + cChaveCte,{"OK"},2,"ImpXML_CTe")//"Erro"#"CT-e cancelado
				EndIf
				
				aAdd(aErros,{cFile,"COM036 - CT-e cancelado: " + cChaveCte,""})
				aAdd(aErroErp,{cFile,"COM036"})
				
				lRet := .F.
			Else //Rejeitado
				If !lJob
					Aviso('STR0001',"CT-e rejeitado: " + cChaveCte + " - Motivo: " + cMotivo,{"OK"},2,"ImpXML_CTe")//"Erro"#"CT-e cancelado
				EndIf
				
				aAdd(aErros,{cFile,"COM037 - CT-e rejeitado: " + cChaveCte + " - Motivo: " + cMotivo,""})
				aAdd(aErroErp,{cFile,"COM037"})
				lRet := .F.
			Endif	
		Endif
	Endif
	
	If Valtype(XmlChildEx(oXMLCanc:_infProt,"_DHRECBTO")) <> "U"
		cHrEmis := Substr(oXMLCanc:_infProt:_DhRecbto:Text,12)
	Endif
Endif
                                      
//-- Verifica se o ID ja foi processado
If lRet
	SDS->(dbSetOrder(2))
	If lRet .And. SDS->(dbSeek(xFilial("SDS")+Right(AllTrim(oXML:_InfCte:_Id:Text),44)))
		If lJob
			aAdd(aErros,{cFile,"COM019 - " + 'STR0003' +SDS->(DS_DOC+"/"+SerieNfId('SDS',2,'DS_SERIE')); //"ID de CT-e j� registrado na NF "
							 + 'STR0004' + " (" +SDS->(DS_FORNEC +"/" +DS_LOJA)+ ").",'STR0005'}) //"do Fornecedor/Cliente "#"Exclua o documento registrado na ocorr�ncia."
		Else 
			msgalert('Aten��o CTE j� importado !')
			lRet := .F.
		EndIf
		aAdd(aErroErp,{cFile,"COM019"})
		lRet := .F.
	EndIf
EndIf

//-- Verifica se CTe � do tipo complementar
If lRet .And. Valtype(XmlChildEx(oXml:_InfCte,"_INFCTECOMP")) != "U"
	lComp := .T.
EndIf

//-- Verifica se o fornecedor do conhecimento esta cadastrado no sistema.
If lRet
	If ValType(XmlChildEx(oXML:_InfCte:_Emit,"_CNPJ")) <> "U"
		cCNPJ_CT := AllTrim(oXML:_InfCte:_Emit:_CNPJ:Text)
	Else
		cCNPJ_CT := AllTrim(oXML:_InfCte:_Emit:_CPF:Text)
	EndIf
	
	// Envio das letras E, D ou R pela funcao A116ICLIFOR. E = Emitente, D = Destinatario e R = Remetente.
	// Essa informacao sera utilizada no P.E. A116IFOR.
	aDadosAux := A116ICLIFOR(cCNPJ_CT,AllTrim(oXML:_InfCte:_Emit:_IE:Text),"SA2",,oXML,'E')
	
	If Len(aDadosAux) == 0
		If lJob
			aAdd(aErros,{cFile,"COM007 - " + 'STR0006' + oXML:_InfCte:_Emit:_Xnome:Text +" [" + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") +"] "+ 'STR0007','STR0008'}) //"Fornecedor"#"inexistente na base."#"Gere cadastro para este fornecedor."
		Else
			Aviso('STR0001','STR0006' + oXML:_InfCte:_Emit:_Xnome:Text +" [" + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") +"] "+ 'STR0007','STR0008',2,"ImpXML_CTe") //"Fornecedor inexistente na base. Gere cadastro para este fornecedor."#"	
		EndIF		
		aAdd(aErroErp,{cFile,"COM007"})			
		lRet := .F.
	ElseIf !aDadosAux[1,1] .And. aDadosAux[1,5] // Se encontrou o cadastro e o mesmo esta bloqueado.
		If lJob
			aAdd(aErros,{cFile,"COM030 - " + 'STR0006' + oXML:_InfCte:_Emit:_Xnome:Text +" [" + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") +"] "+ 'STR0037','STR0038'}) //"Fornecedor"#"bloqueado."#"Realize o desbloqueio"
		Else
			Aviso('STR0001','STR0006' + oXML:_InfCte:_Emit:_Xnome:Text +" [" + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") +"] "+ 'STR0037','STR0038',2,"ImpXML_CTe") //"Fornecedor""bloqueado."#"Realize o desbloqueio"#"	
		EndIF		
		aAdd(aErroErp,{cFile,"COM030"})			
		lRet := .F.
	Elseif !aDadosAux[1,1]
		If lJob
			aAdd(aErros,{cFile,"COM028 - " + 'STR0032' + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") + 'STR0033', 'STR0034'})
		Else
			Aviso('STR0001','STR0032' + Transform(cCNPJ_CT,"@R 99.999.999/9999-99") + 'STR0033' ,{"OK"},2,"ImpXML_CTe")
		EndIf
		aAdd(aErroErp,{cFile,"COM028"})
		lRet := .F.
	Else
		cFornCTe := aDadosAux[1,2]
		cLojaCTe := aDadosAux[1,3]
		cNomeCTe := Posicione("SA2",1,xFilial("SA2") + cFornCTe + cLojaCTe,"A2_NOME")
	Endif
EndIf

//Identifica se a empresa foi remetente das notas fiscais contidas no conhecimento:

//Se sim, significa que as notas contidas no conhecimento sao notas de saida, podendo ser
//notas de venda, devolucao de compras ou devolucao de remessa para beneficiamento.

//Se nao, significa que as notas contidas no conhecimento sao notas de entrada, podendo ser
//notas de compra, devolucao de vendas ou remessa para beneficiamento.
If lRet
	aDadosAux := {}
	cTagRem := If(ValType(XmlChildEx(oXML:_InfCte,"_REM")) == "O",If(ValType(XmlChildEx(oXML:_InfCte:_Rem,"_CNPJ")) == "O","_CNPJ","_CPF"),"")
	If ( lRemet := (SM0->M0_CGC == If(!Empty(cTagRem),(AllTrim(XmlChildEx(oXML:_InfCte:_Rem,cTagRem):Text)),"")) .And. !lToma3Dest .And. lT3Remet )
		cTagDest := If(ValType(XmlChildEx(oXML:_InfCte:_Dest,"_CNPJ")) == "O","_CNPJ","_CPF")
		cIEDestT4:= If(ValType(XmlChildEx(oXML:_InfCte:_Dest,"_IE")) == "O",AllTrim(oXML:_InfCte:_Dest:_IE:Text),"")
		cCNPJ_CF := AllTrim(XmlChildEx(oXML:_InfCte:_Dest,cTagDest):Text) //-- Armazena o CNPJ do destinatario das notas contidas no conhecimento
		cTipoFrete := "F"
		
		If !Empty(cCNPJ_CF) 
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA2",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA1",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		EndIf
		
		// Verifica a TAG <TpCte> para analisar se a nota eh complementar.
		cTpCte := If(ValType(XmlChildEx(oXML:_InfCte,"_IDE")) == "O",AllTrim(oXML:_InfCte:_Ide:_tpCTe:Text),"") //-- Armazena o tipo do CT-e.
		
		// Opcoes para <TpCte>:
		// 0 - CT-e Normal;
		// 1 - CT-e de Complemento de Valores;
		// 2 - CT-e de Anula��o de Valores;
		// 3 - CT-e Substituto.
		
		If cTpCte == '1' // CT-e de Complemento de Valores
			lRemet := .F.
		EndIf
		
		cTagMsg := '_Dest'
	ElseIf lToma4
		If ValType(XmlChildEx(oXML:_InfCte,"_REM")) == "O"
			cCNPJ_CF	:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,cTagRem):Text)
			
			If ValType(XmlChildEx(oXML:_InfCte:_Rem,"_IE")) == "O"
				cIEDestT4		:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,"_IE"):Text)
			Else
				cIEDestT4		:= ""
			Endif
		Else
			cCNPJ_CF	:= ""
			cIEDestT4	:= ""
		Endif
		
		cTipoFrete := "F"
		
		If !Empty(cCNPJ_CF)
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA2",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEDestT4,"SA1",,oXML,'D')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		EndIf
		
		cTagMsg := '_Rem'
		
	Elseif lT3Exped
		If ValType(XmlChildEx(oXML:_InfCte,"_EXPED")) == "O"
			cTagExped := If(ValType(XmlChildEx(oXML:_InfCte:_Exped,"_CNPJ")) == "O","_CNPJ","_CPF")
			cCNPJ_CF	:= AllTrim(XmlChildEx(oXML:_InfCte:_Exped,cTagExped):Text)
			
			If ValType(XmlChildEx(oXML:_InfCte:_Exped,"_IE")) == "O"
				cIEExped		:= AllTrim(XmlChildEx(oXML:_InfCte:_Exped,"_IE"):Text)
			Else
				cIEExped		:= ""
			Endif
		Else
			cCNPJ_CF	:= ""
			cIEExped	:= ""
		Endif
		
		cTipoFrete := "C"
		
		If !Empty(cCNPJ_CF)
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEExped,"SA2",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIEExped,"SA1",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		Endif
		
		cTagMsg := '_Exped'
	Else
		If ValType(XmlChildEx(oXML:_InfCte,"_REM")) == "O"
			cCNPJ_CF	:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,cTagRem):Text)
			
			If ValType(XmlChildEx(oXML:_InfCte:_Rem,"_IE")) == "O"
				cIERem		:= AllTrim(XmlChildEx(oXML:_InfCte:_Rem,"_IE"):Text)
			Else
				cIERem		:= ""
			Endif
		Else
			cCNPJ_CF	:= ""
			cIERem		:= ""
		Endif
		
		If lToma3Dest
			cTipoFrete := "F"
		Else
			cTipoFrete := "C"
		EndIf
		
		If !Empty(cCNPJ_CF)
			//Fornecedor
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIERem,"SA2",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"F"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
			
			//Cliente
			aAuxCliFor := A116ICLIFOR(cCNPJ_CF,cIERem,"SA1",,oXML,'R')
			
			If Len(aAuxCliFor) == 0
				aAdd(aDadosAux,{.T.,"","",.F.,.F.,"C"})
			Else
				aAdd(aDadosAux,aAuxCliFor[1])
			Endif
		Endif
		
		cTagMsg := '_Rem'
	Endif
	
	// Verifica a TAG <TpCte> para analisar se a nota eh de Anulacao de Valores.
	If cTpCte <> '1'
		cTpCte := If(ValType(XmlChildEx(oXML:_InfCte,"_IDE")) == "O",AllTrim(oXML:_InfCte:_Ide:_tpCTe:Text),"") //-- Armazena o tipo do CT-e.
	EndIf
		
	If lComp
		aAux1 := If(ValType(oXML:_InfCte:_InfCTeComp) == "O",{oXML:_InfCte:_InfCTeComp},oXML:_InfCte:_InfCTeComp)
	ElseIf cTpCte == '2'
		If ValType(XmlChildEx(oXML:_InfCte:_infCteAnu,"_INFDOC")) != "U" .And. ValType(XmlChildEx(oXML:_InfCte:_infCteAnu:_InfDoc,"_INFNF")) != "U"
			aAux := If(ValType(oXML:_InfCte:_infCteAnu:_InfDoc:_INFNF) == "O",{oXML:_InfCte:_infCteAnu:_InfDoc:_INFNF},oXML:_InfCte:_infCteAnu:_InfDoc:_INFNF)
		EndIf
	ElseIf cVersao >= "2.00"
		If Valtype(XmlChildEx(oXml:_InfCte,"_INFCTENORM")) != "U"
			If ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm,"_INFDOC")) != "U" .And. ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm:_InfDoc,"_INFNF")) != "U"
				aAux := If(ValType(oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNF) == "O",{oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNF},oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNF)
			ElseIf ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm,"_INFDOC")) != "U" .And. ValType(XmlChildEx(oXML:_InfCte:_InfCTeNorm:_InfDoc,"_INFNFE")) != "U"
				aAux1 := If(ValType(oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNFE) == "O",{oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNFE},oXML:_InfCte:_InfCTeNorm:_InfDoc:_INFNFE)
			EndIf
		EndIf
	Else
		If ValType(XmlChildEx(oXML:_InfCte:_Rem,"_INFNF")) != "U"
			aAux := If(ValType(oXML:_InfCte:_Rem:_INFNF) == "O",{oXML:_InfCte:_Rem:_INFNF},oXML:_InfCte:_Rem:_INFNF)
		ElseIf ValType(XmlChildEx(oXML:_InfCte:_Rem,"_INFNFE")) != "U"
			aAux1 := If(ValType(oXML:_InfCte:_Rem:_INFNFE) == "O",{oXML:_InfCte:_Rem:_INFNFE},oXML:_InfCte:_Rem:_INFNFE)
		EndIf
	EndIf
	
	If Len(aAux) > 0 .Or. (Len(aAux) == 0 .And. Len(aAux1) == 0)
		If Len(aAux) > 0
			cCodiRem 		:= CriaVar("A2_COD",.F.)
			cLojaRem 		:= CriaVar("A2_LOJA",.F.)
			aDadosFor[1]	:= CriaVar("A2_COD",.F.)
			aDadosFor[2]	:= CriaVar("A2_LOJA",.F.)
			aDadosCli[1]	:= CriaVar("A1_COD",.F.)
			aDadosCli[2]	:= CriaVar("A1_LOJA",.F.)
		Endif
		
		If lRet //Validou fornecedor ou cliente
			
			SF2->(dbClearFilter())
			SF2->(dbSetOrder(1))
			
			SF1->(dbClearFilter())
			SF1->(dbSetOrder(1))
			
			SDS->(dbClearFilter())
			SDS->(dbSetOrder(1))
			
			For nX := 1 To Len(aAux)
				lDSNotF1 := .F.
				//Verifica CFOP para saber se documento origem � Beneficamento
				lNFOriBen	:= aAux[nX]:_nCFOP:Text $ cCFBN
				
				cChaveNF :=	PadL(AllTrim(aAux[nX]:_nDoc:Text),TamSX3("F1_DOC")[1], "0") +;
								PadR(AllTrim(aAux[nX]:_Serie:Text),TamSX3("F1_SERIE")[1])
					
				//Ponto de entrada para manipula�a da chave(n� do doc + serie) do doc de origem
				If lA116ICHV
					aChave := ExecBlock("A116ICHV",.F.,.F.,{AllTrim(aAux[nX]:_nDoc:Text),AllTrim(aAux[nX]:_Serie:Text)})
					If ValType(aChave) == "A" .And. Len(aChave) >= 2
						cChaveNF :=	Padr(AllTrim(aChave[1]),TamSX3("F1_DOC")[1],) +;
										Padr(AllTrim(aChave[2]),TamSX3("F1_SERIE")[1])   
					EndIf
				EndIf
					
				If lNFOriBen
					//Busca documento de origem (beneficiamento)
					aDadosCli := A116IFOCL(aDadosAux,"C")
					
					If aDadosCli[1] .And. !Empty(aDadosCli[2])
						If SF1->(dbSeek(xFilial("SF1")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
							cCodiRem := aDadosCli[2]
							cLojaRem := aDadosCli[3]
						Else
							If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
								lDSNotF1 := .T.
							Endif
							lRet := .F.
						EndIf
					Else
						lRet := .F.
					Endif
				Else
					//N�o sabe o tipo do documento, busca pelo fornecedor e/ou cliente
					aDadosFor := A116IFOCL(aDadosAux,"F")
					
					If aDadosFor[1] .And. !Empty(aDadosFor[2])
						If SF1->(dbSeek(xFilial("SF1")+cChaveNF+aDadosFor[2]+aDadosFor[3]))
							cCodiRem := aDadosFor[2]
							cLojaRem := aDadosFor[3]
						Else
							If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosFor[2]+aDadosFor[3]))
								lDSNotF1 := .T.
							Endif
							lRet := .F.
						EndIf
					Else
						lRet := .F.
					Endif
					
					If !lRet
						aDadosCli := A116IFOCL(aDadosAux,"C")
					
						If aDadosCli[1] .And. !Empty(aDadosCli[2])
							If SF1->(dbSeek(xFilial("SF1")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
								cCodiRem := aDadosCli[2]
								cLojaRem := aDadosCli[3]
								lRet := .T.
							Else
								If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
									lDSNotF1 := .T.
								Endif
								lRet := .F.
							EndIf
						Else
							lRet := .F.
						Endif
					Endif
				Endif
				
				If !lRet
					// Tratamento para CTe de devolucao quando o cliente nao aceitou receber a mercadoria
					// Neste caso a transportadora emite um novo CTe referenciando as notas de venda, as notas estarao em SF2 e nao SF1
					cChaveNF :=	Padr(AllTrim(aAux[nX]:_nDoc:Text),TamSX3("F2_DOC")[1]) +;
									Padr(AllTrim(aAux[nX]:_Serie:Text),TamSX3("F2_SERIE")[1])
									
					aDadosCli := A116IFOCL(aDadosAux,"C") 
					
					If aDadosCli[1] .And. !Empty(aDadosCli[2])
						If SF2->(dbSeek(xFilial("SF2")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
							cCodiRem := aDadosCli[2]
							cLojaRem := aDadosCli[3]
						
							aErros	:= {}
							cTipoFrete := "F"
							lDevSemSF1 := .T.
						Else
							If SDS->(dbSeek(xFilial("SDS")+cChaveNF+aDadosCli[2]+aDadosCli[3]))
								lDSNotF1 := .T.
							Endif
						EndIf
					Else
						lRet := .F.
					Endif
				Endif
				
				//-- Registra notas que farao parte do conhecimento
				If lRet
					nTamFil := Len(xFilial("SF1"))
					cChvNFOr := SF1->F1_CHVNFE
					aAdd(aItens116,{{"PRIMARYKEY",SubStr(SF1->&(IndexKey()),nTamFil+1), cCodiRem, cLojaRem}})
					
					lDevBen	:= SF1->F1_TIPO $ "D*B"
					
					If lDevBen
						nDevOri++
					Else
						nNorOri++
					Endif
					
					If nDevOri > 0 .And. nNorOri > 0
						lFornOri := .F.
					Endif
				Else
					If lT3Exped .Or. lT4DifDest .Or. lT3Remet .Or. lDevSemSF1
						lRet := .T.
					Else
						If lDSNotF1
							If lJob
								aAdd(aErros,{cFile,"COM044 - " + 'STR0011' + cChaveNF + 'STR0049','STR0050'}) //"Documento de entrada existente no monitor. Processe o recebimento deste documento de entrada para importar o CTE corretamente"
							Else
								Aviso('STR0001','STR0011' + cChaveNF + 'STR0049' + 'STR0050',2,"ImpXML_CTe") //"Documento de entrada existente no monitor. Processe o recebimento deste documento de entrada para importar o CTE corretamente"
							EndIf
							aAdd(aErroErp,{cFile,"COM044"})							
						Else
							If lJob
								aAdd(aErros,{cFile,"COM020 - " + 'STR0011' + cChaveNF + 'STR0007','STR0012'}) //"Documento de entrada inexistente na base. Processe o recebimento deste documento de entrada."
							Else
								Aviso('STR0001','STR0011' + cChaveNF + 'STR0007' + 'STR0012',2,"ImpXML_CTe") //"Documento de entrada inexistente na base. Processe o recebimento deste documento de entrada."
							EndIf
							aAdd(aErroErp,{cFile,"COM020"})
						Endif
						Exit
					Endif
				EndIf
			Next nX
		Endif
	Endif
	
	If lRet .And. Len(aAux1) > 0
		For nX := 1 To Len(aAux1)
			lDSNotF1 := .F.
			SDS->(dbSetOrder(2))
			
			SF1->(dbSetOrder(8))
			
			If ValType(XmlChildEx(aAux1[nX],"_CHAVE")) == "O"
				cChaveNF := Padr(AllTrim(aAux1[nX]:_chave:Text),TamSX3("F1_CHVNFE")[1])
			ElseIf ValType(XmlChildEx(aAux1[nX],"_CHCTE")) == "O"
				cChaveNF := Padr(AllTrim(aAux1[nX]:_chCTE:Text),TamSX3("F1_CHVNFE")[1])
			EndIf
				cChaveXML := cChaveNF
			//Verifica exist�ncia da nota
			If SF1->(dbSeek(xFilial("SF1")+cChaveNF)) .And. !Empty(cChaveNF) //Se nota existir, preenche informa��es do Remetente com dados da nota
				cCodiRem 	:= SF1->F1_FORNECE
				cLojaRem 	:= SF1->F1_LOJA
				lDevBen	:= SF1->F1_TIPO $ "D*B"
				
				If lDevBen
					nDevOri++
				Else
					nNorOri++
				Endif
				
				//Se as chaves nfe pertencem a mais de 1 fornecedor, ao incluir o documento de frente
				//n�o deve ser filtrado pelo fornecedor.
				nPFornChv := aScan(aFornChv,{|x| x == cCodiRem+cLojaRem})
				If nPFornChv == 0
					aAdd(aFornChv,cCodiRem+cLojaRem)
				Endif
				
				If (nDevOri > 0 .And. nNorOri > 0) .Or. Len(aFornChv) > 1
					lFornOri := .F.
				Endif
				
				//-- Registra notas que farao parte do conhecimento
				SF1->(dbSetOrder(1))
				nTamFil := Len(xFilial("SF1"))
				cChvNFOr := SF1->F1_CHVNFE
				aAdd(aItens116,{{"PRIMARYKEY",SubStr(SF1->&(IndexKey()),nTamFil+1), cCodiRem, cLojaRem}})
			Else
				If SDS->(dbSeek(xFilial("SDS")+cChaveNF))
					lDSNotF1 := .T.
					lRet	 := .F.
					If lJob
						aAdd(aErros,{cFile,"COM044 - " + 'STR0011' + cChaveNF + " existente no monitor.","Processe o recebimento deste documento de entrada para importar o CTE corretamente"}) //"Documento de entrada existente no monitor. Processe o recebimento deste documento de entrada para importar o CTE corretamente"
					Else
						Aviso('STR0001','STR0011' + cChaveNF + " existente no monitor." + "Processe o recebimento deste documento de entrada para importar o CTE corretamente",2,"ImpXML_CTe") //"Documento de entrada existente no monitor. Processe o recebimento deste documento de entrada para importar o CTE corretamente"
					EndIf
					aAdd(aErroErp,{cFile,"COM044"})
					Exit
				Endif
				
				SF2->(dbClearFilter())
				SF2->(dbSetOrder(1))
				
				cQuery := " SELECT R_E_C_N_O_ AS RECNO "
				cQuery += " FROM " + RetSqlName("SF2") + " SF2 "
				cQuery += " WHERE D_E_L_E_T_ = ' ' AND"
				cQuery += " F2_CHVNFE = '" + cChaveNF + "' AND"
				cQuery += " F2_FILIAL = '" + xFilial("SF2") + "'" 	
				cQuery := ChangeQuery(cQuery)
				
				if select('TMP59') > 0 
					TMP59->(DbCloseArea())
				endif 

				dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),"TMP59", .T., .T.)

				TMP59->(dbGoTop())
				If TMP59->(!Eof())
					aErros	:= {}
					cTipoFrete := "F"
					lDevSemSF1 := .T.
				EndIf
				TMP59->(dbCloseArea())
			EndIf			
		Next nX
	Endif

	If lDevBen .And. !lRateiaDev
		aItens116 := {}
	EndIf

	If lToma4 .And. Len(aItens116) == 0 
		lToma4NFOri := .F.
	Endif

	If lRet .And. (Len(aAux1) == 0 .And. Len(aAux) == 0) .Or. lDevSemSF1 .Or. Len(aItens116) == 0
		//-- Ponto de entrada para mudar o produto frete
 		If ExistBlock("A116PRDF")
			cPrdFrete := ExecBlock("A116PRDF",.F.,.F.,{oXML})
			If ValType(cPrdFrete) <> "C" .Or. !SB1->(dbSeek(xFilial("SB1")+cPrdFrete))
				cPrdFrete := SuperGetMV("MV_XMLPFCT",.F.,"")
				If At(";",cPrdFrete) > 0
					cPrdFrete := SubStr(cPrdFrete,1,(At(";",cPrdFrete)-1))
				EndIf
				cPrdFrete := PadR(cPrdFrete,TamSX3("B1_COD")[1])
			EndIf
		Elseif lA116ITPCT
			aParamPE := If(Len(aAux) > 0,aAux,aAux1)
			cRetPrd := ExecBlock("A116ITPCT",.F.,.F.,{aParamPE, oXML})
			If ValType(cRetPrd) == "C" .And. SB1->(dbSeek(xFilial("SB1")+cRetPrd))
				cPrdFrete := PadR(cRetPrd,TamSX3("B1_COD")[1])
				cTipoFrete := "F"
				lRemet := .T.
			Else
				cPrdFrete := SuperGetMV("MV_XMLPFCT",.F.,"")
				If At(";",cPrdFrete) > 0
					cPrdFrete := SubStr(cPrdFrete,1,(At(";",cPrdFrete)-1))
				EndIf
				cPrdFrete := PadR(cPrdFrete,TamSX3("B1_COD")[1])	
			EndIf
		Else
			cPrdFrete := SuperGetMV("MV_XMLPFCT",.F.,"")
			If At(";",cPrdFrete) > 0
				cPrdFrete := SubStr(cPrdFrete,1,(At(";",cPrdFrete)-1))
			EndIf
			cPrdFrete := PadR(cPrdFrete,TamSX3("B1_COD")[1])
		EndIf
	
		If Empty(cPrdFrete) .Or. !SB1->(dbSeek(xFilial("SB1")+cPrdFrete))
			If lJob
				aAdd(aErros,{cFile,"COM023 - " + 'STR0017','STR0018'}) //"Produto frete n�o informado no par�metro MV_XMLPFCT ou inexistente no cadastro correspondente."#"Verifique a configura��o do par�metro."
			Else
				Aviso('STR0001','STR0017'+'STR0018',2,"ImpXML_CTe")
			EndIf
			aAdd(aErroErp,{cFile,"COM023"})
			lRet := .F.
		Else
			cTipoFrete := "F"
			lRemet := .T.
		EndIf
	Endif
	
	//-- Obtem TES e cond. pagto para utilizacao no CT-e
	CTe_RetTES(oXML,@cTES_CT,@cCPag_CT)
	
	//-- Valida existencia da TES a utilizar no CTe
	If lRet .And. !lPreNota .And. (Empty(cTES_CT) .Or. !SF4->(dbSeek(xFilial("SF4")+cTES_CT))) 
 		If lJob
			aAdd(aErros,{cFile,"COM021 - " + 'STR0013','STR0014'}) //"TES n�o informada no par�metro MV_XMLTECT ou inexistente no cadastro correspondente."
		Else
			Aviso('STR0001','STR0013' + 'STR0014',2,"ImpXML_CTe")
		EndIf
		aAdd(aErroErp,{cFile,"COM021"})
		lRet := .F.
	EndIf
	
	//-- Valida se o TES esta desbloqueado.
	If lRet
		If SF4->F4_MSBLQL == '1' .And. !lPreNota // TES bloqueado.
	 		If lJob
				aAdd(aErros,{cFile,"COM029 - " + 'STR0035' + SF4->F4_CODIGO,'STR0036'}) //"TES bloqueado. Codigo: "#"Verifique a configura��o do cadastro."
			Else
				Aviso('STR0001','STR0035' + SF4->F4_CODIGO + Space(1) + 'STR0036',2,"ImpXML_CTe")
			EndIf
			aAdd(aErroErp,{cFile,"COM029"})
			lRet := .F.
		EndIf
	EndIf
	
	//-- Se TES gera dup., valida existencia da cond. pgto a utilizar no CTe
	If lRet .And. SF4->F4_DUPLIC == "S" .And. (Empty(cCPag_CT) .Or. !SE4->(dbSeek(xFilial("SE4")+cCPag_CT)))
 		If lJob
			aAdd(aErros,{cFile,"COM022 - " + 'STR0015','STR0016'}) //"Condi��o de pagamento n�o informada no par�metro MV_XMLCPCT ou inexistente no cadastro correspondente.Verifique a configura��o do par�metro.
		Else
			Aviso('STR0001','STR0015'+'STR0016',2,"ImpXML_CTe") //"Condi��o de pagamento n�o informada no par�metro MV_XMLCPCT ou inexistente no cadastro correspondente.Verifique a configura��o do par�metro.
		EndIf
		lRet := .F.
		aAdd(aErroErp,{cFile,"COM022"})
	EndIf
Endif

// Verifica exist�ncia de documento com mesma numera��o.
If lRet	
	cDoc	:= PadL(oXML:_InfCte:_Ide:_nCt:Text,TamSx3("F1_DOC")[1],"0")
	cSerie	:= PadR(oXML:_InfCte:_Ide:_Serie:Text,SerieNfId("SF1",6,"F1_SERIE")) 
	
	If ExistBlock("A116IDOC") //Manipula numero e serie do documento
		aA116IDOC := ExecBlock("A116IDOC",.F.,.F.,{cDoc, cSerie, cFornCTe, cLojaCTe})
		If ValType(aA116IDOC) == 'A' .And. Len(aA116IDOC) >= 2
			cDoc	:= aA116IDOC[1]
			cSerie := aA116IDOC[2]
		EndIf
	EndIf
	
	dbSelectArea("SDS")
	dbSetorder(1)
	If msSeek(xFilial("SDS")+cDoc+cSerie+cFornCTe+cLojaCTe)
		If lJob
			aAdd(aErros,{cFile,"COM025 - " + 'STR0026'+" "+cDoc+", "+'STR0027'+" "+cSerie+" , "+'STR0028','STR0029'}) //Documento cdoc, serie cSerie, j� cadastrado. verifique se o documento j� foi importado para o ERP. 
		Else
			Aviso('STR0001','STR0026'+" "+cDoc+", "+'STR0027'+""+cSerie+" , "+'STR0028'+" "+'STR0029',2,"ImpXML_CTe")
		EndIf
		aAdd(aErroErp,{cFile,"COM025"})
		lRet := .F.
	EndIf
EndIf

If lRet
	//-- Separa secao que contem as notas do conhecimento para laco
	If ValType(XmlChildEx(oXML:_InfCte,"_INFCTENORM")) <> "U"
	 	aAux := If(ValType(oXML:_InfCte:_InfCteNorm:_InfCarga:_InfQ) == "O",{oXML:_InfCte:_InfCteNorm:_InfCarga:_InfQ},oXML:_InfCte:_InfCteNorm:_InfCarga:_InfQ)
	EndIf
 	For nX := 1 To Len(aAux)
		If Upper(AllTrim(aAux[nX]:_TPMED:Text)) == "PESO BRUTO"
			nPesoBruto := Val(aAux[nX]:_QCARGA:Text)
		EndIf
		If Upper(AllTrim(aAux[nX]:_TPMED:Text)) == "PESO L�QUIDO"
			nPesoLiqui := Val(aAux[nX]:_QCARGA:Text)
		EndIf
		If !("PESO" $ Upper(aAux[nX]:_TPMED:Text)) .And. Len(aEspecVol) < 5
			aAdd(aEspecVol,{AllTrim(aAux[nX]:_TPMED:Text),Val(aAux[nX]:_QCARGA:Text)})
		EndIf
	Next nX
	
	// Apuracao do ICMS para as diversas situacoes tributarias
	If ValType(XmlChildEx(oXML:_InfCte:_imp,"_ICMS")) <> "U"
		If ( oICMS := oXML:_INFCTE:_IMP:_ICMS ) != Nil
			If ( oICMSTipo := XmlGetChild( oICMS, 1 )) != Nil
				For nZ := 1 To 5	// O nivel maximo para descer dentro da tag que define o tipo do ICMS para obter tanto base quanto valor � 5, conforme manual de orientacao do CTe
					If ( oICMSNode := XmlGetChild( oICMSTipo, nZ )) != Nil
						If "vBC" $ oICMSNode:REALNAME
							nBaseICMS := Val(oICMSNode:TEXT)
							lBaseICMS := .T.
						ElseIf "vICMS" $ oICMSNode:REALNAME
							nValICMS := Val(oICMSNode:TEXT)
							lValICMS := .T.
						ElseIf "pICMS" $ oICMSNode:REALNAME
							nAliqICMS := Val(oICMSNode:TEXT)
							lAliqICMS := .T.
						EndIf
						If lBaseICMS .And. lValICMS .And. lAliqICMS
							Exit
						EndIf
					EndIf
				Next nZ
			EndIf
		EndIf
		
		lRet := !COMXIMPVLD({{nAliqICMS,">=100"}})
		If !lRet
			If lJob
				aAdd(aErros,{cFile,"COM043 - " + 'STR0046','STR0047'}) 
			Else
				Aviso('STR0001',"COM043 - " + 'STR0046',2,"ImpXML_CTe")
			EndIf
			aAdd(aErroErp,{cFile,"COM043"})
		Endif
	EndIf
Endif

// Ponto de entrada utilizado para manipular a gravacao dos itens da nota de frete
// Exemplo: Desmembrar o valor do frete entre seus componentes (tags <vPrest><Comp>)
If lRet .And. lA116iComp
	If (lRemet .And. !lToma4) .Or. lDevSemSF1 .Or. (lToma4 .And. !lToma4NFOri)
		aItensComp := ExecBlock("A116ICOMP",.F.,.F.,{oXml})
		If ValType(aItensComp) == "A" .And. Len(aItensComp) > 0
			For nX := 1 To Len(aItensComp)
				If	aScan(aItensComp[nX],{|x| x[1] == "DT_ITEM" })	> 0 .And.;
					aScan(aItensComp[nX],{|x| x[1] == "DT_COD" })	> 0 .And.;
					aScan(aItensComp[nX],{|x| x[1] == "DT_VUNIT" })	> 0 .And.;
					aScan(aItensComp[nX],{|x| x[1] == "DT_TOTAL" })	> 0
					lRet := .T.
				Else
					lRet := .F.
					Exit
				EndIf
			Next nX
		Else
			lRet := .F.
		EndIf
		If !lRet
			aAdd(aErros,{cFile,"COM032 - A116ICOMP - " + 'STR0039', 'STR0040'}) //"Retorno do ponto de entrada A116ICOMP inconsistente." - "Verifique a documentacao do mesmo no portal TDN."
			aAdd(aErroErp,{cFile,"COM032"})
		EndIf
	EndIf
EndIf

//Valor do frete
If lRet .And. ValType(XmlChildEx(oXML:_InfCte,"_VPREST")) <> "U"
	If ValType(XmlChildEx(oXML:_InfCte:_vPrest,"_VREC")) <> "U" .And. ValType(XmlChildEx(oXML:_InfCte:_vPrest,"_VTPREST")) <> "U"
		If Val(oXML:_InfCte:_VPrest:_VRec:Text) == 0 .And. Val(oXML:_InfCte:_VPrest:_vTPrest:Text) == 0
			
			If cTpCte == "1" //Complemento de imposto, pois n�o tem valor de presta��o e valor recolhido.
				lRet := .F.
				aAdd(aErros,{cFile,"COM047 - Complemento de imposto n�o � tratado pelo Totvs Colabora��o/Importador. Gere o documento manualmente.", 'STR0040'})
				aAdd(aErroErp,{cFile,"COM047"})
			Endif
			
			lVlrFrtZero := .T.
		Elseif Val(oXML:_InfCte:_VPrest:_VRec:Text) == 0 .And. Val(oXML:_InfCte:_VPrest:_vTPrest:Text) > 0
			nVlrFrt := Val(oXML:_InfCte:_VPrest:_vTPrest:Text)
			lVlrFrtZero := .T.
		Else
			nVlrFrt := Val(oXML:_InfCte:_VPrest:_VRec:Text)
		Endif
	Endif
Endif

If lRet
	//Estado de origem do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_UFINI")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_UFIni:Text)
		cUFOrigem := oXML:_InfCte:_Ide:_UFIni:Text		
	Else
		cUFOrigem := Posicione("SA2",1,xFilial("SA2")+cFornCTe+cLojaCTe,"A2_EST")
	Endif
	
	//Codigo Municipio de origem do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_CMUNINI")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_cMunIni:Text)
		cMunOrigem := SubStr(oXML:_InfCte:_Ide:_cMunIni:Text,3,5)
	Endif
	
	//Estado de destino do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_UFFIM")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_UFFim:Text)
		cUFDestino := oXML:_InfCte:_Ide:_UFFim:Text
	Endif
	
	//Codigo Municipio de destino do CTE
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_CMUNFIM")) <> "U" .And. !Empty(oXML:_InfCte:_Ide:_cMunFim:Text)
		cMunDestin := SubStr(oXML:_InfCte:_Ide:_cMunFim:Text,3,5)
	Endif
Endif

If lRet
	Begin Transaction

	//-- Grava cabeca do conhecimento de transporte
	RecLock("SDS",.T.)
	SDS->DS_FILIAL	:= xFilial("SDS")																					// Filial			
    SDS->DS_CNPJ		:= cCNPJ_CT																						// CGC
    SDS->DS_DOC		:= cDoc																							// Numero do Documento
    SDS->DS_FORNEC	:= cFornCTe																						// Fornecedor do Conhecimento de transporte
    SDS->DS_LOJA		:= cLojaCTe																						// Loja do Fornecedor do Conhecimento de transporte
    SDS->DS_EMISSA	:= StoD(StrTran(AllTrim(oXML:_InfCte:_Ide:_Dhemi:Text),"-",""))							// Data de Emiss�o
    SDS->DS_EST		:= cUFOrigem																			// Estado de emissao da NF
    SDS->DS_TIPO		:= "T"													 											// Tipo da Nota
    SDS->DS_FORMUL	:= "N" 																							// Formulario proprio
    SDS->DS_ESPECI	:= "CTE"															  								// Especie
    SDS->DS_ARQUIVO	:= AllTrim(cFile)																					// Arquivo importado
    SDS->DS_CHAVENF	:= Right(AllTrim(oXML:_InfCte:_Id:Text),44)													// Chave de Acesso da NF
    SDS->DS_VERSAO	:= AllTrim(oXML:_InfCte:_Versao:Text) 															// Vers�o
    SDS->DS_USERIMP	:= If(!lJob,cUserName,"TOTVS Colabora��o")													// Usuario na importacao
    SDS->DS_DATAIMP	:= dDataBase																						// Data importacao do XML
    SDS->DS_HORAIMP	:= SubStr(Time(),1,5)																			// Hora importacao XML
    SDS->DS_VALMERC	:= nVlrFrt																	  						// Valor Mercadoria
    SDS->DS_TPFRETE	:= cTipoFrete																						// Tipo de Frete
    SDS->DS_PBRUTO	:= nPesoBruto																						// Peso Bruto
    SDS->DS_PLIQUI	:= nPesoLiqui																						// Peso Liquido
    
    For nX := 1 To Len(aEspecVol)
    	If SDS->(ColumnPos("DS_ESPECI" +Str(nX,1))) > 0
		    SDS->&("DS_ESPECI" +Str(nX,1)) := aEspecVol[nX,1]							 							// Especie
			SDS->&("DS_VOLUME" +Str(nX,1)) := aEspecVol[nX,2]							 							// Volume
		EndIf
	Next nX
	
	If SDS->(ColumnPos("DS_BASEICM")) > 0 .And. lBaseICMS
		SDS->DS_BASEICM := nBaseICMS
	EndIf
	
	If SDS->(ColumnPos("DS_VALICM")) > 0 .And. lValICMS
		SDS->DS_VALICM := nValICMS
	EndIf
	
	If SDS->(ColumnPos('DS_TPCTE')) > 0
		SDS->DS_TPCTE := TIPOCTE116(oXML:_InfCte:_Ide:_tpCTe:Text)
	EndIf
	
	//Chave da Nota de Origem
    If SDS->(ColumnPos("DS_CHVNFOR")) > 0
    	SDS->DS_CHVNFOR		:= cChvNFOr
    Endif
    
   	If !Empty(cHrEmis)
		SDS->DS_HORNFE := cHrEmis
	Endif
	
	//Valor a pagar igual a 0
	If lVlrFrtZero
		SDS->DS_FRETE := Iif(nVlrFrt==0,1,nVlrFrt)
	Endif
	
	If ValType(XmlChildEx(oXML:_InfCte:_Ide,"_MODAL")) <> "U" .And. SDS->(ColumnPos("DS_MODAL")) > 0
		SDS->DS_MODAL := AllTrim(oXML:_InfCte:_Ide:_modal:Text)
	Endif
	
	//Estado/Municipio de Origem e Destino do CTE
	If SDS->(ColumnPos("DS_UFDESTR")) > 0 .And. SDS->(ColumnPos("DS_MUDESTR")) > 0 .And. SDS->(ColumnPos("DS_UFORITR")) > 0 .And. SDS->(ColumnPos("DS_MUORITR")) > 0
		SDS->DS_UFDESTR := cUFDestino
		SDS->DS_MUDESTR := cMunDestin
		SDS->DS_UFORITR := cUFOrigem
		SDS->DS_MUORITR := cMunOrigem
	Endif
	
	SerieNfId("SDS",1,"DS_SERIE",SDS->DS_EMISSA,SDS->DS_ESPECI,cSerie) 
	
	SDS->(MsUnlock())
	
	//Se for remetente mas a notas de origem existem no sistema
	If (lRemet .And. !lToma4) .And. Len(aItens116) > 0
		lRemet := .F.
	Endif
	
	//-- Grava itens do conhecimento de transporte
	If (lRemet .And. !lToma4) .Or. lDevSemSF1 .Or. (lToma4 .And. !lToma4NFOri)
		If lA116iComp
			For nX := 1 To Len(aItensComp)
				RecLock("SDT",.T.)
				SDT->DT_FILIAL:= xFilial("SDT")
				SDT->DT_DOC	:= cDoc
				SDT->DT_SERIE	:= cSerie
				SDT->DT_FORNEC:= cFornCTe
				SDT->DT_LOJA	:= cLojaCTe
				SDT->DT_CNPJ	:= SDS->DS_CNPJ
				SDT->DT_QUANT	:= 1
				For nY := 1 To Len(aItensComp[nX])
					&("SDT->"+AllTrim(aItensComp[nX][nY][1])) := aItensComp[nX][nY][2]
				Next nY
				SDT->(MsUnlock())
			Next nX
		Else
			RecLock("SDT",.T.)
			SDT->DT_FILIAL		:= xFilial("SDT")													// Filial
			SDT->DT_ITEM			:= StrZero(1,TamSX3("DT_ITEM")[1])						   		// Item
			SDT->DT_COD			:= cPrdFrete														// Codigo do produto
			SDT->DT_FORNEC		:= cFornCTe														// Forncedor
			SDT->DT_LOJA			:= cLojaCTe														// Loja
			SDT->DT_DOC			:= cDoc															// Docto
			SDT->DT_SERIE			:= cSerie 					   										// Serie
			SDT->DT_CNPJ			:= SDS->DS_CNPJ													// Cnpj do Fornecedor
			SDT->DT_QUANT			:= 1																// Quantidade
			SDT->DT_VUNIT			:= Val(oXML:_InfCte:_VPrest:_VRec:Text)						// Valor Unit�rio 			
			SDT->DT_TOTAL			:= Val(oXML:_InfCte:_VPrest:_VRec:Text)				   		// Vlor Total 			 	

			If SDT->(FieldPos("DT_PICM")) > 0 .And. lAliqICMS
				SDT->DT_PICM := nAliqICMS
			EndIf
			
			If lUnidMed
				SDT->DT_UM		:= GetAdvFVal("SB1","B1_UM",xFilial("SB1")+cPrdFrete,1)
				SDT->DT_SEGUM	:= GetAdvFVal("SB1","B1_SEGUM",xFilial("SB1")+cPrdFrete,1)
				
				If !Empty(SDT->DT_SEGUM)
					SDT->DT_QTSEGUM := ConvUM(cPrdFrete,1,1,2)
				Endif
			Endif
			SDT->(MsUnlock())
		EndIf
		If lMt116XmlCt
			ExecBlock("Mt116XmlCt",.F.,.F.,{oXML,cDoc,cSerie,cFornCTe,cLojaCTe,"T","PF"})
		EndIf
	Else

		aTESCON := tescond()
	
		aAdd(aCabec116,{"",dDataBase-90})       												// Data inicial para filtro das notas
		aAdd(aCabec116,{"",dDataBase})          												// Data final para filtro das notas
		aAdd(aCabec116,{"",2})                  												// 2-Inclusao ; 1=Exclusao
		aAdd(aCabec116,{"",Space(TamSx3("F1_FORNECE")[1])}) 									// Rementente das notas contidas no conhecimento
		aAdd(aCabec116,{"",Space(TamSx3("F1_LOJA")[1])})
		aAdd(aCabec116,{"",IIf(lDevBen,2,1)})                  												// Tipo das notas contidas no conhecimento: 1=Normal ; 2=Devol/Benef
		aAdd(aCabec116,{"",2})                  												// 1=Aglutina itens ; 2=Nao aglutina itens
		aAdd(aCabec116,{"F1_EST",""})  		  													// UF das notas contidas no conhecimento
		aAdd(aCabec116,{"",nVlrFrt}) 															// Valor do conhecimento
		aAdd(aCabec116,{"F1_FORMUL",1})															// Formulario proprio: 1=Nao ; 2=Sim
		aAdd(aCabec116,{"F1_DOC",PadR(oXML:_InfCte:_Ide:_nCt:Text,TamSx3("F1_DOC")[1])})		// Numero da nota de conhecimento
		aAdd(aCabec116,{"F1_SERIE",PadR(oXML:_InfCte:_Ide:_Serie:Text,SerieNfId("SF1",6,"F1_SERIE"))})	// Serie da nota de conhecimento
		aAdd(aCabec116,{"F1_FORNECE",cFornCTe}) 												// Fornecedor da nota de conhecimento
		aAdd(aCabec116,{"F1_LOJA",cLojaCTe})													// Loja do fornecedor da nota de conhecimento
		aAdd(aCabec116,{"",aTESCON[1]})															// TES a ser utilizada nos itens do conhecimento
		aAdd(aCabec116,{"F1_BASERET",nBaseICMS})												// Valor da base de calculo do ICMS retido
		aAdd(aCabec116,{"F1_ICMRET",nValICMS})													// Valor do ICMS retido
		aAdd(aCabec116,{"F1_COND",aTESCON[2]})											   		// Condicao de pagamento
		aAdd(aCabec116,{"F1_EMISSAO",SToD(Substr(StrTran(oXML:_InfCte:_Ide:_dhEmi:Text,"-",""),1,8))}) // Data de emissao do conhecimento
		aAdd(aCabec116,{"F1_ESPECIE","CTE"})															 // Especie do documento
		aadd(aCabec116,{"E2_NATUREZ",""})  	
		
		If SDT->(FieldPos("DT_PICM")) > 0 .And. lAliqICMS
			aAdd(aCabec116,{"DT_PICM",nAliqICMS})
		EndIf												   				 // Chave para tratamentos especificos
		
		If SDS->(ColumnPos('DS_TPCTE')) > 0
			aAdd(aCabec116,{"F1_TPCTE",TIPOCTE116(oXML:_InfCte:_Ide:_tpCTe:Text)})				// Tipo do CT-e.
		EndIf
		
		//���������������������������������������������������������������������������������Ŀ
		//� Executa a ExecAuto do MATA116 para gravar os itens com o valor de frete rateado �
		//�����������������������������������������������������������������������������������
		lMsErroAuto    := .F.
		lAutoErrNoFile := .T.
		MsExecAuto({|x,y,z| MATA116(x,y,,z)},aCabec116,aItens116,lPreNota)
		
		If lMsErroAuto
			//-- Desfaz transacao
			lSF1 := .T.
			DisarmTran()
	 		If lJob
	 			aAux := GetAutoGRLog()
	 			For nX := 1 To Len(aAux)
		 			cError += aAux[nX]
		 		Next nX
		 		aAdd(aErros,{cFile,"COM024 - " + cError,'STR0019'}) //"Corrija a inconsist�ncia apontada no log."
			Else
				MostraErro()
			EndIf
			aAdd(aErroErp,{cFile,"COM024"})
			lRet := .F.
		Else
			If lMt116XmlCt
				ExecBlock("Mt116XmlCt",.F.,.F.,{oXML,cDoc,cSerie,cFornCTe,cLojaCTe,"T","PN"})
			EndIf
		EndIf
	EndIf
	
	End Transaction
EndIf

If lRet
	aAdd(aProc,{	PadL(oXML:_InfCte:_Ide:_nCt:Text,TamSx3("F1_DOC")[1],"0"),;
					PadR(oXML:_InfCte:_Ide:_Serie:Text,SerieNfId("SF1",6,"F1_SERIE")),;
					cNomeCTe})
Endif

oXML := Nil
DelClassIntf()

DisarmTran()
Return lRet

Static Function A116ICLIFOR(c116CNPJ,c116INSC,c116Alias,aFilDest,oXML,cBusca116)

Local aRet			:= {}
Local aAuxFor		:= {}
Local aArea			:= {}
Local c116Fil		:= xFilial(c116Alias)
Local cCodigo		:= ""
Local cLoja			:= ""
Local cNomeFor		:= ""
Local nQCNPJ		:= 0
Local nQINSC		:= 0
Local lAchoCliFor	:= .F.
Local lCliente		:= .F.
Local nBloq			:= 0
Local lBloq      	:= .F.
Local cCodBlq		:= ""
Local cLojBlq		:= ""
Local cNomFBlq		:= ""
Local cCliFor		:= ""

Default cBusca116 := ''

aArea := GetArea()

If c116Alias == "SA2"
	cCliFor := "F"
Elseif c116Alias == "SA1"
	cCliFor := "C"
Endif

DbSelectArea(c116Alias)
(c116Alias)->(DbSetOrder(3))

If (c116Alias)->(DbSeek(c116Fil+c116CNPJ))
	While !(c116Alias)->(EOF()) .And. c116CNPJ $ A140ICAES((c116Alias)->&(Substr(c116Alias,2,2)+"_CGC"))
		// Validacao para cliente ou fornecedor bloqueado.
		If (c116Alias)->&(Substr(c116Alias,2,2)+"_MSBLQL") == "1" .And. (A140INSC(c116INSC,(c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")) .Or. Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))))
			cCodBlq		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
			cLojBlq		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
			cNomFBlq	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
			nBloq++
		ElseIf (c116Alias)->&(Substr(c116Alias,2,2)+"_MSBLQL") <> "1" .And. (A140INSC(c116INSC,(c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")) .Or. Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))))
			If A140INSC(c116INSC,(c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR"))  
				cCodigo 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
				cLoja   	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
				cNomeFor 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
				nQCNPJ++
				nQINSC++
				lAchoCliFor := .T.
			ElseIf Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))) .And. nQINSC = 0
				cCodigo	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
				cLoja		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
				cNomeFor 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
				nQCNPJ++
				lAchoCliFor := .T.
			ElseIf Empty(A140ICAES(Alltrim((c116Alias)->&(Substr(c116Alias,2,2)+"_INSCR")))) .And. nQINSC <> 0 
				nQCNPJ++
				lAchoCliFor := .T.
			ElseIf Empty(c116INSC) // Caso nao tenha a TAG <IE> no XML ou a mesma venha vazia.
				cCodigo		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_COD")
				cLoja		:= (c116Alias)->&(Substr(c116Alias,2,2)+"_LOJA")
				cNomeFor 	:= (c116Alias)->&(Substr(c116Alias,2,2)+"_NOME")
				nQCNPJ++
				lAchoCliFor := .T.
			EndIf
		EndIf
		(c116Alias)->(DbSkip())
	EndDo
	
	If nQCNPJ > 1 .And. nQINSC <> 1 .And. ExistBlock("A116IFor")
		aAuxFor := ExecBlock("A116IFor",.F.,.F.,{oXML, c116Alias, cBusca116})
		If	Len(aAuxFor) == 3 .And.;
			ValType(aAuxFor[1]) == "C" .And. !Empty(aAuxFor[1]) .And. (aAuxFor[1] $ "SA1#SA2") .And.;
			ValType(aAuxFor[2]) == "C" .And. !Empty(aAuxFor[2]) .And.;
			ValType(aAuxFor[3]) == "C" .And. !Empty(aAuxFor[3])
			
			c116Alias	:= aAuxFor[1]
			cCodigo		:= Padr(aAuxFor[2],TamSX3("A2_COD")[1])
			cLoja		:= Padr(aAuxFor[3],TamSX3("A2_LOJA")[1])
			
			If c116Alias == "SA1" .Or. c116Alias == "SA2"
				(c116Alias)->(dbSetOrder(1))
				If (c116Alias)->(dbSeek(xFilial(c116Alias)+cCodigo+cLoja))
					If c116Alias $ "SA1"
						cCodigo		:= SA1->A1_COD
						cLoja		:= SA1->A1_LOJA
						lCliente	:= .T.
						nQCNPJ 		:= 1
						nQINSC 		:= 1
						lAchoCliFor	:= .T.
					Else
						cCodigo		:= SA2->A2_COD
						cLoja		:= SA2->A2_LOJA
						nQCNPJ		:= 1
						nQINSC		:= 1
						lAchoCliFor	:= .T.
					EndIf
				EndIf
			EndIf
		EndIf
	Endif
Endif

If !lAchoCliFor .And. nBloq > 0
	lBloq		:= .T.
	cCodigo	:= cCodBlq
	cLoja		:= cLojBlq
Endif

If (nQCNPJ > 1 .And. nQINSC <> 1) .Or. lBloq
	aAdd(aRet,{.F.,cCodigo,cLoja,lCliente,lBloq,cCliFor})
ElseIf lAchoCliFor
	aAdd(aRet,{.T.,cCodigo,cLoja,lCliente,lBloq,cCliFor})
EndIf

RestArea(aArea)

Return aRet

User Function IMPXML2()    //    [4.0]
*****************************************************************
//������������������������������������Ŀ
//� Seleciona XML - Pasta\Email        |
//� Chamada -> Rotina Carrega XML      �
//��������������������������������������
Local	aAlias4  := GetArea()
Local  cArqImp    := ' '
Local  aProc	:= {}
Local  aErros:= {}
Local  aErroErp:= {}
Local cXml :=''
Local oXml
Local cError := ' '
Local  cWarning :=' '
Local  lSF1    := .F.
Private	cTpCarga :=	''


cArqImp := cGetFile("Arquivo .XML |*.XML","Selecione o Arquivo XML",0,"",.T.,GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE)
cXml := LerDeArquivo(cArqImp)
oXml  := 	XmlParser(cXml,"_",@cError, @cWarning )

if '#01' $ cXml
	msgalert('Erro ao ler aquivo !')
	return ''
endif 
lRet := .T.
lRet := ImpXML_CTe(cArqImp,.F.,@aProc,@aErros,oXML:_CTeProc:_Cte,@aErroErp,oXML:_CTeProc:_ProtCte,@lSF1)
if lRet 
	msgalert('CT-E importado com sucesso ! ')
else 
	msgalert('Falha ao importar CT-E')
endif 
RestArea(aAlias4)
Return cArqImp


static function IMCT() 



return 


static function LerDeArquivo(cArquivo )
Local cRet:=''
Local cTeste:=''


If File(cArquivo)
				FT_FUSE(cArquivo)
				FT_FGOTOP()
				Do While !FT_FEOF()
					cRet += FT_FREADLN()
					FT_FSKIP()
               	EndDo
               	FT_FUSE()
else
	cRet := '#01 arquivo nao encontrado'
endif 

return cRet




