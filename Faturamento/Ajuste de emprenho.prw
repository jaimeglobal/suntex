#include "rwmake.ch"
#include "TbiConn.ch"
#include "protheus.ch"
User Function MA381BUT()
Local nOpcao  := PARAMIXB[1]          // Opção escolhidaLocal 
aBotoes := aClone(PARAMIXB[2])  // Array com botões caso exista
aAdd( aBotoes, { 'CRITICA', { || u_ajustaemp(nOpcao) }, 'Ajusta pela estrutura' } )
Return( aClone( aBotoes ) )


User Function ajustaemp(nopcx)

Local oButton1
Local oButton2
Local oGet1
Local cGet1 := Space(9)
Local oSay1
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Ajuste de empenho" FROM 000, 000  TO 180, 400 COLORS 0, 16777215 PIXEL

    
    @ 021, 066 SAY oSay1 PROMPT "Nova quantidade do PA" SIZE 058, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 035, 061 MSGET oGet1 VAR cGet1 SIZE 080, 010 OF oDlg PICTURE "@E 999,999.99" COLORS 0, 16777215 PIXEL
    @ 060, 060 BUTTON oButton1 PROMPT "Confirma" SIZE 037, 012 OF oDlg PIXEL
    @ 060, 106 BUTTON oButton2 PROMPT "Cancela" SIZE 037, 012 OF oDlg PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED




Return
