#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"
#DEFINE  ENTER CHR(13)+CHR(10)

User function CriaZxm(cStatus, cDoc, cSerie, cFonrnec, cLoja ,cChave, cXml , cTipo)
aArea:=GetArea()
SA2->(DbSetOrder(1))
if !SA2->(DbSeek(xFilial('SA2')+cFonrnec+cLoja))
    return .F.
ENDIF
if empty(cStatus) .or.  empty(cDoc) .or. empty(cFonrnec) .or. empty(cLoja) .or. empty(cChave) .or. empty(cXml) .or. empty(cTipo) 
    msgalert('Um ou mais campos da tabela ZXM não foram preenchidos ! ')
    return .F.
endif 

ZXM->(Reclock())
ZXM->ZXM_FILIAL := XFilial()
ZXM->ZXM_STATUS := cStatus
ZXM->ZXM_DOC    := cDoc
ZXM->ZXM_SERIE  := cSerie
ZXM->ZXM_FORNEC := cFonrnec
ZXM->ZXM_LOJA   := cLoja
ZXM->ZXM_NOMFOR := SA1->A1_NOME
ZXM->ZXM_UFFOR  := SA1->A1_EST
ZXM->ZXM_CGC    := SA1->A1_CGC
ZXM->ZXM_DTIMP  := date()
ZXM->ZXM_XML    := cXml
ZXM->ZXM_TIPO   := cTipo
ZXM->(Unlokck())

Restarea(aArea)
Return  .T.
