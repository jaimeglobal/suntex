#include 'totvs.ch'   
#INCLUDE 'TOPCONN.CH'

// Classe usada para inclus�o e altera��o de comissoes !!
Class Comissao 
Data perc1 as numeric
Data perc2 as numeric
Data perc3 as numeric
Data perc4 as numeric
Data perc5 as numeric
Data vendor1     as String
Data vendor2     as String
Data vendor3     as String
Data vendor4     as String
Data vendor5     as String
Data pedido      as String
Data Itens       as array 
Data serie       as string 
Data nota        as object  
Data titulos     as object  

Method new(cNota , cSerie , cpedido ) Constructor
Method grava() 
endclass


Method new(nota , serie ,pedido) Class comissao
Default nota := ''
Default serie := ''
Default pedido := ''


if !SF2->(DbSeek(xFilial('SF2')+nota+serie)) 
    msgalert('n�o foi poss�vel encontrar a nota ','Aten��o ')
    return .f.
endif 
if !SC6->(DbSeek(xFilial('SC5')+pedido)) 
    msgalert('n�o foi poss�vel encontrar o pedido ','Aten��o')
    return .f.
endif 
if  !SE1->(DbSeek(xFilial('SE1')+SF2->SERIE+SF2->NOTA))
    msgalert('n�o foi possivel encontrar o Titulo !','Aten��o')
    return .f.
endif 

self:pedido      := SC5->C5_NUM
self:nota        := SF2->F2_DOC
self:serie       := SF2->F2_SERIE
self:vendedor1   := SF2->F2_VEND1
self:vendedor2   := SF2->F2_VEND2
self:vendedor3   := SF2->F2_VEND3
self:vendedor4   := SF2->F2_VEND4
self:vendedor5   := SF2->F2_VEND5
self:Itens       := GetItem(SC5->C5_NUM)
self:titutlos    := GetTitulos(SF2->F2_SERIE+SF2->F2_DOC)
self:perc1       :=SE1->COMIS1
self:perc2       :=SE1->COMIS2
self:perc3       :=SE1->COMIS3
self:perc4       :=SE1->COMIS4
self:perc5       :=SE1->COMIS5
self:nota        := nota:new(SF2->(RECNO()))

return .t. 

Method grava() Class Comissao
Local nAux :=1 
SE3->(DbSetOrder(2))
for nAux:=1 to len (self:titulos)
    if !empty(self:titulos[nAux]:vendedor1)
        if SE3->(DbSeek(xFilial('SE3')+self:titulos[nAux]:vend1+self:titulos[nAux]:prefixo+self:titulos[nAux]:numero+self:titulos[nAux]:parcela))  
            self:titulos[nAux]:altera()    
        else
            self:titulos[nAux]:cria() 
        endif  
     
    endif 
next

return .t.



//Classe usada pra processo de comiss�o na nota 
Class nota
Data nota as string  
Data serie as string 
Data Vend1 as string 
Data Vend2 as string
Data Vend3 as string
Data Vend4 as string
Data Vend5 as string
Data Valor as numeric
data Itens as array 
Method new(recno) Constructor
Endclass

method new(recno) Class nota 
SF2->(DbGoto(recno))
self:nota := SF2->F2_COD
self:serie:= SF2->F2_SERIE
self:vend1:= SF2->F2_VEND1
self:vend2:= SF2->F2_VEND2
self:vend3:= SF2->F2_VEND3
self:vend4:= SF2->F2_VEND4
self:vend5:= SF2->F2_VEND5
self:valor:= SF2->F2_VALFAT
self:itens:=GetItensNota()

return .t. 



Class Itemnota  
Data nota as string 
Data serie as string 
Data Item as string 
Data perc1 as numeric
Data perc2 as numeric
Data perc3 as numeric
Data perc4 as numeric
Data perc5 as numeric
Method new(recno) Constructor
Method grava()
endclass 

method new (recno) Class Itemnota
SD2->(DbGoto(recno))
self:nota   := SD2->D2_DOC
self:serie  := SD2->D2_SERIE
self:item   := SD2->D2_ITEM
self:perc1 := SD2->D2_COMIS1
self:perc2 := SD2->D2_COMIS2
self:perc3 := SD2->D2_COMIS3
self:perc4 := SD2->D2_COMIS4
self:perc5 := SD2->D2_COMIS5
return .t.


Class titulo 
Data prefixo     as string 
Data numero      as string 
Data parcela     as string 
Data perc1       as numeric
Data perc2       as numeric
Data perc3       as numeric
Data perc4       as numeric
Data perc5       as numeric
Data vendor1     as String
Data vendor2     as String
Data vendor3     as String
Data vendor4     as String
Data vendor5     as String
Data valor       as numeric 
Data baixa       as date 
Data cliente     as string 
Data loja        as string 
Data tipo        as string 

Method new(chavese1) Constructor
Method cria ()
Method Altera()
endclass


Method new (chavese1) Class Titulo 
SE1->(DbSeek(chavese1))
self:prefixo      := SE1->E1_PREFIXO
self:numero       := SE1->E1_NUM
self:parcela      := SE1->E1_PARCELA
self:perc1        := SE1->E1_COMIS1
self:perc2        := SE1->E1_COMIS2
self:perc3        := SE1->E1_COMIS3
self:perc4        := SE1->E1_COMIS4
self:perc5        := SE1->E1_COMIS5
self:vendor1      := SE1->E1_VEND1
self:vendor2      := SE1->E1_VEND2
self:vendor3      := SE1->E1_VEND3
self:vendor4      := SE1->E1_VEND4
self:vendor5      := SE1->E1_VEND5
self:valor        := SE1->E1_VALOR
self:baixa        := SE1->E1_BAIXA
self:cliente      := SE1->E1_CLIENTE
self:loja         := SE1->E1_LOJA 
self:tipo         := SE1->E1_TIPO

return .t.


Method Cria ( Vendedor , perc) Class Titulo
SE3->(Reclock('SE3',.t.))
SE3->E3_FILIAL := xFilial('SE3')
SE3->E3_VEND   := Vendedor
SE3->E3_NUM    := self:numero
SE3->E3_EMISSAO:= date()
SE3->E3_SERIE  :=self:serie
SE3->E3_CODCLI :=self:cliente
SE3->E3_LOJA   :=self:loja
SE3->E3_BASE   :=self:valor
SE3->E3_PORC   := perc
SE3->E3_COMIS  :=round(self:valor*(perc/100),2)
SE3->E3_PREFIXO := self:prefixo
SE3->E3_PARCELA := self:parcela
SE3->E3_TIPO    := self:tipo
SE3->E3_
SE3->(MsUnlock())
return .t.


Method Altera () Class Titulo
return .t.
