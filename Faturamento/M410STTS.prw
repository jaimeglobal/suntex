//Bibliotecas
#Include "Protheus.ch"
#Include "TBIConn.ch" 
#Include "Colors.ch"
#Include "RPTDef.ch"
#Include "FWPrintSetup.ch"
#include "topconn.ch" 
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'AP5MAIL.CH'
#INCLUDE 'TBICONN.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'

/*
PROGRAMA FsONTE: MATA410.PRW
Ponto-de-Entrada: M410STTS
Vers�es:	Advanced Protheus 7.10 , Microsiga Protheus 8.11 , Microsiga Protheus 10
Descri��o:Este ponto de entrada pertence � rotina de pedidos de venda, MATA410(). Est� localizado na rotina de altera��o do pedido, A410INCLUI(). � executado ap�s a grava��o das informa��es.
Sintaxe: M410STTS ( ) --> Nil
Retorno: Nil(nulo)
*/
User Function M410STTS()
Local _aArea := GetArea()
Local _nOper := PARAMIXB[1]
//Local _cPed    := SC5->C5_NUM
Local _cFilial := xFilial("DA0")
Local _cTabela := SC5->C5_TABELA

//Alert("Entrou M410TTS")
//Alert(_nOper)
     // VALIDA SE O PEDIDO EST� OU N�O LIBERADO POR REGRA DE NEGOCIO ... 
     //SE N�O ESTIVER VALIDA REGRA DE NEGOCIO DA COMISS�O

If _nOper == 3 .or. _nOper == 4 .or. _nOper == 6 
//Alert(SC5->C5_NUM)
     If (SC5->C5_BLQ <> "1" .or. SC5->C5_BLQ <> "2" ).and. empty(SC5->C5_NOTA)

          DbSelectArea("DA0")
          dbSetOrder(1)
          If dbSeek(_cFilial+_cTabela,.F.)
                //Alert(SC5->C5_TABELA)
                If  (DA0->DA0_COMIS1 > 0 .and. DA0->DA0_COMIS1 <> SC5->C5_COMIS1) .or. ;
                    (DA0->DA0_COMIS2 > 0 .and. DA0->DA0_COMIS2 <> SC5->C5_COMIS2) .or. ;
                    (DA0->DA0_COMIS3 > 0 .and. DA0->DA0_COMIS3 <> SC5->C5_COMIS3) .or. ;
                    (DA0->DA0_COMIS4 > 0 .and. DA0->DA0_COMIS4 <> SC5->C5_COMIS4) .or. ;
                    (DA0->DA0_COMIS5 > 0 .and. DA0->DA0_COMIS5 <> SC5->C5_COMIS5)
                    DbSelectArea("SC5")
                    If  RecLock("SC5",.F.)
                        SC5->C5_BLQ := "1"
                        MsUnlock()
                    Endif
                EndIf
          EndIF
          DbSelectArea("SC5")
     EndIf
EndIf
u_AjustaLeg(SC5->C5_NUM)
RestArea(_aArea)
Return
