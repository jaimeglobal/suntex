User Function MA410COR()

Local aCores := PARAMIXB
Local aCores2 :={}
Local naux := 1

aAdd(aCores2,{ "SC5->C5_BLQ == '1' .And. Empty(SC5->C5_NOTA)  "												,	'BR_AZUL'		,	'Pedido Bloquedo por regra'	})	//Pedido Bloquedo por regra
aAdd(aCores2,{ "SC5->C5_BLQ == '2' .And. Empty(SC5->C5_NOTA) "												,	'BR_LARANJA'	,	'Pedido Bloquedo por verba'	})
aAdd(aCores2,{ "SC5->C5_BLCRED =='1'.And.Empty(SC5->C5_NOTA)",'BR_PINK','Pedido Bloquedo por Credito'}) //Pedido Bloquedo por regra
aAdd(aCores2,{ "SC5->C5_BLEST =='1'.And.Empty(SC5->C5_NOTA)",'BR_PRETO','Pedido Bloquedo por Estoque'}) //Pedido Bloquedo por regra
aAdd(aCores2,{ "Empty(SC5->C5_LIBEROK).And.Empty(SC5->C5_NOTA) .And. Empty(SC5->C5_BLQ)"	,	'ENABLE'		,	'Pedido em Aberto'	})		//Pedido em Aberto
aAdd(aCores2,{ "!Empty(SC5->C5_NOTA).Or.SC5->C5_LIBEROK=='E' .And. Empty(SC5->C5_BLQ)"		,	'DISABLE'		,	'Pedido Encerrado'	})	//Pedido Encerrado
aAdd(aCores2,{ "!Empty(SC5->C5_LIBEROK).And.Empty(SC5->C5_NOTA).And. Empty(SC5->C5_BLQ)"	,	'BR_AMARELO'	,	'Pedido Liberado'	})	//Pedido Liberado

return aCores2

User Function MA410LEG()
Local aLegNew := PARAMIXB
    
AADD( aLegNew, {"BR_PINK","Pedido de Venda com Bloqueio de Credito "} )
AADD( aLegNew, {"BR_PRETO","Pedido de Venda com Bloqueio de Estoque "} )
Return( aLegNew )

//return ParamIXB

// fun��o para deixar a tela de pedidos mais lenta , ta renderizando muito rapido 
USER FUNCTION blcred (cNumero)
Local lRet := .F. 
cQuery := " Select * from "+RetSqlName('SC9')+ ""
cQuery += " where C9_FILIAL ='"+xFilial('SC9')+"' "
cQuery += " and D_E_L_E_T_ = ' ' and C9_PEDIDO ='"+cNumero+"' AND C9_BLCRED IN ('01','04')"  

cQuery := ChangeQuery(cQuery)

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),'TMPC9',.T.,.T.)
IF Select('TMPC9')  > 0 
    if !TMPC9->(eof())
        lRet :=.T.  
    endif
    TMPC9->(DbCloseArea())
ELSE 
    lRet :=.F.
ENDIF  

return lRet 





USER FUNCTION blest (cNumero)
Local lRet := .F. 
cQuery := " Select * from "+RetSqlName('SC9')+ ""
cQuery += " where C9_FILIAL ='"+xFilial('SC9')+"' "
cQuery += " and D_E_L_E_T_ = ' ' and C9_PEDIDO ='"+cNumero+"' AND C9_BLEST IN ('02','03')"  

cQuery := ChangeQuery(cQuery)

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),'TMPC9',.T.,.T.)
IF Select('TMPC9')  > 0 
    if !TMPC9->(eof())
        lRet :=.T.  
    endif
    TMPC9->(DbCloseArea())
ELSE 
    lRet :=.F.
ENDIF  

return lRet 
