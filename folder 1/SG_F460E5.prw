#Include "PROTHEUS.CH"
#INCLUDE 'RWMAKE.CH'
#include "topconn.ch"

/*
Descri��o:O ponto de entrada SE5FI460 ser� utilizado na grava��o complementar no SE5, pela grava��o da baixa do t�tulo liquidado.
Programa Fonte: FINA460.PRW -- Chamada na fun��o: A460Grava()
*/
USER FUNCTION SE5FI460() 
Local cQry     := ''
Local cxChvFK1 := ''
Local nxValLiq := SE1->E1_SALDO
If nxValLiq == 0
    nxValLiq := SE1->E1_VALOR
EndIf
//Alert(SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_CLIENTE+SE1->E1_LOJA)
//Alert(nxValLiq)
//Local cxChvSE5 := SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_CLIENTE+SE1->E1_LOJA
//ALERT("PONTO DE ENTRADA SE5FI460 CHAMADO COM SUCESSO! " + cxChvSE5)

If AllTrim(SE5->E5_TABORI)=='FK1' .AND. SE5->E5_SITUACA # "C" .AND. SE5->E5_TIPODOC == "BA"
    If  SE5->E5_VALOR <> nxVALLIQ
        Reclock('SE5',.F.)
            SE5->E5_VALOR    := nxVALLIQ
            SE5->E5_VLMOED2  := nxVALLIQ
        MsUnlock()
        cxChvFK1 := SE5->E5_FILIAL+SE5->E5_IDORIG
        DBSELECTAREA("FK1")
        DbSetOrder(1)
        If DbSeek(cxChvFK1,.F.)
            Reclock('FK1',.F.)
                FK1->FK1_VALOR  := nxVALLIQ
                FK1->FK1_VLMOE2 := nxVALLIQ
            MsUnlock()
        EndIf
        cQry := "SELECT FO1010.R_E_C_N_O_ RECNO FROM FO1010 WHERE FO1010.D_E_L_E_T_='' AND FO1010.FO1_IDDOC ='"+FK1->FK1_IDDOC+"'"
        TCQuery cQry ALIAS "WFO1" NEW
        dbSelectArea("WFO1")
        DbSelectArea("FO1")
        DbGoTo(WFO1->RECNO)
        If FK1->FK1_IDDOC == FO1->FO1_IDDOC
            Reclock('FO1',.F.)
                FO1->FO1_SALDO := nxVALLIQ
                FO1->FO1_TOTAL := nxVALLIQ
            MsUnlock()
        EndIf
        DbSelectArea("WFO1")
        DbCloseArea()
    EndIf
    DBSELECTAREA("SE5")
EndIf
RETURN
