//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
 

#Define STR_PULA    Chr(13)+Chr(10)

user function SG_RELRESIFO()
Local oExcel
Local oExcel2
Local aNatureza := {}
Local cArquivo    := GetTempPath()+'residuos.xml'

If ! ( Pergunte("MTA500",.T.) )
    msgaler('Atekcao a operacao foi cancelada pelo usuario ')
    RETURN
endif


	cQuery		:= " select * from SC6010 WHERE C6_FILIAL='"+xFilial("SC6")+"' AND "
	cQuery		+= "C6_NUM>='"+MV_PAR04+"' AND "
	cQuery		+= "C6_NUM<='"+MV_PAR05+"' AND "
	cQuery		+= "C6_PRODUTO>='"+MV_PAR07+"' AND "
	cQuery		+= "C6_PRODUTO<='"+MV_PAR08+"' AND "
	cQuery		+= "C6_ENTREG BETWEEN '"+Dtos(MV_PAR10)+"' AND '"+Dtos(MV_PAR11)+"' AND "
	cQuery		+= "C6_BLQ<>'R ' AND C6_BLQ<>'S ' AND d_e_l_e_t_=' ' and "
	cQuery		+= "(C6_QTDVEN-C6_QTDENT)>0 AND "
	cQuery 		+= "C6_RESERVA='"+Space(Len(SC6->C6_RESERVA))+"'"
    If  ExistBlock("MTA500QRY")
		cQuery    += " AND "+ExecBlock("MTA500QRY",.F.,.F.,)
	EndIf

    if select ('TMPSC6') >0 
        TMPSC6->(DbCloseArea())
    ENDIF 

    TCQuery cQuery New Alias "TMPSC6"

    layout(@oExcel)

    while ! TMPSC6->(eof())
        SC5->(DbSeek(xFilial('SC5')+TMPSC6->C6_NUM))
        Get_Natu(@aNatureza,TMPSC6->C6_NUM)
        cProduto := posicione('SB1',1,xFilial('SB1')+ TMPSC6->C6_PRODUTO, 'B1_DESC')
        saldo :=0
        SB2->(DbSetOrder(1))
        if SB2->(DbSeek(xFilial('SB2')+TMPSC6->C6_PRODUTO+'PRA'))
            saldo := SB2->B2_QATU
        endif 
        cVendedor := ' '
        if SA3->(DbSeek(xFilial('SA3')+SC5->C5_VEND1))
            cVendedor := SA3->A3_NOME
        endif 

        oExcel:AddRow("residuos","previsao",{TMPSC6->C6_NUM,;
        TMPSC6->C6_CLI,;
        GET_CLI(TMPSC6->C6_CLI,TMPSC6->C6_LOJA),;
        iif (type("SC5->C5_EMISSAO")=='D',dtoc(SC5->C5_EMISSAO),dtoc(stod(SC5->C5_EMISSAO))),;
        SC5->C5_VEND1,;
        cVendedor,;
        aNatureza[1],;
        aNatureza[2],;
        TMPSC6->C6_ITEM,;
        TMPSC6->C6_PRODUTO,;
        cProduto,;
        (TMPSC6->(C6_QTDVEN - C6_QTDENT)),;
        TMPSC6->C6_QTDVEN ,;
        TMPSC6->C6_QTDENT,;
        saldo,;
        TMPSC6->C6_PEDCLI,;
        iif (type("TMPSC6->C6_ENTREG")=='D',dtoc(TMPSC6->C6_ENTREG),dtoc(stod(TMPSC6->C6_ENTREG)))})

        TMPSC6->(DbSkip())
    enddo


    cQuery		:= " select * from SC6010 WHERE C6_FILIAL='"+xFilial("SC6")+"' AND "
	cQuery		+= "C6_ENTREG BETWEEN '"+Dtos(MV_PAR10)+"' AND '"+Dtos(MV_PAR11)+"' AND "
	cQuery		+= "C6_BLQ='R'  AND d_e_l_e_t_=' ' "
    if select ('TMPSC6') >0 
        TMPSC6->(DbCloseArea())
    ENDIF 
    TCQuery cQuery New Alias "TMPSC6"

TMPSC6->(DbGotop())
    while ! TMPSC6->(eof())
        SC5->(DbSeek(xFilial('SC5')+TMPSC6->C6_NUM))
        Get_Natu(@aNatureza,TMPSC6->C6_NUM)
        cProduto := posicione('SB1',1,xFilial('SB1')+ TMPSC6->C6_PRODUTO, 'B1_DESC')
        saldo :=0
        SB2->(DbSetOrder(1))
        if SB2->(DbSeek(xFilial('SB2')+TMPSC6->C6_PRODUTO+'PRA'))
            saldo := SB2->B2_QATU
        endif 

         cVendedor := ' '
        if SA3->(DbSeek(xFilial('SA3')+SC5->C5_VEND1))
            cVendedor := SA3->A3_NOME
        endif 

        oExcel:AddRow("efetivo","efetivo",{TMPSC6->C6_NUM,;
        TMPSC6->C6_CLI,;
        GET_CLI(TMPSC6->C6_CLI,TMPSC6->C6_LOJA),;
        iif (type("SC5->C5_EMISSAO")=='D',dtoc(SC5->C5_EMISSAO),dtoc(stod(SC5->C5_EMISSAO))),;
        SC5->C5_VEND1,;
        cVendedor,;
        aNatureza[1],;
        aNatureza[2],;
        TMPSC6->C6_ITEM,;
        TMPSC6->C6_PRODUTO,;
        cProduto,;
        (TMPSC6->(C6_QTDVEN - C6_QTDENT)),;
        TMPSC6->C6_QTDVEN ,;
        TMPSC6->C6_QTDENT,;
        saldo,;
        TMPSC6->C6_PEDCLI,;
       iif (type("TMPSC6->C6_ENTREG")=='D',dtoc(TMPSC6->C6_ENTREG),dtoc(stod(TMPSC6->C6_ENTREG))) })

        TMPSC6->(DbSkip())
    enddo





    oExcel:Activate()
    oExcel:GetXMLFile(cArquivo)

    oExcel2 := MsExcel():New()             //Abre uma nova conex�o com Excel
    oExcel2:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel2:SetVisible(.T.)                 //Visualiza a planilha
    oExcel2:Destroy()                        //Encerra o processo do gerenciador de tarefas

return 

static function efetivo ()
return 


static function layout(oFWMsExcel)

    oFWMsExcel := FWMSExcel():New()
    oFWMsExcel:AddworkSheet("residuos")
    oFWMsExcel:AddTable('residuos','previsao')
    oFWMsExcel:AddColumn("residuos","previsao","Pedido",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","CodCliente",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Cliente",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Emissao",1,4)
    oFWMsExcel:AddColumn("residuos","previsao","Vendedor",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Nome_Vendedor",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Natureza",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Descricao",1,1)
//    oFWMsExcel:AddColumn("residuos","previsao","IT",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","IT",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","CodProduto",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Produto",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","SaldoPedido",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Vendido",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Atendido",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Estoque_disponivel",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","PedCLi",1,1)
    oFWMsExcel:AddColumn("residuos","previsao","Entrega",1,4)
    oFWMsExcel:AddworkSheet("efetivo")
    oFWMsExcel:AddTable('efetivo','efetivo')
    oFWMsExcel:AddColumn("efetivo","efetivo","Pedido",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","CodCliente",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Cliente",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Emissao",1,4)
    oFWMsExcel:AddColumn("efetivo","efetivo","Vendedor",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Nome_Vendedor",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Natureza",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Descricao",1,1)
  //  oFWMsExcel:AddColumn("residuos","efetivo","IT",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","IT",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","CodProduto",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Produto",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Saldo Eliminado",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Vendido",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Atendido",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Estoque_disponivel",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","PedCLi",1,1)
    oFWMsExcel:AddColumn("efetivo","efetivo","Entrega",1,4)

return



STATIC FUNCTION GET_CLI(cCli, cLoja)
Local cRet := ' '
   cRet:= posicione('SA1',1,xFilial('SA1')+cCli+cLoja,'A1_NOME' )
return cRet


static function Get_Natu(aNatureza, cNum )

SC5->(DbSetOrder(1))
SED->(DbSetOrder(1))
if SC5->(DbSeek(xFilial('SC5')+cNum))
    if SED->(DbSeek(xFilial('SED')+SC5->C5_NATUREZ))
        aadd(aNatureza,SED->ED_CODIGO)
        aadd(aNatureza,SED->ED_DESCRIC)
    else 
        aadd(aNatureza,'erro')
         aadd(aNatureza,'erro')
    endif 

else
 aadd(aNatureza,'erro')
 aadd(aNatureza,'erro')
endif 


return 


