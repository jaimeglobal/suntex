#INCLUDE "protheus.ch"

// Classe para tratar bloquio customizado de pedidos 

Class Bloqueio_pedido

Data Filial     as string 
Data Pedido     as string
Data Item       as string
Data Cliente    as string
Data Loja       as string   
Data Produto    as string
Data QtdLib     as Numeric
Data NF         as string
Data Serie      as string 
Data DataLib    as date 
Data Sequen     as string
Data Grupo      as string 
Data PrecoVenda as Numeric 
Data BloqEst    as string 
Data BloqCred   as string
Data Lote       as string
Data Armazem    as string   
Data DataEnt    as date 



Method New(cPedido ,cItem ) constructor 
Method Grava()
Method Libera(cPedido,cItem)
Endclass




Method New(cPedido,cItem) Class Bloqueio_pedido
    if SC6->(DbSeek(xFilial('SC6')+cPedido+cItem))
        Self:Filial     :=SC6->C6_FILIAL
        Self:Pedido     :=SC6->C6_NUM
        Self:Item       :=SC6->C6_ITEM
        Self:Cliente    :=SC6->C6_CLI
        Self:Loja       :=SC6->C6_LOJA
        Self:Produto    :=SC6->C6_PRODUTO
        Self:QtdLib     :=SC6->C6_QTDLIB
        Self:NF         :=SC6->C6_NOTA
        Self:Serie      :=SC6->C6_SERIE
        Self:DataLib    :=date()
        Self:Sequen     :=Get_Sequen(cPedido,cItem)
        Self:Grupo      :=posicione('SB1',1,xFilial('SB1')+SC6->C6_PRODUTO,'B1_GRUPO')
        Self:PrecoVenda :=SC6->C6_PRCVEN
        Self:BloqEst    :="09"
        Self:BloqCred   :="01"
        Self:Lote       :=SC6->C6_LOTECTL
        Self:Armazem    :=SC6->C6_LOCAL
        Self:DataEnt    :=date()
    else
        Self:Filial     :=xFilial('SC6')
        Self:Pedido     :=' '
        Self:Item       :=' '
        Self:Cliente    :=' '
        Self:Loja       :=' '
        Self:Produto    :=' '
        Self:QtdLib     :=0
        Self:NF         :=' '
        Self:Serie      :=' '
        Self:DataLib    :=date()
        Self:Sequen     :=Get_Sequen(cPedido,cItem)
        Self:Grupo      :=' '
        Self:PrecoVenda :=0
        Self:BloqEst    :="09"
        Self:BloqCred   :="01"
        Self:Lote       :=' '
        Self:Armazem    :=' '
        Self:DataEnt    :=date()
    endif 

return  

Method Grava () Class Bloqueio_pedido

SC9->(reclock("SC9",.T.))
SC9->C9_FILIAL  := Self:Filial
SC9->C9_PEDIDO  := Self:Pedido
SC9->C9_ITEM    := Self:Item
SC9->C9_CLIENTE := Self:Cliente
SC9->C9_LOJA    := Self:Loja
SC9->C9_PRODUTO := Self:Produto
SC9->C9_QTDLIB  := Self:QtdLib
SC9->C9_SEQUEN  := Self:Sequen
SC9->C9_GRUPO   := Self:Grupo
SC9->C9_BLEST   := Self:BloqEst
SC9->C9_BLCRED  := Self:BloqCred
SC9->C9_LOTECTL := Self:Lote
SC9->C9_LOCAL   := Self:Armazem
SC9->C9_DATENT  := Self:DataEnt
SC9->C9_BLINF   :='BLOQUEIO API'
SC9->(MsUnlock())

return


Method Libera(cPedido,cItem) Class Bloqueio_pedido

if SC9->(DbSeek(xFilial('SC9')+cPedido+cItem))
    while SC9->C9_PEDIDO == cPedido .and. SC9->C9_ITEM == cItem .and.! SC9->(EOF())
        if SC9->C9_BLINF  == "BLOQUEIO API"
            SC9->(DbDelete())
        endif 
    enddo
endif

return .t.




static function Get_Sequen(cPedido,cItem)
cRet:= '01' 

if SC9->(DbSeek(xFilial('SC9')+cPedido+cItem))
    while SC9->C9_PEDIDO == cPedido .AND. SC9->C9_ITEM == cItem .and. ! SC9->(eof())
        cRet:=soma1(SC9->C9_SEQUEN) 
         SC9->(DbSkip())
    enddo

endif

return cRet 
